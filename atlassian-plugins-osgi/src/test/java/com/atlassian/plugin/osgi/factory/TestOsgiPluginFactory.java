package com.atlassian.plugin.osgi.factory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.packageadmin.PackageAdmin;
import org.osgi.util.tracker.ServiceTracker;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.XmlPluginArtifact;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.impl.DefaultOsgiPersistentCache;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.atlassian.plugin.test.PluginTestUtils;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.ReferenceMode.FORBID_REFERENCE;
import static com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE;

public class TestOsgiPluginFactory {
    OsgiPluginFactory factory;

    private File tmpDir;
    private File jar;
    private OsgiContainerManager osgiContainerManager;
    private Mock mockBundle;
    private Mock mockSystemBundle;

    @Before
    public void setUp() throws IOException {
        tmpDir = PluginTestUtils.createTempDirectory(TestOsgiPluginFactory.class);
        osgiContainerManager = mock(OsgiContainerManager.class);

        final ServiceTracker tracker = mock(ServiceTracker.class);
        when(tracker.getServices()).thenReturn(new Object[0]);
        when(osgiContainerManager.getServiceTracker(ModuleDescriptorFactory.class.getName()))
                .thenReturn(tracker);

        factory = new OsgiPluginFactory(
                PluginAccessor.Descriptor.FILENAME,
                Collections.emptySet(),
                new DefaultOsgiPersistentCache(tmpDir),
                osgiContainerManager,
                new DefaultPluginEventManager());
        jar = new PluginJarBuilder("someplugin")
                .addPluginInformation("plugin.key", "My Plugin", "1.0")
                .build();

        mockBundle = new Mock(Bundle.class);
        final Dictionary<String, String> dict = new Hashtable<>();
        dict.put(Constants.BUNDLE_DESCRIPTION, "desc");
        dict.put(Constants.BUNDLE_VERSION, "1.0");
        mockBundle.matchAndReturn("getHeaders", dict);

        mockSystemBundle = new Mock(Bundle.class);
        final Dictionary<String, String> sysDict = new Hashtable<>();
        sysDict.put(Constants.BUNDLE_DESCRIPTION, "desc");
        sysDict.put(Constants.BUNDLE_VERSION, "1.0");
        mockSystemBundle.matchAndReturn("getHeaders", sysDict);
        mockSystemBundle.matchAndReturn("getLastModified", System.currentTimeMillis());
        mockSystemBundle.matchAndReturn("getSymbolicName", "system.bundle");

        final Mock mockSysContext = new Mock(BundleContext.class);
        mockSystemBundle.matchAndReturn("getBundleContext", mockSysContext.proxy());

        mockSysContext.matchAndReturn("getServiceReference", C.ANY_ARGS, null);
        mockSysContext.matchAndReturn("getService", C.ANY_ARGS, new Mock(PackageAdmin.class).proxy());
    }

    @After
    public void tearDown() throws IOException {
        factory = null;
        FileUtils.cleanDirectory(tmpDir);
        jar.delete();
    }

    @Test
    public void createOsgiPlugin() {
        mockBundle.expectAndReturn("getSymbolicName", "plugin.key");
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(ImmutableList.of());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});
        final Plugin plugin = factory.create(
                new JarPluginArtifact(jar), (ModuleDescriptorFactory) new Mock(ModuleDescriptorFactory.class).proxy());
        assertNotNull(plugin);
        assertTrue(plugin instanceof OsgiPlugin);
    }

    @Test
    public void createOsgiPluginWithBadVersion() throws PluginParseException, IOException {
        jar = new PluginJarBuilder("someplugin")
                .addPluginInformation("plugin.key", "My Plugin", "beta.1.0")
                .build();
        mockBundle.expectAndReturn("getSymbolicName", "plugin.key");
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(ImmutableList.of());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});
        try {
            factory.create(new JarPluginArtifact(jar), (ModuleDescriptorFactory)
                    new Mock(ModuleDescriptorFactory.class).proxy());
            fail("Should have complained about osgi version");
        } catch (final PluginParseException ex) {
            // expected
        }
    }

    @Test
    public void createOsgiPluginWithBadVersion2() throws PluginParseException, IOException {
        jar = new PluginJarBuilder("someplugin")
                .addPluginInformation("plugin.key", "My Plugin", "3.2-rc1")
                .build();
        mockBundle.expectAndReturn("getSymbolicName", "plugin.key");
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(ImmutableList.of());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});
        final Plugin plugin = factory.create(
                new JarPluginArtifact(jar), (ModuleDescriptorFactory) new Mock(ModuleDescriptorFactory.class).proxy());
        assertTrue(plugin instanceof OsgiPlugin);
    }

    @Test
    public void createOsgiPluginWithManifestKey() throws PluginParseException, IOException {
        mockBundle.expectAndReturn("getSymbolicName", "plugin.key");
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(ImmutableList.of());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});

        final File pluginFile = new PluginJarBuilder("loadwithxml")
                .manifest(new ImmutableMap.Builder<String, String>()
                        .put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "somekey")
                        .put(Constants.BUNDLE_VERSION, "1.0")
                        .put(Constants.BUNDLE_NAME, "My awesome plugin")
                        .put(Constants.BUNDLE_DESCRIPTION, "Bundle Description")
                        .put(Constants.BUNDLE_VENDOR, "My vendor Name")
                        .build())
                .build();

        final Plugin plugin = factory.create(new JarPluginArtifact(pluginFile), (ModuleDescriptorFactory)
                new Mock(ModuleDescriptorFactory.class).proxy());
        assertNotNull(plugin);
        assertTrue(plugin instanceof OsgiPlugin);
    }

    @Test
    public void createOsgiPluginWithManifestKeyAndDescriptorSkippingTransform()
            throws PluginParseException, IOException {
        final File jar = new PluginJarBuilder("someplugin")
                .addPluginInformation("plugin.key", "My Plugin", "1.0")
                .manifest(new ImmutableMap.Builder<String, String>()
                        .put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "somekey")
                        .put(Constants.BUNDLE_VERSION, "1.0")
                        .build())
                .build();
        mockBundle.expectAndReturn("getSymbolicName", "plugin.key");
        mockBundle.expectAndReturn("getHeaders", new Hashtable() {
            {
                put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "plugin.key");
                put(Constants.BUNDLE_VERSION, "1.0");
            }
        });
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(Collections.emptyList());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});
        when(osgiContainerManager.installBundle(jar, FORBID_REFERENCE)).thenReturn((Bundle) mockBundle.proxy());
        final Plugin plugin = factory.create(
                new JarPluginArtifact(jar), (ModuleDescriptorFactory) new Mock(ModuleDescriptorFactory.class).proxy());
        assertNotNull(plugin);
        assertTrue(plugin instanceof OsgiPlugin);

        plugin.install();
        verify(osgiContainerManager).installBundle(jar, FORBID_REFERENCE);
    }

    @Test
    public void pluginInformationAndPluginNameIsExtractedFromManifest() throws IOException {
        final PluginInformation expectedPluginInformation =
                createPluginInformation("My Description", "My Awesome Vendor Name", "1.0");
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(ImmutableList.of());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});

        final String pluginName = "My awesome plugin";

        final Plugin plugin = createPlugin(pluginName, expectedPluginInformation);
        assertNotNull(plugin);
        assertTrue(plugin instanceof OsgiPlugin);

        assertThat(plugin.getName(), equalTo(pluginName));

        final PluginInformation actualPluginInformation = plugin.getPluginInformation();
        assertThat(actualPluginInformation.getDescription(), equalTo(expectedPluginInformation.getDescription()));
        assertThat(actualPluginInformation.getVendorName(), equalTo(expectedPluginInformation.getVendorName()));
        assertThat(actualPluginInformation.getVersion(), equalTo(expectedPluginInformation.getVersion()));
    }

    @Test
    public void shouldLoadPluginIfManifestDoesNotHaveOptionalInformation() throws IOException {
        final PluginInformation pluginInformation = createPluginInformation("", "", "1.0");
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(ImmutableList.of());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});

        final String pluginName = "My Awesome Plugin";
        final Plugin plugin = createPlugin(pluginName, pluginInformation);

        assertNotNull(plugin);
        assertTrue(plugin instanceof OsgiPlugin);
        assertThat(plugin.getName(), equalTo(pluginName));
    }

    private Plugin createPlugin(final String pluginName, final PluginInformation pluginInformation) throws IOException {
        final File pluginFile = new PluginJarBuilder("loadwithxml")
                .manifest(new ImmutableMap.Builder<String, String>()
                        .put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, pluginName)
                        .put(Constants.BUNDLE_VERSION, pluginInformation.getVersion())
                        .put(Constants.BUNDLE_NAME, pluginName)
                        .put(Constants.BUNDLE_DESCRIPTION, pluginInformation.getDescription())
                        .put(Constants.BUNDLE_VENDOR, pluginInformation.getVendorName())
                        .build())
                .build();

        return factory.create(new JarPluginArtifact(pluginFile), (ModuleDescriptorFactory)
                new Mock(ModuleDescriptorFactory.class).proxy());
    }

    private PluginInformation createPluginInformation(
            final String description, final String vendorName, final String version) {
        final PluginInformation pluginInformation = new PluginInformation();
        pluginInformation.setDescription(description);
        pluginInformation.setVendorName(vendorName);
        pluginInformation.setVersion(version);
        return pluginInformation;
    }

    @Test
    public void createOsgiPluginWithManifestKeyNoVersion() throws PluginParseException, IOException {
        mockBundle.expectAndReturn("getSymbolicName", "plugin.key");
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(ImmutableList.of());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});

        final File pluginFile = new PluginJarBuilder("loadwithxml")
                .manifest(new ImmutableMap.Builder<String, String>()
                        .put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "somekey")
                        .build())
                .build();

        try {
            factory.create(new JarPluginArtifact(pluginFile), (ModuleDescriptorFactory)
                    new Mock(ModuleDescriptorFactory.class).proxy());
            fail("Should have failed due to no version");
        } catch (final NullPointerException ex) {
            // test passed
        }
    }

    @Test
    public void canLoadWithXml() throws PluginParseException, IOException {
        final File plugin = new PluginJarBuilder("loadwithxml")
                .addPluginInformation("foo.bar", "", "1.0")
                .build();
        final String key = factory.canCreate(new JarPluginArtifact(plugin));
        assertEquals("foo.bar", key);
    }

    @Test
    public void canLoadWithXmlArtifact() throws PluginParseException, IOException {
        final File xmlFile = new File(tmpDir, "plugin.xml");
        FileUtils.writeStringToFile(xmlFile, "<somexml />");
        final String key = factory.canCreate(new XmlPluginArtifact(xmlFile));
        assertNull(key);
    }

    @Test
    public void canLoadJarWithNoManifest() throws PluginParseException, IOException {
        final File plugin = new PluginJarBuilder("loadwithxml")
                .addResource("foo.xml", "<foo/>")
                .buildWithNoManifest();
        final String key = factory.canCreate(new JarPluginArtifact(plugin));
        assertNull(key);
    }

    @Test
    public void canLoadNoXml() throws PluginParseException, IOException {
        final File plugin = new PluginJarBuilder("loadwithxml").build();
        final String key = factory.canCreate(new JarPluginArtifact(plugin));
        assertNull(key);
    }

    @Test
    public void canLoadNoXmlButWithManifestEntry() throws PluginParseException, IOException {
        final File plugin = new PluginJarBuilder("loadwithxml")
                .manifest(new ImmutableMap.Builder<String, String>()
                        .put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "somekey")
                        .put(Constants.BUNDLE_VERSION, "1.0")
                        .put("Spring-Context", "*")
                        .build())
                .build();
        final String key = factory.canCreate(new JarPluginArtifact(plugin));
        assertEquals("somekey", key);
    }

    @Test
    public void canLoadNoXmlButWithManifestEntryNoVersion() throws PluginParseException, IOException {
        final File plugin = new PluginJarBuilder("loadwithxml")
                .manifest(new ImmutableMap.Builder<String, String>()
                        .put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "somekey")
                        .build())
                .build();
        final String key = factory.canCreate(new JarPluginArtifact(plugin));
        assertNull(key);
    }

    @Test
    public void canLoadWithXmlVersion3() throws Exception {
        final File plugin = new PluginJarBuilder("loadwithxml")
                .addPluginInformation("foo.bar", "", "1.0", 3)
                .build();
        final String key = factory.canCreate(new JarPluginArtifact(plugin));
        assertNull(key);
    }

    @Test
    public void transformedArtifactAllowsReferenceInstall() {
        mockBundle.expectAndReturn("getSymbolicName", "plugin.key");
        // We need to insert the Atlassian-Plugin-Key header that transformation would for installation to work
        ((Bundle) mockBundle.proxy()).getHeaders().put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "plugin.key");

        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(ImmutableList.of());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});
        // Have our mock OsgiContainerManager respond only to a reference install (the PERMIT_REFERENCE in the next
        // line)
        when(osgiContainerManager.installBundle(any(File.class), eq(PERMIT_REFERENCE)))
                .thenReturn((Bundle) mockBundle.proxy());
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);
        final Plugin plugin = factory.create(new JarPluginArtifact(jar), moduleDescriptorFactory);
        plugin.install();
        assertThat(plugin, instanceOf(OsgiPlugin.class));
        final Bundle actualBundle = ((OsgiPlugin) plugin).getBundle();
        assertThat(actualBundle, sameInstance(mockBundle.proxy()));
        // The verify here needs to be any(File.class) not jar, because the artifact is transformed, so the file isn't
        // jar
        verify(osgiContainerManager).installBundle(any(File.class), eq(PERMIT_REFERENCE));
    }

    @Test
    public void canLoadExtraModuleDescriptors() throws Exception {
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);
        final File pluginFile = new PluginJarBuilder("loadextramodules")
                .manifest(new ImmutableMap.Builder<String, String>()
                        .put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "pluginKey")
                        .put(Constants.BUNDLE_VERSION, "3")
                        .put(Constants.BUNDLE_NAME, "bundleName")
                        .put(Constants.BUNDLE_DESCRIPTION, "bundleDescription")
                        .put(Constants.BUNDLE_VENDOR, "Atlassian")
                        .put("Atlassian-Scan-Folders", "META-INF/atlassian")
                        .build())
                .addResource(
                        "META-INF/atlassian/foo.xml",
                        "<atlassian-plugin><rest key=\"sample\" path=\"/sample\" version=\"1\"/></atlassian-plugin>")
                .addResource("META-INF/atlassian/bar.xml", "<atlassian-plugin/>")
                .addPluginInformation("pluginKey", "1.1", "3")
                .build();
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(ImmutableList.of());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[] {(Bundle) mockSystemBundle.proxy()});
        final Plugin plugin = factory.create(new JarPluginArtifact(pluginFile), moduleDescriptorFactory);
        assertThat(plugin.getModuleDescriptor("sample"), instanceOf(ModuleDescriptor.class));
    }

    @Test
    public void createModuleIncorrectPluginType() {
        final Plugin plugin = mock(Plugin.class);
        final Element module = mock(Element.class);
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        assertThat(factory.createModule(plugin, module, moduleDescriptorFactory), nullValue());
    }

    @Test
    public void createModule() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        final OsgiPlugin plugin = mock(OsgiPlugin.class);
        final Element module = mock(Element.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        when(module.getName()).thenReturn("william");
        when(moduleDescriptorFactory.hasModuleDescriptor("william")).thenReturn(true);
        when(moduleDescriptorFactory.getModuleDescriptor("william")).thenReturn(moduleDescriptor);

        assertThat(factory.createModule(plugin, module, moduleDescriptorFactory), is(moduleDescriptor));
    }
}
