package com.atlassian.plugin.osgi.factory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.util.tracker.ServiceTracker;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestOsgiBundleFactory {
    @Mock
    private OsgiContainerManager osgiContainerManager;

    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;

    @Mock
    private ServiceTracker serviceTracker;

    private OsgiBundleFactory osgiBundleFactory;

    @Before
    public void setUp() {
        when(osgiContainerManager.getServiceTracker(anyString())).thenReturn(serviceTracker);

        osgiBundleFactory = new OsgiBundleFactory(osgiContainerManager);
    }

    @Test
    public void canCreateReturnsPluginKeyForJarFileContainingBundle() throws Exception {
        final String pluginKey = osgiBundleFactory.canCreate(TestOsgiBundlePlugin.getBundleJarPluginArtifact());
        assertThat(pluginKey, is(TestOsgiBundlePlugin.PLUGIN_KEY));
    }

    @Test
    public void canCreateReturnsNullForJarFileContainingBundleAndPlugin() throws Exception {
        final String pluginKey = osgiBundleFactory.canCreate(getBundlePluginJarPluginArtifact());
        assertThat(pluginKey, nullValue());
    }

    @Test
    public void canCreateReturnsNullForPluginJar() throws Exception {
        final String pluginKey = osgiBundleFactory.canCreate(getPluginJarPluginArtifact());
        assertThat(pluginKey, nullValue());
    }

    @Test
    public void canCreateReturnsNullForNonFileArtifact() throws Exception {
        final String pluginKey = osgiBundleFactory.canCreate(getNonFilePluginArtifact());
        assertThat(pluginKey, nullValue());
    }

    @Test
    public void createReturnsConfiguredPluginWhenBundleInstallSucceeds() throws Exception {
        final PluginArtifact pluginArtifact = TestOsgiBundlePlugin.getBundleJarPluginArtifact();
        final Plugin plugin = osgiBundleFactory.create(pluginArtifact, moduleDescriptorFactory);
        assertThat(plugin, notNullValue());
        assertThat(plugin, instanceOf(OsgiBundlePlugin.class));
        TestOsgiBundlePlugin.assertThatPluginIsConfigured(plugin);

        // create does _not_ install the bundle
        verifyZeroInteractions(osgiContainerManager);
    }

    @Test
    public void createReturnsUnloadablePluginForJarFileContainingBundleAndPlugin() throws Exception {
        final Plugin plugin = osgiBundleFactory.create(getBundlePluginJarPluginArtifact(), moduleDescriptorFactory);
        assertThat(plugin, notNullValue());
        assertThat(plugin, instanceOf(UnloadablePlugin.class));
    }

    @Test
    public void createReturnsUnloadablePluginForPluginJar() throws Exception {
        final Plugin plugin = osgiBundleFactory.create(getPluginJarPluginArtifact(), moduleDescriptorFactory);
        assertThat(plugin, notNullValue());
        assertThat(plugin, instanceOf(UnloadablePlugin.class));
    }

    @Test
    public void createReturnsUnloadablePluginForNonFileArtifact() throws Exception {
        final Plugin plugin = osgiBundleFactory.create(getNonFilePluginArtifact(), moduleDescriptorFactory);
        assertThat(plugin, notNullValue());
        assertThat(plugin, instanceOf(UnloadablePlugin.class));
    }

    @Test
    public void createModuleIncorrectPluginType() {
        final Plugin plugin = mock(Plugin.class);
        final Element module = mock(Element.class);

        assertThat(osgiBundleFactory.createModule(plugin, module, moduleDescriptorFactory), nullValue());
    }

    @Test
    public void createModule() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        final OsgiBundlePlugin plugin = mock(OsgiBundlePlugin.class);
        final Element module = mock(Element.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);

        when(module.getName()).thenReturn("william");
        when(moduleDescriptorFactory.hasModuleDescriptor("william")).thenReturn(true);
        when(moduleDescriptorFactory.getModuleDescriptor("william")).thenReturn(moduleDescriptor);

        assertThat(osgiBundleFactory.createModule(plugin, module, moduleDescriptorFactory), is(moduleDescriptor));
    }

    private static PluginArtifact getBundlePluginJarPluginArtifact() throws IOException {
        return new JarPluginArtifact(new PluginJarBuilder("somebundleplugin")
                .manifest(TestOsgiBundlePlugin.getBundleManifest())
                .addPluginInformation(
                        TestOsgiBundlePlugin.PLUGIN_KEY, TestOsgiBundlePlugin.NAME, TestOsgiBundlePlugin.VERSION, 2)
                .build());
    }

    private static PluginArtifact getPluginJarPluginArtifact() throws IOException {
        return new JarPluginArtifact(new PluginJarBuilder("someplugin")
                .addPluginInformation(
                        TestOsgiBundlePlugin.PLUGIN_KEY, TestOsgiBundlePlugin.NAME, TestOsgiBundlePlugin.VERSION, 2)
                .build());
    }

    private static PluginArtifact getNonFilePluginArtifact() throws IOException {
        final File tempFile = File.createTempFile("foo", "bar");
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        when(pluginArtifact.getName()).thenReturn(tempFile.getPath());
        when(pluginArtifact.getInputStream()).thenAnswer(invocation -> new FileInputStream(tempFile));
        when(pluginArtifact.toFile()).thenReturn(tempFile);
        return pluginArtifact;
    }
}
