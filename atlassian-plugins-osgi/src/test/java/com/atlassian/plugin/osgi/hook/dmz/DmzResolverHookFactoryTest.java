package com.atlassian.plugin.osgi.hook.dmz;

import java.util.Collection;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.osgi.framework.hooks.resolver.ResolverHook;
import org.osgi.framework.wiring.BundleRevision;
import com.google.common.collect.Sets;

import com.atlassian.plugin.osgi.container.impl.DefaultPackageScannerConfiguration;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class DmzResolverHookFactoryTest {

    @Test
    public void testBegin() {
        DefaultPackageScannerConfiguration configuration = new DefaultPackageScannerConfiguration();
        configuration.setOsgiPublicPackages(Sets.newHashSet());
        configuration.setApplicationBundledInternalPlugins(Sets.newHashSet());

        Collection<BundleRevision> revisions = mock(Collection.class);

        ResolverHook hook = new DmzResolverHookFactory(configuration).begin(revisions);

        assertNotNull(hook);
        MatcherAssert.assertThat(hook, instanceOf(DmzResolverHook.class));
        verifyNoMoreInteractions(revisions);
    }
}
