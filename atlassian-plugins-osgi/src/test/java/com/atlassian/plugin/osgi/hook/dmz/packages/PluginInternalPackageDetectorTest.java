package com.atlassian.plugin.osgi.hook.dmz.packages;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.osgi.framework.wiring.BundleCapability;

import com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.UnknownAtlassianAccessValue;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.osgi.hook.TestUtils.ATTR_WIRING_PACKAGE;
import static com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.ATLASSIAN_ACCESS_DEPRECATED;
import static com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.ATLASSIAN_ACCESS_INHERITED;
import static com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.ATLASSIAN_ACCESS_INTERNAL;
import static com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.ATLASSIAN_ACCESS_PUBLIC;

public class PluginInternalPackageDetectorTest {

    @Test
    public void atlassianAccessPublicShouldNotBeInternal() {
        BundleCapability export = mockCapability("com.atlassian.dmz", ATLASSIAN_ACCESS_PUBLIC);
        PluginInternalPackageDetector packageDetector =
                new PluginInternalPackageDetector(mock(InternalPackageDetector.class));

        assertFalse(packageDetector.isInternalPackage(export));
    }

    @Test
    public void atlassianAccessDeprecatedShouldNotBeInternal() {
        BundleCapability export = mockCapability("com.atlassian.dmz", ATLASSIAN_ACCESS_DEPRECATED);
        PluginInternalPackageDetector packageDetector =
                new PluginInternalPackageDetector(mock(InternalPackageDetector.class));

        assertFalse(packageDetector.isInternalPackage(export));
    }

    @Test
    public void atlassianAccessInternalShouldBeInternal() {
        BundleCapability export = mockCapability("com.atlassian.dmz", ATLASSIAN_ACCESS_INTERNAL);
        PluginInternalPackageDetector packageDetector =
                new PluginInternalPackageDetector(mock(InternalPackageDetector.class));

        assertTrue(packageDetector.isInternalPackage(export));
    }

    @Test
    public void atlassianAccessInheritedShouldDelegate() {
        BundleCapability export = mockCapability("com.atlassian.dmz", ATLASSIAN_ACCESS_INHERITED);
        InternalPackageDetector mock = mock(InternalPackageDetector.class);
        when(mock.isInternalPackage(any())).thenReturn(true);
        PluginInternalPackageDetector packageDetector = new PluginInternalPackageDetector(mock);

        assertTrue(packageDetector.isInternalPackage(export));
        verify(mock).isInternalPackage(export);
    }

    @Test(expected = UnknownAtlassianAccessValue.class)
    public void throwExceptionWhenUnknownValue() {
        BundleCapability export = mockCapability("com.atlassian.dmz", "garbage");
        PluginInternalPackageDetector packageDetector =
                new PluginInternalPackageDetector(mock(InternalPackageDetector.class));

        packageDetector.isInternalPackage(export);
    }

    @Test
    public void atlassianAccessPublicShouldNotBeDeprecated() {
        BundleCapability export = mockCapability("com.atlassian.dmz", ATLASSIAN_ACCESS_PUBLIC);
        PluginInternalPackageDetector packageDetector =
                new PluginInternalPackageDetector(mock(InternalPackageDetector.class));

        assertFalse(packageDetector.isDeprecatedPackage(export));
    }

    @Test
    public void atlassianAccessDeprecatedShouldBeDeprecated() {
        BundleCapability export = mockCapability("com.atlassian.dmz", ATLASSIAN_ACCESS_DEPRECATED);
        PluginInternalPackageDetector packageDetector =
                new PluginInternalPackageDetector(mock(InternalPackageDetector.class));

        assertTrue(packageDetector.isDeprecatedPackage(export));
    }

    @Test
    public void atlassianAccessInternalShouldNotBeDeprecated() {
        BundleCapability export = mockCapability("com.atlassian.dmz", ATLASSIAN_ACCESS_INTERNAL);
        PluginInternalPackageDetector packageDetector =
                new PluginInternalPackageDetector(mock(InternalPackageDetector.class));

        assertFalse(packageDetector.isDeprecatedPackage(export));
    }

    @Test
    public void atlassianAccessInheritedShouldDelegateDeprecated() {
        BundleCapability export = mockCapability("com.atlassian.dmz", ATLASSIAN_ACCESS_INHERITED);
        InternalPackageDetector mock = mock(InternalPackageDetector.class);
        when(mock.isDeprecatedPackage(any())).thenReturn(false);
        PluginInternalPackageDetector packageDetector = new PluginInternalPackageDetector(mock);

        assertFalse(packageDetector.isDeprecatedPackage(export));
        verify(mock).isDeprecatedPackage(export);
    }

    @Test(expected = UnknownAtlassianAccessValue.class)
    public void throwExceptionWhenUnknownValueDeprecated() {
        BundleCapability export = mockCapability("com.atlassian.dmz", "garbage");
        PluginInternalPackageDetector packageDetector =
                new PluginInternalPackageDetector(mock(InternalPackageDetector.class));

        packageDetector.isDeprecatedPackage(export);
    }

    public static BundleCapability mockCapability(String wiringPackage, String atlassianAccess) {
        Map<String, Object> map = new HashMap<>();
        map.put(ATTR_WIRING_PACKAGE, wiringPackage);
        map.put(PluginInternalPackageDetector.ATTR_ATLASSIAN_ACCESS, atlassianAccess);
        BundleCapability capability = mock(BundleCapability.class, RETURNS_DEEP_STUBS);
        when(capability.getAttributes()).thenReturn(map);
        return capability;
    }
}
