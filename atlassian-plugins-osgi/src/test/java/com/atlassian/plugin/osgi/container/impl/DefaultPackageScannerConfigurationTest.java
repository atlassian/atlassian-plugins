package com.atlassian.plugin.osgi.container.impl;

import java.util.Set;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;

public class DefaultPackageScannerConfigurationTest {
    @Test
    public void osgiPublicPackagesExcludesWildcardsAreWellFormed() {
        DefaultPackageScannerConfiguration configuration = new DefaultPackageScannerConfiguration();
        Set<String> osgiPublicPackagesExcludes = configuration.getOsgiPublicPackagesExcludes();
        for (String packageName : osgiPublicPackagesExcludes) {
            if (!packageName.endsWith("*")) {
                continue;
            }
            assertThat("OSGi package with a wildcard must end with '.*'", packageName, endsWith(".*"));
        }
    }

    @Test
    public void osgiDeprecatedPackagesWildcardsAreWellFormed() {
        DefaultPackageScannerConfiguration configuration = new DefaultPackageScannerConfiguration();
        Set<String> osgiDeprecatedPackages = configuration.getOsgiDeprecatedPackages();
        for (String packageName : osgiDeprecatedPackages) {
            if (!packageName.endsWith("*")) {
                continue;
            }
            assertThat("OSGi package with a wildcard must end with '.*'", packageName, endsWith(".*"));
        }
    }
}
