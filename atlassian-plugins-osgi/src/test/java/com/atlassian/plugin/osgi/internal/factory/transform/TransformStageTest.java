package com.atlassian.plugin.osgi.internal.factory.transform;

import java.lang.reflect.Field;
import java.util.Set;

import org.junit.Test;
import org.reflections.Reflections;

import static java.lang.String.format;
import static java.lang.reflect.Modifier.STATIC;
import static org.junit.Assert.assertTrue;
import static org.reflections.ReflectionUtils.getAllFields;
import static org.reflections.ReflectionUtils.withModifier;

public class TransformStageTest {
    @Test
    public void testTransformStageImplIsStateless() {
        Reflections reflections =
                new Reflections(TransformStage.class.getPackage().getName());
        for (Class<?> implCls : reflections.getSubTypesOf(TransformStage.class)) {
            final Set<Field> fields = getAllFields(implCls, withModifier(STATIC).negate());
            assertTrue(
                    format(
                            "'%s' subtypes must be stateless. Class '%s' contains non-static fields: %s",
                            TransformStage.class.getSimpleName(), implCls.getName(), fields),
                    fields.isEmpty());
        }
    }
}
