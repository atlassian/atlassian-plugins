package com.atlassian.plugin.osgi.hostcomponents.impl;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultComponentRegistrar {
    @Test
    public void testRegister() {
        DefaultComponentRegistrar registrar = new DefaultComponentRegistrar();
        Class[] ifs = new Class[] {Serializable.class};
        registrar.register(ifs).forInstance("Foo").withName("foo").withProperty("jim", "bar");
        HostComponentRegistration reg = registrar.getRegistry().get(0);

        assertNotNull(reg);
        assertEquals("Foo", reg.getInstance());
        assertEquals(Serializable.class.getName(), reg.getMainInterfaces()[0]);
        assertEquals("foo", reg.getProperties().get(DefaultPropertyBuilder.BEAN_NAME));
        assertEquals("bar", reg.getProperties().get("jim"));
    }

    @Test
    public void testRegisterMultiple() {
        DefaultComponentRegistrar registrar = new DefaultComponentRegistrar();
        Class[] ifs = new Class[] {Serializable.class};
        registrar.register(ifs).forInstance("Foo").withName("foo").withProperty("jim", "bar");
        registrar.register(ifs).forInstance("Foo").withName("foo").withProperty("sarah", "bar");
        assertEquals(2, registrar.getRegistry().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterOnlyInterfaces() {
        DefaultComponentRegistrar registrar = new DefaultComponentRegistrar();
        Class[] ifs = new Class[] {Object.class};
        registrar.register(ifs).forInstance("Foo").withName("foo").withProperty("jim", "bar");
    }

    @Test
    public void testWriteRegistry() {
        Class[] ifs = new Class[] {Serializable.class};
        DefaultComponentRegistrar registrar = new DefaultComponentRegistrar();
        registrar.register(ifs).forInstance("Foo").withName("foo");

        BundleContext ctx = mock(BundleContext.class);
        registrar.writeRegistry(ctx);

        verifyRegistration(ctx, ifs, "Foo", "foo");
    }

    @Test
    public void testWriteRegistryRemovesHostContainer() {
        Class[] ifs = new Class[] {HostContainer.class};
        DefaultComponentRegistrar registrar = new DefaultComponentRegistrar();
        registrar.register(ifs).forInstance("Foo").withName("foo");

        BundleContext ctx = mock(BundleContext.class);
        registrar.writeRegistry(ctx);

        verifyZeroInteractions(ctx);
        assertEquals(0, registrar.getRegistry().size());
    }

    @Test
    public void testWriteRegistryNoInterface() {
        Class[] ifs = new Class[] {};
        DefaultComponentRegistrar registrar = new DefaultComponentRegistrar();
        registrar.register(ifs).forInstance("Foo").withName("foo");

        BundleContext ctx = mock(BundleContext.class);
        registrar.writeRegistry(ctx);

        verifyRegistration(ctx, ifs, "Foo", "foo");
    }

    @Test
    public void testWriteRegistryGenBeanName() {
        Class[] ifs = new Class[] {Serializable.class};
        DefaultComponentRegistrar registrar = new DefaultComponentRegistrar();
        registrar.register(ifs).forInstance("Foo");

        BundleContext ctx = mock(BundleContext.class);
        registrar.writeRegistry(ctx);

        verifyRegistration(
                ctx,
                ifs,
                "Foo",
                "hostComponent-"
                        + Arrays.asList(registrar.getRegistry().get(0).getMainInterfaces())
                                .hashCode());
    }

    private void verifyRegistration(BundleContext ctx, Class[] ifs, Object instance, String name) {
        Dictionary<String, String> properties = new Hashtable<>();
        properties.put(DefaultPropertyBuilder.BEAN_NAME, name);
        properties.put(DefaultComponentRegistrar.HOST_COMPONENT_FLAG, "true");
        // argThat(equalTo(instance)) is used instead of eq(instance) as hamcrest does
        // proxy.equals(original) where are mockito does original.equals(proxy) and
        // unfortunately original.equals(proxy) != proxy.equals(original)
        verify(ctx).registerService(eq(toClassNames(ifs)), argThat(equalTo(instance)), eq(properties));
    }

    private static String[] toClassNames(Class[] classes) {
        String[] classNames = new String[classes.length];
        for (int i = 0; i < classNames.length; i++) {
            classNames[i] = classes[i].getName();
        }
        return classNames;
    }
}
