package com.atlassian.plugin.osgi.factory;

import java.io.ByteArrayInputStream;

import org.dom4j.Element;
import org.dom4j.tree.DefaultElement;
import org.junit.Test;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestOsgiPluginXmlDescriptorParser {

    @Test
    public void testCreateModuleDescriptor()
            throws PluginParseException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        OsgiPluginXmlDescriptorParser parser =
                new OsgiPluginXmlDescriptorParser(new ByteArrayInputStream("<foo/>".getBytes()), ImmutableSet.of());

        ModuleDescriptor desc = mock(ModuleDescriptor.class);
        when(desc.getKey()).thenReturn("foo");
        ModuleDescriptorFactory factory = mock(ModuleDescriptorFactory.class);
        when(factory.getModuleDescriptor("foo")).thenReturn(desc);

        OsgiPlugin plugin = mock(OsgiPlugin.class);
        Element fooElement = new DefaultElement("foo");
        fooElement.addAttribute("key", "bob");
        assertNotNull(parser.createModuleDescriptor(plugin, new Dom4jDelegatingElement(fooElement), factory));
        verify(plugin).addModuleDescriptorElement("foo", new Dom4jDelegatingElement(fooElement));
    }
}
