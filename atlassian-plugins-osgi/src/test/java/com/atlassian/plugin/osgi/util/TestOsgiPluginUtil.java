package com.atlassian.plugin.osgi.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;
import org.osgi.framework.Bundle;
import org.osgi.framework.wiring.BundleRequirement;
import org.osgi.framework.wiring.BundleRevision;
import org.osgi.framework.wiring.BundleRevisions;
import org.osgi.framework.wiring.BundleWire;
import org.osgi.framework.wiring.BundleWiring;
import com.google.common.collect.ImmutableMap;
import io.atlassian.fugue.Pair;

import com.atlassian.plugin.PluginDependencies;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.mock;

public class TestOsgiPluginUtil {
    private BundleImpl bundle;

    @Test
    public void getRequiredPluginKeysUsesBundleWiring() {
        bundle = createBundleWithWiring(
                new Pair<>("1", "mandatory"), new Pair<>("2", "optional"), new Pair<>("3", "dynamic"));

        Set<String> keys = OsgiPluginUtil.getDependencies(bundle).getAll();

        assertThat(keys, containsInAnyOrder("1-null", "2-null", "3-null"));
    }

    @Test
    public void getDependentPluginKeysByResolution() {
        bundle = createBundleWithWiring(
                new Pair<>("1", "mandatory"), new Pair<>("2", "optional"), new Pair<>("3", "dynamic"));

        final PluginDependencies pluginDependencies = OsgiPluginUtil.getDependencies(bundle);

        assertThat(pluginDependencies.getMandatory(), contains("1-null"));
        assertThat(pluginDependencies.getOptional(), contains("2-null"));
        assertThat(pluginDependencies.getDynamic(), contains("3-null"));
    }

    @Test
    public void getDependentPluginKeysByResolutionWithMultipleWires() {
        bundle = createBundleWithWiring(
                new Pair<>("1mod", "mandatory"),
                new Pair<>("1mod", "optional"),
                new Pair<>("1mod", "dynamic"),
                new Pair<>("2mo", "optional"),
                new Pair<>("2mo", "mandatory"),
                new Pair<>("3do", "dynamic"),
                new Pair<>("3do", "optional"));

        final PluginDependencies pluginDependencies = OsgiPluginUtil.getDependencies(bundle);

        assertThat(pluginDependencies.getMandatory(), containsInAnyOrder("1mod-null", "2mo-null"));
        assertThat(pluginDependencies.getOptional(), containsInAnyOrder("1mod-null", "2mo-null", "3do-null"));
        assertThat(pluginDependencies.getDynamic(), containsInAnyOrder("1mod-null", "3do-null"));
    }

    private interface BundleImpl extends Bundle, BundleRevisions {}

    private BundleImpl createBundleWithWiring(final Pair<String, String>... wires) {
        List<BundleWire> requiredWires = new ArrayList<>();

        for (Pair<String, String> wire : wires) {
            Bundle requiredBundle = mock(Bundle.class);
            Mockito.when(requiredBundle.getSymbolicName()).thenReturn(wire.left());
            Mockito.when(requiredBundle.getHeaders()).thenReturn(new Hashtable<String, String>());

            BundleWiring provider = mock(BundleWiring.class);
            Mockito.when(provider.getBundle()).thenReturn(requiredBundle);

            BundleRequirement requirement = mock(BundleRequirement.class);
            Map<String, String> directives;
            if ("mandatory".equals(wire.right())) {
                directives = ImmutableMap.of();
            } else {
                directives = ImmutableMap.of("resolution", wire.right());
            }
            Mockito.when(requirement.getDirectives()).thenReturn(directives);

            BundleWire requiredWire = mock(BundleWire.class);
            Mockito.when(requiredWire.getProviderWiring()).thenReturn(provider);
            Mockito.when(requiredWire.getRequirement()).thenReturn(requirement);

            requiredWires.add(requiredWire);
        }

        BundleWiring wiring = mock(BundleWiring.class);
        Mockito.when(wiring.getRequiredWires(null)).thenReturn(requiredWires);

        BundleRevision bundleRevision = mock(BundleRevision.class);
        List<BundleRevision> bundleRevisions = new ArrayList<>();
        bundleRevisions.add(bundleRevision);
        Mockito.when(bundleRevision.getWiring()).thenReturn(wiring);

        BundleImpl bundle = mock(BundleImpl.class);
        Mockito.when(bundle.getRevisions()).thenReturn(bundleRevisions);

        return bundle;
    }
}
