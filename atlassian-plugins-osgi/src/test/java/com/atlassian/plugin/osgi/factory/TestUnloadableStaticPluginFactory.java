package com.atlassian.plugin.osgi.factory;

import java.io.ByteArrayInputStream;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.module.Element;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUnloadableStaticPluginFactory {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void canCreate() {
        UnloadableStaticPluginFactory factory = new UnloadableStaticPluginFactory("foo.xml");
        PluginArtifact artifact = mock(PluginArtifact.class);
        when(artifact.getResourceAsStream("foo.xml"))
                .thenReturn(new ByteArrayInputStream("<atlassian-plugin key=\"foo\" />".getBytes()));
        assertEquals("foo", factory.canCreate(artifact));
    }

    @Test
    public void canCreateWithOsgi() {
        UnloadableStaticPluginFactory factory = new UnloadableStaticPluginFactory("foo.xml");
        PluginArtifact artifact = mock(PluginArtifact.class);
        when(artifact.getResourceAsStream("foo.xml"))
                .thenReturn(
                        new ByteArrayInputStream("<atlassian-plugin key=\"foo\" plugins-version=\"2\"/>".getBytes()));
        assertEquals(null, factory.canCreate(artifact));
    }

    @Test
    public void canCreateWithNoDescriptor() {
        UnloadableStaticPluginFactory factory = new UnloadableStaticPluginFactory("foo.xml");
        PluginArtifact artifact = mock(PluginArtifact.class);
        when(artifact.getResourceAsStream("foo.xml")).thenReturn(null);
        assertEquals(null, factory.canCreate(artifact));
    }

    @Test
    public void create() {
        UnloadableStaticPluginFactory factory = new UnloadableStaticPluginFactory("foo.xml");
        PluginArtifact artifact = mock(PluginArtifact.class);
        when(artifact.getResourceAsStream("foo.xml"))
                .thenReturn(new ByteArrayInputStream("<atlassian-plugin key=\"foo\" />".getBytes()));
        when(artifact.toString()).thenReturn("plugin.jar");
        UnloadablePlugin plugin = (UnloadablePlugin)
                factory.create(artifact, new DefaultModuleDescriptorFactory(new DefaultHostContainer()));
        assertNotNull(plugin);
        assertEquals("foo", plugin.getKey());
        assertTrue(plugin.getErrorText().contains("plugin.jar"));
    }

    @Test
    public void createWithNoKey() {
        UnloadableStaticPluginFactory factory = new UnloadableStaticPluginFactory("foo.xml");
        PluginArtifact artifact = mock(PluginArtifact.class);
        when(artifact.getResourceAsStream("foo.xml"))
                .thenReturn(new ByteArrayInputStream("<atlassian-plugin />".getBytes()));
        when(artifact.toString()).thenReturn("plugin.jar");
        UnloadablePlugin plugin = (UnloadablePlugin)
                factory.create(artifact, new DefaultModuleDescriptorFactory(new DefaultHostContainer()));
        assertNotNull(plugin);
        assertNull(plugin.getKey());
        assertTrue(plugin.getErrorText().contains("plugin.jar"));
    }

    @Test
    public void createModuleInvalidPluginType() {
        final UnloadableStaticPluginFactory factory = new UnloadableStaticPluginFactory("foo.xml");

        final Plugin plugin = mock(Plugin.class);
        final Element module = mock(Element.class);
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        assertThat(factory.createModule(plugin, module, moduleDescriptorFactory), nullValue());
    }

    @Test
    public void createModuleNotForUnloadable() {
        final UnloadableStaticPluginFactory factory = new UnloadableStaticPluginFactory("foo.xml");

        final UnloadablePlugin plugin = mock(UnloadablePlugin.class);
        final Element module = mock(Element.class);
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        expectedException.expect(PluginException.class);

        factory.createModule(plugin, module, moduleDescriptorFactory);
    }
}
