package com.atlassian.plugin.osgi;

import com.google.common.annotations.VisibleForTesting;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

public class DummyModuleDescriptor extends AbstractModuleDescriptor<Void> {
    private boolean enabled = false;

    // required to silence log errors when using DefaultHostContainer in tests
    @VisibleForTesting
    public DummyModuleDescriptor() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    public DummyModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public Void getModule() {
        return null;
    }

    @Override
    public void enabled() {
        super.enabled();
        enabled = true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
