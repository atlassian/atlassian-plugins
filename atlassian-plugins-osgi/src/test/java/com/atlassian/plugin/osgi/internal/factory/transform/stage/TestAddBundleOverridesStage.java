package com.atlassian.plugin.osgi.internal.factory.transform.stage;

import java.io.File;
import java.util.Collections;

import org.junit.Test;
import org.osgi.framework.ServiceReference;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.factory.transform.model.SystemExports;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformContext;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestAddBundleOverridesStage {

    @Test
    public void testTransform() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test Bundle instruction plugin 2' key='test.plugin'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions>",
                        "            <Export-Package>!*.internal.*,*</Export-Package>",
                        "        </bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();

        final AddBundleOverridesStage stage = new AddBundleOverridesStage();
        OsgiContainerManager osgiContainerManager = mock(OsgiContainerManager.class);
        when(osgiContainerManager.getRegisteredServices()).thenReturn(new ServiceReference[0]);
        final TransformContext context = new TransformContext(
                Collections.emptyList(),
                SystemExports.NONE,
                new JarPluginArtifact(plugin),
                null,
                PluginAccessor.Descriptor.FILENAME,
                osgiContainerManager);
        stage.execute(context);
        assertEquals("!*.internal.*,*", context.getBndInstructions().get("Export-Package"));
    }
}
