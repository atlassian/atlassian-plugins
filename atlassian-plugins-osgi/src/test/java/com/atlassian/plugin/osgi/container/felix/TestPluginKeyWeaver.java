package com.atlassian.plugin.osgi.container.felix;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.io.IOUtils;
import org.eclipse.gemini.blueprint.service.importer.support.LocalBundleContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.hooks.weaving.WovenClass;
import org.osgi.framework.wiring.BundleWiring;

import net.bytebuddy.dynamic.loading.ByteArrayClassLoader;

import com.atlassian.plugin.util.PluginKeyStack;

import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestPluginKeyWeaver {
    private static final String WOVEN_CLASS_NAME =
            "org.eclipse.gemini.blueprint.service.importer.support.LocalBundleContextAdvice";
    private static final String FOO_PLUGIN_NAME = "atlassian-foo-plugin";
    private static final String FOO_PLUGIN_VERSION = "0.1.0";

    @Mock
    private WovenClass wovenClass;

    @Mock
    private BundleWiring bundleWiring;

    @Mock
    private Bundle bundle;

    @Mock
    private BundleContext bundleContext;

    @Mock
    private MethodInvocation methodInvocation;

    @Captor
    private ArgumentCaptor<byte[]> bytesCaptor;

    private final PluginKeyWeaver weaver = new PluginKeyWeaver();

    @Before
    public void setup() throws IOException {
        when(wovenClass.getClassName()).thenReturn(WOVEN_CLASS_NAME);
        when(wovenClass.getBundleWiring()).thenReturn(bundleWiring);
        when(wovenClass.getBytes()).thenReturn(getBytesFor(WOVEN_CLASS_NAME));
        doNothing().when(wovenClass).setBytes(bytesCaptor.capture());

        when(bundleWiring.getClassLoader()).thenReturn(LocalBundleContext.class.getClassLoader());

        when(bundleContext.getBundle()).thenReturn(bundle);
        when(bundle.getSymbolicName()).thenReturn(FOO_PLUGIN_NAME);
        when(bundle.getHeaders()).thenReturn(new Hashtable<String, String>() {
            {
                put("Bundle-Version", FOO_PLUGIN_VERSION);
            }
        });
    }

    @Test
    public void testWovenClassModifiesBytecodeToCapturePluginKeys() throws Throwable {
        // arrange
        System.setProperty("atlassian.plugins.profiling.disabled", "false");

        final String pluginKey = format("%s-%s", FOO_PLUGIN_NAME, FOO_PLUGIN_VERSION);
        final List<String> dynamicImports = new ArrayList<>();

        when(wovenClass.getDynamicImports()).thenReturn(dynamicImports);
        when(methodInvocation.proceed()).then((a) -> {
            assertThat(PluginKeyStack.getPluginKeys(), containsInAnyOrder(pluginKey));
            return "foo";
        });

        // act
        weaver.weave(wovenClass);

        final Object wovenTypeInstance = createInstance();
        final Field pluginKeyField = getPluginKeyField(wovenTypeInstance);
        final Method invokeMethod = getInvokeMethod(wovenTypeInstance);

        invokeMethod.invoke(wovenTypeInstance, methodInvocation);

        // assert
        assertThat(
                dynamicImports,
                containsInAnyOrder(
                        "com.atlassian.plugin.util",
                        "com.atlassian.plugin.osgi.util",
                        "com.atlassian.plugin.osgi.container.felix"));
        assertThat(pluginKeyField.get(wovenTypeInstance), equalTo(pluginKey));
        verify(methodInvocation).proceed();
        assertThat(PluginKeyStack.getPluginKeys(), empty());
    }

    @Test
    public void testUnWovenClassDoesNotModifyBytecode() {
        when(wovenClass.getClassName()).thenReturn(String.class.getName());

        weaver.weave(wovenClass);

        verify(wovenClass, never()).setBytes(any());
    }

    private Object createInstance() throws Exception {
        final ClassLoader byteArrayClassLoader =
                new ByteArrayClassLoader.ChildFirst(getClass().getClassLoader(), new HashMap<String, byte[]>() {
                    {
                        put(WOVEN_CLASS_NAME, bytesCaptor.getValue());
                        put(LocalBundleContext.class.getName(), getBytesFor(LocalBundleContext.class.getName()));
                    }
                });

        final Class<?> wovenClassType = byteArrayClassLoader.loadClass(WOVEN_CLASS_NAME);
        final Constructor<?> wovenTypeConstructor = wovenClassType.getDeclaredConstructor(BundleContext.class);
        wovenTypeConstructor.setAccessible(true);
        return wovenTypeConstructor.newInstance(bundleContext);
    }

    private byte[] getBytesFor(final String wovenClassName) throws IOException {
        try (InputStream inputStream =
                getClass().getClassLoader().getResourceAsStream(wovenClassName.replace('.', '/') + ".class")) {
            return IOUtils.toByteArray(inputStream);
        }
    }

    private static Field getPluginKeyField(Object wovenTypeInstance) throws NoSuchFieldException {
        final Field pluginKeyField = wovenTypeInstance.getClass().getDeclaredField("pluginKey");
        pluginKeyField.setAccessible(true);

        return pluginKeyField;
    }

    private static Method getInvokeMethod(Object wovenTypeInstance) throws NoSuchMethodException {
        final Method invokeMethod = wovenTypeInstance.getClass().getDeclaredMethod("invoke", MethodInvocation.class);
        invokeMethod.setAccessible(true);
        return invokeMethod;
    }
}
