package com.atlassian.plugin.osgi;

import javax.inject.Inject;

public class HostClassUsingHostComponentSetter {
    private SomeInterface someInterface;

    public HostClassUsingHostComponentSetter() {}

    @Inject
    public void setSomeInterface(SomeInterface someInterface) {
        this.someInterface = someInterface;
    }

    public SomeInterface getSomeInterface() {
        return someInterface;
    }
}
