package com.atlassian.plugin.osgi.hook.dmz.packages;

import java.util.Set;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.osgi.framework.wiring.BundleCapability;
import com.google.common.collect.Sets;

import static java.util.Collections.emptySet;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugin.osgi.hook.TestUtils.mockCapability;

public class DefaultInternalPackageDetectorTest {
    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Test
    public void testPublicPackagesFullName() {
        DefaultInternalPackageDetector detector =
                getDetector(Sets.newHashSet("com.atlassian.bitbucket.dmz"), Sets.newHashSet());
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.dmz");

        assertFalse(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesWithStar2() {
        DefaultInternalPackageDetector detector =
                getDetector(Sets.newHashSet("com.atlassian.bitbucket.*"), Sets.newHashSet());
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.dmz");

        assertFalse(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesWithStar3() {
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("com.atlassian.bitbucket.api.*"),
                Sets.newHashSet("com.atlassian.bitbucket.api.internal.*"));
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.api");

        assertFalse(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicAndExcludeMatchAll() {
        DefaultInternalPackageDetector detector = getDetector(Sets.newHashSet("*"), Sets.newHashSet("*"));
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.api");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesWithStar() {
        DefaultInternalPackageDetector detector =
                getDetector(Sets.newHashSet("com.atlassian.bitbucket.*"), Sets.newHashSet());
        BundleCapability capability = mockCapability("com.atlassian.bitbucket2");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesExcludesFullName() {
        DefaultInternalPackageDetector detector =
                getDetector(Sets.newHashSet(), Sets.newHashSet("com.atlassian.bitbucket.dmz"));
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.dmz");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesExcludesWithStar() {
        DefaultInternalPackageDetector detector =
                getDetector(Sets.newHashSet(), Sets.newHashSet("com.atlassian.bitbucket.*"));
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.dmz");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testDeprecatedPackage() {
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("com.atlassian.bitbucket.*"),
                Sets.newHashSet(),
                Sets.newHashSet("com.atlassian.bitbucket.deprecated"));
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.deprecated");

        assertFalse(detector.isInternalPackage(capability));
        assertTrue(detector.isDeprecatedPackage(capability));
    }

    @Test
    public void testPublicPackagesAndExcludes() {
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("com.atlassian.bitbucket.*"), Sets.newHashSet("com.atlassian.bitbucket.dmz"));
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.dmz");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesAndExcludes2() {
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("com.atlassian.bitbucket.dmz"), Sets.newHashSet("com.atlassian.bitbucket.dmz"));
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.dmz");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesAndExclude3() {
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("com.atlassian.bitbucket.dmz.*"), Sets.newHashSet("com.atlassian.bitbucket.dmz.*"));
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.dmz");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesAndExcludes4() {
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("com.atlassian.bitbucket.api.foo"), Sets.newHashSet("com.atlassian.bitbucket.*"));
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.api.foo");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesExcludes() {
        DefaultInternalPackageDetector detector =
                getDetector(Sets.newHashSet(), Sets.newHashSet("com.atlassian.bitbucket.dmz"));

        // to make it available, it needs to be listed as public package
        BundleCapability capability = mockCapability("com.atlassian.bitbucket.api");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesExcludesOverridesPublicPackages() {
        // public packages excludes have the final say and overrides the public packages
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("com.fasterxml.jackson.annotation.*"), Sets.newHashSet("com.fasterxml.jackson.*"));
        BundleCapability capability = mockCapability("com.fasterxml.jackson.annotation");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicPackagesExcludesOverridesPublicPackages2() {
        // public packages excludes have the final say and overrides the public packages
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("*", "com.fasterxml.jackson.annotation.*"), Sets.newHashSet("com.fasterxml.jackson.*"));
        BundleCapability capability = mockCapability("com.fasterxml.jackson.annotation");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testExcludesOverridesPublicPackagesIfPackagesAreEquals() {
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("com.fasterxml.jackson.annotation.*"),
                Sets.newHashSet("com.fasterxml.jackson.annotation.*"));
        BundleCapability capability = mockCapability("com.fasterxml.jackson.annotation");

        assertTrue(detector.isInternalPackage(capability));
    }

    @Test
    public void testPublicAccessWhenMultipleExcludesDefined() {
        DefaultInternalPackageDetector detector = getDetector(
                Sets.newHashSet("*"),
                Sets.newHashSet("com.fasterxml.jackson.core.*", "com.fasterxml.jackson.databind.*"));
        BundleCapability capability = mockCapability("com.fasterxml.jackson.annotation");

        assertFalse(detector.isInternalPackage(capability));
    }

    public DefaultInternalPackageDetector getDetector(Set<String> publicPackages, Set<String> publicPackagesExcludes) {
        return getDetector(publicPackages, publicPackagesExcludes, emptySet());
    }

    public DefaultInternalPackageDetector getDetector(
            Set<String> publicPackages, Set<String> publicPackagesExcludes, Set<String> deprecatedPackages) {
        return new DefaultInternalPackageDetector(
                new DmzPackagePatterns(publicPackages, publicPackagesExcludes, deprecatedPackages));
    }
}
