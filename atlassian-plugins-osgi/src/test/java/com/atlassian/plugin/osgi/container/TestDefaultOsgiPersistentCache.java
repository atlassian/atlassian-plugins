package com.atlassian.plugin.osgi.container;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.atlassian.plugin.osgi.container.impl.DefaultOsgiPersistentCache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestDefaultOsgiPersistentCache {

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    private File tmpDir;

    @Before
    public void setUp() {
        tmpDir = temporaryFolder.getRoot();
    }

    @Test
    public void testRecordLastVersion() throws IOException {
        DefaultOsgiPersistentCache cache = new DefaultOsgiPersistentCache(tmpDir);
        File versionFile = new File(new File(tmpDir, "transformed-plugins"), "cache.key");
        cache.validate("1.0");
        assertTrue(versionFile.exists());
        String txt = FileUtils.readFileToString(versionFile);

        // "e8dc057d3346e56aed7cf252185dbe1fa6454411" == SHA1("1.0").
        assertEquals(String.valueOf("e8dc057d3346e56aed7cf252185dbe1fa6454411"), txt);
    }

    @Test
    public void testCleanOnUpgrade() throws IOException {
        DefaultOsgiPersistentCache cache = new DefaultOsgiPersistentCache(tmpDir);
        File tmp = File.createTempFile("foo", ".txt", new File(tmpDir, "transformed-plugins"));
        cache.validate("1.0");
        assertTrue(tmp.exists());
        cache.validate("2.0");
        assertFalse(tmp.exists());
    }

    @Test
    public void testNullVersion() throws IOException {
        DefaultOsgiPersistentCache cache = new DefaultOsgiPersistentCache(tmpDir);
        cache.validate(null);
        File tmp = File.createTempFile("foo", ".txt", new File(tmpDir, "transformed-plugins"));
        assertTrue(tmp.exists());
        cache.validate(null);
        assertTrue(tmp.exists());
    }
}
