package com.atlassian.plugin.osgi.hook.dmz;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PackageMatcherTest {

    @Test
    public void testMatcher() {
        assertTrue(new PackageMatcher("com.atlassian.api", "com.atlassian.api").match());
        assertTrue(new PackageMatcher("com.atlassian.api.*", "com.atlassian.api").match());
        assertTrue(new PackageMatcher("com.atlassian.api.*", "com.atlassian.api.rest").match());
        assertTrue(new PackageMatcher("com.atlassian.api.*", "com.atlassian.api.level1.level2").match());
        assertTrue(new PackageMatcher("com.atlassian.*.api.rest", "com.atlassian.api.rest").match());
        assertTrue(new PackageMatcher("com.atlassian.*.api.rest.*", "com.atlassian.api.rest").match());
        assertTrue(new PackageMatcher("com.atlassian.*.api.*", "com.atlassian.api.rest").match());
        assertTrue(new PackageMatcher("com.atlassian.*.api.*", "com.atlassian.api.rest.v2").match());
        assertTrue(new PackageMatcher("com.atlassian.*.api", "com.atlassian.plugins.api").match());
        assertTrue(new PackageMatcher("com.atlassian.*.api.*", "com.atlassian.plugins.api.rest.v2").match());
        assertTrue(new PackageMatcher("com.atlassian.*.api.*.v2", "com.atlassian.plugins.new.api.rest.v2").match());
        assertTrue(new PackageMatcher("*", "com.atlassian.plugins.new.api.rest.v2").match());

        assertFalse(new PackageMatcher("com.atlassian.api", "com.atlassian.api.rest").match());
    }
}
