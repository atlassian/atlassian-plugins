package com.atlassian.plugin.osgi;

import java.util.concurrent.atomic.AtomicInteger;

import com.atlassian.plugin.StateAware;

public class DummyStateAwareModuleDescriptorWithKey extends DummyModuleDescriptorWithKey implements StateAware {
    private final AtomicInteger timesEnabled;
    private final AtomicInteger timesDisabled;

    public DummyStateAwareModuleDescriptorWithKey(final AtomicInteger timesEnabled, final AtomicInteger timesDisabled) {
        this.timesEnabled = timesEnabled;
        this.timesDisabled = timesDisabled;
    }

    @Override
    public void enabled() {
        timesEnabled.incrementAndGet();
        super.enabled();
    }

    @Override
    public void disabled() {
        timesDisabled.incrementAndGet();
        super.disabled();
    }
}
