package com.atlassian.plugin.osgi.factory;

import java.io.File;
import java.util.Dictionary;
import java.util.Hashtable;

import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.ReferenceMode.FORBID_REFERENCE;
import static com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE;

public class TestOsgiPluginUninstalledHelper {

    private final String key = "key";
    private OsgiContainerManager mgr;
    private OsgiPluginUninstalledHelper helper;

    @Before
    public void setUp() {
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        mgr = mock(OsgiContainerManager.class);
        when(pluginArtifact.getReferenceMode()).thenReturn(FORBID_REFERENCE);
        helper = new OsgiPluginUninstalledHelper(key, mgr, pluginArtifact);
    }

    @Test
    public void testInstall() {
        final Dictionary<String, String> dict = new Hashtable<>();
        dict.put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, key);
        final Bundle bundle = mock(Bundle.class);
        when(bundle.getHeaders()).thenReturn(dict);
        when(bundle.getSymbolicName()).thenReturn(key);
        when(mgr.installBundle(null, FORBID_REFERENCE)).thenReturn(bundle);
        assertEquals(bundle, helper.install());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstallDifferentSymbolicName() {
        final Dictionary<String, String> dict = new Hashtable<>();
        final Bundle bundle = mock(Bundle.class);
        when(bundle.getHeaders()).thenReturn(dict);
        when(bundle.getSymbolicName()).thenReturn("bar");
        when(mgr.installBundle(null, FORBID_REFERENCE)).thenReturn(bundle);

        helper.install();
    }

    @Test
    public void testInstallDifferentSymbolicNameButAltassianKeyFound() {
        final Dictionary<String, String> dict = new Hashtable<>();
        dict.put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, key);
        final Bundle bundle = mock(Bundle.class);
        when(bundle.getHeaders()).thenReturn(dict);
        when(bundle.getSymbolicName()).thenReturn("bar");
        when(mgr.installBundle(null, FORBID_REFERENCE)).thenReturn(bundle);

        helper.install();
    }

    @Test
    public void testPluginArtifactThatAllowsReferenceIsInstalledByReference() {
        final String bundleSymbolicName = getClass().getName() + ".testBundle";
        final String bundleVersion = "1.2";
        final String pluginKey = bundleSymbolicName + "-" + bundleVersion;

        final File jarFile = new File("some.jar");
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        when(pluginArtifact.toFile()).thenReturn(jarFile);
        when(pluginArtifact.getReferenceMode()).thenReturn(PERMIT_REFERENCE);
        final Bundle expectedBundle = mock(Bundle.class);
        // Need a basic set of headers to pass the plugin key tests on install
        final Dictionary<String, String> headers = new Hashtable<>(ImmutableMap.<String, String>builder()
                .put(Constants.BUNDLE_SYMBOLICNAME, bundleSymbolicName)
                .put(Constants.BUNDLE_VERSION, bundleVersion)
                .build());
        when(expectedBundle.getSymbolicName()).thenReturn(bundleSymbolicName);
        when(expectedBundle.getHeaders()).thenReturn(headers);
        final OsgiContainerManager osgiContainerManager = mock(OsgiContainerManager.class);
        // Have our mock OsgiContainerManager respond only to a reference install (the PERMIT_REFERENCE in the next
        // line)
        when(osgiContainerManager.installBundle(jarFile, PERMIT_REFERENCE)).thenReturn(expectedBundle);
        final OsgiPluginUninstalledHelper osgiPluginUninstalledHelper =
                new OsgiPluginUninstalledHelper(pluginKey, osgiContainerManager, pluginArtifact);
        final Bundle actualBundle = osgiPluginUninstalledHelper.install();
        assertThat(actualBundle, sameInstance(expectedBundle));
        verify(osgiContainerManager).installBundle(jarFile, PERMIT_REFERENCE);
    }
}
