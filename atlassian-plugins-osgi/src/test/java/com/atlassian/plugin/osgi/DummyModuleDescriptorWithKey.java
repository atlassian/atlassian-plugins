package com.atlassian.plugin.osgi;

import java.io.InputStream;
import java.net.URL;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.impl.AbstractPlugin;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.ModuleFactory;

public class DummyModuleDescriptorWithKey extends AbstractModuleDescriptor<Void> {
    private final String key;

    public DummyModuleDescriptorWithKey() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
        final Element e = DocumentHelper.createElement("somecrap");
        e.addAttribute("key", "foo");
        init(new MockPlugin(this.getClass().getName()), new Dom4jDelegatingElement(e));
        this.key = "somekey";
    }

    @Override
    public String getCompleteKey() {
        return "com.atlassian.test.plugin:somekey";
    }

    @Override
    public String getPluginKey() {
        return "com.atlassian.test.plugin";
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Void getModule() {
        return null;
    }

    private class MockPlugin extends AbstractPlugin {
        MockPlugin(final String key) {
            super(null);
            setKey(key);
            setName(key);
        }

        public boolean isUninstallable() {
            return false;
        }

        public boolean isDeleteable() {
            return false;
        }

        public boolean isDynamicallyLoaded() {
            return false;
        }

        public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
            return null;
        }

        public ClassLoader getClassLoader() {
            return this.getClass().getClassLoader();
        }

        public URL getResource(final String path) {
            return null;
        }

        public InputStream getResourceAsStream(final String name) {
            return null;
        }
    }
}
