package com.atlassian.plugin.osgi.hook.dmz;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.osgi.framework.wiring.BundleCapability;
import org.osgi.framework.wiring.BundleRequirement;
import org.osgi.framework.wiring.BundleRevision;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.osgi.hook.dmz.packages.DmzPackagePatterns;
import com.atlassian.plugin.osgi.hook.dmz.packages.ExportTypeBasedInternalPackageDetector;
import com.atlassian.plugin.osgi.hook.dmz.packages.InternalPackageDetector;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonMap;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.osgi.hook.dmz.DmzResolverHook.ATTR_WIRING_PACKAGE;
import static com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.ATLASSIAN_ACCESS_DEPRECATED;
import static com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.ATLASSIAN_ACCESS_INHERITED;
import static com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.ATLASSIAN_ACCESS_INTERNAL;
import static com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.ATLASSIAN_ACCESS_PUBLIC;
import static com.atlassian.plugin.osgi.hook.dmz.packages.PluginInternalPackageDetector.ATTR_ATLASSIAN_ACCESS;

public class DmzResolverHookTest {

    private static final String SYSTEM_BUNDLE_KEY = "system-bundle";
    private static final String INTERNAL_PLUGIN_KEY = "com.atlassian.internal";
    private static final String CUSTOM_INTERNAL_PLUGIN_KEY = "bundled-plugin:key";
    private static final String EXTERNAL_PLUGIN_KEY = "org.other.company";

    private static final String UNLISTED_PACKAGE = "com.atlassian.bamboo";

    private static final String PUBLIC_PACKAGE_PATTERN = "com.atlassian.bitbucket.*";
    private static final String PUBLIC_PACKAGE = "com.atlassian.bitbucket.public";

    private static final String PUBLIC_PACKAGE_EXCLUDE_PATTERN = "com.atlassian.bitbucket.dmz.*";
    private static final String INTERNAL_PACKAGE = "com.atlassian.bitbucket.dmz.internal";

    private static final String DEPRECATED_PACKAGE_PATTERN = "com.atlassian.bitbucket.deprecated.*";
    private static final String DEPRECATED_PACKAGE = "com.atlassian.bitbucket.deprecated";

    private static final String GUAVA_PACKAGE = "com.google.common.collect";

    @Test
    public void internalPluginsShouldHaveAllImports() {
        DmzResolverHook hook = createTestDmzResolverHook(false);

        BundleRequirement requirement = mockRequirement(INTERNAL_PLUGIN_KEY);

        Set<BundleCapability> capabilities = new HashSet<>();
        capabilities.add(mockCapability(SYSTEM_BUNDLE_KEY, UNLISTED_PACKAGE));
        capabilities.add(mockCapability(SYSTEM_BUNDLE_KEY, PUBLIC_PACKAGE));
        capabilities.add(mockCapability(SYSTEM_BUNDLE_KEY, INTERNAL_PACKAGE));
        capabilities.add(mockCapability(SYSTEM_BUNDLE_KEY, DEPRECATED_PACKAGE));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE, ATLASSIAN_ACCESS_PUBLIC));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE, ATLASSIAN_ACCESS_DEPRECATED));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE, ATLASSIAN_ACCESS_INHERITED));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_PUBLIC));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_DEPRECATED));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_INHERITED));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, INTERNAL_PACKAGE));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, INTERNAL_PACKAGE, ATLASSIAN_ACCESS_PUBLIC));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, INTERNAL_PACKAGE, INTERNAL_PLUGIN_KEY));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, INTERNAL_PACKAGE, ATLASSIAN_ACCESS_DEPRECATED));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, INTERNAL_PACKAGE, ATLASSIAN_ACCESS_INHERITED));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE, ATLASSIAN_ACCESS_PUBLIC));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE, INTERNAL_PLUGIN_KEY));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE, ATLASSIAN_ACCESS_DEPRECATED));
        capabilities.add(mockCapability(INTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE, ATLASSIAN_ACCESS_INHERITED));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE));
        // external plugins shouldn't define `atlassian-access` but there is no point at forbidding it
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, INTERNAL_PACKAGE));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, INTERNAL_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));

        hook.filterMatches(requirement, capabilities);

        assertEquals(32, capabilities.size());
    }

    @Test
    public void packagesFromExternalPluginShouldNotBeFiltered() {
        DmzResolverHook hook = createTestDmzResolverHook(false);

        BundleRequirement requirement = mockRequirement(EXTERNAL_PLUGIN_KEY);

        Set<BundleCapability> capabilities = new HashSet<>();
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE));
        // external plugins shouldn't define `atlassian-access` but there is no point at forbidding it
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, INTERNAL_PACKAGE));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, INTERNAL_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE));
        capabilities.add(mockCapability(EXTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE, ATLASSIAN_ACCESS_INTERNAL));

        hook.filterMatches(requirement, capabilities);

        assertEquals(8, capabilities.size());
    }

    @Test
    public void internalPackagesFromSystemBundleShouldBeFiltered() {
        DmzResolverHook hook = createTestDmzResolverHook(false);

        BundleRequirement requirement = mockRequirement(EXTERNAL_PLUGIN_KEY);

        BundleCapability unlistedIsInternalByDefault = mockCapability(SYSTEM_BUNDLE_KEY, UNLISTED_PACKAGE);
        BundleCapability matchesPublicPattern = mockCapability(SYSTEM_BUNDLE_KEY, PUBLIC_PACKAGE);
        BundleCapability matchesPublicExcludePattern = mockCapability(SYSTEM_BUNDLE_KEY, INTERNAL_PACKAGE);
        BundleCapability matchesDeprecatedPattern = mockCapability(SYSTEM_BUNDLE_KEY, DEPRECATED_PACKAGE);

        Set<BundleCapability> capabilities = new HashSet<>();
        capabilities.add(unlistedIsInternalByDefault);
        capabilities.add(matchesPublicPattern);
        capabilities.add(matchesPublicExcludePattern);
        capabilities.add(matchesDeprecatedPattern);

        hook.filterMatches(requirement, capabilities);

        assertEquals(1, capabilities.size());
        assertTrue(capabilities.contains(matchesPublicPattern));
    }

    @Test
    public void deprecatedPackagesFromSystemBundleCanBeTreatedAsPublic() {
        DmzResolverHook hook = createTestDmzResolverHook(true);

        BundleRequirement requirement = mockRequirement(EXTERNAL_PLUGIN_KEY);

        BundleCapability unlistedIsInternalByDefault = mockCapability(SYSTEM_BUNDLE_KEY, UNLISTED_PACKAGE);
        BundleCapability matchesPublicPattern = mockCapability(SYSTEM_BUNDLE_KEY, PUBLIC_PACKAGE);
        BundleCapability matchesPublicExcludePattern = mockCapability(SYSTEM_BUNDLE_KEY, INTERNAL_PACKAGE);
        BundleCapability matchesDeprecatedPattern = mockCapability(SYSTEM_BUNDLE_KEY, DEPRECATED_PACKAGE);

        Set<BundleCapability> capabilities = new HashSet<>();
        capabilities.add(unlistedIsInternalByDefault);
        capabilities.add(matchesPublicPattern);
        capabilities.add(matchesPublicExcludePattern);
        capabilities.add(matchesDeprecatedPattern);

        hook.filterMatches(requirement, capabilities);

        assertEquals(2, capabilities.size());
        assertTrue(capabilities.contains(matchesPublicPattern));
        assertTrue(capabilities.contains(matchesDeprecatedPattern));
    }

    @Test
    public void internalPackagesFromInternalPluginShouldBeFiltered() {
        DmzResolverHook hook = createTestDmzResolverHook(false);

        BundleRequirement requirement = mockRequirement(EXTERNAL_PLUGIN_KEY);

        BundleCapability publicExport = mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_PUBLIC);
        BundleCapability internalExport =
                mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_INTERNAL);
        BundleCapability deprecatedExport =
                mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_DEPRECATED);
        BundleCapability inheritedPublicExport =
                mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_INHERITED);
        BundleCapability inheritedInternalExport =
                mockCapability(INTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE, ATLASSIAN_ACCESS_INHERITED);
        BundleCapability inheritedDeprecatedExport =
                mockCapability(INTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE, ATLASSIAN_ACCESS_INHERITED);
        BundleCapability defaultInheritedPublicExport = mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE);
        BundleCapability defaultInheritedInternalExport = mockCapability(INTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE);
        BundleCapability defaultInheritedDeprecatedExport = mockCapability(INTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE);

        Set<BundleCapability> capabilities = new HashSet<>();
        capabilities.add(publicExport);
        capabilities.add(internalExport);
        capabilities.add(deprecatedExport);
        capabilities.add(inheritedPublicExport);
        capabilities.add(inheritedInternalExport);
        capabilities.add(inheritedDeprecatedExport);
        capabilities.add(defaultInheritedPublicExport);
        capabilities.add(defaultInheritedInternalExport);
        capabilities.add(defaultInheritedDeprecatedExport);

        hook.filterMatches(requirement, capabilities);

        Set<BundleCapability> publicCapabilities = new HashSet<>();
        publicCapabilities.add(publicExport);
        publicCapabilities.add(inheritedPublicExport);
        publicCapabilities.add(defaultInheritedPublicExport);

        assertEquals(publicCapabilities.size(), capabilities.size());
        for (BundleCapability publicCapability : publicCapabilities) {
            assertTrue(capabilities.contains(publicCapability));
        }
    }

    @Test
    public void deprecatedPackagesFromInternalPluginCanBeTreatedAsPublic() {
        DmzResolverHook hook = createTestDmzResolverHook(true);

        BundleRequirement requirement = mockRequirement(EXTERNAL_PLUGIN_KEY);

        BundleCapability publicExport = mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_PUBLIC);
        BundleCapability internalExport =
                mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_INTERNAL);
        BundleCapability deprecatedExport =
                mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_DEPRECATED);
        BundleCapability inheritedPublicExport =
                mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE, ATLASSIAN_ACCESS_INHERITED);
        BundleCapability inheritedInternalExport =
                mockCapability(INTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE, ATLASSIAN_ACCESS_INHERITED);
        BundleCapability inheritedDeprecatedExport =
                mockCapability(INTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE, ATLASSIAN_ACCESS_INHERITED);
        BundleCapability defaultInheritedPublicExport = mockCapability(INTERNAL_PLUGIN_KEY, PUBLIC_PACKAGE);
        BundleCapability defaultInheritedInternalExport = mockCapability(INTERNAL_PLUGIN_KEY, UNLISTED_PACKAGE);
        BundleCapability defaultInheritedDeprecatedExport = mockCapability(INTERNAL_PLUGIN_KEY, DEPRECATED_PACKAGE);

        Set<BundleCapability> capabilities = new HashSet<>();
        capabilities.add(publicExport);
        capabilities.add(internalExport);
        capabilities.add(deprecatedExport);
        capabilities.add(inheritedPublicExport);
        capabilities.add(inheritedInternalExport);
        capabilities.add(inheritedDeprecatedExport);
        capabilities.add(defaultInheritedPublicExport);
        capabilities.add(defaultInheritedInternalExport);
        capabilities.add(defaultInheritedDeprecatedExport);

        hook.filterMatches(requirement, capabilities);

        Set<BundleCapability> publicCapabilities = new HashSet<>();
        publicCapabilities.add(publicExport);
        publicCapabilities.add(deprecatedExport);
        publicCapabilities.add(inheritedPublicExport);
        publicCapabilities.add(inheritedDeprecatedExport);
        publicCapabilities.add(defaultInheritedPublicExport);
        publicCapabilities.add(defaultInheritedDeprecatedExport);

        assertEquals(publicCapabilities.size(), capabilities.size());
        for (BundleCapability publicCapability : publicCapabilities) {
            assertTrue(capabilities.contains(publicCapability));
        }
    }

    @Test
    public void testFilterMatchesForThirdPartyPlugin() {
        DmzResolverHook hook = createTestDmzResolverHook(false);

        BundleRequirement requirement = mockRequirement(EXTERNAL_PLUGIN_KEY);

        BundleCapability internalPackage = mockCapability(SYSTEM_BUNDLE_KEY, INTERNAL_PACKAGE);

        BundleCapability guava = mockCapability(EXTERNAL_PLUGIN_KEY, GUAVA_PACKAGE);
        BundleCapability nonString =
                mockCapability(SYSTEM_BUNDLE_KEY, getClass().getPackage()); // Attribute isn't a String
        BundleCapability noAttributes =
                mockCapability(SYSTEM_BUNDLE_KEY, Collections.emptyMap()); // Attribute isn't set
        BundleCapability publicPackage = mockCapability(SYSTEM_BUNDLE_KEY, PUBLIC_PACKAGE);

        // The capabilities that define String wiring packages aren't ordered alphabetically because there's no
        // guarantee the ResolverHook will be called with sorted candidates. This verifies the implementation
        // doesn't make assumptions that it can exit early
        Set<BundleCapability> candidates = new HashSet<>();
        candidates.add(publicPackage);
        candidates.add(guava);
        candidates.add(nonString);
        candidates.add(internalPackage);
        candidates.add(noAttributes);

        // before filtering
        assertEquals(5, candidates.size());

        hook.filterMatches(requirement, candidates);

        assertEquals(4, candidates.size());
        MatcherAssert.assertThat(candidates, not(hasItem(internalPackage)));
    }

    @Test
    public void testFilterResolvable() {
        DmzResolverHook hook = createTestDmzResolverHook(false);

        Collection<BundleRevision> candidates = mock(Collection.class);

        hook.filterResolvable(candidates);

        verifyNoMoreInteractions(candidates);
    }

    @Test
    public void testFilterSingletonCollisions() {
        DmzResolverHook hook = createTestDmzResolverHook(false);

        BundleCapability singleton = mock(BundleCapability.class);
        Collection<BundleCapability> collisions = mock(Collection.class);

        hook.filterSingletonCollisions(singleton, collisions);

        verifyNoMoreInteractions(singleton, collisions);
    }

    private static BundleCapability mockCapability(String pluginKey, Object wiringPackage) {
        return mockCapability(pluginKey, singletonMap(ATTR_WIRING_PACKAGE, wiringPackage));
    }

    private static BundleCapability mockCapability(
            String pluginKey, String exportPackage, String atlassianAccessValue) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put(ATTR_WIRING_PACKAGE, exportPackage);
        attributes.put(ATTR_ATLASSIAN_ACCESS, atlassianAccessValue);
        return mockCapability(pluginKey, attributes);
    }

    private static BundleCapability mockCapability(String pluginKey, Map<String, Object> map) {
        BundleCapability capability = mock(BundleCapability.class, RETURNS_DEEP_STUBS);
        when(capability.getAttributes()).thenReturn(map);

        Hashtable<String, String> headers = new Hashtable<>();
        headers.put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, pluginKey);
        when(capability.getRevision().getBundle().getHeaders()).thenReturn(headers);
        when(capability.getRevision().getBundle().getBundleId())
                .thenReturn(SYSTEM_BUNDLE_KEY.equals(pluginKey) ? 0L : 1L);

        return capability;
    }

    private static BundleRequirement mockRequirement(String pluginKey) {
        Hashtable<String, String> headers = new Hashtable<>();
        headers.put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, pluginKey);

        BundleRequirement requirement = mock(BundleRequirement.class, RETURNS_DEEP_STUBS);
        when(requirement.getRevision().getBundle().getHeaders()).thenReturn(headers);

        return requirement;
    }

    private DmzResolverHook createTestDmzResolverHook(boolean deprecatedAsPublic) {
        PluginTypeDetector pluginTypeDetector = new PluginTypeDetector(singleton(CUSTOM_INTERNAL_PLUGIN_KEY));
        DmzPackagePatterns dmzPackagePatterns = new DmzPackagePatterns(
                singleton(PUBLIC_PACKAGE_PATTERN),
                singleton(PUBLIC_PACKAGE_EXCLUDE_PATTERN),
                singleton(DEPRECATED_PACKAGE_PATTERN));
        InternalPackageDetector internalPackageDetector =
                new ExportTypeBasedInternalPackageDetector(dmzPackagePatterns, pluginTypeDetector);

        return new DmzResolverHook(pluginTypeDetector, internalPackageDetector, deprecatedAsPublic);
    }
}
