package com.atlassian.plugin.osgi.test;

import javax.servlet.http.HttpServlet;

/**
 * Used in {@link it.com.atlassian.plugin.osgi.TestPluginInstall}
 * does nothing at all.
 */
public class TestServlet extends HttpServlet {}
