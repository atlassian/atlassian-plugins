package com.atlassian.plugin.osgi.container.felix;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.osgi.framework.Version;
import com.google.common.collect.ImmutableMap;

import com.atlassian.plugin.util.ClassLoaderStack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestExportsBuilderUtils {

    @Test
    public void testParseExportWithVersions() throws IOException {
        Map<String, String> expected = ImmutableMap.of(
                "com.atlassian.dummy", "1.2.3",
                "com.atlassian.dummy.sub", "4.5.6",
                "com.atlassian.wahaha", "1.3.6.SNAPSHOT");

        runParsingTest(
                expected,
                " # comment",
                "com.atlassian.dummy  =1.2.3",
                "com.atlassian.dummy.sub=  4.5.6",
                "com.atlassian.wahaha= 1.3.6-SNAPSHOT");
    }

    @Test
    public void testParseExportNoVersions() throws IOException {
        Map<String, String> expected = ImmutableMap.of(
                "com.atlassian.dummy", Version.emptyVersion.toString(),
                "com.atlassian.dummy.sub", Version.emptyVersion.toString(),
                "com.atlassian.wahaha", Version.emptyVersion.toString());

        runParsingTest(
                expected,
                " # comment",
                "com.atlassian.dummy",
                "com.atlassian.dummy.sub       ",
                "      com.atlassian.wahaha");
    }

    @Test
    public void testConstructJdkExportsFromTestResource() {
        Map<String, String> content = ExportBuilderUtils.parseExportFile("jdk-packages.test.txt");
        Map<String, String> expected =
                ImmutableMap.of("foo.bar", Version.emptyVersion.toString(), "foo.baz", Version.emptyVersion.toString());

        assertEquals(expected, content);
    }

    @Test
    public void testConstructJdkExports() {
        Map<String, String> content = ExportBuilderUtils.parseExportFile(ExportsBuilder.JDK8_PACKAGES_PATH);
        assertTrue(content.containsKey("org.xml.sax"));
    }

    private String createExportFile(String[] lines) throws IOException {
        File file = File.createTempFile("TestExportsBuilderUtils", "txt");
        file.deleteOnExit();

        PrintStream ps = null;
        try {
            ps = new PrintStream(file);

            for (String line : lines) {
                ps.println(line);
            }
        } finally {
            IOUtils.closeQuietly(ps);
        }

        return file.getAbsolutePath();
    }

    private void runParsingTest(Map<String, String> expected, String... inputLines) throws IOException {
        String exportFile = createExportFile(inputLines);

        ClassLoaderStack.push(new MockedClassLoader(exportFile));
        Map<String, String> output;

        try {
            output = ExportBuilderUtils.parseExportFile(exportFile);
        } finally {
            ClassLoaderStack.pop();
        }

        assertEquals(expected, output);
    }

    static class MockedClassLoader extends ClassLoader {
        private String filePath;

        MockedClassLoader(String filePath) {
            super(null);
            this.filePath = filePath;
        }

        @Override
        public URL getResource(String name) {
            try {
                return new File(name).toURI().toURL();
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
