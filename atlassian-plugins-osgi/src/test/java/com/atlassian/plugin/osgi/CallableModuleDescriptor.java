package com.atlassian.plugin.osgi;

import java.util.concurrent.Callable;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

public class CallableModuleDescriptor extends AbstractModuleDescriptor<Callable> {
    public CallableModuleDescriptor(ModuleFactory moduleCreator) {
        super(moduleCreator);
    }

    @Override
    public Callable getModule() {
        return moduleFactory.createModule(moduleClassName, this);
    }
}
