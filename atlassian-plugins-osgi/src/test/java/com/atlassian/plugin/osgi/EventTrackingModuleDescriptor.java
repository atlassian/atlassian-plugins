package com.atlassian.plugin.osgi;

import java.util.concurrent.atomic.AtomicInteger;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

/**
 *
 */
public class EventTrackingModuleDescriptor extends AbstractModuleDescriptor<Void> {
    private final AtomicInteger enabledCount = new AtomicInteger();
    private final AtomicInteger disabledCount = new AtomicInteger();

    public EventTrackingModuleDescriptor() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    @Override
    public Void getModule() {
        return null;
    }

    @Override
    public void enabled() {
        super.enabled();
        enabledCount.incrementAndGet();
    }

    @Override
    public void disabled() {
        super.disabled();
        disabledCount.incrementAndGet();
    }

    public int getEnabledCount() {
        return enabledCount.get();
    }

    public int getDisabledCount() {
        return disabledCount.get();
    }
}
