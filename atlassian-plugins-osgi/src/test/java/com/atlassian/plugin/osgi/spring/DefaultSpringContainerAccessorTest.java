package com.atlassian.plugin.osgi.spring;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class DefaultSpringContainerAccessorTest {
    @Test
    public void resolvesAutowiringMode() {
        assertAutowiringMode(PublicClassWithNoConstructorDefined.class, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE);
        assertAutowiringMode(ClassWithZeroArgConstructorDefined.class, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE);
        assertAutowiringMode(ClassWithConstructorsDefined.class, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE);

        assertAutowiringMode(ClassWithNoDefaultConstructor.class, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR);
        assertAutowiringMode(
                PrivateClassWithNoConstructorDefined.class, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR);
        assertAutowiringMode(
                PublicClassWithNonPublicConstructor.class, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR);
    }

    private void assertAutowiringMode(Class<?> beanClass, int autowiringMode) {
        final AbstractBeanDefinition bd = new RootBeanDefinition(beanClass);
        bd.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_AUTODETECT);
        assertThat(bd.getResolvedAutowireMode(), equalTo(autowiringMode));

        assertThat(DefaultSpringContainerAccessor.resolveAutowiringMode(beanClass), equalTo(autowiringMode));
    }

    public static class PublicClassWithNonPublicConstructor {
        PublicClassWithNonPublicConstructor() {}
    }

    public static class PublicClassWithNoConstructorDefined {}

    private static class PrivateClassWithNoConstructorDefined {}

    private static class ClassWithZeroArgConstructorDefined {
        public ClassWithZeroArgConstructorDefined() {}
    }

    private static class ClassWithConstructorsDefined {
        public ClassWithConstructorsDefined() {}

        public ClassWithConstructorsDefined(int ignored) {}
    }

    private static class ClassWithNoDefaultConstructor {
        public ClassWithNoDefaultConstructor(int ignored) {}
    }
}
