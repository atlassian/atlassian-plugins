package com.atlassian.plugin.osgi.factory;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.plugin.IllegalPluginStateException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TestOsgiPluginDeinstalledHelper {
    private static final String PLUGIN_KEY = "plugin-key";

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    OsgiPluginDeinstalledHelper helper;

    @Before
    public void setUp() {
        helper = new OsgiPluginDeinstalledHelper(PLUGIN_KEY, false);
    }

    @Test
    public void loadClassThrowsExceptionWithClassAndGivenCallingClass() {
        final String loadedClass = "loadedClass";
        expectIllegalPluginStateException(loadedClass, "java.lang.String");
        helper.loadClass(loadedClass, String.class);
    }

    @Test
    public void loadClassThrowsExceptionWithClassAndNullCallingClass() {
        final String loadedClass = "loadedClass";
        expectIllegalPluginStateException(loadedClass, "null");
        helper.loadClass(loadedClass, null);
    }

    @Test
    public void installThrowsException() {
        expectIllegalPluginStateException();
        helper.install();
    }

    @Test
    public void isRemotePluginReturnsConstructorParameter() {
        assertThat(helper.isRemotePlugin(), is(false));
    }

    @Test
    public void getBundleThrowsException() {
        expectIllegalPluginStateException();
        helper.getBundle();
    }

    @Test
    public void getResource() {
        final String resourceName = "some.resource";
        expectIllegalPluginStateException(resourceName);
        helper.getResource(resourceName);
    }

    @Test
    public void getResourceAsStream() {
        final String resourceName = "some.resource";
        expectIllegalPluginStateException(resourceName);
        helper.getResourceAsStream(resourceName);
    }

    @Test
    public void getClassLoader() {
        expectIllegalPluginStateException();
        helper.getClassLoader();
    }

    @Test
    public void onEnable() {
        expectIllegalPluginStateException();
        helper.onEnable();
    }

    @Test
    public void onDisable() {
        expectIllegalPluginStateException();
        helper.onDisable();
    }

    @Test
    public void onUninstall() {
        expectIllegalPluginStateException();
        helper.onUninstall();
    }

    @Test
    public void getDependencies() {
        expectIllegalPluginStateException();
        helper.getDependencies();
    }

    @Test
    public void setPluginContainer() {
        expectIllegalPluginStateException();
        helper.setPluginContainer(new Object());
    }

    @Test
    public void getContainerAccessor() {
        expectIllegalPluginStateException();
        helper.getContainerAccessor();
    }

    @Test
    public void getRequiredContainerAccessor() {
        expectIllegalPluginStateException();
        helper.getRequiredContainerAccessor();
    }

    private void expectIllegalPluginStateException(final String... messages) {
        expectedException.expect(IllegalPluginStateException.class);
        expectedException.expectMessage(PLUGIN_KEY);
        for (final String message : messages) {
            expectedException.expectMessage(message);
        }
    }
}
