package com.atlassian.plugin.osgi.hook;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.osgi.framework.Version;
import org.osgi.framework.wiring.BundleCapability;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public final class TestUtils {

    public static final String ATTR_WIRING_PACKAGE = "osgi.wiring.package";

    private TestUtils() {}

    public static BundleCapability mockCapability(String wiringPackage) {
        Map<String, Object> map = Collections.singletonMap(ATTR_WIRING_PACKAGE, wiringPackage);
        BundleCapability capability = mock(BundleCapability.class, RETURNS_DEEP_STUBS);
        when(capability.getAttributes()).thenReturn(map);
        return capability;
    }

    public static BundleCapability mockCapabilityWithVersion(String wiringPackage, Version version) {
        Map<String, Object> map = new HashMap<>();
        map.put(ATTR_WIRING_PACKAGE, wiringPackage);
        map.put("version", version);
        BundleCapability capability = mock(BundleCapability.class, RETURNS_DEEP_STUBS);
        when(capability.getAttributes()).thenReturn(map);
        return capability;
    }
}
