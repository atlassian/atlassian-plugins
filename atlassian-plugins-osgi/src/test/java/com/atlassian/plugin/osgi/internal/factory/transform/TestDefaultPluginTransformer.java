package com.atlassian.plugin.osgi.internal.factory.transform;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.jaxen.SimpleNamespaceContext;
import org.jaxen.XPath;
import org.jaxen.dom4j.Dom4jXPath;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import com.google.common.collect.Sets;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.impl.DefaultOsgiPersistentCache;
import com.atlassian.plugin.osgi.factory.transform.Foo;
import com.atlassian.plugin.osgi.factory.transform.FooChild;
import com.atlassian.plugin.osgi.factory.transform.Fooable;
import com.atlassian.plugin.osgi.factory.transform.StubHostComponentRegistration;
import com.atlassian.plugin.osgi.factory.transform.model.SystemExports;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.osgi.hostcomponents.PropertyBuilder;
import com.atlassian.plugin.osgi.hostcomponents.impl.MockRegistration;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.atlassian.plugin.test.PluginTestUtils;

import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultPluginTransformer {

    private static final Map<String, String> BEAN_NAMESPACE =
            singletonMap("beans", "http://www.springframework.org/schema/beans");
    private static final Map<String, String> OSGI_NAMESPACE =
            singletonMap("osgi", "http://www.eclipse.org/gemini/blueprint/schema/blueprint");

    private static final String BEAN_XPATH = "//beans:bean";
    private static final String OSGI_REF_XPATH = "//osgi:reference";

    private DefaultPluginTransformer transformer;
    private File tmpDir;

    @Before
    public void setUp() throws Exception {
        OsgiContainerManager osgiContainerManager = mock(OsgiContainerManager.class);
        when(osgiContainerManager.getRegisteredServices()).thenReturn(new ServiceReference[0]);
        tmpDir = PluginTestUtils.createTempDirectory("plugin-transformer");
        transformer = new DefaultPluginTransformer(
                new DefaultOsgiPersistentCache(tmpDir),
                SystemExports.NONE,
                null,
                PluginAccessor.Descriptor.FILENAME,
                osgiContainerManager);
    }

    @After
    public void tearDown() {
        tmpDir = null;
        transformer = null;
    }

    @Test
    public void testAddFilesToZip() throws URISyntaxException, IOException {
        final File file = PluginTestUtils.getFileForResource("myapp-1.0-plugin.jar");

        final Map<String, byte[]> files = new HashMap<String, byte[]>() {
            {
                put("foo", "bar".getBytes());
            }
        };
        final File copy = transformer.addFilesToExistingZip(file, files);
        assertThat(copy, notNullValue());
        assertThat(copy.getName(), not(is(file.getName())));
        assertThat(copy.length(), not(is(file.length())));

        try (final ZipFile zip = new ZipFile(copy)) {
            final ZipEntry entry = zip.getEntry("foo");
            assertThat(entry, notNullValue());
        }
    }

    @Test
    public void testExistingFilesRetainTimestamps() throws URISyntaxException, IOException {
        // When a plugin is transformed, all existing files should retain the same
        // modified timestamp from the source jar
        // (also checks they share the same ordering)

        final File expectedSource = PluginTestUtils.getFileForResource("myapp-1.0-plugin.jar");
        final File actualSource = PluginTestUtils.getFileForResource("myapp-1.0-plugin.jar");
        final Map<String, byte[]> files = new HashMap<>();

        try (final ZipFile expectedZip = new ZipFile(expectedSource);
                final ZipFile actualZip = new ZipFile(transformer.addFilesToExistingZip(actualSource, files))) {
            Enumeration<? extends ZipEntry> expectedEntries = expectedZip.entries();
            Enumeration<? extends ZipEntry> actualEntries = actualZip.entries();

            while (expectedEntries.hasMoreElements()) {
                ZipEntry expected = expectedEntries.nextElement();
                ZipEntry actual = actualEntries.nextElement();
                assertThat(actual.getName(), is(expected.getName()));
                assertThat(actual.getTime(), is(expected.getTime()));
            }
        }
    }

    @Test
    public void testFilesAddedToZipAreInOrder() throws URISyntaxException, IOException {
        // Validate that any custom files added to the zip are present in
        // sorted order (sorted by filename asc)
        // We need this to keep the output jar deterministic

        final File file = PluginTestUtils.getFileForResource("myapp-1.0-plugin.jar");

        final Map<String, byte[]> files = new HashMap<String, byte[]>() {
            {
                put("testcode/ghi", "aaa".getBytes());
                put("testcode/abc", "aaa".getBytes());
                put("testcode/dir/def", "aaa".getBytes());
                put("testcode/dir/abc", "aaa".getBytes());
            }
        };

        List<String> expectedFileOrder =
                Arrays.asList("testcode/abc", "testcode/dir/abc", "testcode/dir/def", "testcode/ghi");
        List<String> actualFileOrder = new ArrayList<>();

        try (final ZipFile zip = new ZipFile(transformer.addFilesToExistingZip(file, files))) {
            Enumeration<? extends ZipEntry> entries = zip.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.getName().contains("testcode")) {
                    actualFileOrder.add(entry.getName());
                }
            }

            assertThat(actualFileOrder.size(), is(expectedFileOrder.size()));
            assertThat(actualFileOrder, is(expectedFileOrder));
        }
    }

    @Test
    public void testTransform() throws Exception {
        final File file = new PluginJarBuilder()
                .addFormattedJava(
                        "my.Foo",
                        "package my;",
                        "public class Foo {",
                        "  com.atlassian.plugin.osgi.factory.transform.Fooable bar;",
                        "}")
                .addPluginInformation("foo", "foo", "1.1")
                .build();

        final File copy =
                transformer.transform(new JarPluginArtifact(file), new ArrayList<HostComponentRegistration>() {
                    {
                        add(new StubHostComponentRegistration(Fooable.class));
                    }
                });

        assertThat(copy, notNullValue(File.class));
        assertThat(copy.getName().contains(String.valueOf(file.lastModified())), is(true));
        assertThat(copy.getName().endsWith(".jar"), is(true));
        assertThat(copy.getParentFile().getParentFile().getAbsolutePath(), is(tmpDir.getAbsolutePath()));

        try (JarFile jar = new JarFile(copy)) {
            final Attributes attrs = jar.getManifest().getMainAttributes();
            assertThat(attrs.getValue(Constants.BUNDLE_VERSION), is("1.1"));
            assertThat(jar.getEntry("META-INF/spring/atlassian-plugins-host-components.xml"), notNullValue());
        }
    }

    @Test
    public void testImportManifestGenerationOnInterfaces() throws Exception {
        final File innerJar = new PluginJarBuilder()
                .addFormattedJava(
                        "my.innerpackage.InnerPackageInterface1",
                        "package my.innerpackage;",
                        "public interface InnerPackageInterface1 {}")
                .build();

        final File pluginJar = new PluginJarBuilder()
                .addFormattedJava(
                        "my.MyFooChild",
                        "package my;",
                        "public class MyFooChild extends com.atlassian.plugin.osgi.factory.transform.dummypackage2.DummyClass2 {",
                        "}")
                .addFormattedJava("my2.MyFooInterface", "package my2;", "public interface MyFooInterface {}")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='plugin1' key='first' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='component1' class='my.MyFooChild' public='true'>",
                        "       <interface>com.atlassian.plugin.osgi.factory.transform.dummypackage0.DummyInterface0</interface>",
                        "       <interface>com.atlassian.plugin.osgi.factory.transform.dummypackage1.DummyInterface1</interface>",
                        "       <interface>my.innerpackage.InnerPackageInterface1</interface>",
                        "       <interface>my2.MyFooInterface</interface>",
                        "    </component>",
                        "</atlassian-plugin>")
                .addFile("META-INF/lib/mylib.jar", innerJar)
                .build();

        File outputFile = transformer.transform(new JarPluginArtifact(pluginJar), new ArrayList<>());

        String importString;
        try (JarFile outputJar = new JarFile(outputFile)) {
            importString = outputJar.getManifest().getMainAttributes().getValue(Constants.IMPORT_PACKAGE);
        }

        // this should be done by binary scanning.
        assertThat(importString, containsString("com.atlassian.plugin.osgi.factory.transform.dummypackage2"));

        // referred to by interface declaration.
        assertThat(importString, containsString("com.atlassian.plugin.osgi.factory.transform.dummypackage1"));

        // referred to by interface declaration
        assertThat(importString, containsString("com.atlassian.plugin.osgi.factory.transform.dummypackage0"));

        // should not import an interface which exists in plugin itself.
        assertThat(importString, not(containsString("my2.MyFooInterface")));

        // should not import an interface which exists in inner jar.
        assertThat(importString, not(containsString("my.innerpackage")));
    }

    @Test
    public void testGenerateCacheName() throws IOException {
        File tmp = File.createTempFile("asdf", ".jar", tmpDir);
        assertThat(DefaultPluginTransformer.generateCacheName(tmp), endsWith(".jar"));
        tmp = File.createTempFile("asdf", "asdf", tmpDir);
        assertThat(DefaultPluginTransformer.generateCacheName(tmp), endsWith(String.valueOf(tmp.lastModified())));

        tmp = File.createTempFile("asdf", "asdf.", tmpDir);
        assertThat(DefaultPluginTransformer.generateCacheName(tmp), endsWith(String.valueOf(tmp.lastModified())));

        tmp = File.createTempFile("asdf", "asdf.s", tmpDir);
        assertThat(DefaultPluginTransformer.generateCacheName(tmp), endsWith(".s"));
    }

    @Test
    public void testTransformComponentMustNotPerformKeyConversion() throws Exception {
        File outputJarFile = runTransform();

        assertBeanNames(
                outputJarFile,
                "META-INF/spring/atlassian-plugins-components.xml",
                BEAN_NAMESPACE,
                BEAN_XPATH,
                Sets.newHashSet("TESTING1", "testing2", "testing3"));
    }

    @Test
    public void testBeansListedInAppearanceOrder() throws Exception {
        // These beans should appear in the order they appear in atlassian-plugin.xml

        File outputJarFile = runTransform();

        List<String> actual =
                runXpath(outputJarFile, "META-INF/spring/atlassian-plugins-components.xml", BEAN_NAMESPACE, BEAN_XPATH);
        List<String> expected = Arrays.asList("testing3", "TESTING1", "testing2");
        assertThat(actual, is(expected));
    }

    @Test
    public void testTransformImportMustNotPerformKeyConversion() throws Exception {
        File outputJarFile = runTransform();

        assertBeanNames(
                outputJarFile,
                "META-INF/spring/atlassian-plugins-component-imports.xml",
                OSGI_NAMESPACE,
                OSGI_REF_XPATH,
                Sets.newHashSet("TESTING3", "testing4"));
    }

    @Test
    public void testTransformHostComponentMustNotPerformKeyConversion() throws Exception {
        File outputJarFile = runTransform();

        assertBeanNames(
                outputJarFile,
                "META-INF/spring/atlassian-plugins-host-components.xml",
                BEAN_NAMESPACE,
                BEAN_XPATH,
                Sets.newHashSet("TESTING5", "testing6", "testing7"));
    }

    @Test
    public void testHostComponentsMustBeSorted() throws Exception {
        // Host components can be activated in any order, so should be sorted in output

        File outputJarFile = runTransform();

        List<String> actual = runXpath(
                outputJarFile, "META-INF/spring/atlassian-plugins-host-components.xml", BEAN_NAMESPACE, BEAN_XPATH);
        List<String> expected = Arrays.asList("TESTING5", "testing6", "testing7");
        assertThat(actual, is(expected));
    }

    @Test
    public void testDefaultTransformedJarIsNotCompressed() throws Exception {
        // This test might be a bit brittle, but i think it's worth persisting with to see how it
        // pans out. If you're here wondering why it failed, keep this in mind.
        final File transformedFile = runTransform();
        for (final JarEntry jarEntry : JarUtils.getEntries(transformedFile)) {
            // It turns out uncompressed jar entries are actually still stored with method
            // ZipEntry.STORED, so we can't check jarEntry.getMethod(). Also, you do need >= in the
            // size check, because uncompressed things grow a little.
            assertThat(jarEntry.getCompressedSize(), greaterThanOrEqualTo(jarEntry.getSize()));
        }
    }

    @Test
    public void testBestSpeedCompressionYieldsCompressedTransformedJar() throws Exception {
        compressionOptionYieldsCompressedTransformedJar(Deflater.BEST_SPEED);
    }

    @Test
    public void testBestSizeCompressionYieldsCompressedTransformedJar() throws Exception {
        compressionOptionYieldsCompressedTransformedJar(Deflater.BEST_COMPRESSION);
    }

    @Test
    public void testMultipleTransformsGenerateSameOutput() throws Exception {
        // Multiple transforms of the same plugin should generate exactly the same output file contents

        File outputA = runTransform();
        File outputB = runTransform();

        // Both files should be the same length
        assertThat(outputA.length(), is(outputB.length()));

        byte[] bytesA = Files.readAllBytes(Paths.get(outputA.getAbsolutePath()));
        byte[] bytesB = Files.readAllBytes(Paths.get(outputB.getAbsolutePath()));

        // Both files should have the same contents
        assertThat(bytesA, is(bytesB));
    }

    private void compressionOptionYieldsCompressedTransformedJar(final int level) throws Exception {
        // This test is slightly brittle, because it assumes a given file is improved by
        // compression. However, an xml file is a good guess, and works with a reasonable margin.

        System.setProperty(DefaultPluginTransformer.TRANSFORM_COMPRESSION_LEVEL, Integer.toString(level));

        final JarEntry jarEntry = JarUtils.getEntry(runTransform(), PluginAccessor.Descriptor.FILENAME);
        // It turns out uncompressed jar entries are actually still stored with method
        // ZipEntry.STORED, so there's no value checking jarEntry.getMethod().
        assertThat(jarEntry.getCompressedSize(), lessThan(jarEntry.getSize()));

        System.clearProperty(DefaultPluginTransformer.TRANSFORM_COMPRESSION_LEVEL);
    }

    private File runTransform() throws Exception {
        final File file = new PluginJarBuilder()
                .addFormattedJava(
                        "my.Foo",
                        "package my;",
                        "import com.atlassian.plugin.osgi.factory.transform.Fooable;",
                        "import com.atlassian.plugin.osgi.factory.transform.FooChild;",
                        "public class Foo {",
                        "  private Fooable bar;",
                        "  public Foo(Fooable bar, FooChild child) { this.bar = bar;} ",
                        "}")
                .addFormattedJava(
                        "com.atlassian.plugin.osgi.SomeInterface",
                        "package com.atlassian.plugin.osgi;",
                        "public interface SomeInterface {}")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='plugin1' key='first' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "   <component key='testing3' class='my.Foo'/>",
                        "   <component key='TESTING1' class='my.Foo'/>",
                        "   <component key='testing2' class='my.Foo'/>",
                        "   <component-import key='TESTING3'>",
                        "       <interface>com.atlassian.plugin.osgi.SomeInterface</interface>",
                        "   </component-import>",
                        "   <component-import key='testing4'>",
                        "       <interface>com.atlassian.plugin.osgi.SomeInterface</interface>",
                        "   </component-import>",
                        "</atlassian-plugin>")
                .build();

        MockRegistration mockReg1 = new MockRegistration(new FooChild(), FooChild.class);
        mockReg1.getProperties().put(PropertyBuilder.BEAN_NAME, "testing7");
        MockRegistration mockReg2 = new MockRegistration(new Foo(), Fooable.class);
        mockReg2.getProperties().put(PropertyBuilder.BEAN_NAME, "TESTING5");
        MockRegistration mockReg3 = new MockRegistration(new FooChild(), FooChild.class);
        mockReg3.getProperties().put(PropertyBuilder.BEAN_NAME, "testing6");

        return transformer.transform(new JarPluginArtifact(file), Arrays.asList(mockReg1, mockReg2, mockReg3));
    }

    private List<String> runXpath(
            final File outputJarFile,
            final String springFileLocation,
            final Map<String, String> namespaces,
            final String xpathQuery)
            throws Exception {

        try (final JarFile jarFile = new JarFile(outputJarFile)) {
            final InputStream inputStream = jarFile.getInputStream(jarFile.getEntry(springFileLocation));
            final SAXReader saxReader = new SAXReader();
            final Document document = saxReader.read(inputStream);
            final XPath xpath = new Dom4jXPath(xpathQuery);
            xpath.setNamespaceContext(new SimpleNamespaceContext(namespaces));
            final List<?> list = xpath.selectNodes(document);
            return list.stream()
                    .filter(Element.class::isInstance)
                    .map(Element.class::cast)
                    .map(el -> el.attribute("id"))
                    .map(Attribute::getValue)
                    .collect(toList());
        }
    }

    private void assertBeanNames(
            File outputJarFile,
            String springFileLocation,
            Map<String, String> namespaces,
            String xpathQuery,
            Set<String> expectedIds)
            throws Exception {
        Set<String> foundBeans = new HashSet<>(runXpath(outputJarFile, springFileLocation, namespaces, xpathQuery));
        assertThat(foundBeans, is(expectedIds));
    }
}
