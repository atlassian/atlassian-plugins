package com.atlassian.plugin.osgi.internal.factory.transform.model;

import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.junit.Test;

import com.atlassian.plugin.PluginParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public class TestComponentImport {

    @Test
    public void testValidate() {
        Element e = DocumentFactory.getInstance().createElement("component-import");
        e.addAttribute("key", " foo ");
        e.addAttribute("interface", " foo.Bar ");
        e.addAttribute("filter", " (bleh=blargh) ");

        ComponentImport ci = new ComponentImport(e);
        assertEquals("foo", ci.getKey());
        assertEquals("foo.Bar", ci.getInterfaces().iterator().next());
        assertEquals("(bleh=blargh)", ci.getFilter());

        e.remove(e.attribute("filter"));
        ci = new ComponentImport(e);
        assertNull(ci.getFilter());

        try {
            e.remove(e.attribute("interface"));
            new ComponentImport(e);
            fail();
        } catch (PluginParseException ex) {
            // test passed
        }

        Element inf = DocumentFactory.getInstance().createElement("interface");
        e.add(inf);
        try {
            new ComponentImport(e);
            fail();
        } catch (PluginParseException ex) {
            // test passed
        }

        inf.setText("foo.Bar");
        ci = new ComponentImport(e);
        assertEquals("foo.Bar", ci.getInterfaces().iterator().next());
    }
}
