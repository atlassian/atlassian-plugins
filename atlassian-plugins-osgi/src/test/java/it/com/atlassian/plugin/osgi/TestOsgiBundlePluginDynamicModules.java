package it.com.atlassian.plugin.osgi;

import org.dom4j.Element;
import org.dom4j.dom.DOMElement;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Tests for non-support of dynamic modules for OsgiBundlePlugin.
 *
 * Tests wiring from DefaultPluginManager <-> ScanningPluginLoader <-> OsgiBundleFactory
 */
public class TestOsgiBundlePluginDynamicModules extends AbstractTestDynamicModules {
    private static final String BUNDLE_SYMBOLIC_NAME = "bundleSymbolicName";
    private static final String BUNDLE_VERSION = "1.0";

    private static final String PLUGIN_NAME = "pluginName";
    private static final String PLUGIN_KEY = BUNDLE_SYMBOLIC_NAME + "-" + BUNDLE_VERSION;

    private static final String MODULE_KEY = "moduleKey";
    private static final String MODULE_CLASS = "moduleClass";

    @Override
    public void setUp() throws Exception {
        super.setUp();

        new PluginJarBuilder(PLUGIN_NAME)
                .addFormattedResource(
                        "META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Bundle-SymbolicName: " + BUNDLE_SYMBOLIC_NAME,
                        "Bundle-Version: " + BUNDLE_VERSION)
                .addJava(MODULE_CLASS, "public class " + MODULE_CLASS + " {}")
                .build(pluginsDir);
        initPluginManager();

        plugin = ((PluginInternal) pluginAccessor.getPlugin(PLUGIN_KEY));
        assertThat("could not retrieve the newly created plugin from pluginManager", plugin, notNullValue());

        assertThat(plugin.getKey(), is(PLUGIN_KEY));
    }

    @Override
    ModuleDescriptor addModule() {
        // add a component which uses a class in the plugin
        final Element e = new DOMElement("component");
        e.addAttribute("key", MODULE_KEY);
        e.addAttribute("class", MODULE_CLASS);
        return pluginController.addDynamicModule(plugin, new Dom4jDelegatingElement(e));
    }

    @Override
    String getPluginKey() {
        return PLUGIN_KEY;
    }

    @Override
    String getModuleKey() {
        return MODULE_KEY;
    }
}
