package it.com.atlassian.plugin.osgi;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.MetricTag;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TestPluginStateChangeCounter extends PluginInContainerTestBase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private static final String PLUGIN_KEY = "count-emitter";

    @Mock
    private MetricStrategy metricStrategy;

    @Captor
    private ArgumentCaptor<MetricKey> metricKeyCaptor;

    @Captor
    private ArgumentCaptor<Long> deltaValueCaptor;

    private File pluginJar;

    @Before
    public void setup() throws Exception {
        StrategiesRegistry.addMetricStrategy(metricStrategy);
        pluginJar = new PluginJarBuilder("emit-plugin-disable-enable")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='" + PLUGIN_KEY + "' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Export-Package>my</Export-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
        initPluginManager(null);
        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
    }

    @Test
    public void testDisableAndEnablePluginEmitsPluginCounterMetrics() {
        // need to disable first to enable
        pluginController.disablePlugin(PLUGIN_KEY);
        pluginController.enablePlugins(PLUGIN_KEY);

        // invocations: install, enable, disable
        verify(metricStrategy, times(3)).incrementCounter(metricKeyCaptor.capture(), deltaValueCaptor.capture());
        List<MetricKey> metricKeys = metricKeyCaptor.getAllValues();
        List<Long> values = deltaValueCaptor.getAllValues();

        verifyCorrectMetricIsEmitted(metricKeys.get(1), "plugin.disabled.counter");
        verifyCorrectMetricIsEmitted(metricKeys.get(2), "plugin.enabled.counter");
        assertTrue(values.stream().allMatch(value -> value.equals(1L)));
    }

    private void verifyCorrectMetricIsEmitted(MetricKey metricKey, String expectedMetricName) {
        assertEquals(expectedMetricName, metricKey.getMetricName());
        assertEquals(1, metricKey.getTags().size());
        assertTrue(metricKey.getTags().contains(MetricTag.of("atl-analytics", true)));
    }
}
