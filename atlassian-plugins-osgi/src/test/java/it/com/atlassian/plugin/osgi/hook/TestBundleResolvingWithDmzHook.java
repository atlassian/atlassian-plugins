package it.com.atlassian.plugin.osgi.hook;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.Sets;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.container.impl.DefaultPackageScannerConfiguration;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class TestBundleResolvingWithDmzHook extends PluginInContainerTestBase {

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        PackageScannerConfiguration scannerConfig = new DefaultPackageScannerConfiguration();
        scannerConfig.getOsgiPublicPackages().clear();
        // default jdk packages
        scannerConfig.getOsgiPublicPackages().addAll(Sets.newHashSet("com.*", "org.*", "sun.*", "javax.*"));
        scannerConfig.getOsgiPublicPackages().add("public.api.*");
        scannerConfig.getOsgiPublicPackagesExcludes().clear();
        scannerConfig.getOsgiPublicPackagesExcludes().add("public.api.exclude.*");
        scannerConfig.getApplicationBundledInternalPlugins().clear();
        scannerConfig.getApplicationBundledInternalPlugins().add("atlassian.bundled.plugin");
        setPackageScannerConfiguration(scannerConfig);
        initPluginManager();
    }

    @Test
    public void testInternalPluginHasAccessToAllExportedPackages() throws Exception {
        File internalPluginJar = new PluginJarBuilder("internal-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='internal' key='com.atlassian.internal' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>",
                        "            atlassian.internal,",
                        "            public.api,",
                        "            atlassian.access.public,",
                        "            atlassian.access.deprecated,",
                        "            atlassian.access.internal,",
                        "            atlassian.access.inherited,",
                        "            public.api.inherited,",
                        "            public.api.exclude.inherited,",
                        "            public.api.exclude",
                        "        </Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
        File internalSupplierJar = getAtlassianInternalSupplierJar();

        pluginController.installPlugins(new JarPluginArtifact(internalSupplierJar));
        pluginController.installPlugins(new JarPluginArtifact(internalPluginJar));

        Plugin internalPlugin = pluginAccessor.getPlugin("com.atlassian.internal");
        assertEquals(PluginState.ENABLED, internalPlugin.getPluginState());
    }

    @Test
    public void testExternalPluginHasAccessToPublicApi() throws Exception {
        File externalPluginJar = new PluginJarBuilder("external-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='external' key='com.example.external' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>",
                        "            public.api,",
                        "            atlassian.access.public,",
                        "            atlassian.access.deprecated,",
                        "            public.api.inherited,",
                        "        </Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
        File publicApiSupplierJar = getAtlassianInternalSupplierJar();

        pluginController.installPlugins(new JarPluginArtifact(publicApiSupplierJar));
        pluginController.installPlugins(new JarPluginArtifact(externalPluginJar));

        Plugin internalPlugin = pluginAccessor.getPlugin("com.example.external");
        assertEquals(PluginState.ENABLED, internalPlugin.getPluginState());
    }

    @Test
    public void testExternalPluginHasNoAccessToImplicitlyInheritedInternalPackage() throws Exception {
        File externalPluginJar = new PluginJarBuilder("external-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='external' key='com.example.external' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>atlassian.internal</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
        File internalSupplierJar = getAtlassianInternalSupplierJar();

        pluginController.installPlugins(new JarPluginArtifact(internalSupplierJar));
        pluginController.installPlugins(new JarPluginArtifact(externalPluginJar));

        Plugin externalPlugin = pluginAccessor.getPlugin("com.example.external");
        assertNotSame(PluginState.ENABLED, externalPlugin.getPluginState());
    }

    @Test
    public void testExternalPluginHasNoAccessToInternalPackage() throws Exception {
        File externalPluginJar = new PluginJarBuilder("external-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='external' key='com.example.external' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>atlassian.access.internal</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
        File publicApiExcludeSupplierJar = getAtlassianInternalSupplierJar();

        pluginController.installPlugins(new JarPluginArtifact(publicApiExcludeSupplierJar));
        pluginController.installPlugins(new JarPluginArtifact(externalPluginJar));

        Plugin externalPlugin = pluginAccessor.getPlugin("com.example.external");
        assertNotSame(PluginState.ENABLED, externalPlugin.getPluginState());
    }

    @Test
    public void testExternalPluginHasNoAccessToExplicitlyInheritedInternalPackage() throws Exception {
        File externalPluginJar = new PluginJarBuilder("external-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='external' key='com.example.external' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>atlassian.access.inherited</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
        File publicApiExcludeSupplierJar = getAtlassianInternalSupplierJar();

        pluginController.installPlugins(new JarPluginArtifact(publicApiExcludeSupplierJar));
        pluginController.installPlugins(new JarPluginArtifact(externalPluginJar));

        Plugin externalPlugin = pluginAccessor.getPlugin("com.example.external");
        assertNotSame(PluginState.ENABLED, externalPlugin.getPluginState());
    }

    @Test
    public void testExternalPluginHasNoAccessToExplicitlyInheritedExcludedPackage() throws Exception {
        File externalPluginJar = new PluginJarBuilder("external-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='external' key='com.example.external' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>public.api.exclude.inherited</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
        File publicApiExcludeSupplierJar = getAtlassianInternalSupplierJar();

        pluginController.installPlugins(new JarPluginArtifact(publicApiExcludeSupplierJar));
        pluginController.installPlugins(new JarPluginArtifact(externalPluginJar));

        Plugin externalPlugin = pluginAccessor.getPlugin("com.example.external");
        assertNotSame(PluginState.ENABLED, externalPlugin.getPluginState());
    }

    @Test
    public void testExternalPluginHasNoAccessToImplicitlyInheritedExcludedPackage() throws Exception {
        File externalPluginJar = new PluginJarBuilder("external-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='external' key='com.example.external' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>public.api.exclude</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
        File publicApiExcludeSupplierJar = getAtlassianInternalSupplierJar();

        pluginController.installPlugins(new JarPluginArtifact(publicApiExcludeSupplierJar));
        pluginController.installPlugins(new JarPluginArtifact(externalPluginJar));

        Plugin externalPlugin = pluginAccessor.getPlugin("com.example.external");
        assertNotSame(PluginState.ENABLED, externalPlugin.getPluginState());
    }

    @Test
    public void testExternalPluginImportingAtlassianBundledPluginPublicApiSuccessfully() throws Exception {
        File externalPluginJar = new PluginJarBuilder("external-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='external' key='com.example.external' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>public.api</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
        File PublicApiAtlassianBundledSupplierJar = getAtlassianBundledSupplierJar();

        pluginController.installPlugins(new JarPluginArtifact(PublicApiAtlassianBundledSupplierJar));
        pluginController.installPlugins(new JarPluginArtifact(externalPluginJar));

        Plugin externalPlugin = pluginAccessor.getPlugin("com.example.external");
        assertEquals(PluginState.ENABLED, externalPlugin.getPluginState());
    }

    @Test
    public void testExternalPluginImportingAtlassianBundledPluginInternalApiAndFailingToEnable() throws Exception {
        File externalPluginJar = new PluginJarBuilder("external-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='external' key='com.example.external' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>atlassian.internal</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();

        File InternalApiAtlassianBundledSupplierJar = getAtlassianBundledSupplierJar();

        pluginController.installPlugins(new JarPluginArtifact(InternalApiAtlassianBundledSupplierJar));
        pluginController.installPlugins(new JarPluginArtifact(externalPluginJar));

        Plugin externalPlugin = pluginAccessor.getPlugin("com.example.external");
        assertNotSame(PluginState.ENABLED, externalPlugin.getPluginState());
    }

    /**
     * The plugin exports a lot of packages.
     * Public packages are:
     * <ul>
     *     <li>public.api - based on implicit inheritance</li>
     *     <li>atlassian.access.public - based on the `public` atlassian-access property</li>
     *     <li>atlassian.access.deprecated - based on the `deprecated` atlassian-access property</li>
     *     <li>public.api.inherited - based on the explicit inheritance (`inherited` atlassian-access property)</li>
     * </ul>
     * Internal packages are:
     * <ul>
     *     <li>atlassian.internal - based on implicit inheritance</li>
     *     <li>atlassian.access.internal - based on the `internal` atlassian-access property</li>
     *     <li>atlassian.access.inherited - based on the explicit inheritance (`inherited` atlassian-access property)</li>
     *     <li>public.api.exclude.inherited - based on the explicit inheritance and exclusion mechanism</li>
     *     <li>public.api.exclude - based on the implicit inheritance and exclusion mechanism</li>
     * </ul>
     * @return
     * @throws IOException
     */
    private static File getAtlassianInternalSupplierJar() throws IOException {
        return new PluginJarBuilder("atlassian-internal-supplier-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='supplier' key='com.atlassian.supplier' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Export-Package>",
                        "            atlassian.internal,",
                        "            public.api,",
                        "            atlassian.access.public;atlassian-access='public',",
                        "            atlassian.access.deprecated;atlassian-access='deprecated',",
                        "            atlassian.access.internal;atlassian-access='internal',",
                        "            atlassian.access.inherited;atlassian-access='inherited',",
                        "            public.api.inherited;atlassian-access='inherited',",
                        "            public.api.exclude.inherited;atlassian-access='inherited',",
                        "            public.api.exclude",
                        "        </Export-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
    }

    private static File getAtlassianBundledSupplierJar() throws IOException {
        return new PluginJarBuilder("atlassian-bundled-supplier-plugin")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='supplier' key='atlassian.bundled.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Export-Package>",
                        "            public.api,",
                        "            atlassian.internal",
                        "        </Export-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build();
    }
}
