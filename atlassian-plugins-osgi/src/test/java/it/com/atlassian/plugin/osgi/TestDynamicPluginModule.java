package it.com.atlassian.plugin.osgi;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import io.atlassian.fugue.Pair;

import my.FooModule;
import my.FooModuleDescriptor;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.osgi.BasicWaitCondition;
import com.atlassian.plugin.osgi.DummyModuleDescriptorWithKey;
import com.atlassian.plugin.osgi.DummyStateAwareModuleDescriptorWithKey;
import com.atlassian.plugin.osgi.EventTrackingModuleDescriptor;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.atlassian.plugin.util.WaitUntil;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

public class TestDynamicPluginModule extends PluginInContainerTestBase {

    @Test
    public void testDynamicPluginModule() throws Exception {
        initPluginManager(registrar -> {});

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='factory' class='foo.MyModuleDescriptorFactory' public='true'>",
                        "       <interface>com.atlassian.plugin.ModuleDescriptorFactory</interface>",
                        "    </component>",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .addFormattedJava(
                        "foo.MyModuleDescriptorFactory",
                        "package foo;",
                        "public class MyModuleDescriptorFactory extends com.atlassian.plugin.DefaultModuleDescriptorFactory {",
                        "  public MyModuleDescriptorFactory() {",
                        "    super(new com.atlassian.plugin.hostcontainer.DefaultHostContainer());",
                        "    addModuleDescriptor('foo', MyModuleDescriptor.class);",
                        "  }",
                        "}")
                .build();
        final File pluginJar2 = buildDynamicModuleClientJar();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        pluginController.installPlugins(new JarPluginArtifact(pluginJar2));
        final Collection<ModuleDescriptor<?>> descriptors =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(1, descriptors.size());
        final ModuleDescriptor<?> descriptor = descriptors.iterator().next();
        assertEquals("MyModuleDescriptor", descriptor.getClass().getSimpleName());
    }

    @Test
    public void testDynamicPluginModuleUsingModuleTypeDescriptorWithReinstall() throws Exception {
        initPluginManager();

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build();
        final File pluginJar2 = buildDynamicModuleClientJar();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        pluginController.installPlugins(new JarPluginArtifact(pluginJar2));
        assertTrue(waitForDynamicModuleEnabled());

        // uninstall the module - the test plugin modules should revert back to Unrecognised
        pluginController.uninstall(pluginAccessor.getPlugin("com.atlassian.test.plugin.module"));
        WaitUntil.invoke(new BasicWaitCondition() {
            public boolean isFinished() {
                ModuleDescriptor<?> descriptor = pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptors()
                        .iterator()
                        .next();
                boolean enabled = pluginAccessor.isPluginModuleEnabled(descriptor.getCompleteKey());
                return descriptor.getClass().getSimpleName().equals("UnrecognisedModuleDescriptor") && !enabled;
            }
        });
        // reinstall the module - the test plugin modules should be correct again
        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        assertTrue(waitForDynamicModuleEnabled());
    }

    @Test
    public void testDynamicPluginModuleUsingModuleTypeDescriptorWithImmediateReinstall() throws Exception {
        initPluginManager();

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build();
        final File pluginJar2 = buildDynamicModuleClientJar();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        pluginController.installPlugins(new JarPluginArtifact(pluginJar2));
        assertTrue(waitForDynamicModuleEnabled());

        PluginModuleDisabledListener disabledListener = new PluginModuleDisabledListener("dum2");
        PluginModuleEnabledListener enabledListener = new PluginModuleEnabledListener("dum2");
        pluginEventManager.register(disabledListener);
        pluginEventManager.register(enabledListener);

        // reinstall the module - the test plugin modules should be correct again
        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        assertTrue(waitForDynamicModuleEnabled());

        assertEquals(1, enabledListener.called);
        assertEquals(1, disabledListener.called);
    }

    @Test
    public void testDynamicPluginModuleUsingModuleTypeDescriptorWithImmediateReinstallOfBoth() throws Exception {
        initPluginManager();

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build();
        final File pluginJar2 = buildDynamicModuleClientJar();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        pluginController.installPlugins(new JarPluginArtifact(pluginJar2));

        assertTrue(waitForDynamicModuleEnabled());

        PluginModuleDisabledListener disabledListener = new PluginModuleDisabledListener("dum2");
        PluginModuleEnabledListener enabledListener = new PluginModuleEnabledListener("dum2");
        pluginEventManager.register(disabledListener);
        pluginEventManager.register(enabledListener);

        // reinstall the module - the test plugin modules should be correct again
        pluginController.installPlugins(new JarPluginArtifact(pluginJar2), new JarPluginArtifact(pluginJar));
        assertTrue(waitForDynamicModuleEnabled());

        assertEquals(
                pluginAccessor.getPluginModule("com.atlassian.test.plugin:dum2").getClass(),
                pluginAccessor.getPlugin("com.atlassian.test.plugin.module").loadClass("foo.MyModuleDescriptor", null));

        assertEquals(1, enabledListener.called);
        assertEquals(1, disabledListener.called);
    }

    private File buildDynamicModuleClientJar() throws IOException {
        return new PluginJarBuilder("fooUser")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test 2' key='com.atlassian.test.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <foo key='dum2'/>",
                        "</atlassian-plugin>")
                .build();
    }

    private boolean waitForDynamicModuleEnabled() {
        return WaitUntil.invoke(new BasicWaitCondition() {
            public boolean isFinished() {
                return pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptors()
                        .iterator()
                        .next()
                        .getClass()
                        .getSimpleName()
                        .equals("MyModuleDescriptor");
            }
        });
    }

    @Test
    public void testUpgradeOfBundledPluginWithDynamicModule() throws Exception {
        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build();

        final DefaultModuleDescriptorFactory factory = new DefaultModuleDescriptorFactory(hostContainer);
        initBundlingPluginManager(factory, pluginJar);
        assertEquals(1, pluginAccessor.getEnabledPlugins().size());

        final File pluginClientOld = buildDynamicModuleClientJar();
        final File pluginClientNew = new PluginJarBuilder("fooUser")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test 2' key='com.atlassian.test.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>2.0</version>",
                        "    </plugin-info>",
                        "    <foo key='dum2'/>",
                        "</atlassian-plugin>")
                .build();
        pluginController.installPlugins(new JarPluginArtifact(pluginClientOld), new JarPluginArtifact(pluginClientNew));

        assertTrue(waitForDynamicModuleEnabled());

        assertEquals(2, pluginAccessor.getEnabledPlugins().size());
        assertEquals(
                "2.0",
                pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getPluginInformation()
                        .getVersion());
    }

    @Test
    public void testDynamicPluginModuleNotLinkToAllPlugins() throws Exception {
        new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor'/>",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build(pluginsDir);
        new PluginJarBuilder("fooUser")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test 2' key='com.atlassian.test.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <foo key='dum2'/>",
                        "</atlassian-plugin>")
                .build(pluginsDir);
        new PluginJarBuilder("foootherUser")
                .addPluginInformation("unusing.plugin", "Unusing plugin", "1.0")
                .build(pluginsDir);

        initPluginManager(registrar -> {});

        assertEquals(
                "MyModuleDescriptor",
                pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptor("dum2")
                        .getClass()
                        .getSimpleName());
        Set<String> deps = findDependentBundles(
                ((OsgiPlugin) pluginAccessor.getPlugin("com.atlassian.test.plugin.module")).getBundle());
        assertTrue(deps.contains("com.atlassian.test.plugin"));
        assertFalse(deps.contains("unusing.plugin"));
    }

    private Set<String> findDependentBundles(Bundle bundle) {
        Set<String> deps = new HashSet<>();
        final ServiceReference[] registeredServices = bundle.getRegisteredServices();
        if (registeredServices == null) {
            return deps;
        }

        for (final ServiceReference serviceReference : registeredServices) {
            final Bundle[] usingBundles = serviceReference.getUsingBundles();
            if (usingBundles == null) {
                continue;
            }
            for (final Bundle usingBundle : usingBundles) {
                deps.add(usingBundle.getSymbolicName());
            }
        }
        return deps;
    }

    @Test
    public void testDynamicPluginModuleUsingModuleTypeDescriptor() throws Exception {
        initPluginManager(registrar -> {});

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build();
        final File pluginJar2 = buildDynamicModuleClientJar();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        pluginController.installPlugins(new JarPluginArtifact(pluginJar2));
        WaitUntil.invoke(new BasicWaitCondition() {
            public boolean isFinished() {
                return pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptor("dum2")
                        .getClass()
                        .getSimpleName()
                        .equals("MyModuleDescriptor");
            }
        });
        final Collection<ModuleDescriptor<?>> descriptors =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(1, descriptors.size());
        final ModuleDescriptor<?> descriptor = descriptors.iterator().next();
        assertEquals("MyModuleDescriptor", descriptor.getClass().getSimpleName());
    }

    @Test
    public void testDynamicPluginModuleWithClientAndHostEnabledSimultaneouslyCheckEvents() throws Exception {
        initPluginManager();

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='host' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='foo' class='foo.MyModuleDescriptorFactory' public='true'>",
                        "       <interface>com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory</interface>",
                        "    </component>",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        "foo.MyModuleDescriptorFactory",
                        "package foo;",
                        "public class MyModuleDescriptorFactory extends com.atlassian.plugin.DefaultModuleDescriptorFactory ",
                        "                                       implements com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory{",
                        "  public MyModuleDescriptorFactory() throws Exception{",
                        "    super(new com.atlassian.plugin.hostcontainer.DefaultHostContainer());",
                        "    Thread.sleep(500);",
                        "    System.out.println('starting descriptor factory');",
                        "    addModuleDescriptor('foo', com.atlassian.plugin.osgi.EventTrackingModuleDescriptor.class);",
                        "  }",
                        "  public Iterable getModuleDescriptorKeys() {",
                        "    return java.util.Collections.singleton('foo');",
                        "  }",
                        "  public java.util.Set getModuleDescriptorClasses() {",
                        "    return java.util.Collections.singleton(com.atlassian.plugin.osgi.EventTrackingModuleDescriptor.class);",
                        "  }",
                        "}")
                .build();
        final File pluginJar2 = new PluginJarBuilder("fooUser")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test 2' key='client' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <foo key='dum2'/>",
                        "</atlassian-plugin>")
                .build();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar), new JarPluginArtifact(pluginJar2));

        WaitUntil.invoke(new BasicWaitCondition() {
            public boolean isFinished() {
                return pluginAccessor
                        .getPlugin("client")
                        .getModuleDescriptor("dum2")
                        .getClass()
                        .getSimpleName()
                        .equals("EventTrackingModuleDescriptor");
            }
        });
        EventTrackingModuleDescriptor desc = (EventTrackingModuleDescriptor)
                pluginAccessor.getPlugin("client").getModuleDescriptor("dum2");
        assertEquals(1, desc.getEnabledCount());
    }

    @Test
    public void testDynamicPluginModuleUsingModuleTypeDescriptorAndComponentInjection() throws Exception {
        initPluginManager(registrar -> {});

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='comp' class='foo.MyComponent' />",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "</atlassian-plugin>")
                .addFormattedJava("foo.MyComponent", "package foo;", "public class MyComponent {", "}")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build();
        final File pluginJar2 = buildDynamicModuleClientJar();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        pluginController.installPlugins(new JarPluginArtifact(pluginJar2));
        assertTrue(waitForDynamicModuleEnabled());
        final Collection<ModuleDescriptor<?>> descriptors =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(1, descriptors.size());
        final ModuleDescriptor<?> descriptor = descriptors.iterator().next();
        assertEquals("MyModuleDescriptor", descriptor.getClass().getSimpleName());
    }

    @Test
    public void testDynamicPluginModuleUsingModuleTypeDescriptorAfterTheFact() throws Exception {
        initPluginManager(registrar -> {});

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build();
        final File pluginJar2 = buildDynamicModuleClientJar();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar2));
        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        waitForDynamicModuleEnabled();

        Collection<ModuleDescriptor<?>> descriptors =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(1, descriptors.size());
        ModuleDescriptor<?> descriptor = descriptors.iterator().next();
        assertEquals("MyModuleDescriptor", descriptor.getClass().getSimpleName());

        pluginController.uninstall(pluginAccessor.getPlugin("com.atlassian.test.plugin.module"));
        WaitUntil.invoke(new BasicWaitCondition() {
            public boolean isFinished() {
                return pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptors()
                        .iterator()
                        .next()
                        .getClass()
                        .getSimpleName()
                        .equals("UnrecognisedModuleDescriptor");
            }
        });
        descriptors = pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(1, descriptors.size());
        descriptor = descriptors.iterator().next();
        assertEquals("UnrecognisedModuleDescriptor", descriptor.getClass().getSimpleName());

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        descriptors = pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(1, descriptors.size());
        descriptor = descriptors.iterator().next();
        assertEquals("MyModuleDescriptor", descriptor.getClass().getSimpleName());
    }

    @Test
    public void testDynamicPluginModuleUsingModuleTypeDescriptorAfterTheFactWithException() throws Exception {
        initPluginManager(registrar -> {});

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin.module' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        "foo.MyModuleDescriptor",
                        "package foo;",
                        "import com.atlassian.plugin.module.ModuleFactory;",
                        "public class MyModuleDescriptor extends com.atlassian.plugin.descriptors.AbstractModuleDescriptor {",
                        "  public MyModuleDescriptor() {",
                        "    super(ModuleFactory.LEGACY_MODULE_FACTORY);",
                        "    throw new RuntimeException('error loading module');",
                        "  }",
                        "  public Object getModule(){return null;}",
                        "}")
                .build();
        final File pluginJar2 = buildDynamicModuleClientJar();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar2));
        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        assertTrue(WaitUntil.invoke(new BasicWaitCondition() {
            public boolean isFinished() {
                UnrecognisedModuleDescriptor des = (UnrecognisedModuleDescriptor)
                        pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptor("dum2");
                return des.getErrorText().contains("error loading module");
            }
        }));
    }

    @Test
    public void testDynamicPluginModuleUsingModuleTypeDescriptorInSamePlugin() throws Exception {
        initPluginManager(registrar -> {});

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "    <foo key='dum2' />",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        WaitUntil.invoke(new BasicWaitCondition() {
            public boolean isFinished() {
                return pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptor("dum2")
                        .getClass()
                        .getSimpleName()
                        .equals("MyModuleDescriptor");
            }
        });
        final Collection<ModuleDescriptor<?>> descriptors =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(2, descriptors.size());
        final ModuleDescriptor<?> descriptor =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptor("dum2");
        assertEquals("MyModuleDescriptor", descriptor.getClass().getSimpleName());
    }

    @Test
    public void testDynamicPluginModuleUsingModuleTypeDescriptorInSamePluginWithRestart() throws Exception {
        initPluginManager();

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <module-type key='foo' class='foo.MyModuleDescriptor' />",
                        "    <foo key='dum2' />",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        getMyModuleDescriptorClass().left(),
                        getMyModuleDescriptorClass().right())
                .build();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        WaitUntil.invoke(new BasicWaitCondition() {
            public boolean isFinished() {
                return pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptor("dum2")
                        .getClass()
                        .getSimpleName()
                        .equals("MyModuleDescriptor");
            }
        });
        Collection<ModuleDescriptor<?>> descriptors =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(2, descriptors.size());
        ModuleDescriptor<?> descriptor =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptor("dum2");
        assertEquals("MyModuleDescriptor", descriptor.getClass().getSimpleName());

        PluginModuleDisabledListener disabledListener = new PluginModuleDisabledListener("dum2");
        PluginModuleEnabledListener enabledListener = new PluginModuleEnabledListener("dum2");
        pluginEventManager.register(disabledListener);
        pluginEventManager.register(enabledListener);

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        WaitUntil.invoke(new BasicWaitCondition() {
            public boolean isFinished() {
                return pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptor("dum2")
                        .getClass()
                        .getSimpleName()
                        .equals("MyModuleDescriptor");
            }
        });
        descriptors = pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(2, descriptors.size());
        ModuleDescriptor<?> newdescriptor =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptor("dum2");
        assertEquals("MyModuleDescriptor", newdescriptor.getClass().getSimpleName());
        assertNotSame(descriptor.getClass(), newdescriptor.getClass());
        assertEquals(1, disabledListener.called);
        assertEquals(1, enabledListener.called);
    }

    @Test
    public void testDynamicModuleDescriptor() throws Exception {
        initPluginManager(null);

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addPluginInformation("com.atlassian.test.plugin", "foo", "1.0")
                .build();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        final BundleContext ctx = ((OsgiPlugin) pluginAccessor.getPlugin("com.atlassian.test.plugin"))
                .getBundle()
                .getBundleContext();
        final ServiceRegistration reg =
                ctx.registerService(ModuleDescriptor.class.getName(), new DummyModuleDescriptorWithKey(), null);

        final Collection<ModuleDescriptor<?>> descriptors =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(1, descriptors.size());
        final ModuleDescriptor<?> descriptor = descriptors.iterator().next();
        assertEquals("DummyModuleDescriptorWithKey", descriptor.getClass().getSimpleName());
        List<DummyModuleDescriptorWithKey> list =
                pluginAccessor.getEnabledModuleDescriptorsByClass(DummyModuleDescriptorWithKey.class);
        assertEquals(1, list.size());
        reg.unregister();
        list = pluginAccessor.getEnabledModuleDescriptorsByClass(DummyModuleDescriptorWithKey.class);
        assertEquals(0, list.size());
    }

    @Test
    public void testStateAwareDynamicModuleDescriptor() throws Exception {
        initPluginManager(null);

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addPluginInformation("com.atlassian.test.plugin", "foo", "1.0")
                .build();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        final BundleContext ctx = ((OsgiPlugin) pluginAccessor.getPlugin("com.atlassian.test.plugin"))
                .getBundle()
                .getBundleContext();

        AtomicInteger timesEnabled = new AtomicInteger();
        AtomicInteger timesDisabled = new AtomicInteger();
        final ServiceRegistration reg = ctx.registerService(
                ModuleDescriptor.class.getName(),
                new DummyStateAwareModuleDescriptorWithKey(timesEnabled, timesDisabled),
                null);

        assertThat(timesEnabled.get(), equalTo(1));
        reg.unregister();
        assertThat(timesDisabled.get(), equalTo(1));
    }

    @Test
    public void testDynamicModuleDescriptorIsolatedToPlugin() throws Exception {
        initPluginManager(null);

        final File pluginJar = new PluginJarBuilder("pluginType")
                .addPluginInformation("com.atlassian.test.plugin", "foo", "1.0")
                .build();

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        final BundleContext ctx = ((OsgiPlugin) pluginAccessor.getPlugin("com.atlassian.test.plugin"))
                .getBundle()
                .getBundleContext();
        ctx.registerService(ModuleDescriptor.class.getName(), new DummyModuleDescriptorWithKey(), null);

        final File pluginJar2 = new PluginJarBuilder("pluginType")
                .addPluginInformation("com.atlassian.test.plugin2", "foo", "1.0")
                .build();
        pluginController.installPlugins(new JarPluginArtifact(pluginJar2));
        final BundleContext ctx2 = ((OsgiPlugin) pluginAccessor.getPlugin("com.atlassian.test.plugin2"))
                .getBundle()
                .getBundleContext();
        final ServiceRegistration reg2 =
                ctx2.registerService(ModuleDescriptor.class.getName(), new DummyModuleDescriptorWithKey(), null);

        Collection<ModuleDescriptor<?>> descriptors =
                pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(1, descriptors.size());
        final ModuleDescriptor<?> descriptor = descriptors.iterator().next();
        assertEquals("DummyModuleDescriptorWithKey", descriptor.getClass().getSimpleName());
        List<DummyModuleDescriptorWithKey> list =
                pluginAccessor.getEnabledModuleDescriptorsByClass(DummyModuleDescriptorWithKey.class);
        assertEquals(2, list.size());
        reg2.unregister();
        list = pluginAccessor.getEnabledModuleDescriptorsByClass(DummyModuleDescriptorWithKey.class);
        assertEquals(1, list.size());
        descriptors = pluginAccessor.getPlugin("com.atlassian.test.plugin").getModuleDescriptors();
        assertEquals(1, descriptors.size());
    }

    @Test
    public void testInstallUninstallInstallWithModuleTypePlugin() throws Exception {
        final PluginArtifact moduleTypeProviderArtifact = new JarPluginArtifact(new PluginJarBuilder()
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Foo Module Type Provider' key='com.atlassian.test.fooModuleTypeProvider' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions>",
                        "            <Export-Package>my</Export-Package>",
                        "        </bundle-instructions>",
                        "    </plugin-info>",
                        "    <module-type key='foo-module' class='my.FooModuleDescriptor'/>",
                        "</atlassian-plugin>")
                .addClass(FooModule.class)
                .addClass(FooModuleDescriptor.class)
                .build());
        final PluginArtifact moduleTypeImplementerArtifact = new JarPluginArtifact(new PluginJarBuilder()
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Foo Module Type Implementer' key='com.atlassian.test.fooModuleTypeImplementer' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions>",
                        "            <Import-Package>my</Import-Package>",
                        "        </bundle-instructions>",
                        "    </plugin-info>",
                        "    <foo-module key='myFooModule' class='my.impl.FooModuleImpl'/>",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        "my.impl.FooModuleImpl",
                        "package my.impl;",
                        "",
                        "import my.FooModule;",
                        "",
                        "public class FooModuleImpl implements FooModule {",
                        "}")
                .build());

        initPluginManager();
        pluginController.installPlugins(moduleTypeProviderArtifact);
        pluginController.installPlugins(moduleTypeImplementerArtifact);

        final long foo1InitialisationTime = assertFooImplEnabledAndGetInitialisationTime();

        pluginController.installPlugins(moduleTypeProviderArtifact);

        final long foo2InitialisationTime = assertFooImplEnabledAndGetInitialisationTime();

        assertTrue(
                "FooModuleImpl implements old version of FooModule", foo2InitialisationTime > foo1InitialisationTime);
    }

    private long assertFooImplEnabledAndGetInitialisationTime() throws IllegalAccessException, NoSuchFieldException {
        assertTrue(pluginAccessor.isPluginModuleEnabled("com.atlassian.test.fooModuleTypeProvider:foo-module"));
        assertTrue(pluginAccessor.isPluginModuleEnabled("com.atlassian.test.fooModuleTypeImplementer:myFooModule"));
        final ModuleDescriptor<?> fooDescriptor =
                pluginAccessor.getEnabledPluginModule("com.atlassian.test.fooModuleTypeImplementer:myFooModule");
        assertNotNull(fooDescriptor);
        final Object foo = fooDescriptor.getModule();
        assertNotNull(foo);
        final Class<? extends Object> fooClass = foo.getClass();
        assertEquals("my.impl.FooModuleImpl", fooClass.getName());
        return fooClass.getField("INITIALISATION_TIME").getLong(foo);
    }

    private static Pair<String, String[]> getMyModuleDescriptorClass() {
        return Pair.pair("foo.MyModuleDescriptor", new String[] {
            "package foo;",
            "import com.atlassian.plugin.module.ModuleFactory;",
            "public class MyModuleDescriptor extends com.atlassian.plugin.descriptors.AbstractModuleDescriptor {",
            "  public MyModuleDescriptor(){ super(ModuleFactory.LEGACY_MODULE_FACTORY); }",
            "  public Object getModule(){return null;}",
            "}"
        });
    }

    public static class PluginModuleEnabledListener {
        public volatile int called;
        private final String key;

        public PluginModuleEnabledListener(String key) {
            this.key = key;
        }

        @PluginEventListener
        public void onEnable(PluginModuleEnabledEvent event) {
            if (event.getModule().getKey().equals(key)) {
                called++;
            }
        }
    }

    public static class PluginModuleDisabledListener {
        public volatile int called;
        private final String key;

        public PluginModuleDisabledListener(String key) {
            this.key = key;
        }

        @PluginEventListener
        public void onDisable(PluginModuleDisabledEvent event) {
            if (event.getModule().getKey().equals(key)) {
                called++;
            }
        }
    }
}
