package it.com.atlassian.plugin.osgi;

import org.junit.After;
import org.junit.Test;

import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.osgi.StaticBooleanFlag;
import com.atlassian.plugin.osgi.StaticHolder;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestPluginSpringInteraction extends PluginInContainerTestBase {

    @After
    public void tearDown() {
        StaticHolder.set(null);
    }

    @Test
    public void testDisposableAndAutowiring() throws Exception {
        StaticBooleanFlag.flag.set(false);
        StaticHolder.set(null);

        createTestPluginJarBuilder().build(pluginsDir);

        initPluginManager();
        // destroy should not have been called yet
        assertFalse(StaticBooleanFlag.flag.get());
        // but autowiring should have happened exactly once
        assertEquals(Integer.valueOf(1), StaticHolder.get());

        // on disable
        pluginController.disablePlugin("test.plugin");
        assertTrue(StaticBooleanFlag.flag.get());
        pluginController.enablePlugins("test.plugin");

        // on framework shutdown
        StaticBooleanFlag.flag.set(false);
        osgiContainerManager.stop();
        assertTrue(StaticBooleanFlag.flag.get());
    }

    @Test
    public void testAutowiringHappensOnce() throws Exception {
        // create the plugin and verify that autowiring happens only once, even if <context:annotation-config/> is
        // configured by the plugin
        createTestPluginJarBuilder()
                .addFormattedResource(
                        "META-INF/spring/plugin-context.xml",
                        "<?xml version='1.0' encoding='UTF-8'?>",
                        "<beans xmlns:osgi='http://www.springframework.org/schema/osgi'",
                        "       xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'",
                        "       xmlns:context='http://www.springframework.org/schema/context'",
                        "       xmlns='http://www.springframework.org/schema/beans'",
                        "              xsi:schemaLocation='http://www.springframework.org/schema/beans ",
                        "              http://www.springframework.org/schema/beans/spring-beans-2.5.xsd",
                        "              http://www.springframework.org/schema/context",
                        "              http://www.springframework.org/schema/context/spring-context.xsd'>",
                        "  <context:annotation-config/>",
                        "</beans>")
                .build(pluginsDir);

        initPluginManager();
        assertEquals(Integer.valueOf(1), StaticHolder.get());
    }

    private PluginJarBuilder createTestPluginJarBuilder() throws Exception {
        return new PluginJarBuilder("testDisposableAndAutowiring")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='test.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='obj' class='my.Foo'/>",
                        "    <component key='bar' class='my.Bar'/>",
                        "</atlassian-plugin>")
                .addFormattedJava("my.Bar", "package my;", "public class Bar {", "}")
                .addFormattedJava(
                        "my.Foo",
                        "package my;",
                        "public class Foo implements org.springframework.beans.factory.DisposableBean{",
                        "  private int counter;",
                        "  @org.springframework.beans.factory.annotation.Autowired",
                        "  public void setBar(my.Bar bar) {",
                        "    com.atlassian.plugin.osgi.StaticHolder.set(++counter);",
                        "  }",
                        "  @Override",
                        "  public void destroy() {",
                        "    com.atlassian.plugin.osgi.StaticBooleanFlag.flag.set(true);",
                        "  }",
                        "}");
    }
}
