package it.com.atlassian.plugin.osgi;

import org.junit.Test;

import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.osgi.StaticBooleanFlag;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public class TestPluginWithJaxp extends PluginInContainerTestBase {
    @Test
    public void testDisposable() throws Exception {
        StaticBooleanFlag.flag.set(false);
        new PluginJarBuilder("testDisposable")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='test.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='obj' class='my.Foo'/>",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        "my.Foo",
                        "package my;",
                        "public class Foo implements org.springframework.beans.factory.DisposableBean{",
                        "  public void destroy() {",
                        "    com.atlassian.plugin.osgi.StaticBooleanFlag.flag.set(true);",
                        "  }",
                        "}")
                .build(pluginsDir);

        initPluginManager();
        assertFalse(StaticBooleanFlag.flag.get());

        // on disable
        pluginController.disablePlugin("test.plugin");
        assertTrue(StaticBooleanFlag.flag.get());
        pluginController.enablePlugins("test.plugin");

        // on framework shutdown
        StaticBooleanFlag.flag.set(false);
        osgiContainerManager.stop();
        assertTrue(StaticBooleanFlag.flag.get());
    }
}
