package it.com.atlassian.plugin.osgi;

import java.io.File;

import org.junit.Test;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.osgi.DummyHostComponent;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

public class TestContextClassLoader extends PluginInContainerTestBase {

    @Test
    public void testCorrectContextClassLoaderForHostComponents() throws Exception {
        final DummyHostComponentImpl comp = new DummyHostComponentImpl(TestContextClassLoader.class.getName());
        File plugin = new PluginJarBuilder("ccltest")
                .addResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin key=\"com.atlassian.ccltest\" pluginsVersion=\"2\">\n"
                                + "    <plugin-info>\n"
                                + "        <version>1.0</version>\n"
                                + "    </plugin-info>\n"
                                + "    <component key=\"foo\" class=\"my.FooImpl\" />\n"
                                + "</atlassian-plugin>")
                .addJava("my.Foo", "package my;public interface Foo {}")
                .addJava(
                        "my.FooImpl",
                        "package my;import com.atlassian.plugin.osgi.DummyHostComponent;"
                                + "public class FooImpl implements Foo {public FooImpl(DummyHostComponent comp) throws Exception { comp.evaluate(); }}")
                .build();
        HostComponentProvider prov = registrar ->
                registrar.register(DummyHostComponent.class).forInstance(comp).withName("hostComp");

        initPluginManager(prov);
        pluginController.installPlugins(new JarPluginArtifact(plugin));

        assertNotNull(comp.cl);
        assertNotNull(comp.testClass);
        assertSame(comp.testClass, TestContextClassLoader.class);
    }

    @Test
    public void testCorrectContextClassLoaderForHostComponentsUsePluginStrategy() throws Exception {
        final DummyHostComponentImpl comp = new DummyHostComponentImpl(TestContextClassLoader.class.getName());
        File plugin = new PluginJarBuilder("ccltest")
                .addResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin key=\"com.atlassian.ccltest\" pluginsVersion=\"2\">\n"
                                + "    <plugin-info>\n"
                                + "        <version>1.0</version>\n"
                                + "    </plugin-info>\n"
                                + "    <component key=\"foo\" class=\"my.FooImpl\" />\n"
                                + "</atlassian-plugin>")
                .addJava("my.Foo", "package my;public interface Foo {}")
                .addJava(
                        "my.FooImpl",
                        "package my;import com.atlassian.plugin.osgi.DummyHostComponent;"
                                + "public class FooImpl implements Foo {public FooImpl(DummyHostComponent comp) throws Exception { comp.evaluate(); }}")
                .build();
        HostComponentProvider prov = registrar -> registrar
                .register(DummyHostComponent.class)
                .forInstance(comp)
                .withName("hostComp")
                .withContextClassLoaderStrategy(ContextClassLoaderStrategy.USE_PLUGIN);

        initPluginManager(prov);
        pluginController.installPlugins(new JarPluginArtifact(plugin));

        assertNotNull(comp.cl);
        assertNull(comp.testClass);
    }

    @Test
    public void testCorrectContextClassLoaderForHostComponentsUsePluginStrategyLoadingLocalClass() throws Exception {
        final DummyHostComponentImpl comp = new DummyHostComponentImpl("my.Foo");
        File plugin = new PluginJarBuilder("ccltest")
                .addResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin key=\"com.atlassian.ccltest\" pluginsVersion=\"2\">\n"
                                + "    <plugin-info>\n"
                                + "        <version>1.0</version>\n"
                                + "    </plugin-info>\n"
                                + "    <component key=\"foo\" class=\"my.FooImpl\" />\n"
                                + "</atlassian-plugin>")
                .addJava("my.Foo", "package my;public interface Foo {}")
                .addJava(
                        "my.FooImpl",
                        "package my;import com.atlassian.plugin.osgi.DummyHostComponent;"
                                + "public class FooImpl implements Foo {public FooImpl(DummyHostComponent comp) throws Exception { comp.evaluate(); }}")
                .build();
        HostComponentProvider prov = registrar -> registrar
                .register(DummyHostComponent.class)
                .forInstance(comp)
                .withName("hostComp")
                .withContextClassLoaderStrategy(ContextClassLoaderStrategy.USE_PLUGIN);

        initPluginManager(prov);
        pluginController.installPlugins(new JarPluginArtifact(plugin));

        assertNotNull(comp.cl);
        assertNotNull(comp.testClass);
    }

    @Test
    public void testCorrectContextClassLoaderForHostComponentsUseHostStrategy() throws Exception {
        final DummyHostComponentImpl comp = new DummyHostComponentImpl(TestContextClassLoader.class.getName());
        File plugin = new PluginJarBuilder("ccltest")
                .addResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin key=\"com.atlassian.ccltest\" pluginsVersion=\"2\">\n"
                                + "    <plugin-info>\n"
                                + "        <version>1.0</version>\n"
                                + "    </plugin-info>\n"
                                + "    <component key=\"foo\" class=\"my.FooImpl\" />\n"
                                + "</atlassian-plugin>")
                .addJava("my.Foo", "package my;public interface Foo {}")
                .addJava(
                        "my.FooImpl",
                        "package my;import com.atlassian.plugin.osgi.DummyHostComponent;"
                                + "public class FooImpl implements Foo {public FooImpl(DummyHostComponent comp) throws Exception { comp.evaluate(); }}")
                .build();
        HostComponentProvider prov = registrar -> registrar
                .register(DummyHostComponent.class)
                .forInstance(comp)
                .withName("hostComp")
                .withContextClassLoaderStrategy(ContextClassLoaderStrategy.USE_HOST);

        initPluginManager(prov);
        pluginController.installPlugins(new JarPluginArtifact(plugin));

        assertNotNull(comp.cl);
        assertNotNull(comp.testClass);
        assertSame(comp.testClass, TestContextClassLoader.class);
    }

    public static class DummyHostComponentImpl implements DummyHostComponent {
        private ClassLoader cl;
        private Class testClass;
        private String classToLoad;

        public DummyHostComponentImpl(String classToLoad) {
            this.classToLoad = classToLoad;
        }

        public void evaluate() {
            cl = Thread.currentThread().getContextClassLoader();
            try {
                testClass = cl.loadClass(classToLoad);
            } catch (ClassNotFoundException ex) {
                // ignored
            }
        }
    }
}
