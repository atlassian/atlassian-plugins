package it.com.atlassian.plugin.osgi;

import java.io.File;

import org.junit.Test;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.test.PluginJarBuilder;

public class TestShutdown extends PluginInContainerTestBase {
    @Test
    public void testShutdown() throws Exception {
        File pluginJar = new PluginJarBuilder("shutdowntest")
                .addPluginInformation("shutdown", "foo", "1.0")
                .build();
        initPluginManager(null);
        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        pluginSystemLifecycle.shutdown();
    }
}
