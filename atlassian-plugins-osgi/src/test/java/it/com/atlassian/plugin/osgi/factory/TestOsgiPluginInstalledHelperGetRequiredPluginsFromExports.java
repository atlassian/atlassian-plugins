package it.com.atlassian.plugin.osgi.factory;

import org.junit.Test;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class TestOsgiPluginInstalledHelperGetRequiredPluginsFromExports extends PluginInContainerTestBase {
    @Test
    public void testEnablingDisabledDependentPluginRecursivelyEnablesDependency() throws Exception {
        new PluginJarBuilder("osgi")
                .addFormattedResource(
                        "META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Bundle-SymbolicName: myA",
                        "Bundle-Version: 1.0",
                        "Export-Package: testpackage",
                        "")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='myA-1.0' pluginsVersion='2' state='disabled'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build(pluginsDir);

        new PluginJarBuilder("osgi")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='consumer' pluginsVersion='2' state='disabled'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>testpackage</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .build(pluginsDir);

        initPluginManager();

        Plugin plugin = pluginAccessor.getPlugin("consumer");
        assertThat(plugin.getDependencies().getAll(), contains("myA-1.0"));
    }
}
