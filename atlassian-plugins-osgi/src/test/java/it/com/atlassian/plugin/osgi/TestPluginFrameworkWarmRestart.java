package it.com.atlassian.plugin.osgi;

import java.util.concurrent.Callable;

import org.junit.Test;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.osgi.CallableModuleDescriptor;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

public class TestPluginFrameworkWarmRestart extends PluginInContainerTestBase {

    @Test
    public void testWarmRestart() throws Exception {
        final DefaultModuleDescriptorFactory factory = new DefaultModuleDescriptorFactory(hostContainer);
        factory.addModuleDescriptor("object", CallableModuleDescriptor.class);

        final HostComponentProvider prov = new HostComponentProvider() {
            private int count = 1;

            public void provide(ComponentRegistrar registrar) {
                registrar.register(Callable.class).forInstance((Callable) () -> "count:" + (count++) + "-");
            }
        };

        new PluginJarBuilder("testWarmRestart")
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='com.atlassian.test.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <object key='obj' class='my.Foo'/>",
                        "    <object key='obj-disabled' class='my.Foo'/>",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        "my.Foo",
                        "package my;",
                        "import java.util.concurrent.Callable;",
                        "public class Foo implements Callable {",
                        "   private Callable host;",
                        "   public Foo(Callable host) { this.host = host; }",
                        "   public Object call() throws Exception { return ((String)host.call()) + System.identityHashCode(this); }",
                        "}")
                .build(pluginsDir);
        initPluginManager(prov, factory);
        pluginController.disablePluginModule("com.atlassian.test.plugin:obj-disabled");

        assertEquals(1, pluginAccessor.getEnabledPlugins().size());
        assertEquals(1, pluginAccessor.getEnabledModulesByClass(Callable.class).size());
        assertEquals(
                "Test", pluginAccessor.getPlugin("com.atlassian.test.plugin").getName());
        assertEquals(
                "my.Foo",
                pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptor("obj")
                        .getModule()
                        .getClass()
                        .getName());
        String value = (String)
                pluginAccessor.getEnabledModulesByClass(Callable.class).get(0).call();
        assertTrue(value.startsWith("count:1-"));
        String id = value.substring("count:1-".length());

        pluginSystemLifecycle.warmRestart();

        assertEquals(1, pluginAccessor.getEnabledPlugins().size());
        assertEquals(1, pluginAccessor.getEnabledModulesByClass(Callable.class).size());
        assertEquals(
                "Test", pluginAccessor.getPlugin("com.atlassian.test.plugin").getName());
        assertEquals(
                "my.Foo",
                pluginAccessor
                        .getPlugin("com.atlassian.test.plugin")
                        .getModuleDescriptor("obj")
                        .getModule()
                        .getClass()
                        .getName());
        String value2 = (String)
                pluginAccessor.getEnabledModulesByClass(Callable.class).get(0).call();
        assertTrue(value2.startsWith("count:2-"));
        String id2 = value2.substring("count:2-".length());

        assertNotSame(id, id2);
    }
}
