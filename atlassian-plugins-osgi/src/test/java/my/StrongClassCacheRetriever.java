package my;

import java.lang.reflect.Field;
import java.util.Map;

import org.springframework.beans.CachedIntrospectionResults;

import com.atlassian.plugin.osgi.StaticHolder;

public class StrongClassCacheRetriever {
    public StrongClassCacheRetriever() {
        try {
            Field field = CachedIntrospectionResults.class.getDeclaredField("strongClassCache");
            field.setAccessible(true);
            Map classCache = (Map) field.get(null);

            StaticHolder.set(classCache);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
