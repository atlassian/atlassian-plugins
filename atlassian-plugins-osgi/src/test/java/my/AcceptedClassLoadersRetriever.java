package my;

import java.lang.reflect.Field;
import java.util.Set;

import org.springframework.beans.CachedIntrospectionResults;

import com.atlassian.plugin.osgi.StaticHolder;

public class AcceptedClassLoadersRetriever {
    public AcceptedClassLoadersRetriever() {
        try {
            Field field = CachedIntrospectionResults.class.getDeclaredField("acceptedClassLoaders");
            field.setAccessible(true);
            Set acceptedClassLoaders = (Set) field.get(null);

            StaticHolder.set(acceptedClassLoaders);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
