package com.atlassian.plugin.osgi.internal.factory.transform.stage;

import java.util.List;

import org.dom4j.Element;

import com.atlassian.plugin.osgi.internal.factory.transform.TransformContext;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformStage;

/**
 * Adds bundle instruction overrides from the plugin descriptor to be later used in the manifest generation process.
 *
 * @since 2.2.0
 */
public class AddBundleOverridesStage implements TransformStage {
    public void execute(TransformContext context) {
        Element pluginInfo = context.getDescriptorDocument().getRootElement().element("plugin-info");
        if (pluginInfo != null) {
            Element instructionRoot = pluginInfo.element("bundle-instructions");
            if (instructionRoot != null) {
                List<Element> instructionsElement = instructionRoot.elements();
                for (Element instructionElement : instructionsElement) {
                    String name = instructionElement.getName();
                    String value = instructionElement.getTextTrim();
                    context.getBndInstructions().put(name, value);
                }
            }
        }
    }
}
