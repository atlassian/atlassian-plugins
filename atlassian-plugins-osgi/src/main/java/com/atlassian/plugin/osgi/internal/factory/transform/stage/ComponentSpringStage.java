package com.atlassian.plugin.osgi.internal.factory.transform.stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarInputStream;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import com.google.common.collect.Sets;

import com.atlassian.plugin.internal.util.PluginUtils;
import com.atlassian.plugin.osgi.factory.transform.PluginTransformationException;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformContext;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformStage;
import com.atlassian.plugin.util.validation.ValidationException;

/**
 * Transforms component tags in the plugin descriptor into the appropriate spring XML configuration file
 *
 * @since 2.2.0
 */
public class ComponentSpringStage implements TransformStage {
    /**
     * Path of generated Spring XML file
     */
    private static final String SPRING_XML = "META-INF/spring/atlassian-plugins-components.xml";

    public static final String BEAN_SOURCE = "Plugin Component";
    private static final String ALIAS = "alias";
    private static final String INTERFACE = "interface";

    public void execute(TransformContext context) {
        if (SpringHelper.shouldGenerateFile(context, SPRING_XML)) {
            Document springDoc = SpringHelper.createSpringDocument();
            Element root = springDoc.getRootElement();
            List<Element> elements =
                    context.getDescriptorDocument().getRootElement().elements("component");

            final Set<String> declaredInterfaces = new LinkedHashSet<>();

            for (Element component : elements) {
                if (!PluginUtils.doesModuleElementApplyToApplication(
                        component, context.getApplications(), context.getInstallationMode())) {
                    continue;
                }
                validate(component);

                String beanId = component.attributeValue("key");
                // make sure the new bean id is not already in use.
                context.trackBean(beanId, BEAN_SOURCE);

                Element bean = root.addElement("beans:bean");
                bean.addAttribute("id", beanId);
                bean.addAttribute("autowire", "default");

                // alias attribute in atlassian-plugin gets converted into alias element.
                if (!StringUtils.isBlank(component.attributeValue(ALIAS))) {
                    Element alias = root.addElement("beans:alias");
                    alias.addAttribute("name", beanId);
                    alias.addAttribute(ALIAS, component.attributeValue(ALIAS));
                }

                List<String> interfaceNames = new ArrayList<>();
                List<Element> compInterfaces = component.elements(INTERFACE);
                for (Element inf : compInterfaces) {
                    interfaceNames.add(inf.getTextTrim());
                }
                if (component.attributeValue(INTERFACE) != null) {
                    interfaceNames.add(component.attributeValue(INTERFACE));
                }

                bean.addAttribute("class", component.attributeValue("class"));

                if ("true".equalsIgnoreCase(component.attributeValue("public"))) {
                    Element osgiService = root.addElement("osgi:service");
                    osgiService.addAttribute("id", component.attributeValue("key") + "_osgiService");
                    osgiService.addAttribute("ref", component.attributeValue("key"));

                    // Collect for the interface names which will be used for import generation.
                    declaredInterfaces.addAll(interfaceNames);

                    Element interfaces = osgiService.addElement("osgi:interfaces");
                    for (String name : interfaceNames) {
                        ensureExported(name, context);
                        Element e = interfaces.addElement("beans:value");
                        e.setText(name);
                    }

                    Element svcprops = component.element("service-properties");
                    if (svcprops != null) {
                        Element targetSvcprops = osgiService.addElement("osgi:service-properties");
                        for (Element prop : new ArrayList<Element>(svcprops.elements("entry"))) {
                            Element e = targetSvcprops.addElement("beans:entry");
                            e.addAttribute("key", prop.attributeValue("key"));
                            e.addAttribute("value", prop.attributeValue("value"));
                        }
                    }
                }
            }

            if (root.elements().size() > 0) {
                context.setShouldRequireSpring(true);
                context.getFileOverrides().put(SPRING_XML, SpringHelper.documentToBytes(springDoc));
            }

            // calculate the required interfaces to be imported. this is (all the classes) - (classes available in the
            // plugin).
            Set<String> requiredInterfaces;
            try {
                requiredInterfaces = calculateRequiredImports(
                        context.getPluginFile(), declaredInterfaces, context.getBundleClassPathJars());
            } catch (PluginTransformationException e) {
                throw new PluginTransformationException("Error while calculating import manifest", e);
            }

            // dump all the outstanding imports as extra imports.
            context.getExtraImports().addAll(TransformStageUtils.getPackageNames(requiredInterfaces));
        }
    }

    private void validate(Element element) {
        final String msg = "There were validation errors:";
        if (element.attributeValue("key") == null) {
            throw new ValidationException(msg, Collections.singletonList("The key is required"));
        }
        if (element.attributeValue("class") == null) {
            throw new ValidationException(msg, Collections.singletonList("The class is required"));
        }
        if (element.attributeValue("public") != null) {
            String publicValue = element.attributeValue("public");
            boolean isPublic = Boolean.parseBoolean(publicValue);
            Element interfaceElement = element.element(INTERFACE);
            String interfaceAttribute = element.attributeValue(INTERFACE);
            if (isPublic && (interfaceElement == null && interfaceAttribute == null)) {
                throw new ValidationException(
                        msg, Collections.singletonList("Interfaces must be declared for public components"));
            }
        }
        if (element.element("service-properties") != null) {
            Element serviceProperties = element.element("service-properties");
            if (serviceProperties != null) {
                List<Element> entryElements = serviceProperties.elements().stream()
                        .filter(e -> e.getName().equals("entry"))
                        .collect(Collectors.toList());
                if (entryElements.isEmpty()) {
                    throw new ValidationException(
                            msg,
                            Collections.singletonList(
                                    "The service-properties element must contain at least one entry element with key and value attributes"));
                }
                boolean hasOneValidEntry = false;
                for (Element entry : entryElements) {
                    if (entry.attributeValue("key") != null && entry.attributeValue("value") != null) {
                        hasOneValidEntry = true;
                    }
                }
                if (!hasOneValidEntry) {
                    throw new ValidationException(
                            msg,
                            Collections.singletonList(
                                    "The service-properties element must contain at least one entry element with key and value attributes"));
                }
            }
        }
    }

    private void ensureExported(String className, TransformContext context) {
        String pkg = className.substring(0, className.lastIndexOf('.'));
        if (!context.getExtraExports().contains(pkg)) {
            String fileName = className.replace('.', '/') + ".class";

            if (context.getPluginArtifact().doesResourceExist(fileName)) {
                context.getExtraExports().add(pkg);
            }
        }
    }

    /**
     * Calculate the the interfaces that need to be imported.
     *
     * @return the set of interfaces that cannot be resolved in the pluginFile.
     */
    private Set<String> calculateRequiredImports(
            final File pluginFile, final Set<String> declaredInterfaces, final Set<String> innerJars) {
        // we only do it if at least one interface is declared as part of component element.
        if (declaredInterfaces.size() > 0) {
            // scan for class files of interest in the jar file, not including classes in inner jars.
            final Set<String> shallowMatches;

            try (FileInputStream fis = new FileInputStream(pluginFile);
                    JarInputStream jarStream = new JarInputStream(fis)) {
                shallowMatches = TransformStageUtils.scanJarForItems(
                        jarStream, declaredInterfaces, TransformStageUtils.JarEntryToClassName.INSTANCE);
            } catch (final IOException ioe) {
                throw new PluginTransformationException("Error reading jar:" + pluginFile.getName(), ioe);
            }

            // the outstanding set = declared set - shallow match set
            final Set<String> remainders = Sets.newLinkedHashSet(Sets.difference(declaredInterfaces, shallowMatches));

            // if all the interfaces are not yet satisfied, we have to scan inner jars as well.
            // this is, of course, subject to the availability of qualified inner jars.
            if ((remainders.size() > 0) && (innerJars.size() > 0)) {
                remainders.removeAll(TransformStageUtils.scanInnerJars(pluginFile, innerJars, remainders));
            }

            return Collections.unmodifiableSet(remainders);
        }

        // if no need to import.
        return Collections.emptySet();
    }
}
