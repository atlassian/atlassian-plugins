package com.atlassian.plugin.osgi.container.felix;

import java.util.Arrays;
import java.util.List;

import org.apache.felix.framework.Logger;
import org.apache.felix.framework.resolver.ResolveException;
import org.osgi.framework.BundleException;

/**
 * Bridges Felix logging messages with the Commons Logging
 */
public class FelixLoggerBridge extends Logger {
    private final org.slf4j.Logger log;

    private static final List<String> messagesToIgnore = Arrays.asList(
            "BeanInfo",
            "sun.beans.editors.",
            "add an import for 'org.eclipse.gemini.blueprint.service.",
            "Class 'org.springframework.util.Assert'",
            "Class '[Lorg.eclipse.gemini.blueprint.service",
            "org.springframework.core.InfrastructureProxy",
            "org.springframework.aop.SpringProxy",
            "org.springframework.aop.IntroductionInfo",
            "Class 'org.apache.commons.logging.impl.Log4JLogger'",
            "org.springframework.util.Assert",
            "org.eclipse.gemini.blueprint.service.importer.ServiceReferenceProxy",
            "org.eclipse.gemini.blueprint.service.importer.ImportedOsgiServiceProxy",
            "org.eclipse.gemini.blueprint.service.importer.support.ImportContextClassLoaderEditor",
            "[Lorg.eclipse.gemini.blueprint.service.importer.OsgiServiceLifecycleListener;Editor");

    public FelixLoggerBridge(org.slf4j.Logger log) {
        this.log = log;
        if (log.isDebugEnabled()) {
            setLogLevel(Logger.LOG_DEBUG);
        } else if (log.isInfoEnabled() || log.isWarnEnabled()) {
            setLogLevel(Logger.LOG_WARNING);
        } else {
            setLogLevel(Logger.LOG_ERROR);
        }
    }

    protected void doLog(
            org.osgi.framework.ServiceReference serviceReference,
            int level,
            java.lang.String message,
            java.lang.Throwable throwable) {
        if (serviceReference != null) message = "Service " + serviceReference + ": " + message;

        switch (level) {
            case LOG_DEBUG:

                // helps debug resolution errors when there are multiple constraint violations (final exception just
                // reports the last one)
                if (throwable != null && throwable instanceof ResolveException) {
                    log.debug(message, throwable);
                } else {
                    log.debug(message);
                }
                break;
            case LOG_ERROR:
                if (throwable != null) {
                    if ((throwable instanceof BundleException)
                            && (((BundleException) throwable).getNestedException() != null)) {
                        throwable = ((BundleException) throwable).getNestedException();
                    }
                    log.error(message, throwable);
                } else log.error(message);
                break;
            case LOG_INFO:
                logInfoUnlessLame(message);
                break;
            case LOG_WARNING:
                // Handles special class loader errors from felix that have quite useful information
                if (throwable != null) {
                    if (throwable instanceof ClassNotFoundException && isClassNotFoundsWeCareAbout(throwable)) {
                        log.debug("Class not found in bundle: {}", message);
                    } else if (throwable instanceof BundleException) {
                        // Felix logs these when resolution fails, and it's useful to know why
                        log.warn("{}: {}", message, throwable.getMessage());
                    }
                } else {
                    logInfoUnlessLame(message);
                }
                break;
            default:
                log.debug("UNKNOWN[{}]: ", message);
        }
    }

    protected void logInfoUnlessLame(String message) {
        if (message != null) {
            // I'm really, really sick of these stupid messages
            for (String dumbBit : messagesToIgnore) if (message.contains(dumbBit)) return;
        }
        log.info(message);
    }

    public boolean isClassNotFoundsWeCareAbout(Throwable t) {
        if (t instanceof ClassNotFoundException) {
            String className = t.getMessage();
            if (className.contains("***") && t.getCause() instanceof ClassNotFoundException) {
                className = t.getCause().getMessage();
            }
            if (!className.startsWith("org.springframework")
                    && !className.endsWith("BeanInfo")
                    && !className.endsWith("Editor")) {
                return true;
            }
        }
        return false;
    }
}
