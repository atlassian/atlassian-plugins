package com.atlassian.plugin.osgi.util;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

/**
 * Utility method for accessing system bundle.
 *
 * @since 3.0
 */
public class OsgiSystemBundleUtil {
    /**
     * The identifier of the system bundle.
     *
     * @since 3.0
     */
    public static final int SYSTEM_BUNDLE_ID = 0;

    /**
     * Returns the system bundle.
     *
     * @param currentBundleContext The bundle context used to get the system bundle.
     * @return The system bundle.
     * @since 3.0
     */
    public static Bundle getSystemBundle(BundleContext currentBundleContext) {
        return currentBundleContext.getBundle(SYSTEM_BUNDLE_ID);
    }

    /**
     * Returns the system bundle context.
     *
     * @param currentBundle The current bundle used to get the system bundle.
     * @return The system bundle context.
     * @throws IllegalStateException if the current bundle is not in the STARTING, ACTIVE or STOPPING states.
     * @since 3.0
     */
    public static BundleContext getSystemBundleContext(Bundle currentBundle) {
        switch (currentBundle.getState()) {
            case Bundle.STARTING:
            case Bundle.ACTIVE:
            case Bundle.STOPPING:
                return getSystemBundleContext(currentBundle.getBundleContext());
            default:
                throw new IllegalStateException(
                        "Cannot get system bundle context when bundle is not in the STARTING, ACTIVE or STOPPING states.");
        }
    }

    /**
     * Returns the system bundle context.
     *
     * @param currentBundleContext The bundle context used to get the system bundle.
     * @return The system bundle context.
     * @since 3.0
     */
    public static BundleContext getSystemBundleContext(BundleContext currentBundleContext) {
        return getSystemBundle(currentBundleContext).getBundleContext();
    }
}
