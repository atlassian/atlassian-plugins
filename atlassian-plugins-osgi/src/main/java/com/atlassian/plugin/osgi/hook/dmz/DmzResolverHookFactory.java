package com.atlassian.plugin.osgi.hook.dmz;

import java.util.Collection;

import org.osgi.framework.hooks.resolver.ResolverHook;
import org.osgi.framework.hooks.resolver.ResolverHookFactory;
import org.osgi.framework.wiring.BundleRevision;

import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.hook.dmz.packages.DmzPackagePatterns;
import com.atlassian.plugin.osgi.hook.dmz.packages.ExportTypeBasedInternalPackageDetector;
import com.atlassian.plugin.osgi.hook.dmz.packages.InternalPackageDetector;

import static java.util.Objects.requireNonNull;

/**
 * Implementation based on stash DMZ impl:
 * <a href="https://stash.atlassian.com/projects/STASH/repos/stash/browse/platform/src/main/java/com/atlassian/stash/internal/osgi/DmzResolverHookFactory.java">DmzResolverHookFactory</a>
 */
public class DmzResolverHookFactory implements ResolverHookFactory {

    private final PackageScannerConfiguration config;

    public DmzResolverHookFactory(PackageScannerConfiguration packageScannerConfig) {
        this.config = requireNonNull(packageScannerConfig, "Configuration required.");
    }

    @Override
    public ResolverHook begin(Collection<BundleRevision> triggers) {
        return createHook();
    }

    private ResolverHook createHook() {
        PluginTypeDetector pluginTypeDetector = new PluginTypeDetector(config.getApplicationBundledInternalPlugins());
        DmzPackagePatterns dmzPackagePatterns = new DmzPackagePatterns(
                config.getOsgiPublicPackages(),
                config.getOsgiPublicPackagesExcludes(),
                config.getOsgiDeprecatedPackages());
        InternalPackageDetector internalPackageDetector =
                new ExportTypeBasedInternalPackageDetector(dmzPackagePatterns, pluginTypeDetector);

        return new DmzResolverHook(
                pluginTypeDetector, internalPackageDetector, config.treatDeprecatedPackagesAsPublic());
    }
}
