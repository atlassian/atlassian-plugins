package com.atlassian.plugin.osgi.hook.dmz;

public class PackageMatcher {

    private static final String WILDCARD_PATTERN = "*";

    private final String[] pattern;
    private int currentPatternIndex = 0;
    private final String[] input;
    private int currentInputIndex = 0;
    private boolean isWildcardMode = false;

    public PackageMatcher(String pattern, String input) {
        this.pattern = pattern.split("\\.");
        this.input = input.split("\\.");
    }

    public boolean match() {
        do {
            if (pattern[currentPatternIndex].equals(WILDCARD_PATTERN)) {
                isWildcardMode = true;
            }

            boolean localSuccess = isWildcardMode ? followsWildcardPattern() : followsRegularPattern();

            if (!localSuccess) {
                return false;
            }
        } while (currentPatternIndex < pattern.length);

        // match only if we iterated through the whole input
        return currentInputIndex == input.length;
    }

    private boolean followsRegularPattern() {
        if (currentInputIndex == input.length) {
            return false;
        }

        String patternPackage = pattern[currentPatternIndex++];
        String inputPackage = input[currentInputIndex++];

        return patternPackage.equals(inputPackage);
    }

    private boolean followsWildcardPattern() {
        isWildcardMode = false;

        // wildcard is the last element, so it matches everything
        if (++currentPatternIndex == pattern.length) {
            currentInputIndex = input.length;
            return true;
        }

        String nextPatternToFind = pattern[currentPatternIndex++];
        boolean success = false;

        do {
            String nextInputPackage = input[currentInputIndex++];

            if (nextPatternToFind.equals(nextInputPackage)) {
                success = true;
            }
        } while (!success && currentInputIndex < input.length);

        return success;
    }
}
