package com.atlassian.plugin.osgi.container.impl;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Default implementation of persistent cache. Handles clearing of directories if an upgrade has been detected.
 *
 * @since 2.2.0
 */
public class DefaultOsgiPersistentCache implements OsgiPersistentCache {
    private final File osgiBundleCache;
    private final File frameworkBundleCache;
    private final File transformedPluginCache;
    private final Logger log = LoggerFactory.getLogger(DefaultOsgiPersistentCache.class);

    /**
     * Constructs a cache, using the passed file as the base directory for cache subdirectories
     *
     * @param baseDir The base directory
     */
    public DefaultOsgiPersistentCache(final File baseDir) {
        checkState(
                checkNotNull(baseDir).exists(),
                "The base directory for OSGi persistent caches should exist, %s",
                baseDir);
        osgiBundleCache = new File(baseDir, "felix");
        frameworkBundleCache = new File(baseDir, "framework-bundles");
        transformedPluginCache = new File(baseDir, "transformed-plugins");
        validate(null);
    }

    public File getFrameworkBundleCache() {
        return frameworkBundleCache;
    }

    public File getOsgiBundleCache() {
        return osgiBundleCache;
    }

    public File getTransformedPluginCache() {
        return transformedPluginCache;
    }

    public void clear() {
        try {
            FileUtils.cleanDirectory(frameworkBundleCache);
            FileUtils.cleanDirectory(osgiBundleCache);
            FileUtils.cleanDirectory(transformedPluginCache);
        } catch (final IOException e) {
            throw new OsgiContainerException("Unable to clear OSGi caches", e);
        }
    }

    public void validate(final String cacheValidationKey) {
        ensureDirectoryExists(frameworkBundleCache);
        ensureDirectoryExists(osgiBundleCache);
        ensureDirectoryExists(transformedPluginCache);

        try {
            FileUtils.cleanDirectory(osgiBundleCache);
        } catch (final IOException e) {
            throw new OsgiContainerException("Unable to clean the cache directory: " + osgiBundleCache, e);
        }

        if (cacheValidationKey != null) {
            final String newHash = Hashing.sha1()
                    .hashString(cacheValidationKey, Charsets.UTF_8)
                    .toString();
            final File versionFile = new File(transformedPluginCache, "cache.key");
            if (versionFile.exists()) {
                String oldVersion = null;
                try {
                    oldVersion = FileUtils.readFileToString(versionFile);
                } catch (final IOException e) {
                    log.debug("Unable to read cache key file", e);
                }
                if (!newHash.equals(oldVersion)) {
                    log.info("Application upgrade detected, clearing OSGi cache directories");
                    clear();
                } else {
                    return;
                }
            }

            try {
                FileUtils.writeStringToFile(versionFile, newHash);
            } catch (final IOException e) {
                log.warn("Unable to write cache key file, so will be unable to detect upgrades", e);
            }
        }
    }

    private void ensureDirectoryExists(final File dir) {
        if (dir.exists() && !dir.isDirectory()) {
            throw new IllegalArgumentException("'" + dir + "' is not a directory");
        }

        if (!dir.exists() && !dir.mkdir()) {
            throw new IllegalArgumentException("Directory '" + dir + "' cannot be created");
        }
    }
}
