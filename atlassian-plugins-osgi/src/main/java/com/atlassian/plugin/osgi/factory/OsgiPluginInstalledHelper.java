package com.atlassian.plugin.osgi.factory;

import java.io.InputStream;
import java.net.URL;
import javax.annotation.Nonnull;

import org.osgi.framework.Bundle;
import org.osgi.service.packageadmin.PackageAdmin;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.IllegalPluginStateException;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.osgi.spring.DefaultSpringContainerAccessor;
import com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor;
import com.atlassian.plugin.osgi.util.OsgiPluginUtil;
import com.atlassian.plugin.util.resource.AlternativeDirectoryResourceLoader;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.ofNullable;

/**
 * Helper class that implements the methods for an OSGi plugin that has been installed
 *
 * @since 2.2.0
 */
final class OsgiPluginInstalledHelper implements OsgiPluginHelper {
    private static final Logger logger = LoggerFactory.getLogger(OsgiPluginInstalledHelper.class);
    private final ClassLoader bundleClassLoader;
    private final Bundle bundle;
    private final PackageAdmin packageAdmin;
    private volatile ContainerAccessor containerAccessor;
    private volatile ServiceTracker[] serviceTrackers;

    /**
     * @param bundle       The bundle
     * @param packageAdmin The package admin
     */
    public OsgiPluginInstalledHelper(final Bundle bundle, final PackageAdmin packageAdmin) {
        this.bundle = checkNotNull(bundle);
        this.packageAdmin = checkNotNull(packageAdmin);
        bundleClassLoader = BundleClassLoaderAccessor.getClassLoader(bundle, new AlternativeDirectoryResourceLoader());
    }

    public Bundle getBundle() {
        return bundle;
    }

    public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
        return BundleClassLoaderAccessor.loadClass(getBundle(), clazz);
    }

    public URL getResource(final String name) {
        return bundleClassLoader.getResource(name);
    }

    public InputStream getResourceAsStream(final String name) {
        return bundleClassLoader.getResourceAsStream(name);
    }

    public ClassLoader getClassLoader() {
        return bundleClassLoader;
    }

    public Bundle install() {
        logger.debug("Not installing OSGi plugin '{}' since it's already installed.", bundle.getSymbolicName());
        throw new IllegalPluginStateException("Plugin '" + bundle.getSymbolicName() + "' has already been installed");
    }

    public void onEnable(final ServiceTracker... serviceTrackers) {
        for (final ServiceTracker svc : checkNotNull(serviceTrackers)) {
            svc.open();
        }

        this.serviceTrackers = serviceTrackers;
    }

    public void onDisable() {
        final ServiceTracker[] serviceTrackers =
                this.serviceTrackers; // cache a copy locally for multi-threaded goodness
        if (serviceTrackers != null) {
            for (final ServiceTracker svc : serviceTrackers) {
                svc.close();
            }
            this.serviceTrackers = null;
        }
        setPluginContainer(null);
    }

    public void onUninstall() {
        // Do nothing
    }

    @Nonnull
    @Override
    public PluginDependencies getDependencies() {
        if (availableForTraversal()) {
            return OsgiPluginUtil.getDependencies(bundle);
        } else {
            return new PluginDependencies(null, null, null);
        }
    }

    // TODO: It doesn't look good to try to resolve bundle here. It seems mask some lifecycle troubles,
    // my gut feeling is that we'd better to be sure that where ever dependencies need Bundle is resolved
    // already.
    //
    // However it seems to be quite a big move to fix right now, so just deprecate that method to come
    // back to that later.
    @Deprecated
    private boolean availableForTraversal() {
        /* A bundle must move from INSTALLED to RESOLVED before we can get its import */
        if (bundle.getState() == Bundle.INSTALLED) {
            logger.debug("Bundle is in INSTALLED for {}", bundle.getSymbolicName());
            if (!packageAdmin.resolveBundles(new Bundle[] {bundle})) {
                // The likely cause of this is installing a plugin without installing everything that it requires.
                logger.error("Cannot determine required plugins, cannot resolve bundle '{}'", bundle.getSymbolicName());
                return false;
            }
            logger.debug("Bundle state is now {}", bundle.getState());
        }
        return true;
    }

    public void setPluginContainer(final Object container) {
        if (container == null) {
            containerAccessor = null;
        } else if (container instanceof ContainerAccessor) {
            containerAccessor = (ContainerAccessor) container;
        } else {
            containerAccessor = new DefaultSpringContainerAccessor(container);
        }
    }

    public ContainerAccessor getContainerAccessor() {
        return containerAccessor;
    }

    /**
     * @throws IllegalPluginStateException if the plugin container is not initialized
     */
    public ContainerAccessor getRequiredContainerAccessor() {
        if (containerAccessor == null) {
            throw new IllegalStateException(
                    "Cannot create object because the plugin container is unavailable for bundle '"
                            + bundle.getSymbolicName() + "'");
        }
        return containerAccessor;
    }

    public boolean isRemotePlugin() {
        return ofNullable(getBundle().getHeaders())
                .map(headers -> headers.get(OsgiPlugin.REMOTE_PLUGIN_KEY) != null)
                .orElse(false);
    }
}
