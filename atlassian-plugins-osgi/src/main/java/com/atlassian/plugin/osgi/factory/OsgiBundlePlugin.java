package com.atlassian.plugin.osgi.factory;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.jar.Manifest;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.SynchronousBundleListener;
import org.osgi.service.packageadmin.PackageAdmin;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.IllegalPluginStateException;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.impl.AbstractPlugin;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor;
import com.atlassian.plugin.osgi.util.OsgiPluginUtil;
import com.atlassian.plugin.util.resource.AlternativeDirectoryResourceLoader;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.extractOsgiPluginInformation;
import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getAttributeWithoutValidation;
import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getManifest;

/**
 * Plugin that wraps an OSGi bundle
 * <p>
 * That kind of plugins might have or do not have plugin descriptor and must manage internal IoC by themselves
 * if it is present. Note that some modules require access to plugin IoC to work properly so it is plugin
 * responsibility to provide a such access and manage internal container lifecycle.
 * <p>
 * To export any container to PluginsFramework bundle should export ContainerAccessor interface implementation
 * as a service before move to ACTIVE state. Behavior is not defined if plugin provides more then one instances of
 * ContainerAccessor
 *
 * Plugin of that type has lifecycle duplicates underlying OSGi Bundle natural states
 */
public class OsgiBundlePlugin extends AbstractPlugin
        implements OsgiBackedPlugin, ContainerManagedPlugin, SynchronousBundleListener {
    private static final Logger log = LoggerFactory.getLogger(OsgiBundlePlugin.class);

    /*
     * Notes on concurrency:
     * All methods with *Internal suffix are called from corresponding wrappers from the base class,
     * i.e installInternal from install. Wrapper call takes care about plugin state update which is effectively
     * volatile variable write (AtomicReference.set). That write is a guarantee of all internal state changes safe
     * publication.
     *
     * However methods are *not* thread safe, as there is no protection against race execution. In some cases
     * that won't lead to any problems except not optimal resource management but special care should be taken in
     * each particular scenario to guarantee data structures valid states
     *
     * Make sure that if any of *Internal method returns PENDING state it takes care to make changes safely
     * published in terms of concurrency
     */

    private final Date dateLoaded;

    /**
     * The OSGi container manager, gateway to underlaying OSGi container
     */
    private OsgiContainerManager osgiContainerManager;

    /**
     * The OSGi bundle, which will be null until installInternal is called.
     */
    private volatile Bundle bundle;

    /**
     * The ClassLoader for the OSGi bundle, which will be null until installInternal is called.
     */
    private ClassLoader bundleClassLoader;

    /*
     * Service exported by bundle to provide access to internal IoC.
     * Field gets initialized on the first access
     */
    private @Nullable ServiceTracker<ContainerAccessor, ContainerAccessor> containerAccessorTracker;

    /*
     * Service to be used to calculate bundle wiring and extract plugin dependencies in terms of
     * Atlassian Plugins Framework
     */
    private @Nullable ServiceTracker<PackageAdmin, PackageAdmin> pkgAdminService;

    private OsgiBundlePlugin(final String pluginKey, final PluginArtifact pluginArtifact) {
        super(checkNotNull(pluginArtifact));
        this.dateLoaded = new Date();
        setPluginsVersion(2);
        setKey(pluginKey);
        setSystemPlugin(false);
    }

    @Override
    public Bundle getBundle() {
        if (bundle == null) {
            throw new IllegalPluginStateException(
                    "This operation must occur while the plugin '" + getKey() + "' is installed");
        }

        return bundle;
    }

    /**
     * Create a plugin wrapper which installs the bundle when the plugin is installed.
     *
     * @param osgiContainerManager the container to install into when the plugin is installed.
     * @param pluginKey            The plugin key.
     * @param pluginArtifact       The The plugin artifact to install.
     */
    public OsgiBundlePlugin(
            final OsgiContainerManager osgiContainerManager,
            final String pluginKey,
            final PluginArtifact pluginArtifact) {
        this(pluginKey, pluginArtifact);
        this.osgiContainerManager = checkNotNull(osgiContainerManager);
        // Leave bundle and bundleClassLoader null until we are installed.

        final Manifest manifest = getManifest(pluginArtifact);
        if (null != manifest) {
            setName(getAttributeWithoutValidation(manifest, Constants.BUNDLE_NAME));
            // The next false is because at the OSGi level, Bundle-Version is not required.
            setPluginInformation(extractOsgiPluginInformation(manifest, false));
        }
        // else this will get flagged as a bad jar, because it's not a bundle, later, and we can let this through
    }

    @Override
    public Date getDateLoaded() {
        return dateLoaded;
    }

    @Override
    public Date getDateInstalled() {
        long date = getPluginArtifact().toFile().lastModified();
        if (date == 0) {
            date = getDateLoaded().getTime();
        }
        return new Date(date);
    }

    @Override
    public boolean isUninstallable() {
        return true;
    }

    @Override
    public boolean isDeleteable() {
        return true;
    }

    @Override
    public boolean isDynamicallyLoaded() {
        return true;
    }

    @Override
    public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
        return BundleClassLoaderAccessor.loadClass(getBundleOrFail(), clazz);
    }

    @Override
    public URL getResource(final String name) {
        return getBundleClassLoaderOrFail().getResource(name);
    }

    @Override
    public InputStream getResourceAsStream(final String name) {
        return getBundleClassLoaderOrFail().getResourceAsStream(name);
    }

    @Override
    public void resolve() {
        // Force resolve underlaying bundle. That method gets called by PluginEnabler
        // to perform resolving of plugins set in parallel before actual dependency resolution
        if (pkgAdminService == null) {
            // Should never happens actually
            return;
        }
        PackageAdmin packageAdmin = pkgAdminService.getService();
        packageAdmin.resolveBundles(new Bundle[] {bundle});
    }

    /**
     * @see Plugin#getDependencies()
     */
    @Nonnull
    @Override
    public PluginDependencies getDependencies() {
        // OSGiBundlePlugin might depends on other bundles in the OSGI instance, some of
        // that bundles might be plugins, so it is make sense to implement method to allow
        // PluginEnabler process dependencies correctly
        if (this.getPluginState() == PluginState.UNINSTALLED) {
            throw new IllegalPluginStateException(
                    "This operation requires the plugin '" + getKey() + "' to be installed");
        }
        return OsgiPluginUtil.getDependencies(bundle);
    }

    @Override
    protected void installInternal() throws OsgiContainerException, IllegalPluginStateException {
        super.installInternal();
        if (null != osgiContainerManager) {
            // During all time when bundle exists Plugin must reflects state of OSGi bundle, listener below
            // takes care of it. Note that listener registered before any action about bundle is taken that
            // allows to trace all lifecycle changes
            osgiContainerManager.addBundleListener(this);

            // We're pending installation, so install
            final File file = pluginArtifact.toFile();
            bundle = osgiContainerManager.installBundle(file, pluginArtifact.getReferenceMode());
            bundleClassLoader =
                    BundleClassLoaderAccessor.getClassLoader(bundle, new AlternativeDirectoryResourceLoader());
            pkgAdminService = osgiContainerManager.getServiceTracker(PackageAdmin.class.getName());
        } else if (null == bundle) {
            throw new IllegalPluginStateException("Cannot reuse instance for bundle '" + getKey() + "'");
        }
        // else this could be a reinstall or not, but we can't tell, so we let it slide
    }

    @Override
    protected void uninstallInternal() {
        try {
            if (bundleIsUsable("uninstall")) {
                if (bundle.getState() != Bundle.UNINSTALLED) {
                    if (null != osgiContainerManager && osgiContainerManager.isRunning()) {
                        pkgAdminService.close();
                        osgiContainerManager.removeBundleListener(this);
                    } else {
                        log.warn(
                                "OSGi container not running or undefined: Will not remove bundle listener and will not close package admin service");
                    }
                    bundle.uninstall();
                } else {
                    // A previous uninstall aborted early ?
                    log.warn("Bundle '{}' already UNINSTALLED, but still held", getKey());
                }
                bundle = null;
                bundleClassLoader = null;
            }
        } catch (final BundleException e) {
            throw new PluginException(e);
        }
    }

    @Override
    protected PluginState enableInternal() {
        log.debug("Enabling OSGi bundled plugin '{}'", getKey());
        try {
            if (bundleIsUsable("enable")) {
                if (bundle.getHeaders().get(Constants.FRAGMENT_HOST) == null) {
                    log.debug("Plugin '{}' bundle is NOT a fragment, starting.", getKey());
                    // It is needs to give plugin time to launch Activator and register any services
                    // before to move on. During activator work, plugin might register containerAccessor
                    // service which will be used later during ModuleDescriptors activation. Note that it is not
                    // necessary to bind any listeners here as at time when Bundle#start returns Bundle is already
                    // in ACTIVE state
                    setPluginState(PluginState.ENABLING);

                    // enable() might be caused by OSGi framework if some code starts underlaying bundle directly in
                    // which case to synchronize Plugin state listener below calls enableInternal and it is necessary
                    // to skip bundle activation to avoid deadlock/cycle
                    if (bundle.getState() == Bundle.INSTALLED || bundle.getState() == Bundle.RESOLVED) {
                        log.debug("Start plugin '{}' bundle", getKey());
                        bundle.start();
                    } else {
                        log.debug(
                                "Skip plugin '{}' bundle start because of its state: {}", getKey(), bundle.getState());
                    }

                    // Bundle moved to ACTIVE state, so it is time to take care of ContainerAccessor
                    // IMPORTANT: ServiceTracker will handle ContainerAccessors provided by ALL bundles,
                    // so special care should be taken to filter only services provided by the current bundle...
                    //
                    // TODO: Instead of service tracker it is possible to bind ServiceListener and fire
                    // ContextRefresh family of events to be closer to how Spring staff works
                    containerAccessorTracker = new ServiceTracker<>(
                            bundle.getBundleContext(),
                            ContainerAccessor.class,
                            new ServiceTrackerCustomizer<ContainerAccessor, ContainerAccessor>() {

                                @Override
                                public ContainerAccessor addingService(ServiceReference<ContainerAccessor> reference) {
                                    if (reference.getBundle() == bundle) {
                                        return bundle.getBundleContext().getService(reference);
                                    }
                                    return null;
                                }

                                @Override
                                public void modifiedService(
                                        ServiceReference<ContainerAccessor> reference, ContainerAccessor service) {
                                    // Do nothing
                                }

                                @Override
                                public void removedService(
                                        ServiceReference<ContainerAccessor> reference, ContainerAccessor service) {
                                    if (reference.getBundle() == bundle) {
                                        bundle.getBundleContext().ungetService(reference);
                                    }
                                }
                            });
                    containerAccessorTracker.open();
                } else {
                    log.debug("Plugin '{}' bundle is a fragment, not doing anything.", getKey());
                }
            }
            return PluginState.ENABLED;
        } catch (final BundleException e) {
            throw new PluginException(e);
        }
    }

    @Override
    protected void disableInternal() {
        try {
            if (bundleIsUsable("disable")) {
                if (bundle.getState() == Bundle.ACTIVE) {
                    // Should never be null for ACTIVE bundle
                    if (containerAccessorTracker != null) {
                        containerAccessorTracker.close();
                    }
                    bundle.stop();
                } else {
                    log.warn("Cannot disable Bundle '{}', not ACTIVE", getKey());
                }
            }
        } catch (final BundleException e) {
            throw new PluginException(e);
        }
    }

    @Override
    public void bundleChanged(BundleEvent event) {
        // Only events about current bundle are interesting
        if (event.getBundle() != bundle) {
            return;
        }

        switch (event.getType()) {
                // Bundle has been started. There are two possible scenarios how code could get here:
                // 1. enable() has been called on plugin. In that case current state is ENABLING and no
                //    action needs, transition to ENABLED will happens inside the method
                // 2. bundle has been started by code outside of PluginsFramework. In that case plugin either
                //    in INSTALLED or DISABLED state
            case BundleEvent.STARTED:
                log.info("Plugin '{}' bundle started: {}", getKey(), getPluginState());
                if (getPluginState() != PluginState.ENABLING) {
                    enable();
                }
                break;

                // Bundle has been stopped. There are two possible code patch that leads to that situation:
                // 1. disable() was called. In that case nothing should be done, Plugin Framework proxy call and
                //    takes care to update state accordingly
                // 2. bundle has been stopped outside of Plugins Framework. In that case it is neccessary to synchronize
                //    current Plugin state
            case BundleEvent.STOPPED:
                log.info("Plugin '{}' bundle stopped: {}", getKey(), getPluginState());
                if (getPluginState() != PluginState.DISABLING) {
                    disable();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public ContainerAccessor getContainerAccessor() {
        // Check null on each call as service might come and go at any time
        ContainerAccessor result = OsgiPluginUtil.createNonExistingPluginContainer(getKey());
        if (containerAccessorTracker != null) {
            ContainerAccessor tmp = containerAccessorTracker.getService();
            if (tmp != null) {
                result = tmp;
            }
        }
        return result;
    }

    public ClassLoader getClassLoader() {
        return getBundleClassLoaderOrFail();
    }

    private String getInstallationStateExplanation() {
        return (null != osgiContainerManager) ? "not yet installed" : "already uninstalled";
    }

    private boolean bundleIsUsable(final String task) {
        if (null != bundle) {
            return true;
        } else {
            final String why = getInstallationStateExplanation();
            log.warn("Cannot {} {} bundle '{}'", task, why, getKey());
            return false;
        }
    }

    private <T> T getOrFail(final T what, final String name) {
        if (null == what) {
            throw new IllegalPluginStateException("Cannot use " + name + " of " + getInstallationStateExplanation()
                    + " '" + getKey() + "' from '" + pluginArtifact + "'");
        } else {
            return what;
        }
    }

    private Bundle getBundleOrFail() {
        return getOrFail(bundle, "bundle");
    }

    private ClassLoader getBundleClassLoaderOrFail() {
        return getOrFail(bundleClassLoader, "bundleClassLoader");
    }
}
