package com.atlassian.plugin.osgi.factory;

import java.io.InputStream;
import java.util.Set;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.parsers.CompositeDescriptorParserFactory;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.atlassian.plugin.parsers.DescriptorParserFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Descriptor parser factory that creates parsers for Osgi plugins. Must only be used with {@link OsgiPlugin} instances.
 *
 * @since 2.1.2
 */
public class OsgiPluginXmlDescriptorParserFactory implements DescriptorParserFactory, CompositeDescriptorParserFactory {
    /**
     * Gets an instance that filters the modules "component", "component-import", "module-type", "bean", and "spring"
     *
     * @param source The descriptor source
     * @return The parser
     * @throws PluginParseException
     */
    public DescriptorParser getInstance(final InputStream source, final Set<Application> applications) {
        return new OsgiPluginXmlDescriptorParser(
                checkNotNull(source, "The descriptor source must not be null"), applications);
    }

    /**
     * Gets an instance that filters the modules "component", "component-import", "module-type", "bean", and "spring"
     *
     * @param source              The descriptor source
     * @param supplementalSources extra sources describing modules
     * @return The parser
     * @throws PluginParseException
     * @since 3.2.16
     */
    public DescriptorParser getInstance(
            InputStream source, Iterable<InputStream> supplementalSources, Set<Application> applications) {
        return new OsgiPluginXmlDescriptorParser(source, supplementalSources, applications);
    }
}
