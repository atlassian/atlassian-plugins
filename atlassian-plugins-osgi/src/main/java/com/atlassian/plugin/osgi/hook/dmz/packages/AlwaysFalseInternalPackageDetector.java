package com.atlassian.plugin.osgi.hook.dmz.packages;

import org.osgi.framework.wiring.BundleCapability;

public class AlwaysFalseInternalPackageDetector implements InternalPackageDetector {
    @Override
    public boolean isInternalPackage(BundleCapability capability) {
        return false;
    }

    @Override
    public boolean isDeprecatedPackage(BundleCapability capability) {
        return false;
    }
}
