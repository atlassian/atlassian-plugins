package com.atlassian.plugin.osgi.internal.factory.transform.stage;

import java.util.Collections;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;

import com.atlassian.plugin.internal.util.PluginUtils;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformContext;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformStage;
import com.atlassian.plugin.util.validation.ValidationException;

/**
 * Transforms module-type elements into the appropriate Spring XML configuration file
 *
 * @since 2.2.0
 */
public class ModuleTypeSpringStage implements TransformStage {
    /**
     * The name of the generated Spring XML file for this stage
     */
    private static final String SPRING_XML = "META-INF/spring/atlassian-plugins-module-types.xml";

    static final String HOST_CONTAINER = "springHostContainer";
    static final String SPRING_HOST_CONTAINER = "com.atlassian.plugin.osgi.bridge.external.SpringHostContainer";

    public static final String BEAN_SOURCE = "Module Type";
    private static final String CLASS = "class";
    private static final String BEANS_CONSTRUCTOR_ARG = "beans:constructor-arg";
    private static final String INDEX = "index";

    public void execute(TransformContext context) {
        if (SpringHelper.shouldGenerateFile(context, SPRING_XML)) {
            Document doc = SpringHelper.createSpringDocument();
            Element root = doc.getRootElement();
            List<Element> elements =
                    context.getDescriptorDocument().getRootElement().elements("module-type");
            if (elements.size() > 0) {
                context.getExtraImports().add("com.atlassian.plugin.osgi.external");
                context.getExtraImports().add("com.atlassian.plugin.osgi.bridge.external");
                context.getExtraImports().add("com.atlassian.plugin");

                // create a new bean.
                Element hostContainerBean = root.addElement("beans:bean");

                // make sure the new bean id is not already in use.
                context.trackBean(HOST_CONTAINER, BEAN_SOURCE);

                hostContainerBean.addAttribute("id", HOST_CONTAINER);
                hostContainerBean.addAttribute(CLASS, SPRING_HOST_CONTAINER);

                for (Element e : elements) {
                    if (!PluginUtils.doesModuleElementApplyToApplication(
                            e, context.getApplications(), context.getInstallationMode())) {
                        continue;
                    }
                    validate(e);
                    Element bean = root.addElement("beans:bean");

                    String beanId = getBeanId(e);
                    // make sure the new bean id is not already in use.
                    context.trackBean(beanId, BEAN_SOURCE);

                    bean.addAttribute("id", beanId);
                    bean.addAttribute(CLASS, "com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory");

                    Element arg = bean.addElement(BEANS_CONSTRUCTOR_ARG);
                    arg.addAttribute(INDEX, "0");
                    arg.addAttribute("ref", HOST_CONTAINER);
                    Element arg2 = bean.addElement(BEANS_CONSTRUCTOR_ARG);
                    arg2.addAttribute(INDEX, "1");
                    Element value2 = arg2.addElement("beans:value");
                    value2.setText(e.attributeValue("key"));
                    Element arg3 = bean.addElement(BEANS_CONSTRUCTOR_ARG);
                    arg3.addAttribute(INDEX, "2");
                    Element value3 = arg3.addElement("beans:value");
                    value3.setText(e.attributeValue(CLASS));

                    Element osgiService = root.addElement("osgi:service");
                    String serviceBeanId = getBeanId(e) + "_osgiService";
                    // make sure the new bean id is not already in use.
                    context.trackBean(serviceBeanId, BEAN_SOURCE);

                    osgiService.addAttribute("id", serviceBeanId);
                    osgiService.addAttribute("ref", beanId);
                    osgiService.addAttribute("auto-export", "interfaces");
                }
            }

            if (root.elements().size() > 0) {
                context.setShouldRequireSpring(true);
                context.getFileOverrides().put(SPRING_XML, SpringHelper.documentToBytes(doc));
            }
        }
    }

    private void validate(Element element) {
        if (element.attributeValue("key") == null) {
            throw new ValidationException(
                    "There were validation errors:", Collections.singletonList("The key is required"));
        }
        if (element.attributeValue("class") == null) {
            throw new ValidationException(
                    "There were validation errors:", Collections.singletonList("The class is required"));
        }
    }

    private String getBeanId(Element e) {
        return "moduleType-" + e.attributeValue("key");
    }
}
