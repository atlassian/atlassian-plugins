package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.descriptors.RequiresRestart;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptorRequiringRestart;

/**
 * Return placeholder {@link com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor} instances marked with @RequiresRestart
 * for any descriptors in the underlying {@link ModuleDescriptorFactory} that have that annotation. This
 * is for module descriptors that won't be used yet but where we need to report the RequresRestart status
 * back.
 */
public class UnavailableModuleDescriptorRequiringRestartFallbackFactory implements ModuleDescriptorFactory {
    public static final String DESCRIPTOR_TEXT = "Support for this module is not currently installed.";
    private final ModuleDescriptorFactory underlying;

    public UnavailableModuleDescriptorRequiringRestartFallbackFactory(ModuleDescriptorFactory underlying) {
        this.underlying = underlying;
    }

    private boolean requiresRestart(String type) {
        if (underlying.hasModuleDescriptor(type)) {
            return (underlying.getModuleDescriptorClass(type).getAnnotation(RequiresRestart.class) != null);
        } else {
            return false;
        }
    }

    public UnrecognisedModuleDescriptorRequiringRestart getModuleDescriptor(String type) {
        if (hasModuleDescriptor(type)) {
            final UnrecognisedModuleDescriptorRequiringRestart descriptor =
                    new UnrecognisedModuleDescriptorRequiringRestart();
            descriptor.setErrorText(DESCRIPTOR_TEXT);
            return descriptor;
        } else {
            return null;
        }
    }

    public Class<? extends ModuleDescriptor<?>> getModuleDescriptorClass(String type) {
        if (hasModuleDescriptor(type)) {
            return UnrecognisedModuleDescriptorRequiringRestart.class;
        } else {
            return null;
        }
    }

    public boolean hasModuleDescriptor(String type) {
        return requiresRestart(type);
    }
}
