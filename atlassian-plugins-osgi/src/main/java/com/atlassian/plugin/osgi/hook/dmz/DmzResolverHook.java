package com.atlassian.plugin.osgi.hook.dmz;

import java.util.Collection;
import java.util.Iterator;

import org.osgi.framework.Bundle;
import org.osgi.framework.hooks.resolver.ResolverHook;
import org.osgi.framework.wiring.BundleCapability;
import org.osgi.framework.wiring.BundleRequirement;
import org.osgi.framework.wiring.BundleRevision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.osgi.hook.dmz.packages.InternalPackageDetector;

import static java.util.Objects.requireNonNull;

public class DmzResolverHook implements ResolverHook {

    private static final Logger LOG = LoggerFactory.getLogger(DmzResolverHook.class);

    static final String ATTR_WIRING_PACKAGE = "osgi.wiring.package";
    private final PluginTypeDetector pluginDetector;
    private final InternalPackageDetector internalPackageDetector;
    private final boolean treatDeprecatedPackagesAsPublic;

    /**
     * @since 7.3
     */
    DmzResolverHook(
            PluginTypeDetector pluginTypeDetector,
            InternalPackageDetector internalPackageDetector,
            boolean treatDeprecatedPackagesAsPublic) {
        this.pluginDetector = requireNonNull(pluginTypeDetector);
        this.internalPackageDetector = requireNonNull(internalPackageDetector);
        this.treatDeprecatedPackagesAsPublic = treatDeprecatedPackagesAsPublic;
    }

    @Override
    public void filterMatches(BundleRequirement requirement, Collection<BundleCapability> possibleExports) {
        Bundle importingPlugin = requirement.getRevision().getBundle();
        filterMatches(importingPlugin, possibleExports);
    }

    /**
     * @since 7.3
     */
    public void filterMatches(Bundle importingPlugin, Collection<BundleCapability> possibleExports) {
        boolean isExternalPlugin = !pluginDetector.isInternalPlugin(importingPlugin);

        // this is not only Atlassian Plugin, for example PluginKey:
        // org.eclipse.gemini.blueprint.core
        // org.apache.servicemix.bundles.spring-core
        String pluginKey = pluginDetector.getPluginKeyOrSymbolicName(importingPlugin);

        if (isExternalPlugin) {
            // if external plugin than go into modification
            LOG.debug("Filtering package exports to non-internal plugin {}", pluginKey);
            Iterator<BundleCapability> possibleExportsItr = possibleExports.iterator();

            // iterate over all exports
            while (possibleExportsItr.hasNext()) {
                BundleCapability bundleCapability = possibleExportsItr.next();

                // if internal capability than remove this package export
                if (internalPackageDetector.isInternalPackage(bundleCapability)) {
                    possibleExportsItr.remove();
                    LOG.warn(
                            "Package {} is internal and is not available for export to plugin {}",
                            getPackage(bundleCapability),
                            pluginKey);
                } else if (internalPackageDetector.isDeprecatedPackage(bundleCapability)) {
                    if (treatDeprecatedPackagesAsPublic) {
                        LOG.warn(
                                "Package {} is deprecated and will be made unavailable for export to plugin {} in a future release",
                                getPackage(bundleCapability),
                                pluginKey);
                    } else {
                        possibleExportsItr.remove();
                        LOG.warn(
                                "Package {} is deprecated and is not available for export to plugin {}",
                                getPackage(bundleCapability),
                                pluginKey);
                    }
                } else {
                    LOG.debug(
                            "Package {} is not internal and can be exported to plugin {}",
                            getPackage(bundleCapability),
                            pluginKey);
                }
            }
        } else {
            LOG.debug("Skipping package export filtering for internal plugin {}", pluginKey);
        }
    }

    private static Object getPackage(BundleCapability bundleCapability) {
        return bundleCapability.getAttributes().get(ATTR_WIRING_PACKAGE);
    }

    @Override
    public void end() {}

    @Override
    public void filterResolvable(Collection<BundleRevision> candidates) {}

    @Override
    public void filterSingletonCollisions(BundleCapability singleton, Collection<BundleCapability> collisions) {}
}
