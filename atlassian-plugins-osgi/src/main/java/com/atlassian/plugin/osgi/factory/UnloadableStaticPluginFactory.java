package com.atlassian.plugin.osgi.factory;

import java.io.InputStream;
import java.util.function.Predicate;

import org.apache.commons.io.IOUtils;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.factories.AbstractPluginFactory;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.internal.parsers.XmlDescriptorParserFactory;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.parsers.DescriptorParser;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Creates unloadable plugins from static plugins. Used to handle when a static plugin (version 1) is deployed
 * to a directory that only accepts OSGi plugins. This should be placed last in the chain of plugin factories and
 * only if {@link com.atlassian.plugin.factories.LegacyDynamicPluginFactory} is not used.
 *
 * @since 2.2.3
 */
public final class UnloadableStaticPluginFactory extends AbstractPluginFactory {

    private static final Predicate<Integer> IS_PLUGINS_1 = input -> input != null && input == Plugin.VERSION_1;

    private final String pluginDescriptorFileName;

    public UnloadableStaticPluginFactory(String pluginDescriptorFileName) {
        super(new XmlDescriptorParserFactory(), ImmutableSet.of());
        this.pluginDescriptorFileName = pluginDescriptorFileName;
    }

    @Override
    protected InputStream getDescriptorInputStream(PluginArtifact pluginArtifact) {
        return pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
    }

    @Override
    protected Predicate<Integer> isValidPluginsVersion() {
        return IS_PLUGINS_1;
    }

    /**
     * Creates an unloadable plugin
     *
     * @param pluginArtifact          the plugin artifact to deploy
     * @param moduleDescriptorFactory The factory for plugin modules
     * @return The instantiated and populated plugin
     * @throws PluginParseException If the descriptor cannot be parsed
     */
    public Plugin create(PluginArtifact pluginArtifact, ModuleDescriptorFactory moduleDescriptorFactory) {
        checkNotNull(pluginArtifact, "The plugin deployment unit is required");
        checkNotNull(moduleDescriptorFactory, "The module descriptor factory is required");

        UnloadablePlugin plugin;
        InputStream pluginDescriptor = null;
        try {
            pluginDescriptor = pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
            if (pluginDescriptor == null) {
                throw new PluginParseException("No descriptor found in classloader for : " + pluginArtifact);
            }

            DescriptorParser parser = descriptorParserFactory.getInstance(pluginDescriptor, ImmutableSet.of());

            plugin = new UnloadablePlugin();
            // This should be a valid plugin, it just got put in the wrong directory.
            // We'll try to do a full configure because it looks more user-friendly.
            try {
                parser.configurePlugin(moduleDescriptorFactory, plugin);
            } catch (Exception ex) {
                // Error on full configure - we'll just set the key as this is an UnloadablePlugin anyway.
                plugin.setKey(parser.getKey());
            }
            plugin.setErrorText("Unable to load the static '" + pluginArtifact
                    + "' plugin from the plugins directory. Please " + "copy this file into WEB-INF/lib and restart.");
        } finally {
            IOUtils.closeQuietly(pluginDescriptor);
        }
        return plugin;
    }

    /**
     * @deprecated since 7.5.0, use {@link #createModule(Plugin, com.atlassian.plugin.module.Element, ModuleDescriptorFactory)}
     * instead. This will be removed in atlassian-plugins 8.0.0.
     */
    @Override
    @Deprecated
    public ModuleDescriptor<?> createModule(
            final Plugin plugin, final Element module, final ModuleDescriptorFactory moduleDescriptorFactory) {
        if (plugin instanceof UnloadablePlugin) {
            throw new PluginException("cannot create modules for an UnloadablePlugin");
        }
        return null;
    }
}
