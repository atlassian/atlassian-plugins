package com.atlassian.plugin.osgi.advice;

import net.bytebuddy.asm.Advice;

import com.atlassian.plugin.util.PluginKeyStack;

/**
 * Advice used to store the pluginKey before an osgi service method is invoked. The pluginKey is then discarded after invocation.
 * The pluginKey is used to enrich instrumentation for certain osgi service usages.
 * <p>
 * The joinpoint can be found at org.eclipse.gemini.blueprint.service.importer.support.LocalBundleContextAdvice#invoke(MethodInvocation invocation)
 *
 * @since 5.7.x
 */
@SuppressWarnings("squid:S1118")
public class MethodInvocationAdvice {
    @Advice.OnMethodEnter
    public static void onEnter(@Advice.FieldValue(value = "pluginKey") String pluginKey) {
        PluginKeyStack.push(pluginKey);
    }

    @Advice.OnMethodExit(onThrowable = Throwable.class)
    public static void onExit() {
        PluginKeyStack.pop();
    }
}
