package com.atlassian.plugin.osgi.hook.dmz.packages;

import java.util.Set;

public class DmzPackagePatterns {

    private final Set<String> osgiPublicPackages;
    private final Set<String> osgiPublicPackagesExcludes;
    private final Set<String> osgiDeprecatedPackages;

    public DmzPackagePatterns(
            Set<String> osgiPublicPackages,
            Set<String> osgiPublicPackagesExcludes,
            Set<String> osgiDeprecatedPackages) {
        this.osgiPublicPackages = osgiPublicPackages;
        this.osgiPublicPackagesExcludes = osgiPublicPackagesExcludes;
        this.osgiDeprecatedPackages = osgiDeprecatedPackages;
    }

    public Set<String> getOsgiPublicPackages() {
        return osgiPublicPackages;
    }

    public Set<String> getOsgiPublicPackagesExcludes() {
        return osgiPublicPackagesExcludes;
    }

    public Set<String> getOsgiDeprecatedPackages() {
        return osgiDeprecatedPackages;
    }
}
