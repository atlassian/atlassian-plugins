package com.atlassian.plugin.osgi.internal.factory.transform.model;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.dom4j.Element;

import com.atlassian.plugin.util.validation.ValidationException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents the data in a component-import tag in the plugin descriptor
 *
 * @since 2.2.0
 */
public class ComponentImport {
    private static final String INTERFACE = "interface";
    private final String key;
    private final Set<String> interfaces;
    private final String filter;
    private final Element source;

    public ComponentImport(Element element) {
        checkNotNull(element);
        validate(element);

        this.source = element;
        this.key = element.attributeValue("key").trim();
        final String filter = element.attributeValue("filter");
        this.filter = filter != null ? filter.trim() : null;
        this.interfaces = new LinkedHashSet<>();
        if (element.attribute(INTERFACE) != null) {
            interfaces.add(element.attributeValue(INTERFACE).trim());
        } else {
            List<Element> compInterfaces = element.elements(INTERFACE);
            for (Element inf : compInterfaces) {
                interfaces.add(inf.getTextTrim());
            }
        }
    }

    public String getKey() {
        return key;
    }

    public Set<String> getInterfaces() {
        return interfaces;
    }

    public Element getSource() {
        return source;
    }

    /**
     * @return The configured ldap filter
     * @since 2.3.0
     */
    public String getFilter() {
        return filter;
    }

    private void validate(Element element) {
        if (element.attributeValue("key") == null) {
            throw new ValidationException(
                    "There were validation errors:", Collections.singletonList("The key is required"));
        }
        String interfaceAttribute = element.attributeValue(INTERFACE);

        if (interfaceAttribute != null) {
            if (interfaceAttribute.isEmpty()) {
                throwValidationException();
            }
        } else {
            Element interfaceElement = element.element(INTERFACE);
            if (interfaceElement == null) {
                throwValidationException();
            } else {
                String elementText = interfaceElement.getText();
                if (elementText == null || elementText.isEmpty()) {
                    throwValidationException();
                }
            }
        }
    }

    private void throwValidationException() {
        throw new ValidationException(
                "There were validation errors:",
                Collections.singletonList("The interface must be specified either via the 'interface'\" +\n"
                        + "\"attribute or child 'interface' elements"));
    }
}
