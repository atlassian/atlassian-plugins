package com.atlassian.plugin.osgi.container.felix;

import org.osgi.framework.hooks.weaving.WeavingHook;
import org.osgi.framework.hooks.weaving.WovenClass;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.ClassFileLocator;
import net.bytebuddy.pool.TypePool;

import com.atlassian.plugin.osgi.advice.ConstructorAdvice;
import com.atlassian.plugin.osgi.advice.MethodInvocationAdvice;

import static net.bytebuddy.matcher.ElementMatchers.isConstructor;
import static net.bytebuddy.matcher.ElementMatchers.named;

/**
 * A weaving hook used to intercept a plugin and apply the given implementation of LocalBundleContextAdvice
 * in order to store multiple plugin keys.
 *
 * @since 5.7.x
 */
public class PluginKeyWeaver implements WeavingHook {
    @Override
    public void weave(WovenClass wovenClass) {
        final String wovenClassName = wovenClass.getClassName();
        if (wovenClassName.equals("org.eclipse.gemini.blueprint.service.importer.support.LocalBundleContextAdvice")) {
            final TypePool typePool =
                    TypePool.Default.of(wovenClass.getBundleWiring().getClassLoader());
            final TypeDescription def = typePool.describe(wovenClassName).resolve();
            final ClassFileLocator locator = ClassFileLocator.Simple.of(wovenClassName, wovenClass.getBytes());

            final byte[] target = new ByteBuddy()
                    .rebase(def, locator)
                    .defineField("pluginKey", String.class)
                    .visit(Advice.to(ConstructorAdvice.class).on(isConstructor()))
                    .visit(Advice.to(MethodInvocationAdvice.class).on(named("invoke")))
                    .make()
                    .getBytes();

            wovenClass.setBytes(target);
            wovenClass.getDynamicImports().add("com.atlassian.plugin.util");
            wovenClass.getDynamicImports().add("com.atlassian.plugin.osgi.util");
            wovenClass.getDynamicImports().add("com.atlassian.plugin.osgi.container.felix");
        }
    }
}
