package com.atlassian.plugin.osgi.factory;

import java.io.File;

import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.IllegalPluginStateException;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.plugin.osgi.internal.factory.transform.JarUtils.getManifest;
import static com.atlassian.plugin.osgi.internal.factory.transform.JarUtils.hasManifestEntry;

/**
 * Helper class that implements the methods for an OSGi plugin that has not been installed.
 *
 * @since 2.2.0
 */
final class OsgiPluginUninstalledHelper extends OsgiPluginNotInstalledHelperBase {
    private static final Logger log = LoggerFactory.getLogger(OsgiPluginUninstalledHelper.class);

    private final OsgiContainerManager osgiContainerManager;
    private final PluginArtifact pluginArtifact;

    public OsgiPluginUninstalledHelper(
            final String key, final OsgiContainerManager mgr, final PluginArtifact artifact) {
        super(key);
        this.pluginArtifact = checkNotNull(artifact);
        this.osgiContainerManager = checkNotNull(mgr);
    }

    public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) {
        throw new IllegalPluginStateException(getNotInstalledMessage() + " This is probably because the module "
                + "descriptor is trying to load classes in its init() method. Move all classloading into the "
                + "enabled() method, and be sure to properly drop class and instance references in disabled().");
    }

    public Bundle install() {
        final File osgiPlugin = pluginArtifact.toFile();
        log.debug("Installing OSGi plugin '{}'", osgiPlugin);
        final Bundle bundle = osgiContainerManager.installBundle(osgiPlugin, pluginArtifact.getReferenceMode());
        final String key = getKey();
        if (!OsgiHeaderUtil.getPluginKey(bundle).equals(key)) {
            throw new IllegalArgumentException(
                    "The plugin key '" + key + "' must either match the OSGi bundle symbolic "
                            + "name (Bundle-SymbolicName) or be specified in the Atlassian-Plugin-Key manifest header");
        }
        return bundle;
    }

    protected String getNotInstalledMessage() {
        return "This operation requires the plugin '" + getKey() + "' to be installed";
    }

    public boolean isRemotePlugin() {
        return hasManifestEntry(getManifest(pluginArtifact.toFile()), OsgiPlugin.REMOTE_PLUGIN_KEY);
    }
}
