package com.atlassian.plugin.osgi.internal.factory.transform.stage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import aQute.bnd.osgi.Analyzer;
import aQute.bnd.osgi.Builder;
import aQute.bnd.osgi.Jar;

import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.internal.parsers.XmlDescriptorParser;
import com.atlassian.plugin.internal.util.PluginUtils;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.osgi.factory.transform.model.SystemExports;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformContext;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformStage;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;

import static com.atlassian.plugin.internal.util.PluginUtils.isAtlassianDevMode;

/**
 * Generates an OSGi manifest if not already defined. Should be the last stage.
 *
 * @since 2.2.0
 */
public class GenerateManifestStage implements TransformStage {
    private static final Logger log = LoggerFactory.getLogger(GenerateManifestStage.class);

    public static final String SPRING_CONTEXT = "Spring-Context";
    private static final String SPRING_CONTEXT_TIMEOUT = "timeout:=";
    private static final String SPRING_CONTEXT_DELIM = ";";
    private static final String RESOLUTION_DIRECTIVE = "resolution:";
    private static final String EXCLUDE_PLUGIN_XML = "!atlassian-plugin.xml";
    private static final String OPTIONAL_CATCHALL_KEY = "*";
    private static final Map<String, String> OPTIONAL_CATCHALL_VALUE =
            ImmutableMap.of(RESOLUTION_DIRECTIVE, "optional");

    public void execute(final TransformContext context) {
        final Joiner joiner = Joiner.on(",").skipNulls();

        try (Builder builder = new Builder()) {
            builder.setJar(context.getPluginFile());

            // We don't care about the modules, so we pass null
            final XmlDescriptorParser parser =
                    new XmlDescriptorParser(context.getDescriptorDocument(), ImmutableSet.of());

            Manifest mf;
            final Manifest contextManifest = context.getManifest();

            // OSGi instructions, such as imports/exports have already been resolved at build time
            if (isOsgiBundle(contextManifest)) {
                if (context.getExtraImports().isEmpty()) {
                    boolean modified = false;
                    mf = builder.getJar().getManifest();
                    for (final Entry<String, String> entry :
                            getRequiredOsgiHeaders(context, parser.getKey()).entrySet()) {
                        if (manifestDoesntHaveRequiredOsgiHeader(mf, entry)) {
                            mf.getMainAttributes().putValue(entry.getKey(), entry.getValue());
                            modified = true;
                        }
                    }
                    validateOsgiVersionIsValid(mf);
                    if (modified) {
                        writeManifestOverride(context, mf);
                    }
                    // skip any manifest manipulation by bnd
                    return;
                } else {
                    // Possibly necessary due to Spring XML creation

                    assertSpringAvailableIfRequired(context);
                    mf = builder.getJar().getManifest();

                    // combine the extra imports from atlassian-plugin.xml with the user supplied imports in the MF
                    Map<String, Map<String, String>> importsByPackage = addExtraImports(
                            builder.getJar().getManifest().getMainAttributes().getValue(Constants.IMPORT_PACKAGE),
                            context.getExtraImports());

                    // strip any duplicates, including any duplicates that the user may have supplied
                    importsByPackage =
                            OsgiHeaderUtil.stripDuplicatePackages(importsByPackage, parser.getKey(), "import");

                    // convert imports to a single string and put in the MF
                    final String imports = OsgiHeaderUtil.buildHeader(importsByPackage);
                    mf.getMainAttributes().putValue(Constants.IMPORT_PACKAGE, imports);

                    for (final Entry<String, String> entry :
                            getRequiredOsgiHeaders(context, parser.getKey()).entrySet()) {
                        mf.getMainAttributes().putValue(entry.getKey(), entry.getValue());
                    }
                }
            }
            // there are no OSGi instructions, we need to create them from scratch
            else {
                final PluginInformation info = parser.getPluginInformation();

                final Properties properties = new Properties();

                // Setup defaults
                for (final Entry<String, String> entry :
                        getRequiredOsgiHeaders(context, parser.getKey()).entrySet()) {
                    properties.put(entry.getKey(), entry.getValue());
                }
                // check for extra xml scan folders
                final Set<String> scanFolders = info.getModuleScanFolders();
                if (!scanFolders.isEmpty()) {
                    properties.put(OsgiPlugin.ATLASSIAN_SCAN_FOLDERS, StringUtils.join(scanFolders, ","));
                }

                properties.put(Analyzer.BUNDLE_SYMBOLICNAME, parser.getKey());
                properties.put(Analyzer.BUNDLE_VERSION, info.getVersion());
                // don't include a Require-Capability header for "osgi.ee" in the manifest; without NOEE,
                // module-info classes, for JPMS-ready libraries, can result in transformed plugins that
                // require Java 9+, even if the library itself would work correctly on Java 8
                // an example of a JPMS-ready library is javax.xml.bind:jaxb-api, which is compiled for
                // Java 7 but has a module-info.class that, by nature, must be compiled targeting Java 9
                properties.put(Analyzer.NOEE, "true");
                // remove the verbose Include-Resource entry from generated manifest
                properties.put(Analyzer.REMOVEHEADERS, Analyzer.INCLUDE_RESOURCE);

                header(properties, Analyzer.BUNDLE_DESCRIPTION, info.getDescription());
                header(properties, Analyzer.BUNDLE_NAME, parser.getKey());
                header(properties, Analyzer.BUNDLE_VENDOR, info.getVendorName());
                header(properties, Analyzer.BUNDLE_DOCURL, info.getVendorUrl());

                final List<String> bundleClassPaths = new ArrayList<>();

                // the jar root.
                bundleClassPaths.add(".");

                // inner jars. make the order deterministic here.
                final List<String> innerClassPaths = new ArrayList<>(context.getBundleClassPathJars());
                Collections.sort(innerClassPaths);
                bundleClassPaths.addAll(innerClassPaths);

                // generate bundle classpath.
                header(properties, Analyzer.BUNDLE_CLASSPATH, StringUtils.join(bundleClassPaths, ','));

                // Process any bundle instructions in atlassian-plugin.xml
                properties.putAll(context.getBndInstructions());

                // Add extra imports to the imports list
                Map<String, Map<String, String>> importsByPackage =
                        addExtraImports(properties.getProperty(Analyzer.IMPORT_PACKAGE), context.getExtraImports());

                // shuffle any package imports containing a '*' to the end, in their current order
                importsByPackage = OsgiHeaderUtil.moveStarPackageToEnd(importsByPackage, parser.getKey());

                // if there is not already an absolute '*' package import, put '*;resolution:="optional"' at the end of
                // the imports
                if (!importsByPackage.containsKey(OPTIONAL_CATCHALL_KEY)) {
                    importsByPackage.put(OPTIONAL_CATCHALL_KEY, OPTIONAL_CATCHALL_VALUE);
                }

                // strip any duplicates, including any duplicates that the user may have supplied
                // note that this is not strictly necessary, however it's a good safety net in case any duplicates were
                // found during extra import analysis
                importsByPackage = OsgiHeaderUtil.stripDuplicatePackages(importsByPackage, parser.getKey(), "import");

                // convert imports to a single string and put in the properties
                final String imports = OsgiHeaderUtil.buildHeader(importsByPackage);
                properties.put(Analyzer.IMPORT_PACKAGE, imports);

                // Add extra exports to the exports list
                if (!properties.containsKey(Analyzer.EXPORT_PACKAGE)) {
                    properties.put(Analyzer.EXPORT_PACKAGE, StringUtils.join(context.getExtraExports(), ','));
                }

                properties.put(
                        Analyzer.EXPORT_PACKAGE,
                        joiner.join(EXCLUDE_PLUGIN_XML, properties.getProperty(Analyzer.EXPORT_PACKAGE)));

                builder.setProperties(properties);
                builder.calcManifest();
                try (Jar jar = builder.build()) {
                    mf = jar.getManifest();
                }

                // We want to preserve any extra headers from the context manifest, that is, the original untransformed
                // manifest.
                // However, we don't want to clobber anything which was set by bnd, we want to warn about them being
                // skipped.
                final Attributes attributes = mf.getMainAttributes();
                for (final Entry<Object, Object> entry :
                        contextManifest.getMainAttributes().entrySet()) {
                    final Object name = entry.getKey();
                    if (attributes.containsKey(name)) {
                        log.debug(
                                "Ignoring manifest header {} from {} due to transformer override",
                                name,
                                context.getPluginArtifact());
                    } else {
                        attributes.put(name, entry.getValue());
                    }
                }
            }

            enforceHostVersionsForUnknownImports(mf, context.getSystemExports());
            validateOsgiVersionIsValid(mf);

            writeManifestOverride(context, mf);
        } catch (final Exception t) {
            throw new PluginParseException("Unable to process plugin to generate OSGi manifest", t);
        }
    }

    private Map<String, String> getRequiredOsgiHeaders(final TransformContext context, final String pluginKey) {
        final Map<String, String> props = new LinkedHashMap<>();
        props.put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, pluginKey);
        final String springHeader = getDesiredSpringContextValue(context);
        if (springHeader != null) {
            props.put(SPRING_CONTEXT, springHeader);
        }
        return props;
    }

    private String getDesiredSpringContextValue(final TransformContext context) {
        // Check for the explicit context value
        final String header = context.getManifest().getMainAttributes().getValue(SPRING_CONTEXT);
        if (header != null) {
            return ensureDefaultTimeout(header);
        }

        // Check for the spring files, as the default header value looks here
        // TODO: This is probly not correct since if there is no META-INF/spring/*.xml, it's still not spring-powered.
        if (context.getPluginArtifact().doesResourceExist("META-INF/spring/")
                || context.shouldRequireSpring()
                || context.getDescriptorDocument() != null) {
            return "*;" + SPRING_CONTEXT_TIMEOUT + PluginUtils.getDefaultEnablingWaitPeriod();
        }
        return null;
    }

    private String ensureDefaultTimeout(final String header) {
        final boolean noTimeOutSpecified =
                StringUtils.isEmpty(System.getProperty(PluginUtils.ATLASSIAN_PLUGINS_ENABLE_WAIT));

        if (noTimeOutSpecified) {
            return header;
        }
        final StringBuilder headerBuf;
        // Override existing timeout
        if (header.contains(SPRING_CONTEXT_TIMEOUT)) {
            final StringTokenizer tokenizer = new StringTokenizer(header, SPRING_CONTEXT_DELIM);
            headerBuf = new StringBuilder();
            while (tokenizer.hasMoreElements()) {
                String directive = (String) tokenizer.nextElement();
                if (directive.startsWith(SPRING_CONTEXT_TIMEOUT)) {
                    if (!directive.equals(
                            SPRING_CONTEXT_TIMEOUT + PluginUtils.DEFAULT_ATLASSIAN_PLUGINS_ENABLE_WAIT_SECONDS)) {
                        log.debug(
                                "Overriding configured timeout {} seconds",
                                directive.substring(SPRING_CONTEXT_TIMEOUT.length()));
                    }
                    directive = SPRING_CONTEXT_TIMEOUT + PluginUtils.getDefaultEnablingWaitPeriod();
                }
                headerBuf.append(directive);
                if (tokenizer.hasMoreElements()) {
                    headerBuf.append(SPRING_CONTEXT_DELIM);
                }
            }
        } else {
            // Append new timeout
            headerBuf = new StringBuilder(header);
            headerBuf.append(SPRING_CONTEXT_DELIM + SPRING_CONTEXT_TIMEOUT);
            headerBuf.append(PluginUtils.getDefaultEnablingWaitPeriod());
        }
        return headerBuf.toString();
    }

    private void validateOsgiVersionIsValid(final Manifest mf) {
        final String version = mf.getMainAttributes().getValue(Constants.BUNDLE_VERSION);
        try {
            if (Version.parseVersion(version) == Version.emptyVersion) {
                // we still consider an empty version to be bad
                throw new IllegalArgumentException();
            }
        } catch (final IllegalArgumentException ex) {
            throw new IllegalArgumentException("Plugin version '" + version + "' is required and must be able to be "
                    + "parsed as an OSGi version - MAJOR.MINOR.MICRO.QUALIFIER");
        }
    }

    private void writeManifestOverride(final TransformContext context, final Manifest mf) throws IOException {
        // Removing this field makes the generated manifest deterministic
        final Attributes.Name lastModifiedKey = new Attributes.Name("Bnd-LastModified");
        mf.getMainAttributes().remove(lastModifiedKey);

        final ByteArrayOutputStream bout = new ByteArrayOutputStream();
        mf.write(bout);
        context.getFileOverrides().put("META-INF/MANIFEST.MF", bout.toByteArray());
    }

    /**
     * Scans for any imports with no version specified and locks them into the specific version exported by the host
     * container
     *
     * @param manifest The manifest to read and manipulate
     * @param exports  The list of host exports
     */
    private void enforceHostVersionsForUnknownImports(final Manifest manifest, final SystemExports exports) {
        final String origImports = manifest.getMainAttributes().getValue(Constants.IMPORT_PACKAGE);
        if (origImports != null) {
            final StringBuilder imports = new StringBuilder();
            final Map<String, Map<String, String>> header = OsgiHeaderUtil.parseHeader(origImports);
            for (final Map.Entry<String, Map<String, String>> pkgImport : header.entrySet()) {
                String imp = null;
                if (pkgImport.getValue().isEmpty()) {
                    final String export = exports.getFullExport(pkgImport.getKey());
                    if (!export.equals(imp)) {
                        imp = export;
                    }
                }
                if (imp == null) {
                    imp = OsgiHeaderUtil.buildHeader(pkgImport.getKey(), pkgImport.getValue());
                }
                imports.append(imp);
                imports.append(",");
            }
            if (imports.length() > 0) {
                imports.deleteCharAt(imports.length() - 1);
            }

            manifest.getMainAttributes().putValue(Constants.IMPORT_PACKAGE, imports.toString());
        }
    }

    private boolean isOsgiBundle(final Manifest manifest) {
        // OSGi core spec 4.2 section 3.5.2: The Bundle-SymbolicName manifest header is a mandatory header.
        return manifest.getMainAttributes().getValue(Constants.BUNDLE_SYMBOLICNAME) != null;
    }

    private Map<String, Map<String, String>> addExtraImports(
            final String importsLine, final List<String> extraImports) {
        final Map<String, Map<String, String>> imports = OsgiHeaderUtil.parseHeader(importsLine);
        for (final String exImport : extraImports) {
            if (!exImport.startsWith("java.")) {
                // the extraImportPackage here can be in the form 'package;version=blah'. We only use the package
                // component to check if it's already required.
                final String extraImportPackage = StringUtils.split(exImport, ';')[0];

                final Map attrs = imports.get(extraImportPackage);
                // if the package is already required by the import directive supplied by plugin developer, we use the
                // supplied one.
                if (attrs != null) {
                    final Object resolution = attrs.get(RESOLUTION_DIRECTIVE);
                    if (Constants.RESOLUTION_OPTIONAL.equals(resolution)) {
                        attrs.put(RESOLUTION_DIRECTIVE, Constants.RESOLUTION_MANDATORY);
                    }
                }
                // otherwise, it is system determined.
                else {
                    imports.put(exImport, Collections.emptyMap());
                }
            }
        }
        return imports;
    }

    private boolean manifestDoesntHaveRequiredOsgiHeader(final Manifest mf, final Entry<String, String> entry) {
        if (mf.getMainAttributes().containsKey(new Attributes.Name(entry.getKey()))) {
            return !entry.getValue().equals(mf.getMainAttributes().getValue(entry.getKey()));
        }
        return true;
    }

    private static void header(final Properties properties, final String key, final Object value) {
        if (value == null) {
            return;
        }

        if (value instanceof Collection && ((Collection) value).isEmpty()) {
            return;
        }

        properties.put(key, value.toString().replaceAll("[\r\n]", ""));
    }

    private void assertSpringAvailableIfRequired(final TransformContext context) {
        if (isAtlassianDevMode() && context.shouldRequireSpring()) {
            final String header = context.getManifest().getMainAttributes().getValue(SPRING_CONTEXT);
            if (header == null) {
                log.debug(
                        "Manifest has no 'Spring-Context:' header. Prefer the header 'Spring-Context: *' in the jar '{}'.",
                        context.getPluginArtifact());
            } else if (header.contains(";timeout:=")) {
                log.warn(
                        "Manifest contains a 'Spring-Context:' header with a timeout, namely '{}'. This can cause problems as the "
                                + "timeout is server specific. Use the header 'Spring-Context: *' in the jar '{}'.",
                        header,
                        context.getPluginArtifact());
            }
            // else there's a Spring-Context with no timeout:=, which is fine.
        }
    }
}
