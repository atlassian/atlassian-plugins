package com.atlassian.plugin.osgi.factory;

import org.osgi.framework.Bundle;

import com.atlassian.plugin.IllegalPluginStateException;

/**
 * Helper class that implements the methods for an OSGi plugin that has been uninstalled after being installed.
 *
 * @since 3.0.17
 */
final class OsgiPluginDeinstalledHelper extends OsgiPluginNotInstalledHelperBase {
    private final boolean remotePlugin;

    public OsgiPluginDeinstalledHelper(final String key, final boolean remotePlugin) {
        super(key);
        this.remotePlugin = remotePlugin;
    }

    public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) {
        final String className = (null != callingClass) ? callingClass.getCanonicalName() : "null";
        throw new IllegalPluginStateException(
                " Cannot loadClass(" + clazz + ", " + className + "): " + getNotInstalledMessage()
                        + ". This is probably because the module/plugin code is continuing to execute code after "
                        + "it has been shutdown, for example from a finalize() method, or in response to a timer."
                        + " Ensure all code execution ceases after PluginDisabledEvent is received.");
    }

    public Bundle install() {
        throw new IllegalPluginStateException("Cannot reuse Plugin instance for '" + getKey() + "'");
    }

    protected String getNotInstalledMessage() {
        return "This operation must occur before the plugin '" + getKey() + "' is uninstalled";
    }

    public boolean isRemotePlugin() {
        return remotePlugin;
    }
}
