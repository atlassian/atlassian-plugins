package com.atlassian.plugin.osgi.hook.dmz;

import java.util.Set;

import org.osgi.framework.Bundle;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;

public class PluginTypeDetector {

    private final Set<String> bundledPluginKeys;

    /**
     * @since 7.3
     */
    public PluginTypeDetector(Set<String> bundledPluginKeys) {
        this.bundledPluginKeys = bundledPluginKeys;
    }

    public boolean isInternalPlugin(Bundle bundle) {
        String pluginKey = getPluginKeyOrSymbolicName(bundle);

        return pluginKey.startsWith("com.atlassian")
                ||
                // Adding "com.riadalabs" to support Assets apps
                pluginKey.startsWith("com.riadalabs")
                || bundledPluginKeys.contains(pluginKey);
    }

    public boolean isSystemBundle(Bundle bundle) {
        // According to the documentation (4 Life Cycle Layer - OSGi Core 7 ):
        // https://docs.osgi.org/specification/osgi.core/7.0.0/framework.lifecycle.html#i3070515
        // The system bundle resembles the framework object when a framework is launched,
        // but implementations are not required to use the same object for the framework
        // object and the system bundle. However, both objects must have bundle id 0,
        // same location, and bundle symbolic name.
        // The system bundle is listed in the set of installed bundles returned by BundleContext.getBundles(),
        // although it differs from other bundles in the following ways:
        // The system bundle is always assigned a bundle identifier of zero (0).
        // ...
        return bundle.getBundleId() == 0;
    }

    /**
     * Tried to use API:
     * String pluginKey = OsgiHeaderUtil.getPluginKey(importingPlugin)
     * but for some reason if OsgiPlugin.ATLASSIAN_PLUGIN_KEY is not available,
     * util returns symbolic name with version, examples:
     * org.eclipse.gemini.blueprint.core-3.0.0.M01
     * org.apache.servicemix.bundles.spring-core-5.3.26.1
     * Plugin key should not contain version.
     */
    public String getPluginKeyOrSymbolicName(Bundle bundle) {
        String atlassianPluginKey = bundle.getHeaders().get(OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
        if (atlassianPluginKey == null) {
            return bundle.getSymbolicName();
        }
        return atlassianPluginKey;
    }
}
