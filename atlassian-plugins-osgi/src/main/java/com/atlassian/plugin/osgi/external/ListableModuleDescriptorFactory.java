package com.atlassian.plugin.osgi.external;

import java.util.Set;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;

/**
 * A module descriptor factory that can list its supported module descriptors.
 *
 * @since 2.1.2
 */
public interface ListableModuleDescriptorFactory extends ModuleDescriptorFactory {
    /**
     * @return the list of descriptor keys this module factory knows about.
     * @since 3.0.0
     */
    Iterable<String> getModuleDescriptorKeys();

    Set<Class<? extends ModuleDescriptor>> getModuleDescriptorClasses();
}
