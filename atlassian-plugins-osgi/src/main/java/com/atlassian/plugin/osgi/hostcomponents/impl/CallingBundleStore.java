package com.atlassian.plugin.osgi.hostcomponents.impl;

import org.osgi.framework.Bundle;

/**
 * A store for the current bundle invoking a host component.
 * <p>
 * This should _not_ be used directly. {@link com.atlassian.plugin.osgi.hostcomponents.CallingBundleAccessor} should
 * be used instead
 *
 * @see com.atlassian.plugin.osgi.hostcomponents.CallingBundleAccessor
 */
public class CallingBundleStore {

    private static final ThreadLocal<Bundle> callingBundle = new ThreadLocal<>();

    public static Bundle get() {
        return callingBundle.get();
    }

    static void set(Bundle bundle) {
        if (bundle == null) {
            callingBundle.remove();
        } else {
            callingBundle.set(bundle);
        }
    }
}
