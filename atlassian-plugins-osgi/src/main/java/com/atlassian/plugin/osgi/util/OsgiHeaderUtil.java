package com.atlassian.plugin.osgi.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import aQute.bnd.header.OSGiHeader;
import aQute.bnd.osgi.Analyzer;
import aQute.bnd.osgi.Clazz;

import com.atlassian.annotations.Internal;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginPermission;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.osgi.util.ClassBinaryScanner.InputStreamResource;
import com.atlassian.plugin.osgi.util.ClassBinaryScanner.ScanResult;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.atlassian.plugin.util.ClassUtils;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Utilities to help create OSGi headers
 */
public class OsgiHeaderUtil {
    static Logger log = LoggerFactory.getLogger(OsgiHeaderUtil.class);
    private static final String EMPTY_OSGI_VERSION = Version.emptyVersion.toString();
    private static final String STAR_PACKAGE = "*";
    private static final String DUPLICATE_PACKAGE_SUFFIX = "~";

    /**
     * Finds all referred packages for the specified set of classes/interfaces by scanning their bytecode.
     * Packages starting with "java." are ignored.
     *
     * @param classes The set of classes/interfaces to scan
     * @return The set of referred packages
     * @throws IOException If there are any problems scanning bytecode
     * @since 5.0.0
     */
    public static Set<String> findReferredPackageNames(Collection<Class<?>> classes) throws IOException {
        if (classes == null || classes.isEmpty()) {
            return Collections.emptySet();
        }

        Set<Class> classesToScan = new HashSet<>();
        for (Class<?> clazz : classes) {
            ClassUtils.findAllTypes(clazz, classesToScan);
        }

        Set<String> referredClasses = new HashSet<>();
        Set<String> referredPackages = new HashSet<>();
        for (Class inf : classesToScan) {
            String clsName = inf.getName().replace('.', '/') + ".class";
            crawlReferenceTree(clsName, referredClasses, referredPackages, 1);
        }

        return ImmutableSet.copyOf(referredPackages);
    }

    /**
     * Finds all referred packages for host component registrations by scanning their declared interfaces' bytecode.
     *
     * @param registrations A list of host component registrations
     * @return The referred package map ( package-&gt; version ).
     * @throws IOException If there are any problems scanning bytecode
     * @since 2.7.0
     */
    public static Map<String, String> findReferredPackageVersions(
            List<HostComponentRegistration> registrations, Map<String, String> packageVersions) throws IOException {
        if (registrations == null || registrations.isEmpty()) {
            return Collections.emptyMap();
        }

        Set<Class<?>> declaredInterfaces = registrations.stream()
                .flatMap(reg -> Arrays.stream(reg.getMainInterfaceClasses()))
                .collect(Collectors.toSet());
        return matchPackageVersions(findReferredPackageNames(declaredInterfaces), packageVersions);
    }

    static Map<String, String> matchPackageVersions(Set<String> packageNames, Map<String, String> packageVersions) {
        Map<String, String> output = new HashMap<>();

        for (String pkg : packageNames) {
            String version = packageVersions.get(pkg);

            String effectiveValue = EMPTY_OSGI_VERSION;

            if (version != null) {
                try {
                    Version.parseVersion(version);
                    effectiveValue = version;
                } catch (IllegalArgumentException ex) {
                    log.info("Unable to parse version: {}", version);
                }
            }
            output.put(pkg, effectiveValue);
        }

        return ImmutableMap.copyOf(output);
    }

    /**
     * Helps filter "java." packages.
     */
    private static final Predicate<String> JAVA_PACKAGE_FILTER = pkg -> !pkg.startsWith("java.");

    /**
     * Helps filter class entries under "java." packages.
     */
    private static final Predicate<String> JAVA_CLASS_FILTER = classEntry -> !classEntry.startsWith("java/");

    /**
     * This will crawl the class interfaces to the desired level.
     *
     * @param className      name of the class.
     * @param scannedClasses set of classes that have been scanned.
     * @param packageImports set of imports that have been found.
     * @param level          depth of scan (recursion).
     * @throws IOException error loading a class.
     */
    static void crawlReferenceTree(String className, Set<String> scannedClasses, Set<String> packageImports, int level)
            throws IOException {
        if (level <= 0) {
            return;
        }

        if (className.startsWith("java/")) return;

        if (scannedClasses.contains(className)) return;
        else scannedClasses.add(className);

        if (log.isDebugEnabled()) log.debug("Crawling {}", className);

        InputStream in = ClassLoaderUtils.getResourceAsStream(className, OsgiHeaderUtil.class);
        if (in == null) {
            log.error("Cannot find class: [{}]", className);
            return;
        }
        try (InputStreamResource classBinaryResource = new InputStreamResource(in)) {
            // look for the class binary by asking class loader.

            // read the class binary and scan it.
            final ScanResult scanResult =
                    ClassBinaryScanner.scanClassBinary(new Clazz(new Analyzer(), className, classBinaryResource));

            // remember all the imported packages. ignore java packages.
            scanResult.getReferredPackages().stream()
                    .filter(JAVA_PACKAGE_FILTER)
                    .forEach(packageImports::add);

            // crawl
            Set<String> referredClasses = scanResult.getReferredClasses().stream()
                    .filter(JAVA_CLASS_FILTER)
                    .collect(Collectors.toSet());
            for (String ref : referredClasses) {
                crawlReferenceTree(ref + ".class", scannedClasses, packageImports, level - 1);
            }
        }
    }

    /**
     * Parses an OSGi header line into a map structure
     *
     * @param header The header line
     * @return A map with the key the entry value and the value a map of attributes
     * @since 2.2.0
     */
    public static Map<String, Map<String, String>> parseHeader(String header) {
        return new LinkedHashMap<>(OSGiHeader.parseHeader(header).asMapMap());
    }

    /**
     * Builds the header string from a map
     *
     * @param values The header values
     * @return A string, suitable for inclusion into an OSGI header string
     * @since 2.6
     */
    public static String buildHeader(Map<String, Map<String, String>> values) {
        StringBuilder header = new StringBuilder();
        for (Iterator<Map.Entry<String, Map<String, String>>> i =
                        values.entrySet().iterator();
                i.hasNext(); ) {
            Map.Entry<String, Map<String, String>> entry = i.next();
            buildHeader(entry.getKey(), entry.getValue(), header);
            if (i.hasNext()) {
                header.append(",");
            }
        }
        return header.toString();
    }

    /**
     * Builds the header string from a map
     *
     * @param key   The header value
     * @param attrs The map of attributes
     * @return A string, suitable for inclusion into an OSGI header string
     * @since 2.2.0
     */
    public static String buildHeader(String key, Map<String, String> attrs) {
        StringBuilder fullPkg = new StringBuilder();
        buildHeader(key, attrs, fullPkg);
        return fullPkg.toString();
    }

    /**
     * Builds the header string from a map
     *
     * @since 2.6
     */
    private static void buildHeader(String key, Map<String, String> attrs, StringBuilder builder) {
        builder.append(key);
        if (attrs != null && !attrs.isEmpty()) {
            for (Map.Entry<String, String> entry : attrs.entrySet()) {
                builder.append(";");
                builder.append(entry.getKey());
                builder.append("=\"");
                builder.append(entry.getValue());
                builder.append("\"");
            }
        }
    }

    /**
     * Gets the plugin key from the bundle
     *
     * WARNING: shamelessly copied at {@link com.atlassian.plugin.osgi.bridge.PluginBundleUtils}, which can't use
     * this class due to creating a cyclic build dependency. Ensure these two implementations are in sync.
     *
     * This method shouldn't be used directly. Instead consider consuming the {@link com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService}.
     *
     * @param bundle The plugin bundle
     * @return The plugin key, cannot be null
     * @since 2.2.0
     */
    public static String getPluginKey(Bundle bundle) {
        return getPluginKey(
                bundle.getSymbolicName(),
                bundle.getHeaders().get(OsgiPlugin.ATLASSIAN_PLUGIN_KEY),
                bundle.getHeaders().get(Constants.BUNDLE_VERSION));
    }

    /**
     * Obtain the plugin key for a bundle in the given File.
     *
     * @param file the file containing the bundle.
     * @return the pluginKey, or null if the bundle is malformed.
     */
    public static String getPluginKey(final File file) {
        try (JarFile jar = new JarFile(file)) {
            final Manifest manifest = jar.getManifest();
            if (manifest != null) {
                return getPluginKey(manifest);
            }
        } catch (final IOException eio) {
            log.warn("Cannot read jar file '{}': {}", file, eio.getMessage());
        }
        return null;
    }

    /**
     * Gets the plugin key from the jar manifest
     *
     * @param mf The plugin jar manifest
     * @return The plugin key, cannot be null
     * @since 2.2.0
     */
    public static String getPluginKey(Manifest mf) {
        return getPluginKey(
                getAttributeWithoutValidation(mf, Constants.BUNDLE_SYMBOLICNAME),
                getAttributeWithoutValidation(mf, OsgiPlugin.ATLASSIAN_PLUGIN_KEY),
                getAttributeWithoutValidation(mf, Constants.BUNDLE_VERSION));
    }

    private static String getPluginKey(Object bundleName, Object atlKey, Object version) {
        Object key = atlKey;
        if (key == null) {
            String bName = bundleName.toString();
            final int scPos = bName.indexOf(';');
            if (scPos > -1) {
                bName = bName.substring(0, scPos);
            }
            key = bName + "-" + version;
        }

        return key.toString();
    }

    /**
     * Generate package version string such as "com.abc;version=1.2,com.atlassian".
     * The output can be used for import or export.
     *
     * @param packages map of packagename -&gt; version.
     */
    public static String generatePackageVersionString(Map<String, String> packages) {
        if (packages == null || packages.size() == 0) {
            return "";
        }

        final StringBuilder sb = new StringBuilder();

        // add deterministism to string generation.
        List<String> packageNames = new ArrayList<>(packages.keySet());
        Collections.sort(packageNames);

        for (String packageName : packageNames) {
            sb.append(",");
            sb.append(packageName);

            String version = packages.get(packageName);

            // we can drop the version component if it's empty for a slight performance gain.
            if (version != null && !version.equals(EMPTY_OSGI_VERSION)) {
                sb.append(";version=").append(version);
            }
        }

        // delete the initial ",".
        sb.delete(0, 1);

        return sb.toString();
    }

    /**
     * Extract the attribute from manifest and validate it's not null and not empty
     *
     * @return value for the matching attribute key or will raise an Exception in case constraints are not met
     * @throws NullPointerException     if attribute value is null
     * @throws IllegalArgumentException if attribute is empty
     */
    public static String getValidatedAttribute(final Manifest manifest, String key) {
        String value = getAttributeWithoutValidation(manifest, key);
        checkNotNull(value);
        checkArgument(!value.isEmpty());
        return value;
    }

    /**
     * Extract the attribute from manifest and validate if it's not empty
     *
     * @return value for the matching attribute key
     * @throws IllegalArgumentException if attribute value is empty
     */
    public static String getNonEmptyAttribute(final Manifest manifest, String key) {
        String attributeWithoutValidation = getAttributeWithoutValidation(manifest, key);
        checkArgument(!attributeWithoutValidation.isEmpty());
        return attributeWithoutValidation;
    }

    /**
     * Extract the attribute from manifest(no-validations)
     *
     * @return value for the matching attribute key
     */
    public static String getAttributeWithoutValidation(final Manifest manifest, String key) {
        return manifest.getMainAttributes().getValue(key);
    }

    /**
     * Extract a PluginInformation from the Manifest of a jar containing an OSGi bundle.
     *
     * @param manifest the manifest to parse.
     * @return the parsed PluginInformation.
     */
    @Internal
    public static PluginInformation extractOsgiPluginInformation(
            final Manifest manifest, final boolean requireVersion) {
        final String bundleVersion = requireVersion
                ? getValidatedAttribute(manifest, Constants.BUNDLE_VERSION)
                : getAttributeWithoutValidation(manifest, Constants.BUNDLE_VERSION);
        final String bundleVendor = getAttributeWithoutValidation(manifest, Constants.BUNDLE_VENDOR);
        final String bundleDescription = getAttributeWithoutValidation(manifest, Constants.BUNDLE_DESCRIPTION);
        final PluginInformation pluginInformation = new PluginInformation();
        pluginInformation.setVersion(bundleVersion);
        pluginInformation.setDescription(bundleDescription);
        pluginInformation.setVendorName(bundleVendor);
        // OSGi plugins require execute Java.
        pluginInformation.setPermissions(ImmutableSet.of(PluginPermission.EXECUTE_JAVA));
        return pluginInformation;
    }

    /**
     * Extract the manifest of a PluginArtifact.
     *
     * @param pluginArtifact the plugin artifact who manifest is required.
     * @return the manifest from pluginArtifact, or null if the manifest is missing, cannot be read, or is corrupted.
     */
    public static Manifest getManifest(final PluginArtifact pluginArtifact) {
        try {
            final InputStream manifestStream = pluginArtifact.getResourceAsStream(JarFile.MANIFEST_NAME);
            if (manifestStream != null) {
                try {
                    return new Manifest(manifestStream);
                } catch (final IOException eio) {
                    log.error(
                            "Cannot read manifest from plugin artifact '{}': {}",
                            pluginArtifact.getName(),
                            eio.getMessage());
                } finally {
                    IOUtils.closeQuietly(manifestStream);
                }
            }
        } catch (final PluginParseException epp) {
            log.error(
                    "Cannot get manifest resource from plugin artifact '{}': {}",
                    pluginArtifact.getName(),
                    epp.getMessage());
        }
        return null;
    }

    /**
     * Reorder any Import-Package or Export-Package or Private-Package value passed, so that packages containing a
     * star-value i.e. '*' to the end, preserving their current order.
     * <p>
     * Note that any reordered packages will be logged at debug level.
     *
     * @param packages  map of package declarations, keyed by package name
     * @param pluginKey only used for debug logging
     * @return copy of packages with star packages at the end
     */
    public static Map<String, Map<String, String>> moveStarPackageToEnd(
            final Map<String, Map<String, String>> packages, final String pluginKey) {
        final Map<String, Map<String, String>> orderedPkgs = new LinkedHashMap<>();
        final Map<String, Map<String, String>> starPkgs = new LinkedHashMap<>();

        // separate into packages containing a '*' and packages without
        for (Map.Entry<String, Map<String, String>> pkg : packages.entrySet()) {
            if (pkg.getKey().contains(STAR_PACKAGE)) {
                starPkgs.put(pkg.getKey(), pkg.getValue());
            } else {
                orderedPkgs.put(pkg.getKey(), pkg.getValue());
            }
        }

        // put '*' package at the end in order
        for (Map.Entry<String, Map<String, String>> starPkg : starPkgs.entrySet()) {
            log.debug("moving {} package to end for plugin {}", starPkg.getKey(), pluginKey);
            orderedPkgs.put(starPkg.getKey(), starPkg.getValue());
        }

        return orderedPkgs;
    }

    /**
     * Remove any duplicate Import-Package or Export-Package or Private-Package value passed. Duplicates are packages
     * that end with a ~. Order is preserved.
     * <p>
     * Note that any removed packages will be logged at warning level.
     *
     * @param packages  map of package declarations, keyed by package name
     * @param pluginKey only used for debug logging
     * @param action    only used for debug logging - "import", "export" etc.
     * @return de-duplicated copy of packages
     */
    public static Map<String, Map<String, String>> stripDuplicatePackages(
            final Map<String, Map<String, String>> packages, final String pluginKey, final String action) {
        final Map<String, Map<String, String>> deduplicatedPackages = new LinkedHashMap<>();

        // add each package, skipping duplicates
        for (Map.Entry<String, Map<String, String>> pkg : packages.entrySet()) {
            if (pkg.getKey().endsWith(DUPLICATE_PACKAGE_SUFFIX)) {
                log.warn(
                        "removing duplicate {} package {} for plugin {} - it is likely that a duplicate package was supplied in the OSGi instructions in the plugin's MANIFEST.MF",
                        action,
                        pkg.getKey(),
                        pluginKey);
            } else {
                deduplicatedPackages.put(pkg.getKey(), pkg.getValue());
            }
        }

        return deduplicatedPackages;
    }
}
