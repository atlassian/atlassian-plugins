package com.atlassian.plugin.osgi.hook.dmz.packages;

import java.util.List;

import org.osgi.framework.wiring.BundleCapability;

import static java.lang.String.format;
import static java.util.Arrays.asList;

/**
 * Internal package detector that implements <i>atlassian-access</i> feature.
 * OSGi exports can define property <i>atlassian-access</i> to control the DMZ mechanism.
 * Possible values are:
 * <ul>
 *     <li><i>public</i> - package should be available for all plugins</li>
 *     <li><i>internal</i> - package should be available only for internal plugins</li>
 *     <li><i>deprecated</i> - package will be <i>internal</i> in the next major version</li>
 *     <li><i>inherited</i> - package should be examined with the default mechanism</li>
 * </ul>
 */
public class PluginInternalPackageDetector implements InternalPackageDetector {

    public static final String ATTR_ATLASSIAN_ACCESS = "atlassian-access";
    public static final String ATLASSIAN_ACCESS_PUBLIC = "public";
    public static final String ATLASSIAN_ACCESS_DEPRECATED = "deprecated";
    public static final String ATLASSIAN_ACCESS_INTERNAL = "internal";
    public static final String ATLASSIAN_ACCESS_INHERITED = "inherited";
    public static final String ATLASSIAN_ACCESS_DEFAULT = ATLASSIAN_ACCESS_INHERITED;

    public static final List<String> ATLASSIAN_ACCESS_ALLOWED_VALUES = asList(
            ATLASSIAN_ACCESS_PUBLIC, ATLASSIAN_ACCESS_DEPRECATED, ATLASSIAN_ACCESS_INTERNAL, ATLASSIAN_ACCESS_DEFAULT);

    private final InternalPackageDetector inheritedInternalPackageDetector;

    public PluginInternalPackageDetector(InternalPackageDetector inheritedInternalPackageDetector) {
        this.inheritedInternalPackageDetector = inheritedInternalPackageDetector;
    }

    @Override
    public boolean isInternalPackage(BundleCapability pluginExport) {
        String propertyValue =
                (String) pluginExport.getAttributes().getOrDefault(ATTR_ATLASSIAN_ACCESS, ATLASSIAN_ACCESS_DEFAULT);
        validateValue(propertyValue);

        if (ATLASSIAN_ACCESS_INHERITED.equals(propertyValue)) {
            return inheritedInternalPackageDetector.isInternalPackage(pluginExport);
        } else {
            return ATLASSIAN_ACCESS_INTERNAL.equals(propertyValue);
        }
    }

    @Override
    public boolean isDeprecatedPackage(BundleCapability pluginExport) {
        String propertyValue =
                (String) pluginExport.getAttributes().getOrDefault(ATTR_ATLASSIAN_ACCESS, ATLASSIAN_ACCESS_DEFAULT);
        validateValue(propertyValue);

        if (ATLASSIAN_ACCESS_INHERITED.equals(propertyValue)) {
            return inheritedInternalPackageDetector.isDeprecatedPackage(pluginExport);
        } else {
            return ATLASSIAN_ACCESS_DEPRECATED.equals(propertyValue);
        }
    }

    private void validateValue(String propertyValue) {
        if (ATLASSIAN_ACCESS_ALLOWED_VALUES.stream().noneMatch(allowedValue -> allowedValue.equals(propertyValue))) {
            throw new UnknownAtlassianAccessValue(propertyValue);
        }
    }

    public static class UnknownAtlassianAccessValue extends RuntimeException {

        public UnknownAtlassianAccessValue(String propertyValue) {
            super(format(
                    "Detected unknown value of the property '%s' set to '%s'", ATTR_ATLASSIAN_ACCESS, propertyValue));
        }
    }
}
