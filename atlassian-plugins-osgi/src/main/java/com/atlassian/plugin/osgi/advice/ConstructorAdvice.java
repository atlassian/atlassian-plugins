package com.atlassian.plugin.osgi.advice;

import org.osgi.framework.BundleContext;

import net.bytebuddy.asm.Advice;

import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;

/**
 * Advice used to capture the pluginKey of a {@link org.osgi.framework.Bundle}. This needs to be invoked when exiting the
 * constructor to ensure that the {@link BundleContext} has been initialised within the
 * org.eclipse.gemini.blueprint.service.importer.support.LocalBundleContextAdvice
 *
 * @since 5.7.x
 */
@SuppressWarnings("squid:S1118")
public class ConstructorAdvice {
    @SuppressWarnings({"squid:S1226", "squid:S1854"})
    @Advice.OnMethodExit
    public static void afterConstructor(
            @Advice.FieldValue(value = "pluginKey", readOnly = false) String pluginKey,
            @Advice.FieldValue("context") BundleContext context) {
        if (context != null) {
            pluginKey = OsgiHeaderUtil.getPluginKey(context.getBundle());
        }
    }
}
