package com.atlassian.plugin.osgi.hook.dmz.packages;

import org.osgi.framework.Bundle;
import org.osgi.framework.wiring.BundleCapability;

import com.atlassian.plugin.osgi.hook.dmz.PluginTypeDetector;

/**
 * This is a top level detector that delegates to other detectors based on the type of "Bundle" exporting the package.
 *
 * <ul>
 *     <li>exports from "System Bundle" are checked with the config patterns</li>
 *     <li>exports from "internal Plugins" are checked with use of `atlassian-access` feature</li>
 *     <li>exports from "external Plugins" are never internal or deprecated</li>
 * </ul>
 */
public class ExportTypeBasedInternalPackageDetector implements InternalPackageDetector {

    private final InternalPackageDetector forSystemBundle;
    private final InternalPackageDetector forInternalPlugins;
    private final InternalPackageDetector forExternalPlugins;

    private final PluginTypeDetector pluginTypeDetector;

    public ExportTypeBasedInternalPackageDetector(
            DmzPackagePatterns dmzPackagePatterns, PluginTypeDetector pluginTypeDetector) {
        this.forSystemBundle = new DefaultInternalPackageDetector(dmzPackagePatterns);
        this.forInternalPlugins = new PluginInternalPackageDetector(forSystemBundle);
        this.forExternalPlugins = new AlwaysFalseInternalPackageDetector();
        this.pluginTypeDetector = pluginTypeDetector;
    }

    @Override
    public boolean isInternalPackage(BundleCapability capability) {
        InternalPackageDetector internalPackageDetector = getDetectorBasedOnExportType(capability);

        return internalPackageDetector.isInternalPackage(capability);
    }

    @Override
    public boolean isDeprecatedPackage(BundleCapability capability) {
        InternalPackageDetector internalPackageDetector = getDetectorBasedOnExportType(capability);

        return internalPackageDetector.isDeprecatedPackage(capability);
    }

    private InternalPackageDetector getDetectorBasedOnExportType(BundleCapability bundleCapability) {
        Bundle bundle = bundleCapability.getRevision().getBundle();

        if (pluginTypeDetector.isSystemBundle(bundle)) {
            return forSystemBundle;
        } else if (pluginTypeDetector.isInternalPlugin(bundle)) {
            return forInternalPlugins;
        } else {
            return forExternalPlugins;
        }
    }
}
