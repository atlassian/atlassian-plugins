package com.atlassian.plugin.osgi.hook.dmz.packages;

import org.osgi.framework.wiring.BundleCapability;

public interface InternalPackageDetector {

    /**
     * Checks if package should be treated as internal and hidden behind DMZ
     * @param capability represents exported package
     * @return if package is internal
     */
    boolean isInternalPackage(BundleCapability capability);

    /**
     * Checks if package should be treated as deprecated by the DMZ mechanism
     * @param capability represents exported package
     * @return if package is deprecated
     */
    boolean isDeprecatedPackage(BundleCapability capability);
}
