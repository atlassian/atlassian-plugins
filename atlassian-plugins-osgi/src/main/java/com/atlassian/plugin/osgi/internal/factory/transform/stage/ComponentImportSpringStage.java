package com.atlassian.plugin.osgi.internal.factory.transform.stage;

import java.util.Arrays;

import org.dom4j.Document;
import org.dom4j.Element;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.internal.util.PluginUtils;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformContext;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformStage;
import com.atlassian.plugin.osgi.internal.factory.transform.model.ComponentImport;

/**
 * Transforms component imports into a Spring XML file
 *
 * @since 2.2.0
 */
public class ComponentImportSpringStage implements TransformStage {
    /**
     * Path of generated Spring XML file
     */
    private static final String SPRING_XML = "META-INF/spring/atlassian-plugins-component-imports.xml";

    public static final String BEAN_SOURCE = "Component Import";

    static Logger log = LoggerFactory.getLogger(ComponentImportSpringStage.class);

    public void execute(TransformContext context) {
        if (SpringHelper.shouldGenerateFile(context, SPRING_XML)) {
            Document springDoc = SpringHelper.createSpringDocument();
            Element root = springDoc.getRootElement();

            ServiceReference[] serviceReferences =
                    context.getOsgiContainerManager().getRegisteredServices();
            for (ComponentImport comp : context.getComponentImports().values()) {
                if (!PluginUtils.doesModuleElementApplyToApplication(
                        comp.getSource(), context.getApplications(), context.getInstallationMode())) {
                    continue;
                }
                Element osgiReference = root.addElement("osgi:reference");

                // make sure the new bean id is not already in use.
                context.trackBean(comp.getKey(), BEAN_SOURCE);

                osgiReference.addAttribute("id", comp.getKey());

                if (comp.getFilter() != null) {
                    osgiReference.addAttribute("filter", comp.getFilter());
                }

                Element interfaces = osgiReference.addElement("osgi:interfaces");
                for (String infName : comp.getInterfaces()) {
                    validateInterface(infName, context.getPluginFile().getName(), serviceReferences);
                    context.getExtraImports().add(infName.substring(0, infName.lastIndexOf('.')));
                    Element e = interfaces.addElement("beans:value");
                    e.setText(infName);
                }
            }
            if (!root.elements().isEmpty()) {
                context.setShouldRequireSpring(true);
                context.getFileOverrides().put(SPRING_XML, SpringHelper.documentToBytes(springDoc));
            }
        }
    }

    private void validateInterface(String interfaceName, String pluginName, ServiceReference[] serviceReferences) {
        if (log.isDebugEnabled() && !findInterface(interfaceName, serviceReferences)) {
            log.debug(
                    "Couldn't confirm that '{}' (used as a <component-import> in the plugin with name '{}')"
                            + " is a public component in the product's OSGi exports. If this is an interface you expect to"
                            + " be provided from the product, double check the spelling of '{}'; if this class"
                            + " is supposed to come from another plugin, you can probably ignore this warning.",
                    interfaceName,
                    pluginName,
                    interfaceName);
        }
    }

    private boolean findInterface(String interfaceName, ServiceReference[] serviceReferences) {
        return Arrays.stream(serviceReferences)
                .flatMap(ref -> Arrays.stream((String[]) ref.getProperty(Constants.OBJECTCLASS)))
                .anyMatch(interfaceName::equals);
    }
}
