package com.atlassian.plugin.osgi.container.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletContext;

import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;

/**
 * Default package scanner configuration. Probably mostly useful for Spring XML configuration.
 */
public class DefaultPackageScannerConfiguration implements PackageScannerConfiguration {

    private List<String> jarIncludes = List.of("*.jar");
    private List<String> jarExcludes = List.of();
    private List<String> packageIncludes = List.of(
            "com.atlassian.*",
            "com.ctc.wstx.stax*",
            "com.fasterxml.jackson.annotation*",
            "com.fasterxml.jackson.core*",
            "com.fasterxml.jackson.databind*",
            "com.fasterxml.jackson.dataformat.cbor*",
            "com.fasterxml.jackson.dataformat.xml*",
            "com.fasterxml.jackson.dataformat.yaml*",
            "com.fasterxml.jackson.datatype.jdk8*",
            "com.fasterxml.jackson.datatype.joda*",
            "com.fasterxml.jackson.datatype.jsr310*",
            "com.fasterxml.jackson.jaxrs.json*",
            "com.fasterxml.jackson.module.jaxb*",
            "com.fasterxml.jackson.module.paramnames*",
            "com.google.common.*",
            "com.google.gson*",
            "com.opensymphony.*",
            "com.perforce*",
            "io.atlassian.*",
            "javax.*",
            "net.bytebuddy*",
            "net.jcip.*",
            "net.minidev.json*",
            "org.apache.*",
            "org.bouncycastle*",
            "org.dom4j*",
            "org.glassfish.hk2*",
            "org.glassfish.jersey*",
            "org.jdom*",
            "org.jfree.*",
            "org.objectweb.asm*",
            "org.ofbiz.*",
            "org.quartz",
            "org.quartz.*",
            "org.slf4j*",
            "org.tuckey.web.filters.urlrewrite.*",
            "org.w3c.*",
            "org.xml.*",
            "software.amazon.awssdk.*",
            "webwork.*");
    private List<String> packageExcludes = List.of(
            // it is coming from WRM
            "com.atlassian.plugin.cache.filecache*",
            "com.atlassian.plugin.osgi.bridge.*",
            "com.atlassian.security.serialblocklist.*",
            "com.atlassian.security.serialfilter.*",
            "com.springframework*",
            "org.apache.catalina.*",
            // apparently Crowd is still using it
            "org.apache.commons.digester*",
            "org.apache.coyote.*",
            "org.apache.jasper.*",
            "org.apache.naming*",
            "org.apache.tomcat.*",
            "com.atlassian.secrets",
            "com.atlassian.secrets.aws*",
            "com.atlassian.secrets.cli*",
            "com.atlassian.secrets.service*",
            "com.atlassian.secrets.store.*",
            "com.atlassian.secrets.vault*",
            "com.atlassian.secrets.tomcat.*",
            "com.atlassian.security.serialfilter*",
            "com.atlassian.security.serialblocklist*");
    private Map<String, String> packageVersions;
    private String hostVersion;
    private ServletContext servletContext;
    private Set<String> osgiPublicPackages = Set.of("*"); // everything is public API
    private Set<String> osgiPublicPackagesExcludes = Set.of(
            "com.atlassian.cache.memory.*",
            "com.atlassian.config.internal.*",
            "com.atlassian.crowd.ao.*",
            "com.atlassian.crowd.applinks.*",
            "com.atlassian.crowd.azure.*",
            "com.atlassian.crowd.cache.*",
            "com.atlassian.crowd.cluster.*",
            "com.atlassian.crowd.config.*",
            "com.atlassian.crowd.console.*",
            "com.atlassian.crowd.core.*",
            "com.atlassian.crowd.dao.audit.processor.*",
            "com.atlassian.crowd.dao.directory.*",
            "com.atlassian.crowd.dao.direntity.*",
            "com.atlassian.crowd.dao.membership.cache.*",
            "com.atlassian.crowd.dao.scheduling.*",
            "com.atlassian.crowd.darkfeature.*",
            "com.atlassian.crowd.directory.authentication.*",
            "com.atlassian.crowd.directory.cache.*",
            "com.atlassian.crowd.directory.hybrid.*",
            "com.atlassian.crowd.directory.ldap.*",
            "com.atlassian.crowd.directory.query.*",
            "com.atlassian.crowd.directory.rest.*",
            "com.atlassian.crowd.directory.rfc4519.*",
            "com.atlassian.crowd.directory.ssl.*",
            "com.atlassian.crowd.directory.synchronisation.*",
            "com.atlassian.crowd.downloads.*",
            "com.atlassian.crowd.embedded.analytics.*",
            "com.atlassian.crowd.embedded.core.*",
            "com.atlassian.crowd.embedded.directory.*",
            "com.atlassian.crowd.embedded.event.*",
            "com.atlassian.crowd.embedded.propertyset.*",
            "com.atlassian.crowd.embedded.validator.*",
            "com.atlassian.crowd.event.listener.*",
            "com.atlassian.crowd.event.remote.*",
            "com.atlassian.crowd.feature.*",
            "com.atlassian.crowd.features.*",
            "com.atlassian.crowd.function.*",
            "com.atlassian.crowd.importer.*",
            "com.atlassian.crowd.integration.*",
            "com.atlassian.crowd.license.*",
            "com.atlassian.crowd.licensing.*",
            "com.atlassian.crowd.listener.*",
            "com.atlassian.crowd.lock.*",
            "com.atlassian.crowd.lookandfeel.*",
            "com.atlassian.crowd.manager.audit.mapper.*",
            "com.atlassian.crowd.manager.application.canonicality.*",
            "com.atlassian.crowd.manager.application.filtering.*",
            "com.atlassian.crowd.manager.application.search.*",
            "com.atlassian.crowd.manager.backup.*",
            "com.atlassian.crowd.manager.bootstrap.*",
            "com.atlassian.crowd.manager.cache.*",
            "com.atlassian.crowd.manager.cluster.*",
            "com.atlassian.crowd.manager.directory.monitor.poller.*",
            "com.atlassian.crowd.manager.directory.nestedgroups.*",
            "com.atlassian.crowd.manager.emailscan.*",
            "com.atlassian.crowd.manager.license.*",
            "com.atlassian.crowd.manager.memberships.*",
            "com.atlassian.crowd.manager.proxy.*",
            "com.atlassian.crowd.manager.recovery.*",
            "com.atlassian.crowd.manager.threadlocal.*",
            "com.atlassian.crowd.manager.token.*",
            "com.atlassian.crowd.manager.tombstone.*",
            "com.atlassian.crowd.manager.upgrade.*",
            "com.atlassian.crowd.manager.user.*",
            "com.atlassian.crowd.manager.validation.*",
            "com.atlassian.crowd.mapper.*",
            "com.atlassian.crowd.migration.*",
            "com.atlassian.crowd.model.config.*",
            "com.atlassian.crowd.password.saltgenerator.*",
            "com.atlassian.crowd.plugin.*",
            "com.atlassian.crowd.ratelimiting.*",
            "com.atlassian.crowd.scheduling.*",
            "com.atlassian.crowd.search.ldap.*",
            "com.atlassian.crowd.service.*",
            "com.atlassian.crowd.status.*",
            "com.atlassian.crowd.tombstone.*",
            "com.atlassian.crowd.upgrade.*",
            "com.atlassian.crowd.util.cache.*",
            // there is no `.*` on purpose, we want to exclude only this specific package
            "com.atlassian.crowd.util.persistence.hibernate",
            "com.atlassian.crowd.util.persistence.hibernate.connection.*",
            "com.atlassian.crowd.util.persistence.hibernate.batch.hibernate5.*",
            "com.atlassian.crowd.util.persistence.hibernate.event.*",
            "com.atlassian.crowd.util.persistence.liquibase.*",
            "com.atlassian.diagnostics.internal.*",
            "com.atlassian.fugue.*",
            "com.atlassian.lookandfeel.spi.internal.*",
            "com.atlassian.lesscss.*",
            "com.atlassian.plugin.internal.*",
            "com.atlassian.plugin.osgi.internal.*",
            "com.atlassian.plugin.web.baseconditions.*",
            "com.atlassian.plugin.web.conditions.*",
            "com.atlassian.plugin.web.descriptors.*",
            "com.atlassian.plugin.web.impl.*",
            "com.atlassian.plugin.web.model.*",
            "com.atlassian.plugin.web.renderer.*",
            "com.atlassian.plugin.webresource.*",
            "com.atlassian.sal.crowd.*",
            "com.atlassian.secrets.api.*",
            "com.atlassian.theme.internal.*",
            "com.atlassian.util.concurrent.*",
            "com.atlassian.util.profiling.*",
            "com.ctc.wstx.stax.*",
            "com.fasterxml.jackson.dataformat.cbor.*",
            "com.fasterxml.jackson.dataformat.xml.*",
            "com.fasterxml.jackson.dataformat.yaml.*",
            "com.fasterxml.jackson.datatype.jdk8.*",
            "com.fasterxml.jackson.datatype.joda.*",
            "com.fasterxml.jackson.datatype.jsr310.*",
            "com.fasterxml.jackson.jaxrs.json.*",
            "com.fasterxml.jackson.module.jaxb.*",
            "com.fasterxml.jackson.module.paramnames.*",
            "com.google.common.*",
            "com.opensymphony.*",
            "net.bytebuddy.*",
            "net.minidev.json.*",
            "org.apache.commons.pool.*",
            "org.apache.commons.pool2.*",
            "org.dom4j.*",
            "org.glassfish.hk2.*",
            "org.glassfish.jersey.*",
            "org.objectweb.asm.*",
            "org.springframework.dao.*",
            "org.springframework.jca.*",
            "org.springframework.jdbc.*",
            "org.springframework.transaction.*",
            "org.tuckey.web.filters.urlrewrite.*",
            "software.amazon.awssdk.*");
    private Set<String> osgiDeprecatedPackages = Set.of(
            "com.google.gson",
            "com.google.gson.*",
            "com.rometools.rome.*",
            "com.atlassian.analytics.api.*",
            "com.atlassian.analytics.client",
            "com.atlassian.analytics.client.api.*");
    private boolean treatDeprecatedPackagesAsPublic = true;
    private Set<String> applicationBundledInternalPlugins;

    public DefaultPackageScannerConfiguration() {
        this(null);
    }

    /**
     * @param hostVersion The current host application version
     * @since 2.2
     */
    public DefaultPackageScannerConfiguration(String hostVersion) {
        this.hostVersion = hostVersion;
        jarIncludes = new ArrayList<>(jarIncludes);
        jarExcludes = new ArrayList<>(jarExcludes);
        packageIncludes = new ArrayList<>(packageIncludes);
        packageExcludes = new ArrayList<>(packageExcludes);
        packageVersions = new HashMap<>();
        osgiPublicPackages = new HashSet<>(osgiPublicPackages);
        osgiPublicPackagesExcludes = new HashSet<>(osgiPublicPackagesExcludes);
        osgiDeprecatedPackages = new HashSet<>(osgiDeprecatedPackages);
        applicationBundledInternalPlugins = new HashSet<>();
    }

    public void setJarIncludes(List<String> jarIncludes) {
        this.jarIncludes = jarIncludes;
    }

    public void setJarExcludes(List<String> jarExcludes) {
        this.jarExcludes = jarExcludes;
    }

    public void setPackageIncludes(List<String> packageIncludes) {
        this.packageIncludes = packageIncludes;
    }

    public void setPackageExcludes(List<String> packageExcludes) {
        this.packageExcludes = packageExcludes;
    }

    /**
     * @since 7.3
     */
    public void setDeprecatedPackages(Set<String> deprecatedPackages) {
        this.osgiDeprecatedPackages = deprecatedPackages;
    }

    /**
     * @since 7.3
     */
    public void setTreatDeprecatedPackagesAsPublic(boolean treatDeprecatedPackagesAsPublic) {
        this.treatDeprecatedPackagesAsPublic = treatDeprecatedPackagesAsPublic;
    }

    /**
     * Sets the jars to include and exclude from scanning
     *
     * @param includes A list of jar patterns to include
     * @param excludes A list of jar patterns to exclude
     */
    public void setJarPatterns(List<String> includes, List<String> excludes) {
        this.jarIncludes = includes;
        this.jarExcludes = excludes;
    }

    /**
     * Sets the packages to include and exclude
     *
     * @param includes A list of patterns to include
     * @param excludes A list of patterns to exclude
     */
    public void setPackagePatterns(List<String> includes, List<String> excludes) {
        this.packageIncludes = includes;
        this.packageExcludes = excludes;
    }

    /**
     * Maps discovered packages to specific versions by overriding autodiscovered versions
     *
     * @param packageToVersions A map of package patterns to version strings
     */
    public void setPackageVersions(Map<String, String> packageToVersions) {
        this.packageVersions = packageToVersions;
    }

    public void setOsgiPublicPackages(Set<String> osgiPublicPackages) {
        this.osgiPublicPackages = osgiPublicPackages;
    }

    public void setOsgiPublicPackagesExcludes(Set<String> osgiPublicPackagesExcludes) {
        this.osgiPublicPackagesExcludes = osgiPublicPackagesExcludes;
    }

    public void setApplicationBundledInternalPlugins(Set<String> applicationBundledInternalPlugins) {
        this.applicationBundledInternalPlugins = applicationBundledInternalPlugins;
    }

    public List<String> getJarIncludes() {
        return jarIncludes;
    }

    public List<String> getJarExcludes() {
        return jarExcludes;
    }

    public List<String> getPackageIncludes() {
        return packageIncludes;
    }

    public List<String> getPackageExcludes() {
        return packageExcludes;
    }

    public Map<String, String> getPackageVersions() {
        return packageVersions;
    }

    public String getCurrentHostVersion() {
        return hostVersion;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public Set<String> getOsgiPublicPackages() {
        return osgiPublicPackages;
    }

    public Set<String> getOsgiPublicPackagesExcludes() {
        return osgiPublicPackagesExcludes;
    }

    /**
     * @since 7.3
     */
    @Override
    public Set<String> getOsgiDeprecatedPackages() {
        return osgiDeprecatedPackages;
    }

    /**
     * @since 7.3
     */
    @Override
    public boolean treatDeprecatedPackagesAsPublic() {
        return treatDeprecatedPackagesAsPublic;
    }

    public Set<String> getApplicationBundledInternalPlugins() {
        return applicationBundledInternalPlugins;
    }
}
