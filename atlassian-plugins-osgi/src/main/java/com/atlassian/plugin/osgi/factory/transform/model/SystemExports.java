package com.atlassian.plugin.osgi.factory.transform.model;

import java.util.LinkedHashMap;
import java.util.Map;

import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;

import static com.google.common.collect.Maps.transformValues;
import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.toMap;

/**
 * Encapsulates the package exports from the system bundle
 *
 * @since 2.2.0
 */
public class SystemExports {
    private static final String VERSION = "version";
    private final Map<String, Map<String, String>> exports;

    public static final SystemExports NONE = new SystemExports("");

    /**
     * Constructs an instance by parsing the exports line from the manifest
     *
     * @param exportsLine The Export-Package header value
     */
    public SystemExports(String exportsLine) {
        if (exportsLine == null) {
            exportsLine = "";
        }

        this.exports = internAttributeKeys(OsgiHeaderUtil.parseHeader(exportsLine));
    }

    /**
     * OSGi manifest header keys come from a very small set of possible strings, and are ripe for interning. The other
     * strings in this map are less suitable for interning, and so are left alone.
     */
    private static Map<String, Map<String, String>> internAttributeKeys(final Map<String, Map<String, String>> map) {
        return transformValues(map, SystemExports::internKeys);
    }

    private static Map<String, String> internKeys(final Map<String, String> innerMap) {
        return unmodifiableMap(innerMap.entrySet().stream()
                .collect(toMap(keyMapper -> keyMapper.getKey().intern(), Map.Entry::getValue)));
    }

    /**
     * Constructs a package export, taking into account any attributes on the system export, including the version.
     * The version is handled special, in that is added as an exact match, i.e. [1.0,1.0].
     *
     * @param pkg The java package
     * @return The full export line to use for a host component import
     */
    public String getFullExport(String pkg) {
        if (exports.containsKey(pkg)) {
            Map<String, String> attrs = new LinkedHashMap<>(exports.get(pkg));
            if (attrs.containsKey(VERSION)) {
                final String version = attrs.get(VERSION);
                attrs.put(VERSION, "[" + version + "," + version + "]");
            }
            return OsgiHeaderUtil.buildHeader(pkg, attrs);
        }
        return pkg;
    }

    /**
     * @param pkg The package to check
     * @return True if the package is being exported, false otherwise
     */
    public boolean isExported(String pkg) {
        return exports.containsKey(pkg);
    }
}
