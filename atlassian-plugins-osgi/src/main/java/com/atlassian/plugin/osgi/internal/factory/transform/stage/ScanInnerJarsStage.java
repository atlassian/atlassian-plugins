package com.atlassian.plugin.osgi.internal.factory.transform.stage;

import java.util.jar.JarEntry;

import com.atlassian.plugin.osgi.internal.factory.transform.TransformContext;
import com.atlassian.plugin.osgi.internal.factory.transform.TransformStage;

/**
 * The stages which scan for inner jars in attempt to create bundle classpath.
 *
 * @since 2.6.0
 */
public class ScanInnerJarsStage implements TransformStage {
    protected static final String INNER_JARS_BASE_LOCATION = "META-INF/lib/";

    public void execute(TransformContext context) {
        for (final JarEntry jarEntry : context.getPluginJarEntries()) {
            // we only want jar files under the defined base location.
            if (jarEntry.getName().startsWith(INNER_JARS_BASE_LOCATION)
                    && jarEntry.getName().endsWith(".jar")) {
                context.addBundleClasspathJar(jarEntry.getName());
            }
        }
    }
}
