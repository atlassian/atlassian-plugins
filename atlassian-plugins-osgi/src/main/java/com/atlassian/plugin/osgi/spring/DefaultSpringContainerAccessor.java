package com.atlassian.plugin.osgi.spring;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.module.ContainerAccessor;

import static com.google.common.base.Preconditions.checkState;
import static org.springframework.beans.factory.config.AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE;
import static org.springframework.beans.factory.config.AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR;

/**
 * Manages spring context access, including autowiring.
 *
 * @since 2.2.0
 */
public class DefaultSpringContainerAccessor implements ContainerAccessor {
    private final Object nativeBeanFactory;
    private final Method nativeCreateBeanMethod;
    private final Method nativeAutowireBeanPropertiesMethod;
    private final Method nativeAutowireBeanMethod;
    private final Method nativeGetBeanMethod;
    private final Method nativeGetBeanOfTypeMethod;
    private final Method nativeGetBeansOfTypeMethod;

    /**
     * Platform versions prior to 5.0 relied on autodetected wiring mode which used reflection logic to determine whether a bean should be wired through constructors or setters.
     * Although we can still autodetect and select the wiring mode like we used to, due to other changes in Spring (related to how wiring candidates are selected among raw types)
     * we can no longer support the setter based injection in all products.
     * This constant enables the old wiring mode globally and can be used to bring back the old behaviour.
     *
     */
    private static final String USE_LEGACY_WIRING_AUTODETECTION_MODE =
            "com.atlassian.plugin.legacy.wiring.autodetection.mode";

    private final boolean useLegacyWiringAutodetectionMode;

    /**
     * The autowire strategy to use when creating and wiring a bean
     * @deprecated in 5.0 for removal in 6.0, without a replacement
     */
    @Deprecated
    public enum AutowireStrategy {
        AUTOWIRE_NO,
        /**
         * Performs setter-based injection by name
         */
        AUTOWIRE_BY_NAME,

        /**
         * Performs setter-based injection by type
         */
        AUTOWIRE_BY_TYPE,

        /**
         * Performs construction-based injection by type
         */
        AUTOWIRE_BY_CONSTRUCTOR,

        /**
         * Autodetects appropriate injection by first seeing if any no-arg constructors exist.  If not, performs constructor
         * injection, and if so, autowires by type then name
         */
        AUTOWIRE_AUTODETECT
    }

    public DefaultSpringContainerAccessor(final Object applicationContext) {
        Object beanFactory = null;
        try {
            final Method m = applicationContext.getClass().getMethod("getAutowireCapableBeanFactory");
            beanFactory = m.invoke(applicationContext);
        } catch (final NoSuchMethodException e) {
            // Should never happen
            throw new PluginException(
                    "Cannot find getAutowireCapableBeanFactory method on " + applicationContext.getClass(), e);
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Cannot access getAutowireCapableBeanFactory method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
        }

        nativeBeanFactory = beanFactory;
        try {
            nativeCreateBeanMethod =
                    beanFactory.getClass().getMethod("createBean", Class.class, int.class, boolean.class);
            nativeAutowireBeanPropertiesMethod =
                    beanFactory.getClass().getMethod("autowireBeanProperties", Object.class, int.class, boolean.class);
            nativeAutowireBeanMethod = beanFactory.getClass().getMethod("autowireBean", Object.class);
            nativeGetBeanMethod = beanFactory.getClass().getMethod("getBean", String.class);
            nativeGetBeanOfTypeMethod = beanFactory.getClass().getMethod("getBean", Class.class);
            nativeGetBeansOfTypeMethod = beanFactory.getClass().getMethod("getBeansOfType", Class.class);

            checkState(Stream.of(
                            nativeGetBeansOfTypeMethod,
                            nativeAutowireBeanPropertiesMethod,
                            nativeAutowireBeanMethod,
                            nativeCreateBeanMethod,
                            nativeGetBeanMethod)
                    .allMatch(Objects::nonNull));
        } catch (final NoSuchMethodException e) {
            // Should never happen
            throw new PluginException(
                    "Cannot find one or more methods on registered bean factory: " + nativeBeanFactory, e);
        }
        useLegacyWiringAutodetectionMode = Boolean.getBoolean(USE_LEGACY_WIRING_AUTODETECTION_MODE);
    }

    private void handleSpringMethodInvocationError(final InvocationTargetException e) {
        if (e.getCause() instanceof Error) {
            throw (Error) e.getCause();
        } else if (e.getCause() instanceof RuntimeException) {
            throw (RuntimeException) e.getCause();
        } else {
            // Should never happen as Spring methods only throw runtime exceptions
            throw new PluginException("Unable to invoke createBean", e.getCause());
        }
    }

    public <T> T createBean(final Class<T> clazz) {
        try {
            final int autowiringMode =
                    useLegacyWiringAutodetectionMode ? resolveAutowiringMode(clazz) : AUTOWIRE_CONSTRUCTOR;
            return clazz.cast(nativeCreateBeanMethod.invoke(nativeBeanFactory, clazz, autowiringMode, false));
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Unable to access createBean method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
            return null;
        }
    }

    @Override
    public <T> T injectBean(final T bean) {
        Method beanAutowiringMethod = null;
        try {
            if (useLegacyWiringAutodetectionMode) {
                beanAutowiringMethod = nativeAutowireBeanPropertiesMethod;
                nativeAutowireBeanPropertiesMethod.invoke(nativeBeanFactory, bean, AUTOWIRE_BY_TYPE, false);
            } else {
                beanAutowiringMethod = nativeAutowireBeanMethod;
                nativeAutowireBeanMethod.invoke(nativeBeanFactory, bean);
            }
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Unable to access autowireBean method: " + beanAutowiringMethod, e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
        }
        return bean;
    }

    public <T> Collection<T> getBeansOfType(Class<T> interfaceClass) {
        try {
            //noinspection unchecked
            Map<String, T> beans =
                    (Map<String, T>) nativeGetBeansOfTypeMethod.invoke(nativeBeanFactory, interfaceClass);
            return beans.values();
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Unable to access getBeansOfType method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
            return null;
        }
    }

    @Override
    public <T> T getBean(final String id) {
        try {
            //noinspection unchecked
            return (T) nativeGetBeanMethod.invoke(nativeBeanFactory, id);
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Unable to access getBean method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
            return null;
        }
    }

    @Override
    public <T> T getBean(Class<T> requiredType) {
        try {
            //noinspection unchecked
            return (T) nativeGetBeanOfTypeMethod.invoke(nativeBeanFactory, requiredType);
        } catch (final IllegalAccessException e) {
            // Should never happen
            throw new PluginException("Unable to access getBean method", e);
        } catch (final InvocationTargetException e) {
            handleSpringMethodInvocationError(e);
            return null;
        }
    }

    static int resolveAutowiringMode(final Class<?> beanClass) {
        // If the class has a no-arg constructor, we assume it's meant to be wired using
        // setters+annotations.
        // Otherwise, we assume that constructor+annotation injection should be used.

        final boolean hasNoArgConstructor =
                Arrays.stream(beanClass.getConstructors()).anyMatch(cons -> cons.getParameterCount() == 0);

        return hasNoArgConstructor ? AUTOWIRE_BY_TYPE : AUTOWIRE_CONSTRUCTOR;
    }
}
