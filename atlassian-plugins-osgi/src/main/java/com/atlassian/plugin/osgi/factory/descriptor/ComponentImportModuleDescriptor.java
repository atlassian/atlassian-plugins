package com.atlassian.plugin.osgi.factory.descriptor;

import javax.annotation.Nonnull;

import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.CannotDisable;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.module.BeanPrefixModuleFactory;

/**
 * Module descriptor for OSGi service imports. Shouldn't be directly used outside providing read-only information.
 *
 * @since 2.2.0
 */
@CannotDisable
@RequirePermission(Permissions.EXECUTE_JAVA)
public final class ComponentImportModuleDescriptor extends AbstractModuleDescriptor<Object> {
    /**
     * @since 2.5.0
     */
    public ComponentImportModuleDescriptor() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) {
        super.init(plugin, element);
        checkPermissions();
    }

    public Object getModule() {
        return new BeanPrefixModuleFactory().createModule(getKey(), this);
    }
}
