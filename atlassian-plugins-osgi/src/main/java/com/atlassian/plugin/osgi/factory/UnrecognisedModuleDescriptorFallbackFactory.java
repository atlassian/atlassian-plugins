package com.atlassian.plugin.osgi.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor;

/**
 * Module descriptor factory for deferred modules. Turns every request for a module descriptor into a deferred
 * module so be sure that this factory is last in a list of factories.
 *
 * @see UnrecognisedModuleDescriptor
 * @since 2.1.2
 */
class UnrecognisedModuleDescriptorFallbackFactory implements ModuleDescriptorFactory {
    private static final Logger log = LoggerFactory.getLogger(UnrecognisedModuleDescriptorFallbackFactory.class);
    public static final String DESCRIPTOR_TEXT = "Support for this module is not currently installed.";

    public UnrecognisedModuleDescriptor getModuleDescriptor(final String type) {
        log.info("Unknown module descriptor of type {} registered as an unrecognised descriptor.", type);
        final UnrecognisedModuleDescriptor descriptor = new UnrecognisedModuleDescriptor();
        descriptor.setErrorText(DESCRIPTOR_TEXT);
        return descriptor;
    }

    public boolean hasModuleDescriptor(final String type) {
        return true;
    }

    public Class<? extends ModuleDescriptor<?>> getModuleDescriptorClass(final String type) {
        return UnrecognisedModuleDescriptor.class;
    }
}
