package com.atlassian.plugin.osgi.hook.dmz.packages;

import java.util.Optional;

import org.osgi.framework.wiring.BundleCapability;

import com.atlassian.plugin.osgi.hook.dmz.PackageMatcher;

/**
 * Internal package detector that uses sets of patterns to determine the result.
 */
public class DefaultInternalPackageDetector implements InternalPackageDetector {

    static final String ATTR_WIRING_PACKAGE = "osgi.wiring.package";

    private final DmzPackagePatterns dmzPackagePatterns;

    /**
     * @since 7.3
     */
    public DefaultInternalPackageDetector(DmzPackagePatterns dmzPackagePatterns) {
        this.dmzPackagePatterns = dmzPackagePatterns;
    }

    private Optional<String> getWiringPackage(BundleCapability capability) {
        return Optional.ofNullable(capability.getAttributes().get(ATTR_WIRING_PACKAGE))
                .filter(String.class::isInstance)
                .map(String.class::cast);
    }

    @Override
    public boolean isInternalPackage(BundleCapability capability) {
        return getWiringPackage(capability)
                .map(wiringPackage -> {
                    boolean isPublic = dmzPackagePatterns.getOsgiPublicPackages().stream()
                            .anyMatch(pattern -> new PackageMatcher(pattern, wiringPackage).match());

                    boolean isNotExcluded = dmzPackagePatterns.getOsgiPublicPackagesExcludes().stream()
                            .noneMatch(pattern -> new PackageMatcher(pattern, wiringPackage).match());

                    boolean publicPackage = isPublic && isNotExcluded;
                    return !publicPackage;
                })
                .orElse(false);
    }

    @Override
    public boolean isDeprecatedPackage(BundleCapability capability) {
        return getWiringPackage(capability)
                .map(wiringPackage -> dmzPackagePatterns.getOsgiDeprecatedPackages().stream()
                        .anyMatch(pattern -> new PackageMatcher(pattern, wiringPackage).match()))
                .orElse(false);
    }
}
