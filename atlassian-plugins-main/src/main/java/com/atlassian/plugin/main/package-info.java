/**
 * Main interface to the plugins framework providing a facade to hide the unnecessary internals.
 * <p>
 * To use the facade, construct a {@link com.atlassian.plugin.main.PluginsConfiguration} instance using the
 * {@link com.atlassian.plugin.main.PluginsConfigurationBuilder} builder class. Then, pass that instance into the
 * constructor for the {@link com.atlassian.plugin.main.AtlassianPlugins} class, and call
 * {@link com.atlassian.plugin.main.AtlassianPlugins#afterPropertiesSet()} or
 * {@link com.atlassian.plugin.main.AtlassianPlugins#getPluginSystemLifecycle}.init(). For example:
 * <pre>
 * PluginsConfiguration config = new PluginsConfigurationBuilder()
 *    .setPluginDirectory(new File("/my/plugin/directory"))
 *    .setPackagesToInclude("org.apache.*", "com.atlassian.*", "org.dom4j*")
 *    .build();
 * final AtlassianPlugins plugins = new AtlassianPlugins(config);
 * plugins.afterPropertiesSet();
 * </pre>
 * <p>
 * This code ensures only packages from Atlassian, Apache, and Dom4j are exposed to plugins. See the
 * {@link com.atlassian.plugin.main.PluginsConfigurationBuilder} for more options.
 *
 * @since 2.2.0
 */
package com.atlassian.plugin.main;
