package it.com.atlassian.plugin.main;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.atlassian.plugin.main.AtlassianPlugins;
import com.atlassian.plugin.main.PackageScannerConfigurationBuilder;
import com.atlassian.plugin.main.PluginsConfiguration;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.junit.Assert.assertEquals;

import static com.atlassian.plugin.main.PluginsConfigurationBuilder.pluginsConfiguration;
import static com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager.ATLASSIAN_BOOTDELEGATION_EXTRA;

public class TestAtlassianPlugins {

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    private File pluginDir;
    private File bundledPluginDir;
    private File frameworkBundlesDir;
    private File bundledPluginZip;
    private AtlassianPlugins plugins;

    @Before
    public void setUp() throws IOException {
        // includes the necessary components which have been injected through the
        // com.atlassian.plugin.osgi.container.felix.PluginKeyWeaver
        System.setProperty(
                ATLASSIAN_BOOTDELEGATION_EXTRA, "com.atlassian.plugin.util,com.atlassian.plugin.osgi.advice");

        final File tmpDir = temporaryFolder.newFolder("target");
        final File targetDir = new File("target");
        frameworkBundlesDir = new File(targetDir, "framework-bundles");
        bundledPluginZip = new File(targetDir, "atlassian-bundled-plugins.zip");

        pluginDir = new File(tmpDir, "plugins");
        pluginDir.mkdirs();
        bundledPluginDir = new File(tmpDir, "bundled-plugins");
        bundledPluginDir.mkdirs();
    }

    @After
    public void tearDown() {
        if (plugins != null) {
            plugins.getPluginSystemLifecycle().shutdown();
            plugins.destroy();
        }
    }

    @Test
    public void testStart() throws Exception {
        // tests
        new PluginJarBuilder().addPluginInformation("mykey", "mykey", "1.0").build(pluginDir);
        final PluginsConfiguration config = pluginsConfiguration()
                .frameworkBundleDirectory(frameworkBundlesDir)
                .pluginDirectory(pluginDir)
                .packageScannerConfiguration(new PackageScannerConfigurationBuilder()
                        .packagesToInclude("org.apache.*", "com.atlassian.*", "org.dom4j*")
                        .packagesVersions(Collections.singletonMap("org.apache.commons.logging", "1.1.3"))
                        .build())
                .build();
        plugins = new AtlassianPlugins(config);
        start(plugins);
        assertEquals(1, plugins.getPluginAccessor().getPlugins().size());
    }

    @Test
    public void testInstalledPluginCanDependOnBundledPlugin() throws Exception {
        PluginJarBuilder bundledJar = new PluginJarBuilder("bundled")
                .addFormattedResource(
                        "META-INF/MANIFEST.MF",
                        "Export-Package: com.atlassian.test.bundled",
                        "Bundle-SymbolicName: bundled",
                        "Bundle-Version: 1.0.0",
                        "Manifest-Version: 1.0",
                        "")
                .addFormattedJava(
                        "com.atlassian.test.bundled.BundledInterface",
                        "package com.atlassian.test.bundled;",
                        "public interface BundledInterface {}");
        bundledJar.build(bundledPluginDir);

        new PluginJarBuilder("installed", bundledJar.getClassLoader())
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin name='Installed Plugin' key='installed' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='installed' class='com.atlassian.test.installed.InstalledClass'/>",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        "com.atlassian.test.installed.InstalledClass",
                        "package com.atlassian.test.installed;",
                        "import com.atlassian.test.bundled.BundledInterface;",
                        "public class InstalledClass implements BundledInterface {}")
                .build(pluginDir);

        zipBundledPlugins();

        final PluginsConfiguration config = pluginsConfiguration()
                .pluginDirectory(pluginDir)
                .frameworkBundleDirectory(frameworkBundlesDir)
                .bundledPluginUrl(bundledPluginZip.toURI().toURL())
                .bundledPluginCacheDirectory(bundledPluginDir)
                .packageScannerConfiguration(new PackageScannerConfigurationBuilder()
                        .packagesToInclude("com.atlassian.*", "org.slf4j", "org.apache.commons.logging")
                        .packagesVersions(Collections.singletonMap("org.apache.commons.logging", "1.1.3"))
                        .build())
                .build();
        plugins = new AtlassianPlugins(config);
        start(plugins);
        assertEquals(2, plugins.getPluginAccessor().getEnabledPlugins().size());
    }

    private void start(AtlassianPlugins plugins) {
        plugins.afterPropertiesSet();
        plugins.getPluginSystemLifecycle().init();
    }

    private void zipBundledPlugins() throws IOException {
        ZipOutputStream zip = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(bundledPluginZip)));
        for (File bundledPlugin : bundledPluginDir.listFiles()) {
            zip.putNextEntry(new ZipEntry(bundledPlugin.getName()));
            zip.write(FileUtils.readFileToByteArray(bundledPlugin));
        }
        zip.close();
    }
}
