package com.atlassian.plugin.internal.schema.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.Iterables;

import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.internal.schema.spi.Schema;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.schema.impl.DefaultDescribedModuleDescriptorFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public final class DefaultDescribedModuleDescriptorFactoryTest {
    private DefaultDescribedModuleDescriptorFactory moduleDescriptorFactory;

    @Mock
    private HostContainer hostContainer;

    @Before
    public void setUp() {
        moduleDescriptorFactory = new DefaultDescribedModuleDescriptorFactory(hostContainer);
    }

    @Test
    public void testGetSchemaForUnknownModuleTypeReturnsNull() {
        assertNull(moduleDescriptorFactory.getSchema("type"));
    }

    @Test
    public void testGetSchemaForModuleTypeWithNoDefinedSchemaReturnsNull() {
        moduleDescriptorFactory.addModuleDescriptor("type", TestModuleDescriptor.class);
        assertNull(moduleDescriptorFactory.getSchema("type"));
    }

    @Test
    public void testGetSchemaForModuleTypeWithDefinedSchemaReturnsNotNull() {
        moduleDescriptorFactory.addModuleDescriptor("test-module-type", TestModuleDescriptor.class);
        final Schema schema = moduleDescriptorFactory.getSchema("test-module-type");
        assertNotNull(schema);
        assertTrue(Iterables.isEmpty(schema.getRequiredPermissions()));
    }

    @Test
    public void testGetSchemaForModuleTypeWithDefinedSchemaAndPermissions() {
        moduleDescriptorFactory.addModuleDescriptor(
                "test-module-type", TestModuleDescriptorWithRequirePermission.class);
        final Schema schema = moduleDescriptorFactory.getSchema("test-module-type");
        assertNotNull(schema);
        assertEquals(1, Iterables.size(schema.getRequiredPermissions()));
        assertEquals("a_required_permission", Iterables.getFirst(schema.getRequiredPermissions(), null));
    }

    private static class TestModuleDescriptor extends AbstractModuleDescriptor {
        public TestModuleDescriptor(ModuleFactory moduleFactory) {
            super(moduleFactory);
        }

        @Override
        public Object getModule() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @RequirePermission("a_required_permission")
    private static class TestModuleDescriptorWithRequirePermission extends AbstractModuleDescriptor {
        public TestModuleDescriptorWithRequirePermission(ModuleFactory moduleFactory) {
            super(moduleFactory);
        }

        @Override
        public Object getModule() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
