package com.atlassian.plugin.internal.schema.spi;

/**
 * Describes a module that will be documented in the XML schema
 */
public interface SchemaDocumented {
    String getName();

    String getDescription();
}
