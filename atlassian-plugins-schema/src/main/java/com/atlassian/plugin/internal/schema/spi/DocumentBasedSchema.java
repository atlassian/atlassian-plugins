package com.atlassian.plugin.internal.schema.spi;

import java.net.URL;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.util.resource.AlternativeResourceLoader;
import com.atlassian.security.xml.SecureXmlParserFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptySet;

/**
 * Schema based on a XML document resource.
 */
public final class DocumentBasedSchema implements Schema {
    private final String elementName;

    private final String name;
    private final String description;

    private final String path;
    private final String fileName;
    private final String complexType;
    private final String maxOccurs;
    private final Iterable<String> requiredPermissions;
    private final Iterable<String> optionalPermissions;
    private final AlternativeResourceLoader resourceLoader;
    private final SchemaTransformer schemaTransformer;

    private DocumentBasedSchema(
            String elementName,
            String name,
            String description,
            String path,
            String fileName,
            String complexType,
            String maxOccurs,
            Iterable<String> requiredPermissions,
            Iterable<String> optionalPermissions,
            AlternativeResourceLoader resourceLoader,
            SchemaTransformer schemaTransformer) {
        this.elementName = elementName;

        this.name = name == null ? IdUtils.dashesToTitle(elementName) : name;
        this.description = description == null ? "A " + name + " module" : description;

        this.path = path;
        this.fileName = fileName;
        this.complexType = complexType;
        this.maxOccurs = maxOccurs;
        this.requiredPermissions = requiredPermissions;
        this.optionalPermissions = optionalPermissions;
        this.resourceLoader = resourceLoader;
        this.schemaTransformer = schemaTransformer;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getElementName() {
        return elementName;
    }

    @Override
    public String getComplexType() {
        return complexType;
    }

    @Override
    public String getMaxOccurs() {
        return maxOccurs;
    }

    @Override
    public Iterable<String> getRequiredPermissions() {
        return requiredPermissions;
    }

    @Override
    public Iterable<String> getOptionalPermissions() {
        return optionalPermissions;
    }

    @Override
    public Document getDocument() {
        return getDocument(resourceLoader, path, schemaTransformer);
    }

    public static DynamicSchemaBuilder builder() {
        return new DynamicSchemaBuilder();
    }

    public static DynamicSchemaBuilder builder(String id) {
        return new DynamicSchemaBuilder(id);
    }

    public static class DynamicSchemaBuilder {
        private String name;
        private String description;
        private String path;
        private String fileName;
        private String elementName;
        private String complexType;
        private String maxOccurs = "unbounded";

        // default set of permissions for modules is pretty much unrestricted access to backend and front-end code
        private Iterable<String> requiredPermissions =
                ImmutableSet.of(Permissions.EXECUTE_JAVA, Permissions.GENERATE_ANY_HTML);

        private Iterable<String> optionalPermissions = emptySet();
        private AlternativeResourceLoader resourceLoader;
        private SchemaTransformer schemaTransformer = SchemaTransformer.IDENTITY;

        public DynamicSchemaBuilder() {}

        public DynamicSchemaBuilder(String elementName) {
            this.elementName = elementName;
            this.fileName = elementName + ".xsd";
            this.path = "/xsd/" + this.fileName;
            this.complexType = IdUtils.dashesToCamelCase(elementName) + "Type";
        }

        public DynamicSchemaBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public DynamicSchemaBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public DynamicSchemaBuilder setPath(String path) {
            this.path = path;
            return this;
        }

        public DynamicSchemaBuilder setFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public DynamicSchemaBuilder setElementName(String elementName) {
            this.elementName = elementName;
            return this;
        }

        public DynamicSchemaBuilder setRequiredPermissions(Iterable<String> permissions) {
            this.requiredPermissions = permissions;
            return this;
        }

        public DynamicSchemaBuilder setOptionalPermissions(Iterable<String> permissions) {
            this.optionalPermissions = permissions;
            return this;
        }

        public DynamicSchemaBuilder setComplexType(String complexType) {
            this.complexType = complexType;
            return this;
        }

        public DynamicSchemaBuilder setMaxOccurs(String maxOccurs) {
            this.maxOccurs = maxOccurs;
            return this;
        }

        public DynamicSchemaBuilder setResourceLoader(AlternativeResourceLoader resourceLoader) {
            this.resourceLoader = resourceLoader;
            return this;
        }

        public DynamicSchemaBuilder setTransformer(SchemaTransformer schemaTransformer) {
            this.schemaTransformer = schemaTransformer;
            return this;
        }

        public boolean validate() {
            return resourceLoader.getResource(path) != null;
        }

        public DocumentBasedSchema build() {
            checkNotNull(elementName);
            checkNotNull(fileName);
            checkNotNull(complexType);
            checkNotNull(resourceLoader);
            checkNotNull(requiredPermissions);
            checkNotNull(optionalPermissions);
            return new DocumentBasedSchema(
                    elementName,
                    name,
                    description,
                    path,
                    fileName,
                    complexType,
                    maxOccurs,
                    requiredPermissions,
                    optionalPermissions,
                    resourceLoader,
                    schemaTransformer);
        }
    }

    private static Document getDocument(
            AlternativeResourceLoader resourceLoader, String path, SchemaTransformer transformer) {
        final URL sourceUrl = resourceLoader.getResource(path);
        if (sourceUrl == null) {
            throw new IllegalStateException("Cannot find schema document " + path);
        }
        return transformer.transform(parseDocument(sourceUrl));
    }

    private static Document parseDocument(URL xmlUrl) {
        Document source;
        try {
            source = createSecureSaxReader().read(xmlUrl);
        } catch (DocumentException e) {
            throw new IllegalArgumentException("Unable to parse XML", e);
        }

        return source;
    }

    public static SAXReader createSecureSaxReader() {
        return new SAXReader(SecureXmlParserFactory.newXmlReader(), false);
    }
}
