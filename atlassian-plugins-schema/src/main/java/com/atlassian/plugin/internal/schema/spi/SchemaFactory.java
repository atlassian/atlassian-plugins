package com.atlassian.plugin.internal.schema.spi;

/**
 * Creates schema instances
 */
public interface SchemaFactory {
    Schema getSchema();
}
