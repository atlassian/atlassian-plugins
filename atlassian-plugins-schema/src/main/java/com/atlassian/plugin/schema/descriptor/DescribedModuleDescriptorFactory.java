package com.atlassian.plugin.schema.descriptor;

import javax.annotation.Nullable;

import com.atlassian.plugin.internal.schema.spi.Schema;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;

/**
 * A module factory that provides schemas for module types.
 *
 * @since 3.0.0
 */
public interface DescribedModuleDescriptorFactory extends ListableModuleDescriptorFactory {
    /**
     * Gets the schema for the given module type.
     *
     * @param type the module type for which we'd like to know the schema
     * @return the schema for the given module type, {@code null} if it can't be found.
     */
    @Nullable
    Schema getSchema(String type);
}
