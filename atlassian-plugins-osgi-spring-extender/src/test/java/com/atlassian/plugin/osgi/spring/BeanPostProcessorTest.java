package com.atlassian.plugin.osgi.spring;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BeanPostProcessorTest {
    private static class SomeBean {
        boolean postConstructInvoked;

        @PostConstruct
        void postConstruct() {
            postConstructInvoked = true;
        }
    }

    @Test
    public void invokesPostConstruct() {
        final DefaultListableBeanFactory bf = new DefaultListableBeanFactory();
        bf.registerBeanDefinition("bean", new RootBeanDefinition(SomeBean.class));
        final CommonAnnotationBeanFactoryPostProcessor commonAnnotationBeanFactoryPostProcessor =
                new CommonAnnotationBeanFactoryPostProcessor();

        assertFalse(bf.createBean(SomeBean.class).postConstructInvoked);
        commonAnnotationBeanFactoryPostProcessor.postProcessBeanFactory(null, bf);
        for (final BeanPostProcessor bpp :
                bf.getBeansOfType(BeanPostProcessor.class).values()) {
            bf.addBeanPostProcessor(bpp);
        }
        assertTrue(bf.createBean(SomeBean.class).postConstructInvoked);
    }
}
