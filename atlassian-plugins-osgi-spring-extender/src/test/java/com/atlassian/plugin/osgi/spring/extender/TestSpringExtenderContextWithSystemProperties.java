package com.atlassian.plugin.osgi.spring.extender;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
@ContextConfiguration(
        initializers = TestSpringExtenderContextWithSystemProperties.class,
        locations = "classpath:/META-INF/spring/extender/extender-configuration.xml")
public class TestSpringExtenderContextWithSystemProperties
        implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Autowired
    @Qualifier("extenderProperties")
    private Properties extenderProperties;

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        System.setProperty("com.atlassian.plugin.shutdown.wait.time.millis", "10000");
    }

    @Test
    public void testSystemPropertyShutdownWaitTime() {
        assertThat(extenderProperties.getProperty("shutdown.wait.time"), equalTo("10000"));
    }

    @AfterClass
    public static void afterClass() {
        System.clearProperty("com.atlassian.plugin.shutdown.wait.time.millis");
    }
}
