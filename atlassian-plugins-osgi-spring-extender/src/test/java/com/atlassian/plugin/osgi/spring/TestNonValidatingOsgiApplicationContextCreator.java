package com.atlassian.plugin.osgi.spring;

import java.util.Collections;

import org.eclipse.gemini.blueprint.extender.support.ApplicationContextConfiguration;
import org.junit.Test;
import org.osgi.framework.Bundle;

import com.atlassian.plugin.osgi.spring.external.ApplicationContextPreProcessor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestNonValidatingOsgiApplicationContextCreator {

    @Test
    public void testIsSpringPoweredNormal() {
        ApplicationContextConfiguration config = mock(ApplicationContextConfiguration.class);
        Bundle bundle = mock(Bundle.class);

        NonValidatingOsgiApplicationContextCreator creator =
                new NonValidatingOsgiApplicationContextCreator(Collections.emptyList());
        when(config.isSpringPoweredBundle()).thenReturn(true);
        assertTrue(creator.isSpringPoweredBundle(bundle, config));
    }

    @Test
    public void testIsSpringPoweredFalseNoProcessors() {
        ApplicationContextConfiguration config = mock(ApplicationContextConfiguration.class);
        Bundle bundle = mock(Bundle.class);

        NonValidatingOsgiApplicationContextCreator creator =
                new NonValidatingOsgiApplicationContextCreator(Collections.emptyList());
        when(config.isSpringPoweredBundle()).thenReturn(false);
        assertFalse(creator.isSpringPoweredBundle(bundle, config));
    }

    @Test
    public void testIsSpringPoweredFromPreProcessor() {
        ApplicationContextConfiguration config = mock(ApplicationContextConfiguration.class);
        Bundle bundle = mock(Bundle.class);
        ApplicationContextPreProcessor processor = mock(ApplicationContextPreProcessor.class);

        NonValidatingOsgiApplicationContextCreator creator =
                new NonValidatingOsgiApplicationContextCreator(Collections.singletonList(processor));
        when(config.isSpringPoweredBundle()).thenReturn(false);
        when(processor.isSpringPoweredBundle(bundle)).thenReturn(true);
        assertTrue(creator.isSpringPoweredBundle(bundle, config));
    }

    @Test
    public void testIsSpringPoweredFalseWithPreProcessor() {
        ApplicationContextConfiguration config = mock(ApplicationContextConfiguration.class);
        Bundle bundle = mock(Bundle.class);
        ApplicationContextPreProcessor processor = mock(ApplicationContextPreProcessor.class);

        NonValidatingOsgiApplicationContextCreator creator =
                new NonValidatingOsgiApplicationContextCreator(Collections.singletonList(processor));
        when(config.isSpringPoweredBundle()).thenReturn(false);
        when(processor.isSpringPoweredBundle(bundle)).thenReturn(false);
        assertFalse(creator.isSpringPoweredBundle(bundle, config));
    }
}
