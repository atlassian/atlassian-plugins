package com.atlassian.plugin.osgi.spring;

import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.eclipse.gemini.blueprint.extender.OsgiBeanFactoryPostProcessor;
import org.osgi.framework.BundleContext;

import static org.springframework.context.annotation.AnnotationConfigUtils.AUTOWIRED_ANNOTATION_PROCESSOR_BEAN_NAME;

public class AutowireSupportBeanFactoryPostProcessor implements OsgiBeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(
            final BundleContext bundleContext, final ConfigurableListableBeanFactory beanFactory) {
        BeanFactoryPostProcessorUtils.registerPostProcessor(
                beanFactory,
                AUTOWIRED_ANNOTATION_PROCESSOR_BEAN_NAME,
                AutowiredAnnotationBeanPostProcessor.class,
                AutowiredAnnotationBeanPostProcessor::new);
    }
}
