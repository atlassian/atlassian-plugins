package com.atlassian.plugin.osgi.spring;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.eclipse.gemini.blueprint.extender.OsgiBeanFactoryPostProcessor;
import org.osgi.framework.BundleContext;

import static org.springframework.context.annotation.AnnotationConfigUtils.COMMON_ANNOTATION_PROCESSOR_BEAN_NAME;

import static com.atlassian.plugin.osgi.spring.BeanFactoryPostProcessorUtils.registerPostProcessor;

public class CommonAnnotationBeanFactoryPostProcessor implements OsgiBeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(
            final BundleContext bundleContext, final ConfigurableListableBeanFactory beanFactory) {
        registerPostProcessor(
                beanFactory,
                COMMON_ANNOTATION_PROCESSOR_BEAN_NAME,
                CommonAnnotationBeanPostProcessor.class,
                CommonAnnotationBeanPostProcessor::new);
    }
}
