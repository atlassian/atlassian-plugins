package com.atlassian.plugin.osgi.spring;

import java.lang.reflect.Field;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.beans.CachedIntrospectionResults;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;
import org.osgi.framework.wiring.BundleWiring;
import com.google.common.base.Throwables;

import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;

/**
 * This class ensures that all classes in OSGi bundles can be cached by
 * {@link org.springframework.beans.CachedIntrospectionResults}.
 *
 * @since 3.2.17
 */
public class MarkBundleClassesCacheableListener implements BundleListener, InitializingBean, DisposableBean {
    private final BundleContext bundleContext;
    private final PluginEventManager pluginEventManager;

    private final Object lock = new Object();
    private boolean active = Boolean.getBoolean("atlassian.enable.spring.strong.cache.bean.metadata");

    public MarkBundleClassesCacheableListener(BundleContext bundleContext, PluginEventManager pluginEventManager) {
        this.bundleContext = bundleContext;
        this.pluginEventManager = pluginEventManager;
    }

    @Override
    public void afterPropertiesSet() {
        bundleContext.addBundleListener(this);
        pluginEventManager.register(this);
        for (Bundle bundle : bundleContext.getBundles()) {
            if (bundle.getState() == Bundle.ACTIVE) {
                maybeAcceptClassLoader(bundle);
            }
        }
    }

    @Override
    public void destroy() {
        synchronized (lock) {
            for (Bundle bundle : bundleContext.getBundles()) {
                if ((bundle.getState() & (Bundle.ACTIVE | Bundle.STOPPING)) != 0) {
                    maybeClearClassLoader(bundle);
                }
            }
            active = false;
        }
        // Any events arriving at this stage are ignored as active=false
        pluginEventManager.unregister(this);
        bundleContext.removeBundleListener(this);
    }

    @Override
    public void bundleChanged(@Nonnull BundleEvent event) {
        switch (event.getType()) {
            case BundleEvent.STARTED: {
                maybeAcceptClassLoader(event.getBundle());
                break;
            }
            case BundleEvent.STOPPED: {
                maybeClearClassLoader(event.getBundle());
                break;
            }
            default:
                break;
        }
    }

    private void maybeAcceptClassLoader(@Nonnull Bundle bundle) {
        if (bundle.getBundleId() == 0) {
            return; // No need to add system bundle
        }

        synchronized (lock) {
            ClassLoader bundleClassLoader = getBundleClassLoader(bundle);
            if (bundleClassLoader != null) {
                CachedIntrospectionResults.acceptClassLoader(bundleClassLoader);
            }
        }
    }

    private void maybeClearClassLoader(@Nonnull Bundle bundle) {
        synchronized (lock) {
            ClassLoader bundleClassLoader = getBundleClassLoader(bundle);
            if (bundleClassLoader != null) {
                CachedIntrospectionResults.clearClassLoader(bundleClassLoader);
            }
        }
    }

    @Nullable
    private ClassLoader getBundleClassLoader(@Nonnull Bundle bundle) {
        if (active) {
            final BundleWiring bundleWiring = bundle.adapt(BundleWiring.class);
            if (bundleWiring != null) {
                return bundleWiring.getClassLoader();
            }
        }
        return null;
    }

    @PluginEventListener
    public void onPluginEnabled(PluginFrameworkStartedEvent event) {
        if (Boolean.getBoolean("atlassian.enable.spring.strong.cache.bean.metadata.flush")) {
            try {
                final Field classCacheField = CachedIntrospectionResults.class.getDeclaredField("strongClassCache");
                classCacheField.setAccessible(true);
                Map classCache = (Map) classCacheField.get(null);

                // Flush class cache after plugin framework has started in
                // order to free up entries only needed during start-up
                classCache.clear();
            } catch (Exception e) {
                Throwables.propagate(e);
            }
        }
    }
}
