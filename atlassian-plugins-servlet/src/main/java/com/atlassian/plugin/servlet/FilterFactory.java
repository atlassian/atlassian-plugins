package com.atlassian.plugin.servlet;

import javax.servlet.Filter;

import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.filter.DelegatingPluginFilter;

/**
 * Creates {@link javax.servlet.Filter} instances.
 *
 * @since 3.2.20
 */
class FilterFactory {
    Filter newFilter(final ServletFilterModuleDescriptor descriptor) {
        return new DelegatingPluginFilter(descriptor);
    }
}
