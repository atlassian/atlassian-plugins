package com.atlassian.plugin.servlet.descriptors;

import java.util.Collections;
import javax.annotation.Nonnull;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.CannotDisable;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationException;

/**
 * Allows plugin developers to specify init parameters they would like added to the plugin local
 * {@link javax.servlet.ServletContext}.
 *
 * @since 2.1.0
 */
@CannotDisable
public class ServletContextParamModuleDescriptor extends AbstractModuleDescriptor<Void> {
    private String paramName;
    private String paramValue;
    private static final String PARAM_NAME = "param-name";
    private static final String PARAM_VALUE = "param-value";

    public ServletContextParamModuleDescriptor() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) {
        super.init(plugin, element);

        paramName = element.elementTextTrim(PARAM_NAME);
        paramValue = element.elementTextTrim(PARAM_VALUE);
    }

    @Override
    protected void validate(final Element element) {
        super.validate(element);
        String exceptionMessage = "There were validation errors:";
        if (element.element(PARAM_NAME) == null) {
            throw new ValidationException(exceptionMessage, Collections.singletonList("Parameter name is required"));
        }
        if (element.element(PARAM_VALUE) == null) {
            throw new ValidationException(exceptionMessage, Collections.singletonList("Parameter value is required"));
        }
    }

    public String getParamName() {
        return paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    @Override
    public Void getModule() {
        return null;
    }
}
