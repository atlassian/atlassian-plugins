package com.atlassian.plugin.servlet.descriptors;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.ServletContext;

import com.google.common.annotations.VisibleForTesting;
import io.atlassian.util.concurrent.ResettableLazyReference;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.filter.FilterDispatcherCondition;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugin.util.validation.ValidationException;

import static java.lang.Boolean.getBoolean;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

/**
 * A module descriptor that allows plugin developers to define servlet filters. Developers can define what urls the
 * filter should be applied to by defining one or more &lt;url-pattern&gt; elements and they can decide where in the
 * filter stack a plugin filter should go by defining the "location" and "weight" attributes.
 * <p>
 * The location attribute can have one of four values:
 * <ul>
 * <li>after-encoding - after the character encoding filter</li>
 * <li>before-login - before the login filter</li>
 * <li>before-decoration - before any global decoration like sitemesh</li>
 * <li>before-dispatch - before any dispatching filters or servlets</li>
 * </ul>
 * The default for the location attribute is "before-dispatch".
 * <p>
 * The weight attribute can have any integer value. Filters with lower values of the weight attribute will come before
 * those with higher values within the same location.
 *
 * @since 2.1.0
 */
public class ServletFilterModuleDescriptor extends BaseServletModuleDescriptor<Filter> implements StateAware {

    /**
     * Allows products to override the default {@code &lt;async-supported/&gt;} setting for filters from plugins.
     * By default, if this property is not set, plugin filters will be assumed to <i>not</i> support async. This
     * matches the servlet spec.
     *
     * @see #getDefaultAsyncSupported
     * @since 5.8.0
     */
    @VisibleForTesting
    static final String ASYNC_DEFAULT_SYSPROP = "atlassian.plugins.filter.async.default";

    @VisibleForTesting
    static final ResettableLazyReference<Boolean> ASYNC_DEFAULT = new ResettableLazyReference<Boolean>() {
        @Override
        protected Boolean create() {
            return getBoolean(ASYNC_DEFAULT_SYSPROP);
        }
    };
    /**
     * Allows products to force all plugin filters to include the {@link DispatcherType#ASYNC ASYNC dispatcher}.
     * By default, if this property is not set, plugin filters will only include the {@code ASYNC} dispatcher if
     * they explicitly register for it.
     *
     * @since 4.6.0
     */
    @VisibleForTesting
    static final String FORCE_ASYNC_DISPATCHER_SYSPROP = "atlassian.plugins.filter.force.async.dispatcher";

    @VisibleForTesting
    static final ResettableLazyReference<Boolean> FORCE_ASYNC = new ResettableLazyReference<Boolean>() {
        @Override
        protected Boolean create() {
            return getBoolean(FORCE_ASYNC_DISPATCHER_SYSPROP);
        }
    };

    static final String DEFAULT_LOCATION = FilterLocation.BEFORE_DISPATCH.name();
    static final String DEFAULT_WEIGHT = "100";

    private final Set<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST);
    private final ServletModuleManager servletModuleManager;

    private FilterLocation location;
    private int weight;
    private static final String DISPATCHER = "dispatcher";

    /**
     * Creates a descriptor that uses a module class factory to create instances.
     *
     * @param moduleFactory        The module factory
     * @param servletModuleManager The module manager
     * @since 2.5.0
     */
    public ServletFilterModuleDescriptor(ModuleFactory moduleFactory, ServletModuleManager servletModuleManager) {
        super(moduleFactory);
        this.servletModuleManager = requireNonNull(servletModuleManager, "servletModuleManager");
    }

    public static final Comparator<ServletFilterModuleDescriptor> byWeight =
            Comparator.comparingInt(ServletFilterModuleDescriptor::getWeight);

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) {
        super.init(plugin, element);
        try {
            location = FilterLocation.parse(element.attributeValue("location", DEFAULT_LOCATION));
            weight = Integer.parseInt(element.attributeValue("weight", DEFAULT_WEIGHT));
        } catch (IllegalArgumentException ex) {
            throw new PluginParseException(ex);
        }

        List<Element> dispatcherElements = element.elements(DISPATCHER);
        if (!dispatcherElements.isEmpty()) {
            dispatcherTypes.clear();
            for (Element dispatcher : dispatcherElements) {
                // already been validated via the validation rules
                dispatcherTypes.add(DispatcherType.valueOf(dispatcher.getTextTrim()));
            }
        }
        if (FORCE_ASYNC.get()) {
            dispatcherTypes.add(DispatcherType.ASYNC);
        }
    }

    @Override
    protected void validate(final com.atlassian.plugin.module.Element element) {
        super.validate(element);
        List<com.atlassian.plugin.module.Element> dispatcherElements = element.elements(DISPATCHER);
        if (!dispatcherElements.isEmpty()) {
            for (com.atlassian.plugin.module.Element dispatcherElement : dispatcherElements) {
                if (!isKnownDispatcherType(dispatcherElement.getTextTrim())) {
                    throw new ValidationException(
                            "There were validation errors:",
                            Collections.singletonList("The dispatcher value must be one of the following only "
                                    + Arrays.asList(DispatcherType.values())));
                }
            }
        }
        if (element.attributeValue("class") == null) {
            throw new ValidationException(
                    "There were validation errors:", Collections.singletonList("The class is required"));
        }
    }

    private boolean isKnownDispatcherType(String type) {
        for (DispatcherType dispatcherType : DispatcherType.values()) {
            if (dispatcherType.toString().equals(type)) {
                return true;
            }
        }
        return false;
    }

    public void enabled() {
        super.enabled();
        servletModuleManager.addFilterModule(this);
    }

    public void disabled() {
        servletModuleManager.removeFilterModule(this);
        super.disabled();
    }

    @Override
    public Filter getModule() {
        return moduleFactory.createModule(moduleClassName, this);
    }

    public FilterLocation getLocation() {
        return location;
    }

    public int getWeight() {
        return weight;
    }

    /**
     * Returns a set of dispatcher conditions that have been set for this filter, these conditions
     * will be one of the following: <code>REQUEST, FORWARD, INCLUDE or ERROR</code>.
     *
     * @return A set of dispatcher conditions that have been set for this filter.
     * @since 2.5.0
     * @deprecated since 4.6.0. Use {@link #getDispatcherTypes()} instead.
     */
    @Deprecated
    public Set<FilterDispatcherCondition> getDispatcherConditions() {
        return dispatcherTypes.stream()
                .map(FilterDispatcherCondition::fromDispatcherType)
                .filter(Objects::nonNull)
                .collect(toSet());
    }

    /**
     * Returns a set of dispatcher types that have been set for this filter. These can be any of the values supported by
     * {@link DispatcherType}.
     *
     * @return A set of dispatcher types that have been set for this filter.
     * @since 4.6.0
     */
    public Set<DispatcherType> getDispatcherTypes() {
        return dispatcherTypes;
    }

    /**
     * Overrides the base class's handling to allow using {@link #ASYNC_DEFAULT_SYSPROP} to control whether filters
     * are {@code &lt;async-supported/&gt;} by default or not.
     * <p>
     * The servlet spec documents that filters <i>do not</i> support async unless they are explicitly marked otherwise,
     * and so the default here is also {@code false}. However, Tomcat's {@link ServletContext#addFilter} implementation
     * <i>does not adhere to the spec</i> and filters in Tomcat <i>do</i> support async unless they are explicitly set
     * <i>not</i> to. The system property allows products to opt into Tomcat's behavior.
     *
     * @return {@code false}, unless {@link #ASYNC_DEFAULT_SYSPROP} has been set to override it
     * @since 5.8.0
     */
    @Override
    protected boolean getDefaultAsyncSupported() {
        return ASYNC_DEFAULT.get();
    }
}
