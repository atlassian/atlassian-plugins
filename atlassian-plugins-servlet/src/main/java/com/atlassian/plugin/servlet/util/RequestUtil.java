package com.atlassian.plugin.servlet.util;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

public class RequestUtil {

    public static String getServletPath(HttpServletRequest request) {
        String servletPath = (String) request.getAttribute(RequestDispatcher.INCLUDE_SERVLET_PATH);
        if (servletPath == null) {
            servletPath = request.getServletPath();
        }
        return servletPath == null ? "" : servletPath;
    }

    public static String getPathInfo(HttpServletRequest request) {
        String pathInfo = (String) request.getAttribute(RequestDispatcher.INCLUDE_PATH_INFO);
        if (pathInfo == null) {
            pathInfo = request.getPathInfo();
        }
        return pathInfo == null ? "" : pathInfo;
    }

    public static String getRequestURI(HttpServletRequest request) {
        String requestURI = (String) request.getAttribute(RequestDispatcher.INCLUDE_REQUEST_URI);
        if (requestURI == null) {
            requestURI = request.getRequestURI();
        }
        return requestURI == null ? "" : requestURI;
    }
}
