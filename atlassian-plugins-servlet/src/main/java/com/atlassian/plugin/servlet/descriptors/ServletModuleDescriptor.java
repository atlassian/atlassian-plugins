package com.atlassian.plugin.servlet.descriptors;

import javax.servlet.http.HttpServlet;

import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A module descriptor that allows plugin developers to define servlets. Developers can define what urls the
 * servlet should be serve by defining one or more &lt;url-pattern&gt; elements.
 */
public class ServletModuleDescriptor extends BaseServletModuleDescriptor<HttpServlet> implements StateAware {
    private final ServletModuleManager servletModuleManager;

    /**
     * Creates a descriptor that uses a module factory to create instances
     *
     * @since 2.5.0
     */
    public ServletModuleDescriptor(final ModuleFactory moduleFactory, final ServletModuleManager servletModuleManager) {
        super(moduleFactory);
        this.servletModuleManager = checkNotNull(servletModuleManager);
    }

    @Override
    public void enabled() {
        super.enabled();
        servletModuleManager.addServletModule(this);
    }

    @Override
    public void disabled() {
        servletModuleManager.removeServletModule(this);
        super.disabled();
    }

    @Override
    public HttpServlet getModule() {
        return moduleFactory.createModule(moduleClassName, this);
    }
}
