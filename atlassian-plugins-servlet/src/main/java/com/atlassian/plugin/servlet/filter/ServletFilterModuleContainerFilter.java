package com.atlassian.plugin.servlet.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.util.RequestUtil;
import com.atlassian.plugin.servlet.util.ServletContextServletModuleManagerAccessor;

/**
 * Applications need to create a concrete subclass of this for use in their filter stack. This filters responsiblity
 * is to retrieve the filters to be applied to the request from the {@link ServletModuleManager} and build a
 * {@link FilterChain} from them. Once all the filters in the chain have been applied to the request, this filter
 * returns control to the main chain.
 * <p>
 * There is one init parameters that must be configured for this filter, the "location" parameter. It can be one of
 * "top", "middle" or "bottom". A filter with a "top" location must appear before the filter with a "middle" location
 * which must appear before a filter with a "bottom" location. Where any other application filters lie in the filter
 * stack is completely up to the application.
 *
 * @since 2.1.0
 */
public class ServletFilterModuleContainerFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(ServletFilterModuleContainerFilter.class);

    private FilterConfig filterConfig;
    private FilterLocation location;

    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        location = FilterLocation.parse(filterConfig.getInitParameter("location"));
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        doFilter((HttpServletRequest) request, (HttpServletResponse) response, chain);
    }

    void doFilter(HttpServletRequest request, HttpServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        if (getServletModuleManager() == null) {
            log.info("Could not get ServletModuleManager. Skipping filter plugins.");
            chain.doFilter(request, response);
            return;
        }

        final Iterable<Filter> filters = getServletModuleManager()
                .getFilters(location, getPath(request), filterConfig, request.getDispatcherType());
        FilterChain pluginFilterChain = new IteratingFilterChain(filters.iterator(), chain);
        pluginFilterChain.doFilter(request, response);
    }

    public void destroy() {
        // Do nothing
    }

    /**
     * Retrieve the DefaultServletModuleManager from your container framework. Uses the {@link com.atlassian.plugin.servlet.util.ServletContextServletModuleManagerAccessor}
     * by default.
     */
    protected ServletModuleManager getServletModuleManager() {
        return ServletContextServletModuleManagerAccessor.getServletModuleManager(filterConfig.getServletContext());
    }

    protected final FilterConfig getFilterConfig() {
        return filterConfig;
    }

    protected final FilterLocation getFilterLocation() {
        return location;
    }

    /**
     * Gets the servlet path and concatenates it with path info to match the appropriate filters.
     *
     * @param request The request
     * @return The uri
     */
    private static String getPath(HttpServletRequest request) {
        return RequestUtil.getServletPath(request) + RequestUtil.getPathInfo(request);
    }
}
