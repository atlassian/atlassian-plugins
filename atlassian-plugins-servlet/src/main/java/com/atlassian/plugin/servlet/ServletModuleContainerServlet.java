package com.atlassian.plugin.servlet;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.servlet.util.RequestUtil;
import com.atlassian.plugin.servlet.util.ServletContextServletModuleManagerAccessor;

/**
 * Applications need to create a concrete subclass of this for use in their webapp. This servlet's responsibility
 * is to retrieve the servlet to be used to serve the request from the {@link ServletModuleManager}. If no servlet
 * can be found to serve the request, a 404 should be sent back to the client.
 */
public class ServletModuleContainerServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(ServletModuleContainerServlet.class);
    private ServletConfig servletConfig;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        this.servletConfig = servletConfig;
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletModuleManager servletModuleManager = getServletModuleManager();
        if (servletModuleManager == null) {
            log.error("Could not get ServletModuleManager?");
            response.sendError(500, "Could not get ServletModuleManager.");
            return;
        }

        final HttpServlet servlet = servletModuleManager.getServlet(RequestUtil.getPathInfo(request), servletConfig);

        if (servlet == null) {
            log.debug("No servlet found for: " + RequestUtil.getRequestURI(request));
            response.sendError(404, "Could not find servlet.");
            return;
        }

        try {
            servlet.service(request, response);
        } catch (ServletException e) {
            log.error(e.getMessage(), e);
            response.sendError(500, e.getMessage());
        }
    }

    /**
     * @return the DefaultServletModuleManager from your container framework. Uses the {@link com.atlassian.plugin.servlet.util.ServletContextServletModuleManagerAccessor}
     * by default.
     */
    protected ServletModuleManager getServletModuleManager() {
        return ServletContextServletModuleManagerAccessor.getServletModuleManager(getServletContext());
    }
}
