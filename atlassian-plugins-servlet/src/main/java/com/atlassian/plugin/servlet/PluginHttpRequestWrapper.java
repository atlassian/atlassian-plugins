package com.atlassian.plugin.servlet;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.servlet.AsyncContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestWrapper;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.servlet.descriptors.BaseServletModuleDescriptor;

import static java.util.Objects.requireNonNull;

/**
 * A request wrapper for requests bound for servlets declared in plugins. Does the necessary path munging for requests
 * so they look like they are for the plugin, rather than the {@link ServletModuleContainerServlet} (see PLUG-11).
 * <p>
 * Also wraps the HttpSession in order to work around a Weblogic Session Attribute serialization problem (see PLUG-515)
 */
public class PluginHttpRequestWrapper extends HttpServletRequestWrapper {

    private final boolean asyncSupported;
    private final String basePath;
    private final HttpServletRequest delegate;
    private final BaseServletModuleDescriptor<?> descriptor;

    public PluginHttpRequestWrapper(HttpServletRequest request, BaseServletModuleDescriptor<?> descriptor) {
        super(request);

        this.descriptor = requireNonNull(descriptor, "descriptor");

        asyncSupported = request.isAsyncSupported() && descriptor.isAsyncSupported();
        basePath = findBasePath(descriptor);
        delegate = request;
    }

    @Override
    public String getServletPath() {
        String servletPath = super.getServletPath();
        if (basePath != null) {
            servletPath += basePath;
        }
        return servletPath;
    }

    @Override
    public String getPathInfo() {
        String pathInfo = super.getPathInfo();
        if (pathInfo != null && basePath != null) {
            if (basePath.equals(pathInfo)) {
                return null;
            } else if (pathInfo.startsWith(basePath)) {
                return pathInfo.substring(basePath.length());
            }
        }
        return pathInfo;
    }

    @Override
    public HttpSession getSession() {
        return this.getSession(true);
    }

    @Override
    public HttpSession getSession(final boolean create) {
        HttpSession session = delegate.getSession(create);
        if (session == null) {
            // The delegate returned a null session - so do we.
            return null;
        } else {
            // Wrap this non-null HttpSession
            return session instanceof PluginHttpSessionWrapper ? session : new PluginHttpSessionWrapper(session);
        }
    }

    /**
     * @since 4.6.0
     */
    @Override
    public boolean isAsyncSupported() {
        return asyncSupported;
    }

    /**
     * @since 4.6.0
     */
    @Override
    public AsyncContext startAsync() {
        requireAsyncSupport(this);

        return super.startAsync();
    }

    /**
     * @since 4.6.0
     */
    @Override
    public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) {
        requireAsyncSupport(servletRequest);

        return super.startAsync(servletRequest, servletResponse);
    }

    private static boolean arrayStartsWith(String[] array, String[] prefixArray) {
        // prefix array cannot be longer than the array.
        if (prefixArray.length > array.length) {
            return false;
        }

        // Assume the last bit less likely to match.
        for (int i = prefixArray.length - 1; i >= 0; i--) {
            if (!prefixArray[i].equals(array[i])) {
                return false;
            }
        }

        return true;
    }

    private static String getMappingRootPath(String pathMapping) {
        return pathMapping.substring(0, pathMapping.length() - "/*".length());
    }

    /**
     * Unwraps the provided {@link ServletRequest} to find wrappers for any non-async modules and returns a
     * set containing all of their keys.
     *
     * @param servletRequest the request to unwrap
     * @return a set containing zero or more keys for modules that don't support async
     */
    private static SortedSet<String> getNonAsyncKeys(ServletRequest servletRequest) {
        SortedSet<String> nonAsyncKeys = new TreeSet<>();
        while (servletRequest instanceof ServletRequestWrapper) {
            if (servletRequest instanceof PluginHttpRequestWrapper) {
                PluginHttpRequestWrapper wrapper = (PluginHttpRequestWrapper) servletRequest;
                if (!wrapper.descriptor.isAsyncSupported()) {
                    nonAsyncKeys.add(wrapper.descriptor.getCompleteKey());
                }
            }
            servletRequest = ((ServletRequestWrapper) servletRequest).getRequest();
        }

        return nonAsyncKeys;
    }

    private static boolean isPathMapping(String path) {
        return path.startsWith("/") && path.endsWith("/*");
    }

    /**
     * A <a href="http://bluxte.net/blog/2006-03/29-40-33.html">commenter</a> based on the
     * <a href="http://java.sun.com/products/servlet/2.1/html/introduction.fm.html#1499">servlet mapping spec</a>
     * defined the mapping processing as:
     *
     * <ol>
     * <li>A string beginning with a '/' character and ending with a '/*' postfix is used for path mapping.</li>
     * <li>A string beginning with a'*.' prefix is used as an extension mapping.</li>
     * <li>A string containing only the '/' character indicates the "default" servlet of the application. In this
     * case the servlet path is the request URI minus the context path and the path info is null.</li>
     * <li>All other strings are used for exact matches only.</li>
     * </ol>
     *
     * To find the base path we're really only interested in the first and last case. Everything else will just get a
     * null base path. So we'll iterate through the list of paths specified and for the ones that match (1) above,
     * check if the path info returned by the super class matches. If it does, we return that base path, otherwise we
     * move onto the next one.
     */
    private String findBasePath(BaseServletModuleDescriptor<?> descriptor) {
        String pathInfo = super.getPathInfo();

        if (pathInfo != null) {
            // Exact match
            for (String basePath : descriptor.getPaths()) {
                if (basePath.equals(pathInfo)) {
                    return basePath;
                }
            }

            // Prefix match
            final String[] pathInfoComponents = StringUtils.split(pathInfo, '/');
            for (String basePath : descriptor.getPaths()) {
                if (isPathMapping(basePath)) {
                    final String mappingRootPath = getMappingRootPath(basePath);
                    final String[] mappingRootPathComponents = StringUtils.split(mappingRootPath, '/');

                    if (arrayStartsWith(pathInfoComponents, mappingRootPathComponents)) {
                        return mappingRootPath;
                    }
                }
            }
        }
        return null;
    }

    private void requireAsyncSupport(ServletRequest servletRequest) {
        if (!isAsyncSupported()) {
            // The filter(s) that block async should not be included in the exception message, on the off chance
            // it might be displayed to an end user. However, including them in the _logging_ may be useful for
            // a developer--especially product developers--to track down plugin-provided filters which unexpectedly
            // block async. Note that, if async is blocked on the request itself, this set may be empty
            Set<String> nonAsyncKeys = getNonAsyncKeys(servletRequest);

            IllegalStateException ise =
                    new IllegalStateException("One of the plugins in the filter chain does not support async");
            if (!nonAsyncKeys.isEmpty()) {
                // Logging the exception here may be useful to a developer because it will show the filter or servlet
                // that tried to call startAsync, implying it may be missing an isAsyncSupported check
                LoggerFactory.getLogger(getClass())
                        .warn("The following plugin-provided filter(s) do not support async: {}", nonAsyncKeys, ise);
            }

            throw ise;
        }
    }
}
