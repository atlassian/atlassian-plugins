package com.atlassian.plugin.servlet.descriptors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationException;

/**
 * Acts as a base for other servlet type module descriptors to inherit. It adds parsing and retrieval of any paths
 * declared in the descriptor with &lt;url-pattern&gt; as well as &lt;init-param&gt;s.
 *
 * @since 2.1.0
 */
@RequirePermission(Permissions.EXECUTE_JAVA)
public abstract class BaseServletModuleDescriptor<T> extends AbstractModuleDescriptor<T> {

    protected static final Logger log = LoggerFactory.getLogger(BaseServletModuleDescriptor.class);

    private List<String> paths;
    private Map<String, String> initParams;
    private boolean asyncSupported;
    private static final String URL_PATTERN = "url-pattern";
    private static final String INIT_PARAM = "init-param";
    private static final String PARAM_NAME = "param-name";
    private static final String PARAM_VALUE = "param-value";

    /**
     * @param moduleCreator a factory for creating this descriptor's module
     * @since 2.5.0
     */
    public BaseServletModuleDescriptor(ModuleFactory moduleCreator) {
        super(moduleCreator);

        // There are subclasses of this that live outside this codebase, and some of them never actually
        // call init(Plugin, Element). To ensure such subclasses still pick up the desired default, it's
        // set in the constructor and then, if the XML descriptor explicitly sets a value, that replaces
        // this when init(Plugin, Element) is called
        asyncSupported = getDefaultAsyncSupported();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) {
        super.init(plugin, element);

        checkPermissions(); // do that early

        List<Element> urlPatterns = element.elements(URL_PATTERN);
        paths = new ArrayList<>(urlPatterns.size());
        for (Element urlPattern : urlPatterns) {
            paths.add(urlPattern.getTextTrim());
        }

        initParams = new HashMap<>();
        List<Element> paramsList = element.elements(INIT_PARAM);
        for (Element initParamEl : paramsList) {
            Element paramNameEl = initParamEl.element(PARAM_NAME);
            Element paramValueEl = initParamEl.element(PARAM_VALUE);
            initParams.put(paramNameEl.getTextTrim(), paramValueEl.getTextTrim());
        }

        Element async = element.element("async-supported");
        if (async != null) {
            asyncSupported = Boolean.parseBoolean(async.getTextTrim());
        }
    }

    @Override
    protected void validate(final Element element) {
        super.validate(element);

        String exceptionMessage = "There were validation errors:";
        if (element.element(URL_PATTERN) == null) {
            throw new ValidationException(exceptionMessage, List.of("There must be at least one path specified"));
        }
        for (Element initParamElement : element.elements(INIT_PARAM)) {
            if (initParamElement.element(PARAM_NAME) == null) {
                throw new ValidationException(exceptionMessage, List.of(String.format("%s is required", PARAM_NAME)));
            }
            if (initParamElement.element(PARAM_VALUE) == null) {
                throw new ValidationException(exceptionMessage, List.of(String.format("%s is required", PARAM_VALUE)));
            }
        }
    }

    public List<String> getPaths() {
        return paths;
    }

    public Map<String, String> getInitParams() {
        return initParams;
    }

    /**
     * @since 4.6.0
     */
    public boolean isAsyncSupported() {
        return asyncSupported;
    }

    /**
     * Allows subclasses to override the default {@code &lt;async-supported/&gt;} setting for this descriptor.
     * If a derived class does not override this method, the default will be {@code false}.
     * <p>
     * <b>Note</b>: This method is called by the constructor, so other properties, like {@link #getCompleteKey},
     * are not available when this runs.
     *
     * @return {@code false}
     * @since 5.8.0
     */
    protected boolean getDefaultAsyncSupported() {
        return false;
    }
}
