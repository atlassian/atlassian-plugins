package it.com.atlassian.plugin.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.classloader.PluginClassLoader;
import com.atlassian.plugin.impl.DefaultDynamicPlugin;
import com.atlassian.plugin.servlet.DelegatingPluginServlet;
import com.atlassian.plugin.servlet.PluginHttpRequestWrapper;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptorBuilder;
import com.atlassian.plugin.test.PluginTestUtils;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDelegatingPluginServlet {
    private PluginClassLoader classLoader;
    private Plugin plugin;

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private HttpServletResponse mockResponse;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        classLoader = new PluginClassLoader(PluginTestUtils.getFileForResource(PluginTestUtils.SIMPLE_TEST_JAR));
        plugin = new DefaultDynamicPlugin(mock(PluginArtifact.class), classLoader);
        when(mockRequest.getPathInfo()).thenReturn("/servlet/test");
    }

    /**
     * Test to make sure the plugin class loader is set for the thread context class loader when init is called.
     *
     * @throws Exception on test error
     */
    @Test
    public void testInitCalledWithPluginClassLoaderAsThreadClassLoader() throws Exception {
        HttpServlet wrappedServlet = new HttpServlet() {
            public void init(ServletConfig config) {
                assertSame(classLoader, Thread.currentThread().getContextClassLoader());
            }
        };

        getDelegatingServlet(wrappedServlet).init(null);
    }

    /**
     * Test to make sure the plugin class loader is set for the thread context class loader when service is called.
     *
     * @throws Exception on test error
     */
    @Test
    public void testServiceCalledWithPluginClassLoaderAsThreadClassLoader() throws Exception {
        HttpServlet wrappedServlet = new HttpServlet() {
            public void service(HttpServletRequest request, HttpServletResponse response) {
                assertSame(classLoader, Thread.currentThread().getContextClassLoader());
            }
        };

        getDelegatingServlet(wrappedServlet).service(mockRequest, mockResponse);
    }

    /**
     * Test to make sure the servlet is called with our request wrapper.
     *
     * @throws Exception on test error
     */
    @Test
    public void testServiceCalledWithWrappedRequest() throws Exception {
        HttpServlet wrappedServlet = new HttpServlet() {
            public void service(HttpServletRequest request, HttpServletResponse response) {
                assertTrue(request instanceof PluginHttpRequestWrapper);
            }
        };

        getDelegatingServlet(wrappedServlet).service(mockRequest, mockResponse);
    }

    private DelegatingPluginServlet getDelegatingServlet(HttpServlet wrappedServlet) {
        ServletModuleDescriptor descriptor = new ServletModuleDescriptorBuilder()
                .with(plugin)
                .with(wrappedServlet)
                .withPath("/servlet/*")
                .build();
        return new DelegatingPluginServlet(descriptor);
    }
}
