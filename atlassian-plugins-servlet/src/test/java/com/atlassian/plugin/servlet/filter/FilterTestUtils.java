package com.atlassian.plugin.servlet.filter;

import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public final class FilterTestUtils {
    public static class FilterAdapter implements Filter {
        public void init(FilterConfig filterConfig) throws ServletException {}

        public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                throws IOException, ServletException {}

        public void destroy() {}
    }

    public static final class SoundOffFilter extends FilterAdapter {
        private final List<Integer> filterCallOrder;
        private final int filterId;

        public SoundOffFilter(List<Integer> filterCallOrder, int filterId) {
            this.filterCallOrder = filterCallOrder;
            this.filterId = filterId;
        }

        @Override
        public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                throws IOException, ServletException {
            filterCallOrder.add(filterId);
            chain.doFilter(request, response);
            filterCallOrder.add(filterId);
        }
    }

    public static final FilterChain emptyChain = new FilterChain() {
        public void doFilter(ServletRequest request, ServletResponse response) throws IOException, ServletException {}
    };

    /**
     * Creates a filter chain from the single filter. When this filter is called once, the filter chain is finished.
     */
    static FilterChain singletonFilterChain(final Filter filter) {
        return new FilterChain() {
            boolean called = false;

            public void doFilter(ServletRequest request, ServletResponse response)
                    throws IOException, ServletException {
                if (!called) {
                    called = true;
                    filter.doFilter(request, response, this);
                }
            }
        };
    }
}
