package com.atlassian.plugin.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestServletModuleContainerServlet {
    // ensure that an UnavailableException thrown in the plugin servlet doesn't unload this servlet
    @Test
    public void testServletDoesntUnloadItself() throws IOException, ServletException {
        ServletModuleManager mockServletModuleManager = mock(ServletModuleManager.class);
        ModuleFactory mockModuleClassFactory = mock(ModuleFactory.class);
        ServletModuleDescriptor servletModuleDescriptor =
                new ServletModuleDescriptor(mockModuleClassFactory, mockServletModuleManager);

        final DelegatingPluginServlet delegatingPluginServlet = new DelegatingPluginServlet(servletModuleDescriptor) {
            public void service(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
                    throws ServletException {
                throw new UnavailableException("Error in plugin servlet");
            }
        };

        final ServletModuleManager servletModuleManager =
                new DefaultServletModuleManager(new DefaultPluginEventManager()) {
                    public DelegatingPluginServlet getServlet(String path, ServletConfig servletConfig) {
                        return delegatingPluginServlet;
                    }
                };

        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.getPathInfo()).thenReturn("some/path");
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        ServletModuleContainerServlet servlet = new ServletModuleContainerServlet() {
            protected ServletModuleManager getServletModuleManager() {
                return servletModuleManager;
            }
        };

        servlet.service(mockRequest, mockResponse);
        verify(mockResponse).sendError(eq(500), anyString());
    }

    @Test
    public void testIncludedServletDispatchesCorrectly() throws IOException, ServletException {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.getPathInfo()).thenReturn("/original");
        when(mockRequest.getAttribute(RequestDispatcher.INCLUDE_PATH_INFO)).thenReturn("/included");
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        final MockHttpServlet originalServlet = new MockHttpServlet();
        final MockHttpServlet includedServlet = new MockHttpServlet();

        final ServletModuleManager servletModuleManager =
                new DefaultServletModuleManager(new DefaultPluginEventManager()) {
                    @Override
                    public HttpServlet getServlet(String path, ServletConfig servletConfig) throws ServletException {
                        if (path.equals("/original")) {
                            return originalServlet;
                        } else if (path.equals("/included")) {
                            return includedServlet;
                        }
                        return null;
                    }
                };

        final ServletModuleContainerServlet servlet = new ServletModuleContainerServlet() {
            @Override
            protected ServletModuleManager getServletModuleManager() {
                return servletModuleManager;
            }
        };

        servlet.service(mockRequest, mockResponse);
        assertThat("includedServlet should have been invoked", includedServlet.wasCalled, is(true));
        assertThat("originalServlet should not have been invoked", originalServlet.wasCalled, is(false));
    }

    private static class MockHttpServlet extends HttpServlet {
        private boolean wasCalled = false;

        @Override
        protected void service(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
                throws ServletException, IOException {
            wasCalled = true;
        }
    }
}
