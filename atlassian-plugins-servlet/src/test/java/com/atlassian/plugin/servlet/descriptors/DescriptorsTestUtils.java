package com.atlassian.plugin.servlet.descriptors;

import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;

import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.test.PluginTestUtils;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DescriptorsTestUtils {
    public static void testElementFromResource(String resourceName, Consumer<Dom4jDelegatingElement> test)
            throws Exception {
        try (var inputStream =
                PluginTestUtils.class.getClassLoader().getResource(resourceName).openStream()) {
            final String xml = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
            final Document document = DocumentHelper.parseText(xml);

            test.accept(new Dom4jDelegatingElement(document.getRootElement()));
        }
    }

    public static com.atlassian.plugin.module.Element mockElement() {
        return mock(com.atlassian.plugin.module.Element.class);
    }

    public static com.atlassian.plugin.module.Element mockElement(String text) {
        final var element = mockElement();
        when(element.getTextTrim()).thenReturn(text);

        return element;
    }

    public static com.atlassian.plugin.module.Element mockInitParamElement() {
        final var element = mockElement();

        when(element.element("param-name")).thenReturn(mockElement());
        when(element.element("param-value")).thenReturn(mockElement());

        return element;
    }
}
