package com.atlassian.plugin.servlet.descriptors;

import java.util.Collections;
import java.util.List;
import javax.servlet.DispatcherType;

import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.PrefixDelegatingModuleFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugin.servlet.filter.FilterTestUtils.FilterAdapter;
import com.atlassian.plugin.util.validation.ValidationException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.Permissions.addPermission;

@RunWith(MockitoJUnitRunner.class)
public class TestServletFilterModuleDescriptor {
    public static final String PLUGIN_KEY = "somekey";

    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Mock
    private ServletModuleManager servletModuleManager;

    private ServletFilterModuleDescriptor descriptor;

    @Before
    public void setUp() {
        descriptor = new ServletFilterModuleDescriptor(
                new PrefixDelegatingModuleFactory(Collections.emptySet()), servletModuleManager);
    }

    @After
    public void resetLazyReferences() {
        ServletFilterModuleDescriptor.ASYNC_DEFAULT.reset();
        ServletFilterModuleDescriptor.FORCE_ASYNC.reset();
    }

    @Test
    public void testInit() {
        final Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        descriptor.init(plugin, new Dom4jDelegatingElement(e));
        assertThat(descriptor.getLocation(), is(FilterLocation.BEFORE_DISPATCH));
        assertThat(descriptor.getWeight(), is(100));
    }

    private Plugin newPlugin(String key) {
        Plugin plugin = new StaticPlugin();
        plugin.setKey(key);
        return addPermission(plugin, Permissions.EXECUTE_JAVA, null);
    }

    private Element getValidConfig() {
        Element e = new DOMElement("servlet-filter");
        e.addAttribute("key", "key2");
        e.addAttribute("class", FilterAdapter.class.getName());
        Element url = new DOMElement("url-pattern");
        url.setText("/foo");
        e.add(url);
        Element dispatcher1 = new DOMElement("dispatcher");
        dispatcher1.setText("REQUEST");
        e.add(dispatcher1);
        Element dispatcher2 = new DOMElement("dispatcher");
        dispatcher2.setText("FORWARD");
        e.add(dispatcher2);
        return e;
    }

    @Test
    public void testInitWithNoUrlPattern() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = new DOMElement("servlet-filter");
        e.addAttribute("key", "key2");
        e.addAttribute("class", FilterAdapter.class.getName());
        assertThrows(PluginParseException.class, () -> descriptor.init(plugin, new Dom4jDelegatingElement(e)));
    }

    @Test
    public void testInitWithDetails() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        e.addAttribute("location", "after-encoding");
        e.addAttribute("weight", "122");
        descriptor.init(plugin, new Dom4jDelegatingElement(e));
        assertThat(descriptor.getLocation(), is(FilterLocation.AFTER_ENCODING));
        assertThat(descriptor.getWeight(), is(122));
        assertThat(descriptor.getDispatcherTypes(), containsInAnyOrder(DispatcherType.REQUEST, DispatcherType.FORWARD));
    }

    @Test
    public void testInitWithBadLocation() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        e.addAttribute("location", "t23op");
        assertThrows(PluginParseException.class, () -> descriptor.init(plugin, new Dom4jDelegatingElement(e)));
    }

    @Test
    public void testInitWithBadWeight() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        e.addAttribute("weight", "t23op");
        assertThrows(PluginParseException.class, () -> descriptor.init(plugin, new Dom4jDelegatingElement(e)));
    }

    @Test
    public void testInitWithBadDispatcher() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        Element badDispatcher = new DOMElement("dispatcher");
        badDispatcher.setText("badValue");
        e.add(badDispatcher);
        assertThrows(PluginParseException.class, () -> descriptor.init(plugin, new Dom4jDelegatingElement(e)));
    }

    @Test
    public void testWithNoDispatcher() {
        Plugin plugin = newPlugin(PLUGIN_KEY);

        Element e = new DOMElement("servlet-filter");
        e.addAttribute("key", "key2");
        e.addAttribute("class", FilterAdapter.class.getName());
        Element url = new DOMElement("url-pattern");
        url.setText("/foo");
        e.add(url);

        descriptor.init(plugin, new Dom4jDelegatingElement(e));
        assertThat(descriptor.getDispatcherTypes(), containsInAnyOrder(DispatcherType.REQUEST));
    }

    @Test
    public void testWithNoDispatcherForcedAsync() {
        System.setProperty(ServletFilterModuleDescriptor.FORCE_ASYNC_DISPATCHER_SYSPROP, "true");
        Plugin plugin = newPlugin(PLUGIN_KEY);

        Element e = new DOMElement("servlet-filter");
        e.addAttribute("key", "key2");
        e.addAttribute("class", FilterAdapter.class.getName());
        Element url = new DOMElement("url-pattern");
        url.setText("/foo");
        e.add(url);

        descriptor.init(plugin, new Dom4jDelegatingElement(e));
        assertThat(descriptor.getDispatcherTypes(), containsInAnyOrder(DispatcherType.REQUEST, DispatcherType.ASYNC));
    }

    @Test
    public void testInitWithDetailsForceAsync() {
        System.setProperty(ServletFilterModuleDescriptor.FORCE_ASYNC_DISPATCHER_SYSPROP, "true");
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        e.addAttribute("location", "after-encoding");
        e.addAttribute("weight", "122");
        descriptor.init(plugin, new Dom4jDelegatingElement(e));
        assertThat(descriptor.getLocation(), is(FilterLocation.AFTER_ENCODING));
        assertThat(descriptor.getWeight(), is(122));
        assertThat(
                descriptor.getDispatcherTypes(),
                containsInAnyOrder(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.ASYNC));
    }

    @Test
    public void testInitAppliesAsyncSupportedIfSet() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        e.addElement("async-supported").setText("true");

        descriptor.init(plugin, new Dom4jDelegatingElement(e));
        assertThat(descriptor.isAsyncSupported(), is(true));
    }

    @Test
    public void testInitWithoutAsyncSupportedDefaultsToFalse() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();

        descriptor.init(plugin, new Dom4jDelegatingElement(e));
        assertThat(descriptor.isAsyncSupported(), is(false));
    }

    // Note: The default async value is applied in the constructor, so after we set the system property we
    //      need to reset the references explicitly in this test and construct a new descriptor
    @Test
    public void testInitWithoutAsyncSupportedHonorsSystemProperty() {
        System.setProperty(ServletFilterModuleDescriptor.ASYNC_DEFAULT_SYSPROP, "true");
        resetLazyReferences();

        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();

        descriptor = new ServletFilterModuleDescriptor(
                new PrefixDelegatingModuleFactory(Collections.emptySet()), servletModuleManager);
        descriptor.init(plugin, new Dom4jDelegatingElement(e));
        assertThat(descriptor.isAsyncSupported(), is(true));
    }

    @Test
    public void givenElementWithoutKey_whenValidate_thenThrowsValidationException() {
        descriptor = new ServletFilterModuleDescriptor(
                new PrefixDelegatingModuleFactory(Collections.emptySet()), servletModuleManager);

        var element = DescriptorsTestUtils.mockElement();

        Exception e = assertThrows(ValidationException.class, () -> descriptor.validate(element));
        assertThat(e.getMessage(), containsString("The key attribute is required"));
    }

    @Test
    public void givenElementWithoutDispatcherAndWithoutClass_whenValidate_thenThrowsValidationException() {
        descriptor = new ServletFilterModuleDescriptor(
                new PrefixDelegatingModuleFactory(Collections.emptySet()), servletModuleManager);

        var element = DescriptorsTestUtils.mockElement();
        var initParamElement = DescriptorsTestUtils.mockInitParamElement();

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("url-pattern")).thenReturn(DescriptorsTestUtils.mockElement());
        when(element.elements("dispatcher")).thenReturn(List.of());
        when(element.elements("init-param")).thenReturn(List.of(initParamElement));

        Exception e = assertThrows(ValidationException.class, () -> descriptor.validate(element));
        assertThat(e.getMessage(), containsString("The class is required"));
    }

    @Test
    public void givenElementWithoutDispatcherButWithClass_whenValidate_isValid() {
        descriptor = new ServletFilterModuleDescriptor(
                new PrefixDelegatingModuleFactory(Collections.emptySet()), servletModuleManager);

        var element = DescriptorsTestUtils.mockElement();
        var initParamElement = DescriptorsTestUtils.mockInitParamElement();

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.elements("dispatcher")).thenReturn(Collections.emptyList());
        when(element.attributeValue("class")).thenReturn("some-class-value");
        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("url-pattern")).thenReturn(DescriptorsTestUtils.mockElement());
        when(element.elements("init-param")).thenReturn(List.of(initParamElement));

        descriptor.validate(element);
    }

    @Test
    public void givenElementWithUnknownDispatcherType_whenValidate_thenThrowsValidationException() {
        descriptor = new ServletFilterModuleDescriptor(
                new PrefixDelegatingModuleFactory(Collections.emptySet()), servletModuleManager);

        var element = DescriptorsTestUtils.mockElement();
        var dispatcher = DescriptorsTestUtils.mockElement("unknown-dispatcher-type");
        var initParam = DescriptorsTestUtils.mockInitParamElement();

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.elements("dispatcher")).thenReturn(List.of(dispatcher));
        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("url-pattern")).thenReturn(DescriptorsTestUtils.mockElement());
        when(element.elements("init-param")).thenReturn(List.of(initParam));
        when(dispatcher.getTextTrim()).thenReturn("unknown-type");

        Exception e = assertThrows(ValidationException.class, () -> descriptor.validate(element));
        assertThat(e.getMessage(), containsString("dispatcher"));
    }

    @Test
    public void givenElementWithKnownDispatcherType_whenValidate_isValid() {
        descriptor = new ServletFilterModuleDescriptor(
                new PrefixDelegatingModuleFactory(Collections.emptySet()), servletModuleManager);

        var element = DescriptorsTestUtils.mockElement();
        var dispatcher = DescriptorsTestUtils.mockElement();
        var initParam = DescriptorsTestUtils.mockInitParamElement();

        when(dispatcher.getTextTrim()).thenReturn(DispatcherType.REQUEST.toString());

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.elements("dispatcher")).thenReturn(List.of(dispatcher));
        when(element.attributeValue("class")).thenReturn("some-class-value");
        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("url-pattern")).thenReturn(DescriptorsTestUtils.mockElement());
        when(element.elements("init-param")).thenReturn(List.of(initParam));

        descriptor.validate(element);
    }

    @Test
    public void validateServletFilterWithoutInitParam() throws Exception {
        DescriptorsTestUtils.testElementFromResource(
                "com/atlassian/plugin/servlet/descriptors/servlet-filter-no-init-param.xml", element -> {
                    descriptor.validate(element);
                });
    }

    @Test
    public void validateServletFilterWithInitParam() throws Exception {
        DescriptorsTestUtils.testElementFromResource(
                "com/atlassian/plugin/servlet/descriptors/servlet-filter-with-init-param.xml", element -> {
                    descriptor.validate(element);
                });
    }
}
