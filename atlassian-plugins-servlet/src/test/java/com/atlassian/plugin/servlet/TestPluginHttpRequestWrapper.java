package com.atlassian.plugin.servlet;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Test;

import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptorBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPluginHttpRequestWrapper {

    private static final ServletModuleDescriptor DESCRIPTOR = new ServletModuleDescriptorBuilder().build();

    @Test
    public void testForwardScenario() {
        HttpServletRequest mockInnerRequest = mock(HttpServletRequest.class);

        // Initially mocking it to "/hello" (so that the forward request's basePath is resolved to "/hello". This is
        // replicating the specific issue
        when(mockInnerRequest.getPathInfo()).thenReturn("/forwardSource");

        when(mockInnerRequest.getServletPath()).thenReturn("/plugin/servlet");

        PluginHttpRequestWrapper forwardRequest = new PluginHttpRequestWrapper(
                mockInnerRequest,
                new ServletModuleDescriptorBuilder().withPath("/forwardSource").build());

        // Mocking it to its actual value "/world"
        when(mockInnerRequest.getPathInfo()).thenReturn("/forwardDestination");

        assertEquals("/forwardDestination", forwardRequest.getPathInfo());
    }

    @Test
    public void testWildcardMatching() {
        PluginHttpRequestWrapper request = getWrappedRequest(
                "/context/plugins",
                "/plugin/servlet/path/to/resource",
                new ServletModuleDescriptorBuilder()
                        .withPath("/plugin/servlet/*")
                        .build());

        assertEquals("/path/to/resource", request.getPathInfo());
        assertEquals("/context/plugins/plugin/servlet", request.getServletPath());
    }

    @Test
    public void testExactPathMatching() {
        PluginHttpRequestWrapper request = getWrappedRequest(
                "/context/plugins",
                "/plugin/servlet",
                new ServletModuleDescriptorBuilder().withPath("/plugin/servlet").build());

        assertNull(request.getPathInfo());
        assertEquals("/context/plugins/plugin/servlet", request.getServletPath());
    }

    @Test
    public void testGetSessionFalse() {
        HttpServletRequest mockWrappedRequest = mock(HttpServletRequest.class);
        when(mockWrappedRequest.getPathInfo()).thenReturn(null);
        when(mockWrappedRequest.getSession(false)).thenReturn(null);

        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockWrappedRequest, DESCRIPTOR);

        assertNull(request.getSession(false));
    }

    @Test
    public void testGetSession() {
        // Mock the Session
        HttpSession mockSession = mock(HttpSession.class);
        when(mockSession.getAttribute("foo")).thenReturn("bar");

        // Mock the Request
        HttpServletRequest mockWrappedRequest = mock(HttpServletRequest.class);
        // getPathInfo(0 gets called in constructor
        when(mockWrappedRequest.getPathInfo()).thenReturn(null);
        // delegate will have getSession(true) called and return null.
        when(mockWrappedRequest.getSession(true)).thenReturn(mockSession);

        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockWrappedRequest, DESCRIPTOR);

        HttpSession wrappedSession = request.getSession();
        assertTrue(wrappedSession instanceof PluginHttpSessionWrapper);
        assertEquals("bar", wrappedSession.getAttribute("foo"));
    }

    @Test
    public void testGetSessionTrue() {
        // Mock the Session
        HttpSession mockSession = mock(HttpSession.class);
        when(mockSession.getAttribute("foo")).thenReturn("bar");

        // Mock the Request
        HttpServletRequest mockWrappedRequest = mock(HttpServletRequest.class);
        // getPathInfo(0 gets called in constructor
        when(mockWrappedRequest.getPathInfo()).thenReturn(null);
        // delegate will have getSession(true) called and return null.
        when(mockWrappedRequest.getSession(true)).thenReturn(mockSession);
        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockWrappedRequest, DESCRIPTOR);

        HttpSession wrappedSession = request.getSession(true);
        assertTrue(wrappedSession instanceof PluginHttpSessionWrapper);
        assertEquals("bar", wrappedSession.getAttribute("foo"));
    }

    @Test
    public void testPrefixingWildcardsMatching() {
        PluginHttpRequestWrapper request = getWrappedRequest(
                "/context/plugins",
                "/plugin/servlet-two/path/to/resource",
                new ServletModuleDescriptorBuilder()
                        .withPath("/plugin/servlet/*")
                        .withPath("/plugin/servlet-two/*")
                        .build());

        // should match the second mapping.
        assertEquals("/context/plugins/plugin/servlet-two", request.getServletPath());
        assertEquals("/path/to/resource", request.getPathInfo());
    }

    @Test
    public void testStartAsync() {
        AsyncContext asyncContext = mock(AsyncContext.class);

        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.isAsyncSupported()).thenReturn(true);
        when(mockRequest.startAsync()).thenReturn(asyncContext);

        ServletFilterModuleDescriptor descriptor = mock(ServletFilterModuleDescriptor.class);
        when(descriptor.getCompleteKey()).thenReturn("test.plugin:test.filter");
        when(descriptor.isAsyncSupported()).thenReturn(true);

        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockRequest, descriptor);
        assertTrue(request.isAsyncSupported());
        assertSame(asyncContext, request.startAsync());
    }

    @Test
    public void testStartAsyncThrowsIfNotSupported() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.isAsyncSupported()).thenReturn(true);

        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockRequest, DESCRIPTOR);
        assertFalse(request.isAsyncSupported());
        assertThrows(IllegalStateException.class, request::startAsync);
    }

    @Test
    public void testStartAsyncWithRequestAndResponse() {
        AsyncContext asyncContext = mock(AsyncContext.class);

        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.isAsyncSupported()).thenReturn(true);
        when(mockRequest.startAsync(any(), any())).thenReturn(asyncContext);

        ServletFilterModuleDescriptor descriptor = mock(ServletFilterModuleDescriptor.class);
        when(descriptor.getCompleteKey()).thenReturn("test.plugin:test.filter");
        when(descriptor.isAsyncSupported()).thenReturn(true);

        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockRequest, descriptor);
        assertTrue(request.isAsyncSupported());
        assertSame(asyncContext, request.startAsync(request, mock(HttpServletResponse.class)));
    }

    @Test
    public void testStartAsyncWithRequestAndResponseThrowsIfNotSupported() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.isAsyncSupported()).thenReturn(true);

        PluginHttpRequestWrapper request = new PluginHttpRequestWrapper(mockRequest, DESCRIPTOR);
        assertFalse(request.isAsyncSupported());
        assertThrows(IllegalStateException.class, () -> request.startAsync(request, mock(HttpServletResponse.class)));
    }

    private PluginHttpRequestWrapper getWrappedRequest(
            String servletPath, String pathInfo, ServletModuleDescriptor servletModuleDescriptor) {
        HttpServletRequest mockWrappedRequest = mock(HttpServletRequest.class);
        when(mockWrappedRequest.getServletPath()).thenReturn(servletPath);
        when(mockWrappedRequest.getPathInfo()).thenReturn(pathInfo);

        return new PluginHttpRequestWrapper(mockWrappedRequest, servletModuleDescriptor);
    }
}
