package com.atlassian.plugin.servlet.descriptors;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BaseServletModuleDescriptorTest {
    private final ModuleFactory moduleFactory = mock(ModuleFactory.class);
    private final BaseServletModuleDescriptor baseServletModuleDescriptor =
            new BaseServletModuleDescriptorImpl(moduleFactory);

    @Test
    public void givenElementWithoutKey_whenValidate_thenThrowsValidationException() {
        Element element = mock(Element.class);

        final ValidationException e =
                assertThrows(ValidationException.class, () -> baseServletModuleDescriptor.validate(element));
        assertThat(e.getErrors(), hasItem("The key attribute is required"));
    }

    @Test
    public void givenElementWithoutUrlPattern_whenValidate_thenThrowsValidationException() {
        Element element = mock(Element.class);
        when(element.attributeValue("key")).thenReturn("some-key");

        final ValidationException e =
                assertThrows(ValidationException.class, () -> baseServletModuleDescriptor.validate(element));
        assertThat(e.getErrors(), hasItem("There must be at least one path specified"));
    }

    @Test
    public void givenUrlPatternExistsButInitParamsIsEmpty_whenValidate_thenIsValid() {
        Element element = mock(Element.class);
        Element urlPatternElement = mock(Element.class);

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("url-pattern")).thenReturn(urlPatternElement);
        when(element.elements("init-param")).thenReturn(Collections.emptyList());

        baseServletModuleDescriptor.validate(element);
    }

    @Test
    public void givenInitParamsIsPresentAndBaseElementHasNoParamName_whenValidate_thenThrowsValidationException() {
        var element = mock(Element.class);
        var urlPatternElement = mock(Element.class);
        var initParam = mock(Element.class);

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("url-pattern")).thenReturn(urlPatternElement);
        when(element.elements("init-param")).thenReturn(List.of(initParam));

        final ValidationException e =
                assertThrows(ValidationException.class, () -> baseServletModuleDescriptor.validate(element));
        assertThat(e.getErrors(), hasItem("param-name is required"));
    }

    @Test
    public void givenInitParamsIsPresentAndBaseElementHasNoParamValue_whenValidate_thenThrowsValidationException() {
        var element = mock(Element.class);
        var urlPatternElement = mock(Element.class);
        var initParam = mock(Element.class);
        when(initParam.element("param-name")).thenReturn(mock(Element.class));

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("url-pattern")).thenReturn(urlPatternElement);
        when(element.elements("init-param")).thenReturn(List.of(initParam));

        final ValidationException e =
                assertThrows(ValidationException.class, () -> baseServletModuleDescriptor.validate(element));
        assertThat(e.getErrors(), hasItem("param-value is required"));
    }

    @Test
    public void givenInitParamsIsPresentAndBaseElementHasParamsConfigured_whenValidate_thenIsValid() {
        var element = mock(Element.class);
        var urlPatternElement = mock(Element.class);
        var initParam = DescriptorsTestUtils.mockInitParamElement();

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("url-pattern")).thenReturn(urlPatternElement);
        when(element.elements("init-param")).thenReturn(List.of(initParam));

        baseServletModuleDescriptor.validate(element);
    }

    private class BaseServletModuleDescriptorImpl extends BaseServletModuleDescriptor {
        /**
         * @param moduleCreator a factory for creating this descriptor's module
         * @since 2.5.0
         */
        public BaseServletModuleDescriptorImpl(ModuleFactory moduleCreator) {
            super(moduleCreator);
        }

        @Override
        public Object getModule() {
            return null;
        }
    }
}
