package com.atlassian.plugin.servlet.descriptors;

import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;

public class TestServletContextParamDescriptor {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    ServletContextParamModuleDescriptor descriptor;

    @Before
    public void setUp() {
        descriptor = new ServletContextParamModuleDescriptor();
    }

    @Test
    public void testInit() {
        Plugin plugin = new StaticPlugin();
        plugin.setKey("somekey");
        Element e = getValidConfig();
        descriptor.init(plugin, new Dom4jDelegatingElement(e));
    }

    private Element getValidConfig() {
        Element e = new DOMElement("servlet-context-param");
        e.addAttribute("key", "key2");
        Element paramName = new DOMElement("param-name");
        paramName.setText("test.param.name");
        e.add(paramName);
        Element paramValue = new DOMElement("param-value");
        paramValue.setText("test.param.value");
        e.add(paramValue);
        return e;
    }

    @Test
    public void testInitWithNoParamName() {
        Plugin plugin = new StaticPlugin();
        plugin.setKey("somekey");
        Element e = new DOMElement("servlet-context-param");
        e.addAttribute("key", "key2");
        Element paramValue = new DOMElement("param-value");
        paramValue.setText("test.param.value");
        e.add(paramValue);
        expectedException.expect(PluginParseException.class);
        descriptor.init(plugin, new Dom4jDelegatingElement(e));
    }

    @Test
    public void testInitWithNoParamValue() {
        Plugin plugin = new StaticPlugin();
        plugin.setKey("somekey");
        Element e = new DOMElement("servlet-context-param");
        e.addAttribute("key", "key2");
        Element paramName = new DOMElement("param-name");
        paramName.setText("test.param.name");
        e.add(paramName);
        expectedException.expect(PluginParseException.class);
        descriptor.init(plugin, new Dom4jDelegatingElement(e));
    }
}
