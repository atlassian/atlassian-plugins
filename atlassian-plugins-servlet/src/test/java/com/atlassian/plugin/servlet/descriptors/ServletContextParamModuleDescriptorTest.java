package com.atlassian.plugin.servlet.descriptors;

import org.junit.Test;

import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.util.validation.ValidationException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ServletContextParamModuleDescriptorTest {

    private final ServletContextParamModuleDescriptor servletContextParamModuleDescriptor =
            new ServletContextParamModuleDescriptor();

    @Test(expected = ValidationException.class)
    public void givenElementWithoutKey_whenValidate_thenThrowsValidationException() {
        Element element = mock(Element.class);

        servletContextParamModuleDescriptor.validate(element);
    }

    @Test(expected = ValidationException.class)
    public void givenElementWithoutUrlPattern_whenValidate_thenThrowsValidationException() {
        Element element = mock(Element.class);
        when(element.attributeValue("key")).thenReturn("some-key");

        servletContextParamModuleDescriptor.validate(element);
    }

    @Test(expected = ValidationException.class)
    public void givenParamNameIsNull_whenValidate_thenThrowsValidationException() {
        Element element = mock(Element.class);

        when(element.attributeValue("key")).thenReturn("some-key");

        servletContextParamModuleDescriptor.validate(element);
    }

    @Test(expected = ValidationException.class)
    public void givenParamValueIsNull_whenValidate_thenThrowsValidationException() {
        Element element = mock(Element.class);

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("param-name")).thenReturn(mock(Element.class));

        servletContextParamModuleDescriptor.validate(element);
    }

    @Test
    public void givenParamsAreConfigured_whenValidate_thenIsValid() {
        Element element = mock(Element.class);

        when(element.attributeValue("key")).thenReturn("some-key");
        when(element.element("param-name")).thenReturn(mock(Element.class));
        when(element.element("param-value")).thenReturn(mock(Element.class));

        servletContextParamModuleDescriptor.validate(element);
    }
}
