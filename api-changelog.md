# API Changes

## 5.7.0

### Added

* Access to the current Thread's plugin callstack

#### atlassian-plugins-api
* `com.atlassian.plugin.util.PluginKeyStack`

## 5.1.0

### Added

* Access to persistent plugin state

#### atlassian-plugins-api
* `com.atlassian.plugin.StoredPluginStateAccessor`
* `com.atlassian.plugin.StoredPluginState` - extracted from `com.atlassian.plugins.manager.PluginPersistentState`
* `com.atlassian.plugin.manager.PluginEnabledState` - moved from `atlassian-plugins-core`

## 5.0.0

### Changed

* Replaced all API usages of `atlassian-fugue` `Option` with `java.util.Optional`  

### Removed

#### atlassian-plugins-api
* `com.atlassian.plugin.ModuleDescriptor`
    * `destroy(Plugin)` - use `destroy()` instead
* `com.atlassian.plugin.Plugin`
    * `isEnabled()` - use `getPluginState()` instead 
    * `setEnabled(boolean)` - use `enable()` or `disable()` instead
    * `close()` - use `uninstall()` instead
    * `getRequiredPlugins()` - use `getDependencies()`
    * `NAME_COMPARATOR` - removed without replacement
    * `Resolvable` - the `Resolvable` methods are available directly on `Plugin`
    * `EnabledMetricsSource` - methods are now directly on `Plugin`
* `com.atlassian.plugin.PluginArtifact`
    * `AllowsReference` - use `PluginArtifact.getReferenceMode()`
* `com.atlassian.plugin.PluginInformation`
    * Removed `minVersion` and `maxVersion` without replacement. The fields were unused.
* `com.atlassian.plugin.PluginAccessor`
    * `getEnabledModulesByClassAndDescriptor()` - use `getModules(Predicate)`
    * `getEnabledModulesDescriptorsByType()` - use `getModules(Predicate)`
    * `getPluginResourceAsStream(String key, String resource)` - use `getPlugin(key).getClassLoader().getResourceAsStream(resource)`
    * `getDynamicPluginClass(String)` - use `getClassLoader().loadClass(String)`
* `com.atlassian.plugin.PluginController`
    * `enablePlugin(String)` - use `enablePlugins(String[])`
    * `installPlugin(PluginArtifact)` - use `installPlugins(PluginArtifact[])`
* `com.atlassian.plugin.PluginManager` - use `PluginController`, `PluginAccessor` or `PluginSystemLifecycle`
* `com.atlassian.plugin.Resourced`
    * `getResourceDescriptors(String type)` - use `getResourceDescriptors()` and filter as required
* `com.atlassian.plugin.Resources`
    * constructor `Resources(Iterable<ResourceDescriptor>)` - use `Resources.fromXml(Element)` instead
    * `getResourceDescriptors(String)` - use `getResourceDescriptors()` and filter results
* `com.atlassian.plugins.event.events.BeforePluginDisabledEvent` - use `PluginDisablingEvent`
* `com.atlassian.plugins.event.events.BeforePluginDisabledEvent` - use `PluginDisablingEvent`
* `com.atlassian.plugins.event.events.BeforePluginModuleDisabledEvent` - use `PluginModuleDisablingEvent`
* `com.atlassian.plugin.factories.PluginFactory`
    * `create(DeploymentUnit, ModuleDescriptorFactory)` - use `create(PluginArtifact, ModuleDescriptorFactory)`
* `com.atlassian.plugin.loaders.LoaderUtils`
    * `getResourceDescriptors(Element)` - use `com.atlassian.plugin.Resources.fromXml(Element)`
    
#### atlassian-plugins-core
* `com.atlassian.plugin.DefaultModuleDescriptorFactory`
    * constructor `DefaultModuleDescriptorFactory()` - use `DefaultModuleDescriptorFactory(HostContainer)`
* `com.atlassian.plugin.classloader.PluginsClassLoader`
    * constructor `PluginsClassLoader(ClassLoader, PluginAccessor)` - use `PluginsClassLoader(ClassLoader, PluginAccessor, PluginEventManager)`
* `com.atlassian.plugin.descriptors.AbstractModuleDescriptor`
    * field `singleton` - no replacement
    * `loadClass(Plugin, Element)` - use `loadClass(Plugin, String)`
    * `isSingleton()` - no replacement
    * `isSingletonByDefault()` - no replacement
* `com.atlassian.plugin.descriptors.ResourcedModuleDescriptor` - use `ModuleDescriptor`
* `com.atlassian.plugin.factories.LegacyDynamicPluginFactory`
    * `createPlugin(DeploymentUnit, PluginClassLoader)` - use `createPlugin(PluginArtifact, PluginClassLoader)`
* `com.atlassian.plugin.impl.AbstractPlugin`
    * constructor `AbstractPlugin()` - use `AbstractPlugin(PluginArtifact)`
* `com.atlassian.plugin.loaders.ScanningPluginLoader`
    * `shutDown()` - shutdown will automatically occur when the plugin framework is shutdown
* `com.atlassian.plugin.loaders.SinglePluginLoader`
    * constructor `SinglePluginLoader(InputStream)` - use `SinglePluginLoader(URL)`
* `com.atlassian.plugin.manager.DefaultPluginManager`
    * `getStore()` - no replacement
    * `removeStateFromStore(PluginPersistentStateStore, Plugin)` - no replacement
    * `addPlugin(PluginLoader, Plugin)` - use `addPlugins(PluginLoader, Collection<Plugin>)`
    * `disablePluginModuleState(ModuleDescriptor, PluginPersistentStateStore)` - no replacement
    * `disablePluginState(Plugin, PluginPersistentStateStore)` - no replacement
    * `enablePluginModuleState(ModuleDescriptor, PluginPersistentStateStore)` - no replacement
    * `enablePluginState(Plugin, PluginPersistentStateStore)` - no replacement
    * `notifyPluginDisabled(Plugin)` - listen for `PluginDisablingEvent`, `PluginDisabledEvent` and/or use `disablePlugin`/`disablePluginWithoutPersisting`
    * `notifyPluginEnabled(Plugin)` - listen for `PluginEnablingEvent`, `PluginEnabledEvent` and/or use `enablePlugin`/`enablePluginWithoutPersisting`
* `com.atlassian.plugin.manager.DefaultPluginPersistentState`
    * all public constructors - use `PluginPersistentState.Builder`
* `com.atlassian.plugin.manager.PluginEnabler`    
    * removed support for scanning of optional dependencies (`com.atlassian.plugin.manager.PluginEnabler.enableOptionalRequirements` system property)
* `com.atlassian.plugin.manager.PluginPersistentStateModifier`
    * `getStore()` - no replacement
* `com.atlassian.plugin.module.PrefixDelegatingModuleFactory`
    * `guessModuleClass(String, ModuleDescriptor)` - no replacement
* `com.atlassian.plugin.predicate.EnabledModulePredicate`
    * constructor `EnabledModulePredicate(PluginAccessor)` - use the default constructor instead
* `com.atlassian.plugin.predicate.ModulePredicate` - no replacement 
* `com.atlassian.plugin.util.FileUtils`
    * `toFile(URL)` - use commons-io `FileUtils.toFile(URL)`
    * `deleteDir(File)` - use commons-io `FileUtils.deleteDirectory(File)`
* `com.atlassian.plugin.util.CopyOnWriteMap` - use atlassian-concurrent's `com.atlassian.util.concurrent.CopyOnWriteMap`
* `com.atlassian.plugin.util.collect.Consumer` - switched over all usages to `java.util.function.Consumer`
* `com.atlassian.plugin.util.collect.FilteredIterator` - switched over all usages to use Java streams
* `com.atlassian.plugin.util.collect.Function` - switched over all usages to `java.util.function.Function`
* `com.atlassian.plugin.util.collect.Predicate` - switched over all usages to `java.util.function.Predicate`
* `com.atlassian.plugin.util.collect.TransformingIterator` - switched over all usages to use Java streams
* `com.atlassian.plugin.util.collect.Transforms` - switched over all usages to use Java streams

#### atlassian-plugins-main

* `com.atlassian.plugin.main.AtlassianPlugins`
    * `start()` - use `afterPropertiesSet()`, followed by `getPluginLifecycle().init()`
    * `stop()` - use `getPluginLifecycle().shutdown()`, followed by `destroy()`

#### atlassian-plugins-osgi

* `com.atlassian.plugin.osgi.container.OsgiContainerManager`
    * `installBundle(File)` - use `installBundle(File, ReferenceMode)`. The old implementation used `FORBID_REFERENCE`
    * `AllowsReferenceInstall` - use `installBundle(File, ReferenceMode)`
* `com.atlassian.plugin.osgi.container.felix.ExportsBuilder`
    * `determineExports(List<HostComponentRegistration>, PackageScannerConfiguration, File)` - use `getExports`
* `com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager`
    * constructor `(URL, File, PackageScannerConfiguration, HostComponentProvider, PluginEventManager eventManager)` - use `FelixOsgiContainerManager(URL, OsgiPersistentCache, PackageScannerConfiguration, HostComponentProvider, PluginEventManager)`
* `com.atlassian.plugin.osgi.container.impl.DefaultOsgiPersistentCache`
    * constructor `(File, String)` - use `DefaultOsgiPersistentCache(File)`
* `com.atlassian.plugin.osgi.factory.OsgiBundlePlugin`
    * constructor `(Bundle, String, PluginArtifact)` - use `OsgiBundlePlugin(OsgiContainerManager, String, PluginArtifact)`
* `com.atlassian.plugin.osgi.factory.OsgiPluginHelper` 
    * `getRequiredPlugins()` - use `getDependencies().getAll()`
* `com.atlassian.plugin.osgi.factory.descriptor.ComponentModuleDescriptor`
    * `getModuleClassName()` - removed from interface, but still a protected method on `AbstractModuleDescriptor` 
* `com.atlassian.plugin.osgi.util.OsgiHeaderUtil`
    * `findReferredPackages(List)` - use `findReferredPackageNames(java.util.Collection<Class<?>>)`
    * `findReferredPackages(List, Map)` - use `findReferredPackageVersions(java.util.List, java.util.Map)`
    * `findReferredPackageNames(List)` - use `findReferredPackageNames(java.util.Collection<java.lang.Class>)` 
      where the collection of classes is derived from the list of `HostComponentRegistration` as follows:
      ```java
      Set<Class<?>> declaredInterfaces = registrations.stream()
                      .flatMap(reg -> Arrays.stream(reg.getMainInterfaceClasses()))
                      .collect(Collectors.toSet());
      ```

#### atlassian-plugins-servlet

* `com.atlassian.plugin.servlet.DefaultServletModuleManager`
    * constructor `(PluginEventManager, PathMapper, PathMapper)` - use `DefaultServletModuleManager(PluginEventManager, PathMapper, PathMapper, FilterFactory)`
    * `onPlatformFrameworkShutdown(PlatformFrameworkShutdownEvent)` - cleanup is already done in `onPluginFrameworkBeforeShutdown`
* `com.atlassian.plugin.servlet.ServletModuleManager`
    * `getFilters(FilterLocation, String, FilterConfig)` - use `getFilters(FilterLocation, String, FilterConfig, DispatcherType)`
* `com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor`
    * `getServlet()` - use `getModule()`
* `com.atlassian.plugin.servlet.util.LazyLoadedReference` - use `com.atlassian.util.concurrent.LazyReference` from atlassian-concurrent
* `com.atlassian.plugin.servlet.util.ClassLoaderStack` - use `com.atlassian.plugin.util.ClassLoaderStack`

#### atlassian-plugins-validation

* `com.atlassian.plugin.validation.DescriptorValidator`
    * constructor `(InputSupplier<? extends Reader>, InputSupplier<? extends Reader>, Set<Application>)` - use `DescriptorValidator(InputStream, InputStream, Set)`
* `com.atlassian.plugin.validation.Dom4jUtils`
    * `readDocument(InputSupplier<? extends Reader>` - use `readDocument(InputStream)`
    
#### atlassian-plugins-webresource-common
* `com.atlassian.plugins.servlet.ResourceUrlParser` - no replacement
* `com.atlassian.plugins.servlet.PluginResource` - no replacement
* `com.atlassian.plugins.servlet.ResourceDownloadUtils`
    * `serveFileImpl(HttpServletResponse, InputStream)` - use `IOUtils.copy(InputStream, OutputStream)` and flush and close the streams
    * `addCachingHeaders(HttpServletRequest, HttpServletResponse)` - use `addPublicCachingHeaders(HttpServletRequest, HttpServletResponse)` or `#addPrivateCachingHeaders(HttpServletRequest, HttpServletResponse)`
    
### Deprecated

* `com.atlassian.plugin.Plugin`
    * `getPlugins(PluginPredicate)` - use `getPlugin(Predicate)`
    * `getModules(ModuleDescriptorPredicate)` - use `getModules(Predicate)`
    * `getModuleDescriptors(ModuleDescriptorPredicate)` - use `getModuleDescriptors(Predicate)`
* `com.atlassian.plugin.predicate.ModuleDescriptorPredicate`
* `com.atlassian.plugin.predicate.PluginPredicate`