package com.atlassian.plugin.osgi.bridge;

import java.util.Dictionary;
import java.util.Hashtable;

import org.springframework.context.ApplicationContext;
import org.eclipse.gemini.blueprint.context.ConfigurableOsgiBundleApplicationContext;
import org.eclipse.gemini.blueprint.extender.event.BootstrappingDependencyEvent;
import org.eclipse.gemini.blueprint.service.importer.OsgiServiceDependency;
import org.eclipse.gemini.blueprint.service.importer.event.OsgiServiceDependencyWaitStartingEvent;
import org.eclipse.gemini.blueprint.service.importer.support.AbstractOsgiServiceImportFactoryBean;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.osgi.event.PluginServiceDependencyWaitStartingEvent;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

public class TestSpringContextEventBridge {

    private Bundle bundle;
    private PluginEventManager eventManager;
    private SpringContextEventBridge bridge;

    @Before
    public void setUp() {
        eventManager = mock(PluginEventManager.class);

        bridge = new SpringContextEventBridge(eventManager);

        Dictionary<String, String> headers = new Hashtable<>();
        headers.put("Atlassian-Plugin-Key", "foo");

        bundle = mock(Bundle.class);
        when(bundle.getHeaders()).thenReturn(headers);
    }

    @Test
    public void testWaitingEventWithApplicationContext() {
        ConfigurableOsgiBundleApplicationContext source = mock(ConfigurableOsgiBundleApplicationContext.class);
        when(source.getBundle()).thenReturn(bundle);
        OsgiServiceDependencyWaitStartingEvent startingEvent =
                new OsgiServiceDependencyWaitStartingEvent(source, mock(OsgiServiceDependency.class), 1000);
        BootstrappingDependencyEvent bootstrapEvent =
                new BootstrappingDependencyEvent(mock(ApplicationContext.class), bundle, startingEvent);
        bridge.onOsgiApplicationEvent(bootstrapEvent);

        verify(eventManager).broadcast(isPluginKey("foo"));
    }

    @Test
    public void testWaitingEventWithServiceFactoryBean() {
        AbstractOsgiServiceImportFactoryBean source = mock(AbstractOsgiServiceImportFactoryBean.class);
        when(source.getBeanName()).thenReturn("bar");
        BundleContext ctx = mock(BundleContext.class);
        when(ctx.getBundle()).thenReturn(bundle);
        when(source.getBundleContext()).thenReturn(ctx);
        OsgiServiceDependencyWaitStartingEvent startingEvent =
                new OsgiServiceDependencyWaitStartingEvent(source, mock(OsgiServiceDependency.class), 1000);
        BootstrappingDependencyEvent bootstrapEvent =
                new BootstrappingDependencyEvent(mock(ApplicationContext.class), bundle, startingEvent);
        bridge.onOsgiApplicationEvent(bootstrapEvent);

        verify(eventManager).broadcast(isPluginKey("foo"));
    }

    private Object isPluginKey(String key) {
        return argThat(new PluginKeyMatcher(key));
    }

    private static class PluginKeyMatcher extends BaseMatcher<PluginServiceDependencyWaitStartingEvent> {
        private String key;

        PluginKeyMatcher(String key) {
            this.key = key;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("plugin with key ");
        }

        public boolean matches(Object o) {
            return key.equals(((PluginServiceDependencyWaitStartingEvent) o).getPluginKey());
        }
    }
}
