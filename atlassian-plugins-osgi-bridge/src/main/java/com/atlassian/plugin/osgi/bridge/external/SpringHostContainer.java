package com.atlassian.plugin.osgi.bridge.external;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.atlassian.plugin.hostcontainer.HostContainer;

/**
 * Host container implementation that uses the bundle's application context
 *
 * @since 2.2.0
 */
public class SpringHostContainer implements HostContainer, ApplicationContextAware {
    private ApplicationContext applicationContext;

    public <T> T create(Class<T> moduleClass) {
        if (applicationContext == null) {
            throw new IllegalStateException("Application context missing");
        }
        //noinspection unchecked
        return (T) applicationContext
                .getAutowireCapableBeanFactory()
                .createBean(moduleClass, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false);
    }

    public void setApplicationContext(@Nonnull ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
