package com.atlassian.plugin.internal.validation;

import java.io.IOException;
import java.io.InputStream;

import org.dom4j.Document;

final class ResourcesLoader {
    static Document getTestDocument(String name) throws IOException {
        try (InputStream inputStream = getInput(name)) {
            return Dom4jUtils.readDocument(inputStream);
        }
    }

    static InputStream getInput(final String name) {
        return ResourcesLoader.class.getResourceAsStream(name);
    }
}
