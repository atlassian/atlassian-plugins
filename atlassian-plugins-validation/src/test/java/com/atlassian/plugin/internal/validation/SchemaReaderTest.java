package com.atlassian.plugin.internal.validation;

import java.util.Map;
import java.util.Set;

import org.junit.Test;
import com.google.common.collect.Iterables;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugin.internal.validation.ResourcesLoader.getTestDocument;

public final class SchemaReaderTest {
    @Test
    public void testGetAllowedPermissions() throws Exception {
        final SchemaReader schemaReader = new SchemaReader(getTestDocument("/schema.xsd"));
        final Set<String> allowedPermissions = schemaReader.getAllowedPermissions();

        assertTrue(allowedPermissions.contains("execute_java"));
    }

    @Test
    public void testGetModulesRequiredPermissions() throws Exception {
        final SchemaReader schemaReader = new SchemaReader(getTestDocument("/schema.xsd"));
        final Map<String, Set<String>> modulesRequiredPermissions = schemaReader.getModulesRequiredPermissions();

        assertEquals(0, Iterables.size(modulesRequiredPermissions.get("resource")));

        assertEquals(1, Iterables.size(modulesRequiredPermissions.get("plugin-permission")));
        assertEquals("execute_java", Iterables.getFirst(modulesRequiredPermissions.get("plugin-permission"), null));
    }
}
