package com.atlassian.plugin.internal.validation;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import com.google.common.collect.ImmutableMap;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import static com.atlassian.plugin.internal.parsers.XmlDescriptorParserUtils.removeAllNamespaces;

/**
 * Reads information from the schema of plugin descriptors (dynamic per instance)
 *
 * @since 3.0.0
 */
public final class SchemaReader {
    private Document schema;

    public SchemaReader(Document schema) {
        this.schema = removeAllNamespaces(checkNotNull(schema));
    }

    public Set<String> getAllowedPermissions() {
        return Collections.unmodifiableSet(getPermissionElements().stream()
                .map(permission -> permission.attributeValue("value"))
                .collect(toSet()));
    }

    public Map<String, Set<String>> getModulesRequiredPermissions() {
        final List<Element> modulesDefinitions =
                selectNodes("//complexType[@name='AtlassianPluginType']/sequence/choice/element");
        ImmutableMap.Builder<String, Set<String>> permissions = ImmutableMap.builder();
        for (Element modulesDefinition : modulesDefinitions) {
            permissions.put(modulesDefinition.attributeValue("name"), getModuleRequiredPermissions(modulesDefinition));
        }
        return permissions.build();
    }

    private Set<String> getModuleRequiredPermissions(Element modulesDefinition) {
        return selectNodes(modulesDefinition, "annotation//required-permissions/permission").stream()
                .map(Element::getTextTrim)
                .collect(collectingAndThen(toSet(), Collections::unmodifiableSet));
    }

    private List<Element> getPermissionElements() {
        return selectNodes("//simpleType[@name='PermissionValueType']//enumeration");
    }

    private List<Element> selectNodes(String xpathExpression) {
        return selectNodes(schema, xpathExpression);
    }

    @SuppressWarnings("unchecked")
    private static List<Element> selectNodes(Node node, String xpathExpression) {
        return node.selectNodes(xpathExpression).stream()
                .filter(Element.class::isInstance)
                .map(Element.class::cast)
                .collect(toList());
    }
}
