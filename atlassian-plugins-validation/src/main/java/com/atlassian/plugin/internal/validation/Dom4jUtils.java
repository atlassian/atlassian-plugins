package com.atlassian.plugin.internal.validation;

import java.io.InputStream;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

/**
 * @since 3.0.0
 */
abstract class Dom4jUtils {
    private Dom4jUtils() {}

    public static Document readDocument(final InputStream input) {
        try {
            return getSaxReader().read(input);
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
    }

    private static SAXReader getSaxReader() {
        final SAXReader reader = new SAXReader();
        reader.setMergeAdjacentText(true);
        return reader;
    }
}
