package com.atlassian.plugin.util;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableSet;

/**
 * The stack to maintain the keys of a list of services which the service itself invokes.
 *
 * @since 5.7.x
 */
public final class PluginKeyStack {

    // Cluster-safe because the stack has different contents per node
    private static final ThreadLocal<Deque<String>> PLUGIN_KEY_STACK = new ThreadLocal<>();

    private PluginKeyStack() {}

    /**
     * A service will add a pluginKey to the stack at the point at which it invokes another service
     * @param pluginKey
     */
    public static void push(String pluginKey) {
        Deque<String> stack = PLUGIN_KEY_STACK.get();
        if (stack == null) {
            stack = new LinkedList<>();
            PLUGIN_KEY_STACK.set(stack);
        }
        stack.push(pluginKey);
    }

    /**
     * A service will remove a pluginKey from the stack at the point at which the method
     * invocation of another service is complete
     */
    public static String pop() {
        Deque<String> stack = PLUGIN_KEY_STACK.get();
        if (stack == null || stack.isEmpty()) {
            return null;
        }
        try {
            return stack.pop();
        } finally {
            if (stack.isEmpty()) {
                PLUGIN_KEY_STACK.remove();
            }
        }
    }

    /**
     * A set of pluginKeys which represent the services which the current service has invoked
     */
    public static Set<String> getPluginKeys() {
        Deque<String> stack = PLUGIN_KEY_STACK.get();
        if (stack == null || stack.isEmpty()) {
            return emptySet();
        }

        return unmodifiableSet(new HashSet<>(stack));
    }

    /**
     * The first pluginKey which was pushed to the stack
     */
    public static String getFirstPluginKey() {
        Deque<String> stack = PLUGIN_KEY_STACK.get();
        if (stack == null || stack.isEmpty()) {
            return null;
        }

        return stack.getLast();
    }
}
