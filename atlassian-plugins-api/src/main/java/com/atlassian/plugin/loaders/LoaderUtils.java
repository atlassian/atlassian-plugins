package com.atlassian.plugin.loaders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.module.Element;

public class LoaderUtils {

    public static Map<String, String> getParams(final Element element) {
        final List<Element> elements = element.elements("param");

        final Map<String, String> params = new HashMap<>(elements.size());

        for (final Element paramEl : elements) {
            final String name = paramEl.attributeValue("name");
            String value = paramEl.attributeValue("value");

            if ((value == null) && (paramEl.getTextTrim() != null) && !"".equals(paramEl.getTextTrim())) {
                value = paramEl.getTextTrim();
            }

            params.put(name, value);
        }

        return params;
    }
}
