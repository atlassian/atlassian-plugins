package com.atlassian.plugin;

import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.Internal;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;

/**
 * Dummy plugin implementation that returns java default values and does nothing.
 * <p>
 * Should be extended when a {@link Plugin} implementation is required, however must be compatible with multiple
 * versions of that interface.
 * <p>
 * All methods are guaranteed to be implemented.
 *
 * @since 3.2.22
 */
@Internal
public class DummyPlugin implements Plugin {
    @Override
    public int getPluginsVersion() {
        return 0;
    }

    @Override
    public void setPluginsVersion(final int version) {
        // Do nothing
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setName(final String name) {
        // Do nothing
    }

    @Override
    public String getI18nNameKey() {
        return null;
    }

    @Override
    public void setI18nNameKey(final String i18nNameKey) {
        // Do nothing
    }

    @Override
    public String getKey() {
        return null;
    }

    @Override
    public void setKey(final String aPackage) {
        // Do nothing
    }

    @Override
    public void addModuleDescriptor(final ModuleDescriptor<?> moduleDescriptor) {
        // Do nothing
    }

    @Override
    public Collection<ModuleDescriptor<?>> getModuleDescriptors() {
        return null;
    }

    @Override
    public ModuleDescriptor<?> getModuleDescriptor(final String key) {
        return null;
    }

    @Override
    public <M> List<ModuleDescriptor<M>> getModuleDescriptorsByModuleClass(final Class<M> moduleClass) {
        return null;
    }

    @Override
    public InstallationMode getInstallationMode() {
        return null;
    }

    @Override
    public boolean isEnabledByDefault() {
        return false;
    }

    @Override
    public void setEnabledByDefault(final boolean enabledByDefault) {
        // Do nothing
    }

    @Override
    public PluginInformation getPluginInformation() {
        return null;
    }

    @Override
    public void setPluginInformation(final PluginInformation pluginInformation) {
        // Do nothing
    }

    @Override
    public void setResources(final Resourced resources) {
        // Do nothing
    }

    @Override
    public PluginState getPluginState() {
        return null;
    }

    @Override
    public boolean isSystemPlugin() {
        return false;
    }

    @Override
    public void setSystemPlugin(final boolean system) {
        // Do nothing
    }

    @Override
    public boolean containsSystemModule() {
        return false;
    }

    @Override
    public boolean isBundledPlugin() {
        return false;
    }

    @Override
    public Date getDateLoaded() {
        return null;
    }

    @Override
    public Date getDateInstalled() {
        return null;
    }

    @Override
    public boolean isUninstallable() {
        return false;
    }

    @Override
    public boolean isDeleteable() {
        return false;
    }

    @Override
    public boolean isDynamicallyLoaded() {
        return false;
    }

    @Override
    public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
        return null;
    }

    @Override
    public ClassLoader getClassLoader() {
        return null;
    }

    @Override
    public URL getResource(final String path) {
        return null;
    }

    @Override
    public InputStream getResourceAsStream(final String name) {
        return null;
    }

    @Override
    public void install() {
        // Do nothing
    }

    @Override
    public void uninstall() {
        // Do nothing
    }

    @Override
    public void enable() {
        // Do nothing
    }

    @Override
    public void disable() {
        // Do nothing
    }

    @Nonnull
    @Override
    public PluginDependencies getDependencies() {
        return new PluginDependencies();
    }

    @Override
    public Set<String> getActivePermissions() {
        return null;
    }

    @Override
    public boolean hasAllPermissions() {
        return false;
    }

    @Override
    public void resolve() {
        // Do nothing
    }

    @Nullable
    @Override
    public Date getDateEnabling() {
        return null;
    }

    @Nullable
    @Override
    public Date getDateEnabled() {
        return null;
    }

    @Override
    public PluginArtifact getPluginArtifact() {
        return null;
    }

    @Override
    public int compareTo(final Plugin o) {
        return 0;
    }

    @Override
    public List<ResourceDescriptor> getResourceDescriptors() {
        return null;
    }

    @Override
    public ResourceDescriptor getResourceDescriptor(final String type, final String name) {
        return null;
    }

    @Override
    public ResourceLocation getResourceLocation(final String type, final String name) {
        return null;
    }
}
