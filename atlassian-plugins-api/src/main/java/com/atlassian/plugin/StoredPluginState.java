package com.atlassian.plugin;

import java.util.Map;

import com.atlassian.plugin.manager.PluginEnabledState;

/**
 * A snapshot of persistent state data for all plugins -- whether the plugin has been disabled by the user, and
 * whether anything special is to happen on restart.
 *
 * This class does not know whether a Plugin has successfully started -- it is just reporting whether the plugin has
 * been explicitly disabled.
 *
 * The methods on this interface were pulled up from PluginPersistentState
 *
 * @since 5.1.0
 */
public interface StoredPluginState {
    /**
     * Get the map of all states with the update timestamp for each of them.
     *
     * If the plugin or module has no entry in the Map, then it is in the default state.
     *
     * @return a map of plugins and modules' keys to their stored state
     */
    Map<String, PluginEnabledState> getStatesMap();

    /**
     * Whether or not a plugin is enabled, calculated from its persisted state AND default state.
     *
     * The default state is 'enabled' unless there is a disabled='true' attribute in atlassian-plugin.xml, or the
     * minimum java-version specified in atlassian-plugin.xml is higher than the version we are running in.
     *
     * This does not report whether the plugin has successfully started -- use {@link PluginAccessor} for that
     * information.
     */
    boolean isEnabled(Plugin plugin);

    /**
     * Whether or not a given plugin module is enabled, calculated from its persisted state AND default state.
     *
     * The default state is 'enabled' unless there is a disabled='true' attribute in atlassian-plugin.xml, or the
     * minimum java-version specified in atlassian-plugin.xml is higher than the version we are running in.
     *
     * This does not report whether the plugin has successfully started -- use {@link PluginAccessor} for that
     * information.
     */
    boolean isEnabled(ModuleDescriptor<?> pluginModule);

    /**
     * Get state map of the given plugin and its modules.
     *
     * If the plugin or module has no entry in the Map, then it is in the default state.
     *
     * @param plugin the plugin
     * @return a map of plugin and module keys to their stored state
     */
    Map<String, PluginEnabledState> getPluginEnabledStateMap(Plugin plugin);

    /**
     * Gets whether the plugin is expected to be upgraded, installed, or removed on next restart.
     *
     * @param pluginKey The plugin to query
     * @return The state of the plugin on restart
     */
    PluginRestartState getPluginRestartState(String pluginKey);
}
