package com.atlassian.plugin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Comparator.comparingInt;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;

/**
 * All plugin keys which are dependents of another plugin, divided into OSGi style import resolutions: mandatory
 * (default), optional and dynamic.
 *
 * @see Plugin#getDependencies()
 * @since 4.0
 */
public class PluginDependencies {
    // dependency types in the order of significance
    public enum Type {
        MANDATORY,
        OPTIONAL,
        DYNAMIC;

        public boolean lessSignificant(final Type other) {
            return ordinal() > other.ordinal();
        }
    }

    // Plugin keys of mandatory, optional and dynamic dependencies
    // keys can appear in multiple sets
    private final Set<String> mandatory;
    private final Set<String> optional;
    private final Set<String> dynamic;
    /**
     * Plugin keys of all dependencies
     */
    private final Set<String> all;

    private final Map<String, SortedSet<Type>> byPluginKey;

    public PluginDependencies() {
        this(null, null, null);
    }

    public PluginDependencies(final Set<String> mandatory, final Set<String> optional, final Set<String> dynamic) {
        this.mandatory = mandatory == null ? emptySet() : Set.copyOf(mandatory);
        this.optional = optional == null ? emptySet() : Set.copyOf(optional);
        this.dynamic = dynamic == null ? emptySet() : Set.copyOf(dynamic);

        final Set<String> combined = new HashSet<>();
        combined.addAll(this.mandatory);
        combined.addAll(this.optional);
        combined.addAll(this.dynamic);
        this.all = Set.copyOf(combined);

        final Map<String, SortedSet<Type>> byPluginKeyBuilder = new HashMap<>();
        for (final String key : this.mandatory) {
            byPluginKeyBuilder
                    .computeIfAbsent(key, k -> new TreeSet<>(comparingInt(Type::ordinal)))
                    .add(MANDATORY);
        }
        for (final String key : this.optional) {
            byPluginKeyBuilder
                    .computeIfAbsent(key, k -> new TreeSet<>(comparingInt(Type::ordinal)))
                    .add(OPTIONAL);
        }
        for (final String key : this.dynamic) {
            byPluginKeyBuilder
                    .computeIfAbsent(key, k -> new TreeSet<>(comparingInt(Type::ordinal)))
                    .add(DYNAMIC);
        }

        this.byPluginKey = Map.copyOf(byPluginKeyBuilder);
    }

    public Set<String> getMandatory() {
        return mandatory;
    }

    public Set<String> getOptional() {
        return optional;
    }

    public Set<String> getDynamic() {
        return dynamic;
    }

    public Set<String> getAll() {
        return all;
    }

    /**
     * @since 5.6.0
     * @return map of pluginKey to supported Types.
     */
    public Map<String, SortedSet<Type>> getTypesByPluginKey() {
        final Map<String, TreeSet<Type>> typesByPluginKeyLocal = new HashMap<>();
        for (Map.Entry<String, SortedSet<Type>> entry : byPluginKey.entrySet()) {
            typesByPluginKeyLocal.computeIfAbsent(entry.getKey(), x -> new TreeSet<>(entry.getValue()));
        }
        return Map.copyOf(typesByPluginKeyLocal);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        final Set<String> mandatory = new HashSet<>();
        final Set<String> optional = new HashSet<>();
        final Set<String> dynamic = new HashSet<>();

        private Builder() {}

        public Builder withMandatory(String... pluginKey) {
            mandatory.addAll(asList(pluginKey));
            return this;
        }

        public Builder withOptional(String... pluginKey) {
            optional.addAll(asList(pluginKey));
            return this;
        }

        public Builder withDynamic(String... pluginKey) {
            dynamic.addAll(asList(pluginKey));
            return this;
        }

        public PluginDependencies build() {
            return new PluginDependencies(Set.copyOf(mandatory), Set.copyOf(optional), Set.copyOf(dynamic));
        }
    }
}
