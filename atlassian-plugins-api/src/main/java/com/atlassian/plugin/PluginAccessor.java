package com.atlassian.plugin;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import com.atlassian.plugin.predicate.ModuleDescriptorPredicate;
import com.atlassian.plugin.predicate.PluginPredicate;

/**
 * Allows access to the current plugin system state
 */
public interface PluginAccessor {
    /**
     * The plugin descriptor file.
     *
     * @since 2.2
     */
    final class Descriptor {
        /**
         * The default filename.
         */
        public static final String FILENAME = "atlassian-plugin.xml";

        private Descriptor() {}
    }

    /**
     * Gets all of the currently installed plugins.
     *
     * @return a collection of installed {@link Plugin}s.
     */
    Collection<Plugin> getPlugins();

    /**
     * Gets all installed plugins that match the given predicate.
     *
     * @param pluginPredicate the {@link PluginPredicate} to match.
     * @return a collection of {@link Plugin}s that match the given predicate.
     * @since 0.17
     * @deprecated in 5.0 for removal in 6.0, use {@link #getPlugins(Predicate)} instead
     */
    @Deprecated
    default Collection<Plugin> getPlugins(final PluginPredicate pluginPredicate) {
        return getPlugins((Predicate<Plugin>) pluginPredicate::matches);
    }

    /**
     * Gets all installed plugins that match the given predicate.
     *
     * @param pluginPredicate the {@link Predicate} describing which plugins to match.
     * @return a collection of {@link Plugin}s that match the given predicate.
     * @since 5.0
     */
    Collection<Plugin> getPlugins(Predicate<Plugin> pluginPredicate);

    /**
     * Get all of the currently enabled plugins.
     *
     * @return a collection of installed and enabled {@link Plugin}s.
     */
    Collection<Plugin> getEnabledPlugins();

    /**
     * Gets all installed modules that match the given predicate.
     *
     * @param moduleDescriptorPredicate the {@link com.atlassian.plugin.predicate.ModuleDescriptorPredicate} to match.
     * @return a collection of modules as per {@link ModuleDescriptor#getModule()} that match the given predicate.
     * @since 0.17
     * @deprecated in 5.0 for removal in 6.0. Use {@link #getModules(Predicate)} instead.
     */
    @Deprecated
    default <M> Collection<M> getModules(final ModuleDescriptorPredicate<M> moduleDescriptorPredicate) {
        return getModules((Predicate<ModuleDescriptor<M>>) moduleDescriptorPredicate::matches);
    }

    /**
     * Gets all module descriptors of installed modules that match the given predicate.
     *
     * @param moduleDescriptorPredicate the {@link com.atlassian.plugin.predicate.ModuleDescriptorPredicate} to match.
     * @return a collection of {@link ModuleDescriptor}s that match the given predicate.
     * @since 0.17
     * @deprecated in 5.0 for removal in 6.0. Use {@link #getModuleDescriptors(Predicate)} instead.
     */
    @Deprecated
    default <M> Collection<ModuleDescriptor<M>> getModuleDescriptors(
            final ModuleDescriptorPredicate<M> moduleDescriptorPredicate) {
        return getModuleDescriptors((Predicate<ModuleDescriptor<M>>) moduleDescriptorPredicate::matches);
    }

    /**
     * Gets all installed modules that match the given predicate.
     *
     * @param moduleDescriptorPredicate describes which modules to match
     * @return a collection of modules as per {@link ModuleDescriptor#getModule()} that match the given predicate.
     * @since 5.0
     */
    <M> Collection<M> getModules(Predicate<ModuleDescriptor<M>> moduleDescriptorPredicate);

    /**
     * Gets all module descriptors of installed modules that match the given predicate.
     *
     * @param moduleDescriptorPredicate describes which modules to match
     * @return a collection of {@link ModuleDescriptor}s that match the given predicate.
     * @since 5.0
     */
    <M> Collection<ModuleDescriptor<M>> getModuleDescriptors(Predicate<ModuleDescriptor<M>> moduleDescriptorPredicate);

    /**
     * Retrieve a given plugin (whether enabled or not).
     *
     * @param key The plugin key. Cannot be null.
     * @return The enabled plugin, or null if that plugin does not exist.
     * @throws IllegalArgumentException If the plugin key is null
     */
    Plugin getPlugin(String key);

    /**
     * Retrieve a given plugin if it is enabled.
     *
     * @return The enabled plugin, or null if that plugin does not exist or is disabled.
     * @throws IllegalArgumentException If the plugin key is null
     */
    Plugin getEnabledPlugin(String pluginKey);

    /**
     * Retrieve any plugin module by complete module key.
     * <p>
     * Note: the module may or may not be disabled.
     */
    ModuleDescriptor<?> getPluginModule(String completeKey);

    /**
     * Retrieve an enabled plugin module by complete module key.
     */
    ModuleDescriptor<?> getEnabledPluginModule(String completeKey);

    /**
     * Whether or not a given plugin is currently enabled.
     *
     * @throws IllegalArgumentException If the plugin key is null
     */
    boolean isPluginEnabled(String key);

    /**
     * Whether or not a given plugin module is currently enabled. This also checks
     * if the plugin it is contained within is enabled also
     *
     * @see #isPluginEnabled(String)
     */
    boolean isPluginModuleEnabled(String completeKey);

    /**
     * Retrieve all plugin modules that implement or extend a specific class.
     *
     * @return List of modules that implement or extend the given class.
     */
    <M> List<M> getEnabledModulesByClass(Class<M> moduleClass);

    /**
     * Get all enabled module descriptors that have a specific descriptor class.
     *
     * @param descriptorClazz module descriptor class
     * @return List of {@link ModuleDescriptor}s that implement or extend the given class.
     */
    <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(Class<D> descriptorClazz);

    /**
     * Get all enabled module descriptors that have a specific descriptor class and are considered active for
     * the current request.
     *<p>
     * Result of this method should not be cached across requests.
     *
     * @see com.atlassian.plugin.scope.ScopeManager
     * @param descriptorClazz module descriptor class
     * @return List of {@link ModuleDescriptor}s that implement or extend the given class and active for the current request.
     * @deprecated in 5.0 for removal in 6.0 when {@link com.atlassian.plugin.scope.ScopeManager} is removed. Use
     *             {@link #getEnabledModuleDescriptorsByClass(Class)} instead.
     */
    @Deprecated
    default <D extends ModuleDescriptor<?>> List<D> getActiveModuleDescriptorsByClass(Class<D> descriptorClazz) {
        return getEnabledModuleDescriptorsByClass(descriptorClazz);
    }

    /**
     * Retrieve a resource from a currently loaded (and active) dynamically loaded plugin. Will return the first resource
     * found, so plugins with overlapping resource names will behave erratically.
     *
     * @param resourcePath the path to the resource to retrieve
     * @return the dynamically loaded resource that matches that path, or null if no such resource is found
     */
    InputStream getDynamicResourceAsStream(String resourcePath);

    /**
     * Retrieve the class loader responsible for loading classes and resources from plugins.
     *
     * @return the class loader
     * @since 0.21
     */
    ClassLoader getClassLoader();

    /**
     * @return true if the plugin is a system plugin.
     */
    boolean isSystemPlugin(String key);

    /**
     * Gets the state of the plugin upon restart. Only useful for plugins that contain module descriptors with the
     * \@RestartRequired annotation, and therefore, cannot be dynamically installed, upgraded, or removed at runtime
     *
     * @param key The plugin key
     * @return The state of the plugin on restart
     */
    PluginRestartState getPluginRestartState(String key);

    /**
     * Retrieve all currently registered dynamic modules i.e. any module that has been added to <code>plugin</code> via
     * {@link com.atlassian.plugin.PluginController#addDynamicModule(Plugin, com.atlassian.plugin.module.Element)} during the
     * lifetime of
     * this plugin, but not removed via
     * {@link com.atlassian.plugin.PluginController#removeDynamicModule(Plugin, ModuleDescriptor)}.
     *
     * @param plugin to query
     * @return modules added, may be empty
     * @see com.atlassian.plugin.PluginController#addDynamicModule(Plugin, com.atlassian.plugin.module.Element)
     * @see com.atlassian.plugin.PluginController#removeDynamicModule(Plugin, ModuleDescriptor)
     */
    Iterable<ModuleDescriptor<?>> getDynamicModules(Plugin plugin);
}
