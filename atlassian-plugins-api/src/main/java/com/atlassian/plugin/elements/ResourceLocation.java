package com.atlassian.plugin.elements;

import java.util.Map;
import java.util.function.Predicate;

import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.toMap;

/**
 * This class gives the location of a particular resource
 */
public class ResourceLocation {
    private final String location;
    private final String name;
    private final String type;
    private final String contentType;
    private final String content;
    private final Map<String, String> params;

    public ResourceLocation(
            String location, String name, String type, String contentType, String content, Map<String, String> params) {
        this.location = location;
        this.name = name;
        this.type = type;
        this.contentType = contentType;
        this.content = content;
        this.params = unmodifiableMap(params.entrySet().stream()
                .filter(KEY_AND_VALUE_NOT_NULL)
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue)));
    }

    /**
     * Necessary because {@link com.atlassian.plugin.loaders.LoaderUtils#getParams(org.dom4j.Element)} allows <code>null</code>s through
     */
    private static final Predicate<Map.Entry<?, ?>> KEY_AND_VALUE_NOT_NULL =
            e -> e.getKey() != null && e.getValue() != null;

    public String getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getContentType() {
        return contentType;
    }

    public String getContent() {
        return content;
    }

    public String getParameter(String key) {
        return params.get(key);
    }

    public Map<String, String> getParams() {
        return params;
    }
}
