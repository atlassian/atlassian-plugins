package com.atlassian.plugin;

import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

/**
 * Represents a plugin permission as parsed from the plugin descriptor.
 * <p>
 * A plugin permission here is:
 * <ul>
 *   <li>A <strong>name</strong> which denotes the permission itself.</li>
 *   <li>An <strong>{@link InstallationMode installation mode}</strong> which tells whether the permission is required
 *     for a given type of installation of the plugin. No installation mode defined means that the permission is always
 *     required.</li>
 * </ul>
 *
 * @since 3.0
 */
public final class PluginPermission {
    public static final PluginPermission ALL = new PluginPermission(Permissions.ALL_PERMISSIONS);
    public static final PluginPermission EXECUTE_JAVA = new PluginPermission(Permissions.EXECUTE_JAVA);

    private final String name;
    private final InstallationMode installationMode;

    public PluginPermission(String name) {
        this(name, null);
    }

    public PluginPermission(String name, InstallationMode installationMode) {
        this.name = requireNonNull(name, "name");
        this.installationMode = installationMode;
    }

    public String getName() {
        return name;
    }

    /**
     * The installation mode for that permission.
     *
     * @return the installation mode as an {@link Optional}. If the result is {@link Optional#empty()}
     *         then this means this permission is always valid, however when it is {@link Optional#isPresent() defined}
     *         it will only be valid for the given installation mode.
     */
    @Nonnull
    public Optional<InstallationMode> getInstallationMode() {
        return ofNullable(installationMode);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final PluginPermission that = (PluginPermission) o;
        return Objects.equals(this.name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }
}
