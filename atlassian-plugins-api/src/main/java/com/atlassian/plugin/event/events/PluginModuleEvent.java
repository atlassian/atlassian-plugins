package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;

import static java.util.Objects.requireNonNull;

/**
 * Base class for events with ModuleDescriptor context.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginModuleEvent {
    private final ModuleDescriptor<?> module;

    public PluginModuleEvent(final ModuleDescriptor<?> module) {
        this.module = requireNonNull(module);
    }

    public ModuleDescriptor<?> getModule() {
        return module;
    }

    @Override
    public String toString() {
        return getClass().getName() + " for " + module;
    }
}
