package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

/**
 * Event that signifies the plugin framework has resumed initialization in response to lateStartup.
 *
 * @see com.atlassian.plugin.event.events
 * @since 3.2.0
 */
@PublicApi
public class PluginFrameworkResumingEvent extends PluginFrameworkEvent {
    public PluginFrameworkResumingEvent(final PluginController pluginController, final PluginAccessor pluginAccessor) {
        super(pluginController, pluginAccessor);
    }
}
