package com.atlassian.plugin.event.events;

import java.util.List;
import java.util.function.Predicate;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;

/**
 * Event send at the end of an action wrapping existing plugin events:
 * <ul>
 *     <li>{@link PluginEnablingEvent}</li>
 *     <li>{@link PluginEnabledEvent}</li>
 *     <li>{@link PluginDisablingEvent}</li>
 *     <li>{@link PluginDisabledEvent}</li>
 *     <li>{@link PluginModuleDisablingEvent}</li>
 *     <li>{@link PluginModuleDisabledEvent}</li>
 *     <li>{@link PluginModuleEnablingEvent}</li>
 *     <li>{@link PluginModuleEnabledEvent}</li>
 *     <li>{@link PluginRefreshedEvent}</li>
 *     <li>{@link PluginFrameworkStartingEvent}</li>
 *     <li>{@link PluginFrameworkDelayedEvent}</li>
 *     <li>{@link PluginFrameworkResumingEvent}</li>
 *     <li>{@link PluginFrameworkStartedEvent}</li>
 *     <li>{@link PluginFrameworkShuttingDownEvent}</li>
 *     <li>{@link PluginFrameworkShutdownEvent}</li>
 *     <li>{@link PluginFrameworkWarmRestartingEvent}</li>
 *     <li>{@link PluginFrameworkWarmRestartedEvent}</li>
 *     <li>{@link PluginUninstallingEvent}</li>
 *     <li>{@link PluginUninstalledEvent}</li>
 *     <li>{@link PluginInstallingEvent}</li>
 *     <li>{@link PluginInstalledEvent}</li>
 *     <li>{@link PluginUpgradingEvent}</li>
 *     <li>{@link PluginUpgradedEvent}</li>
 *     <li>{@link PluginDependentsChangedEvent}</li>
 * </ul>
 *
 * Contains information about plugin events which were part of this transaction.
 *
 * @see com.atlassian.plugin.event.events
 * @since 5.1.3
 */
@PublicApi
public class PluginTransactionEndEvent {

    private final long threadId;
    private final List<Object> events;

    public PluginTransactionEndEvent(final List<Object> events) {
        this.events = List.copyOf(events);
        this.threadId = Thread.currentThread().getId();
    }

    /**
     * @since 5.6.0.
     * @return an unmodifiable list of events.
     */
    public List<Object> getUnmodifiableEvents() {
        return events;
    }

    public int numberOfEvents() {
        return events.size();
    }

    /**
     * Returns true if any event of type <code>eventTypeClass</code> matching <code>anyMatchEventPredicate</code> was
     * part of this transaction.
     */
    public <T> boolean hasAnyEventOfTypeMatching(
            final Class<T> eventTypeClass, final Predicate<T> anyMatchEventPredicate) {
        return events.stream()
                .filter(eventTypeClass::isInstance)
                .map(eventTypeClass::cast)
                .anyMatch(anyMatchEventPredicate);
    }

    /**
     * Returns true if any event of type {@link PluginModuleEvent} with {@link PluginModuleEvent#getModule()} matching
     * <code>anyMatchModuleDescriptorPredicate</code> was part of this transaction, or if any event of type {@link PluginEvent}
     * with {@link PluginEvent#getPlugin()} with any {@link Plugin#getModuleDescriptors()} matching
     * <code>anyMatchModuleDescriptorPredicate</code> was part of this transaction.
     */
    public boolean hasAnyEventWithModuleDescriptorMatching(
            final Predicate<ModuleDescriptor<?>> anyMatchModuleDescriptorPredicate) {
        return events.stream()
                        .filter(PluginModuleEvent.class::isInstance)
                        .map(PluginModuleEvent.class::cast)
                        .map(PluginModuleEvent::getModule)
                        .anyMatch(anyMatchModuleDescriptorPredicate)
                || events.stream()
                        .filter(PluginEvent.class::isInstance)
                        .map(PluginEvent.class::cast)
                        .map(PluginEvent::getPlugin)
                        .flatMap(plugin -> plugin.getModuleDescriptors().stream())
                        .anyMatch(anyMatchModuleDescriptorPredicate);
    }

    /**
     * @return thread ID of the thread sending this event; can be used to match {@link PluginTransactionStartEvent} and {@link PluginTransactionEndEvent}
     */
    public long threadId() {
        return threadId;
    }
}
