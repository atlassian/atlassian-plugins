package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

import static java.util.Objects.requireNonNull;

/**
 * Base class for events with Plugin context.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginEvent {
    private final Plugin plugin;

    public PluginEvent(final Plugin plugin) {
        this.plugin = requireNonNull(plugin);
    }

    public Plugin getPlugin() {
        return plugin;
    }

    @Override
    public String toString() {
        return getClass().getName() + " for " + plugin;
    }
}
