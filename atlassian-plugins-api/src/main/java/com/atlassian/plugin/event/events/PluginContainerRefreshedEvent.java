package com.atlassian.plugin.event.events;

import static java.util.Objects.requireNonNull;

/**
 * Event for when the container a plugin is installed into has been refreshed.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.2.0
 */
public class PluginContainerRefreshedEvent {
    private final Object container;
    private final String key;

    public PluginContainerRefreshedEvent(final Object container, final String key) {
        this.container = requireNonNull(container, "The container cannot be null");
        this.key = requireNonNull(key, "The plugin key must be available");
    }

    public Object getContainer() {
        return container;
    }

    public String getPluginKey() {
        return key;
    }
}
