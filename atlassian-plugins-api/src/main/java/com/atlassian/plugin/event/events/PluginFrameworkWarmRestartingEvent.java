package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

/**
 * Signals a warm restart of the plugin framework is about to begin.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.3.0
 */
@PublicApi
public class PluginFrameworkWarmRestartingEvent extends PluginFrameworkEvent {
    public PluginFrameworkWarmRestartingEvent(
            final PluginController pluginController, final PluginAccessor pluginAccessor) {
        super(pluginController, pluginAccessor);
    }
}
