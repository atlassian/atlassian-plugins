package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

/**
 * Event that signifies the plugin framework has been started and initialized.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.0.0
 */
@PublicApi
public class PluginFrameworkStartedEvent extends PluginFrameworkEvent {
    public PluginFrameworkStartedEvent(final PluginController pluginController, final PluginAccessor pluginAccessor) {
        super(pluginController, pluginAccessor);
    }
}
