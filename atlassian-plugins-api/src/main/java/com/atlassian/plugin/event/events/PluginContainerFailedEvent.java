package com.atlassian.plugin.event.events;

import static java.util.Objects.requireNonNull;

/**
 * Event thrown when the container a plugin is installed into either rejects the plugin or fails altogether.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.2.0
 */
public class PluginContainerFailedEvent {
    private final Object container;
    private final String key;
    private final Throwable cause;

    public PluginContainerFailedEvent(final Object container, final String key, final Throwable cause) {
        this.key = requireNonNull(key, "The bundle symbolic name must be available");
        this.container = container;
        this.cause = cause;
    }

    public Object getContainer() {
        return container;
    }

    public String getPluginKey() {
        return key;
    }

    public Throwable getCause() {
        return cause;
    }
}
