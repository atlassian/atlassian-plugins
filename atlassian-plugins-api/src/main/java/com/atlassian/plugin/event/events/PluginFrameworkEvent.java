package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

import static java.util.Objects.requireNonNull;

/**
 * Base class for events with plugin framework context.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginFrameworkEvent {
    private final PluginController pluginController;
    private final PluginAccessor pluginAccessor;

    public PluginFrameworkEvent(final PluginController pluginController, final PluginAccessor pluginAccessor) {
        this.pluginController = requireNonNull(pluginController);
        this.pluginAccessor = requireNonNull(pluginAccessor);
    }

    public PluginController getPluginController() {
        return pluginController;
    }

    public PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }
}
