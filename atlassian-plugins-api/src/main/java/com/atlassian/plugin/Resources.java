package com.atlassian.plugin;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.module.Element;

import static java.util.stream.Collectors.toList;

import static com.atlassian.plugin.util.Assertions.notNull;

/**
 * An aggregate of all resource descriptors within the given plugin module or
 * plugin.
 * <p>
 * See resources in com.atlassian.plugin.impl.AbstractPlugin
 * See resources in com.atlassian.plugin.descriptors.AbstractModuleDescriptor
 */
public class Resources implements Resourced {
    public static final Resources EMPTY_RESOURCES = new Resources(null);

    private final List<ResourceDescriptor> resourceDescriptors;

    /**
     * Parses the resource descriptors from the provided plugin XML element and
     * creates a Resources object containing them.
     * <p>
     * If the module or plugin contains no resource elements, an empty Resources
     * object will be returned. This method will not return null.
     *
     * @param element the plugin or plugin module XML fragment which should not
     *                be null
     * @return a Resources object representing the resources in the plugin or
     * plugin module
     * @throws PluginParseException     if there are two resources with the same
     *                                  name and type in this element, or another parse error occurs
     * @throws IllegalArgumentException if the provided element is null
     */
    public static Resources fromXml(final Element element) {
        if (element == null) {
            throw new IllegalArgumentException("Cannot parse resources from null XML element");
        }

        final List<Element> elements = element.elements("resource");

        final Set<ResourceDescriptor> templates = new HashSet<>();

        for (final Element e : elements) {
            final ResourceDescriptor resourceDescriptor = new ResourceDescriptor(e);

            if (templates.contains(resourceDescriptor)) {
                throw new PluginParseException("Duplicate resource with type '" + resourceDescriptor.getType()
                        + "' and name '" + resourceDescriptor.getName() + "' found");
            }

            templates.add(resourceDescriptor);
        }

        return new Resources(element);
    }

    /**
     * Private constructor to create a Resources object from XML. Entry via fromXml.
     * @param element
     */
    private Resources(final com.atlassian.plugin.module.Element element) {
        if (element != null) {
            this.resourceDescriptors = element.elements("resource").stream()
                    .map(ResourceDescriptor::new)
                    .collect(toList());
        } else {
            this.resourceDescriptors = Collections.emptyList();
        }
    }

    public List<ResourceDescriptor> getResourceDescriptors() {
        return resourceDescriptors;
    }

    public ResourceLocation getResourceLocation(final String type, final String name) {
        for (final ResourceDescriptor resourceDescriptor : getResourceDescriptors()) {
            if (resourceDescriptor.doesTypeAndNameMatch(type, name)) {
                return resourceDescriptor.getResourceLocationForName(name);
            }
        }
        return null;
    }

    public ResourceDescriptor getResourceDescriptor(final String type, final String name) {
        for (final ResourceDescriptor resourceDescriptor : getResourceDescriptors()) {
            if (resourceDescriptor.getType().equalsIgnoreCase(type)
                    && resourceDescriptor.getName().equalsIgnoreCase(name)) {
                return resourceDescriptor;
            }
        }
        return null;
    }

    /**
     * Checks that the {@link ResourceDescriptor} has a matching type, this is determined by comparing the {@link ResourceDescriptor#getType()}
     * with type supplied to the Constructor of this Predicate.
     * This comparison relies on {@link String#equals(Object)}.
     *
     * @since added in 5.6.0 as a replacement to the Guava implementation.
     */
    public static class TypeFilterPredicate implements Predicate<ResourceDescriptor> {
        private final String type;

        public TypeFilterPredicate(final String type) {
            this.type = notNull("type", type);
        }

        @Override
        public boolean test(ResourceDescriptor input) {
            return type.equals(input.getType());
        }
    }
}
