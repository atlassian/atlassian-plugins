/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Jul 31, 2004
 * Time: 12:58:29 PM
 */
package com.atlassian.plugin;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import io.atlassian.util.concurrent.CopyOnWriteMap;

import com.atlassian.plugin.util.JavaVersionUtils;

import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableMap;
import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.StreamSupport.stream;

public class PluginInformation {
    private String description = "";
    private String descriptionKey;
    private String version = "0.0";
    private String vendorName = "(unknown)";
    private String vendorUrl;
    private Optional<String> scopeKey;
    private Float minJavaVersion;
    private Set<PluginPermission> permissions = emptySet(); // by default no permissions
    private final Map<String, String> parameters =
            CopyOnWriteMap.<String, String>builder().stableViews().newHashMap();
    private String startup;
    private Set<String> moduleScanFolders;

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public void setVendorName(final String vendorName) {
        this.vendorName = vendorName;
    }

    public void setVendorUrl(final String vendorUrl) {
        this.vendorUrl = vendorUrl;
    }

    public String getVendorName() {
        return vendorName;
    }

    public String getVendorUrl() {
        return vendorUrl;
    }

    public void setScopeKey(Optional<String> scopeKey) {
        this.scopeKey = scopeKey;
    }

    public Optional<String> getScopeKey() {
        return scopeKey;
    }

    public Float getMinJavaVersion() {
        return minJavaVersion;
    }

    public void setMinJavaVersion(final Float minJavaVersion) {
        this.minJavaVersion = minJavaVersion;
    }

    public Map<String, String> getParameters() {
        return unmodifiableMap(parameters);
    }

    /**
     * The set of permissions that the plugin requires to run.
     *
     * @return the permissions as parsed from the plugin descriptor.
     */
    public Set<PluginPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(final Set<PluginPermission> permissions) {
        this.permissions = unmodifiableSet(new HashSet<>(permissions));
    }

    public void addParameter(final String key, final String value) {
        parameters.put(key, value);
    }

    public boolean satisfiesMinJavaVersion() {
        return (minJavaVersion == null) || JavaVersionUtils.satisfiesMinVersion(minJavaVersion);
    }

    public void setDescriptionKey(final String descriptionKey) {
        this.descriptionKey = descriptionKey;
    }

    public String getDescriptionKey() {
        return descriptionKey;
    }

    public String getStartup() {
        return startup;
    }

    public void setStartup(final String startup) {
        this.startup = startup;
    }

    public Set<String> getModuleScanFolders() {
        return moduleScanFolders;
    }

    public void setModuleScanFolders(Iterable<String> moduleScanFolders) {
        this.moduleScanFolders =
                unmodifiableSet(stream(moduleScanFolders.spliterator(), false).collect(toSet()));
    }
}
