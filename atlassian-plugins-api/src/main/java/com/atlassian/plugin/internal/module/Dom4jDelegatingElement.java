package com.atlassian.plugin.internal.module;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.dom4j.Node;

import com.atlassian.plugin.module.Element;

/**
 * This class is provided as a migration path and should not be depended upon directly as will not be provided in the
 * public API as of atlassian plugins 8.0.0.
 */
public class Dom4jDelegatingElement implements Element {

    private final org.dom4j.Element dom4jElement;

    /**
     * Not for external use, we cannot guarantee the version of dom4j that will be provided. External Plugin developers
     * should not have a dependency on this Constructor. Using this can lead to unintended breaking changes for external
     * plugin developers.
     */
    public Dom4jDelegatingElement(org.dom4j.Element dom4jElement) {
        this.dom4jElement = dom4jElement;
    }

    @Override
    public String attributeValue(String key) {
        if (dom4jElement == null) {
            return null;
        }
        return dom4jElement.attributeValue(key);
    }

    @Override
    public String attributeValue(String key, String defaultValue) {
        String attributeValue = attributeValue(key);
        return attributeValue == null ? defaultValue : attributeValue;
    }

    @Override
    public Element element(String element) {
        if (dom4jElement == null) {
            return null;
        }
        return dom4jElement.element(element) == null ? null : new Dom4jDelegatingElement(dom4jElement.element(element));
    }

    @Override
    public List<Element> elements(String name) {
        if (dom4jElement == null) {
            return Collections.emptyList();
        }
        return dom4jElement.elements(name).stream()
                .map(Dom4jDelegatingElement::new)
                .collect(Collectors.toList());
    }

    @Override
    public String getTextTrim() {
        if (dom4jElement == null) {
            return "";
        }
        return dom4jElement.getTextTrim();
    }

    @Override
    public String elementTextTrim(String name) {
        if (dom4jElement == null) {
            return null;
        }
        return dom4jElement.elementTextTrim(name);
    }

    @Override
    public String getText() {
        if (dom4jElement == null) {
            return "";
        }
        return dom4jElement.getText();
    }

    @Override
    public String getName() {
        if (dom4jElement == null) {
            return null;
        }
        return dom4jElement.getName();
    }

    @Override
    public List<Element> elements() {
        if (dom4jElement == null) {
            return Collections.emptyList();
        }
        return dom4jElement.elements().stream().map(Dom4jDelegatingElement::new).collect(Collectors.toList());
    }

    @Override
    public List<String> attributeNames() {
        if (dom4jElement == null) {
            return Collections.emptyList();
        }
        return dom4jElement.attributes().stream().map(Node::getName).collect(Collectors.toList());
    }

    public org.dom4j.Element getDelegate() {
        return dom4jElement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dom4jDelegatingElement that = (Dom4jDelegatingElement) o;
        return Objects.equals(dom4jElement, that.dom4jElement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dom4jElement);
    }
}
