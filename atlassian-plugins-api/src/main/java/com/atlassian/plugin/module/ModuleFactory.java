package com.atlassian.plugin.module;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;

/**
 * The {@link ModuleFactory} creates the module class of a {@link com.atlassian.plugin.ModuleDescriptor}.
 * The ModuleFactory is injected into the {@code AbstractModuleDescriptor} and encapsulates the different strategies
 * how the module class can be created.
 *
 * @since 2.5.0
 */
public interface ModuleFactory {
    /**
     * Creates the module instance. The module class name can contain a prefix. The delimiter of the prefix and the
     * class name is ':'. E.g.: 'bean:httpServletBean'. Which prefixes are supported depends on the registered
     * {@code ModuleCreator}. The prefix is case in-sensitive.
     *
     * @param name             module class name, can contain a prefix followed by ":" and the class name. Cannot be
     *                         {@code null}. If no prefix provided a default behaviour is assumed how to create the
     *                         module class.
     * @param moduleDescriptor the {@link com.atlassian.plugin.ModuleDescriptor}. Cannot be {@code null}
     * @return an instantiated object of the module class.
     * @throws PluginParseException If it failed to create the object.
     */
    <T> T createModule(String name, ModuleDescriptor<T> moduleDescriptor);

    ModuleFactory LEGACY_MODULE_FACTORY = new LegacyModuleFactory();
}
