package com.atlassian.plugin.module;

import java.util.Collection;

/**
 * The ContainerAccessor allows access to the underlying plugin container (e.g. spring).
 *
 * @since 2.5.0
 */
public interface ContainerAccessor {
    /**
     * Will ask the container to instantiate a bean of the given class and does inject all constructor defined dependencies.
     * Currently we have only spring as a container that will autowire this bean.
     *
     * @param clazz the Class to instantiate. Cannot be null.
     * @return an instantiated bean.
     */
    <T> T createBean(Class<T> clazz);

    /**
     * Injects an existing bean instance with any dependencies via setters or private field injection
     *
     * @param bean The instantiated bean to inject
     * @param <T>  The bean type
     * @since 3.0
     */
    <T> T injectBean(T bean);

    /**
     * Retrieves a bean by name from the container.
     *
     * @param id the id of the container bean, cannot be null
     * @return the bean object, or null if cannot be found
     * @since 3.0
     */
    <T> T getBean(String id);

    /**
     * Gets single bean of given type
     *
     * @param requiredType The expected bean type
     * @param <T> The bean type
     * @return The bean object, or null if cannot be found
     * @since 8.0
     */
    <T> T getBean(Class<T> requiredType);

    /**
     * Gets all the beans that implement a given interface
     *
     * @param interfaceClass The interface class
     * @param <T>            The target interface type
     * @return A collection of implementations from the plugin's container
     */
    <T> Collection<T> getBeansOfType(Class<T> interfaceClass);
}
