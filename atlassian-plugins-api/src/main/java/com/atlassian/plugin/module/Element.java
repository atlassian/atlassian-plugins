package com.atlassian.plugin.module;

import java.util.List;

/**
 * This represents a given element in the atlassian-plugin.xml. This interface replaces the {@link org.dom4j.Element} used
 * in {@link com.atlassian.plugin.ModuleDescriptor}.
 */
public interface Element {
    /**
     * Returns the value for an attribute based on the provided key.
     * @param key that maps to the value to be returned.
     * @return the value associated to the provided key.
     */
    String attributeValue(String key);

    /**
     * Returns the value for an attribute based on the provided key. If the value that would be returned is null, then
     * returns the default value instead.
     * @param key that maps to the value to be returned.
     * @param defaultValue to be returned if the value mapped to the provided key is null.
     * @return the attribute mapped to the provided key or the defualtValue provided.
     */
    String attributeValue(String key, String defaultValue);

    /**
     * Returns the first element which matches the given name
     * @param name as specified in atlassian-plugins.xml for a given element.
     * @return the first element with a matching name (case-sensitive), or null.
     */
    Element element(String name);

    /**
     * Returns all elements which matches the given name
     * @param name as specified in atlassian-plugins.xml for a given element.
     * @return the all elements with a matching name (case-sensitive), or an empty list.
     */
    List<Element> elements(String name);

    /**
     * @return the text for the current element where whitespace is trimmed and normalised into single spaces.
     * This method does not return null if the text is unknown, but instead an empty String.
     */
    String getTextTrim();

    /**
     * @param name of the element in which the text trim will be provided from.
     * @return the text for the element whose name matches the provided name. Where whitespace is trimmed and normalised
     * into single spaces. This method returns null if the text is unknown.
     */
    String elementTextTrim(String name);

    /**
     * @return the text for the current element. This method does not return null if the text is unknown, but instead an
     * empty String.
     */
    String getText();

    /**
     * @return the name for this element. This is the local name of the element.
     */
    String getName();

    /**
     * @return all elements contained within this element, if no elements contained within this element will this return
     * an empty list.
     */
    List<Element> elements();

    /**
     * @return all attribute names contained by this element as a list. If no attributes are in the contained element then
     * this will return an empty list.
     */
    List<String> attributeNames();
}
