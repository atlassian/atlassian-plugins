package com.atlassian.plugin;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

public class TestModuleCompleteKey {

    @Test
    public void spacesAreStrippedFromKeys() {
        String pluginKey = "    com.acme  ";
        String moduleKey = "   tools  ";

        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey(pluginKey, moduleKey);

        assertEquals(pluginKey.trim(), moduleCompleteKey.getPluginKey());
        assertEquals(moduleKey.trim(), moduleCompleteKey.getModuleKey());
    }

    @Test
    public void spacesAreStrippedFromKeys2() {
        String moduleCompleteKeyString = "    com.acme  :         tools       ";

        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey(moduleCompleteKeyString);

        assertEquals("com.acme", moduleCompleteKey.getPluginKey());
        assertEquals("tools", moduleCompleteKey.getModuleKey());
    }

    @Test
    public void pluginKeyAndModuleKeyConstructor() {
        String pluginKey = "com.acme";
        String moduleKey = "tools";

        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey(pluginKey, moduleKey);

        assertEquals(pluginKey, moduleCompleteKey.getPluginKey());
        assertEquals(moduleKey, moduleCompleteKey.getModuleKey());
    }

    @Test
    public void moduleCompleteKeyCanBeUsedAsMapKey() {
        Map<ModuleCompleteKey, String> map = new HashMap<>();
        String expected = "bananas";

        map.put(new ModuleCompleteKey("foo", "bar"), expected);

        assertEquals(expected, map.get(new ModuleCompleteKey("foo", "bar")));
    }

    @Test
    public void equals() {
        assertEquals(new ModuleCompleteKey("foo", "bar"), new ModuleCompleteKey("foo", "bar"));
        assertNotEquals(new ModuleCompleteKey("foo", "bar"), new ModuleCompleteKey("foo", "baz"));
        assertNotEquals(new ModuleCompleteKey("fon", "bar"), new ModuleCompleteKey("foo", "baz"));
    }

    @Test
    public void testToString() {
        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey("foo", "bar");

        assertEquals(moduleCompleteKey.getCompleteKey(), moduleCompleteKey.toString());
    }

    @Test
    public void workingKey() {
        ModuleCompleteKey key = new ModuleCompleteKey("foo:bar");

        assertEquals("foo", key.getPluginKey());
        assertEquals("bar", key.getModuleKey());
        assertEquals("foo:bar", key.getCompleteKey());
    }

    @Test(expected = IllegalArgumentException.class)
    public void colonInPluginKey() {
        new ModuleCompleteKey("foo:", "bar");
    }

    @Test
    public void colonInModuleKeyDoesNotThrowException() {
        final ModuleCompleteKey completeKey = new ModuleCompleteKey("foo", ":bar");
        assertEquals("foo", completeKey.getPluginKey());
        assertEquals(":bar", completeKey.getModuleKey());
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey1() {
        new ModuleCompleteKey("foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey2() {
        new ModuleCompleteKey("foo:");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey3() {
        new ModuleCompleteKey(":foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey4() {
        new ModuleCompleteKey("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badKey5() {
        new ModuleCompleteKey(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullPluginKey() {
        new ModuleCompleteKey(null, "foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullModuleKey() {
        new ModuleCompleteKey("foo", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullPluginKeyAndNullModuleKey() {
        new ModuleCompleteKey(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyPluginKey() {
        new ModuleCompleteKey("", "foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyModuleKey() {
        new ModuleCompleteKey("foo", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyPluginKeyAndEmptyModuleKey() {
        new ModuleCompleteKey("", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void pluginKeyOfSpaces() {
        new ModuleCompleteKey("      ", "foo");
    }

    @Test(expected = IllegalArgumentException.class)
    public void moduleKeyOfSpaces() {
        new ModuleCompleteKey("foo", "      ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void pluginKeyAndModuleKeyOfSpaces() {
        new ModuleCompleteKey("       ", "      ");
    }

    @Test
    public void pluginModuleKeyCanHaveAColonPresent() {
        String pluginKey = "com.acme";
        String moduleKey = "tools:foo";
        String completeKey = "com.acme:tools:foo";

        ModuleCompleteKey moduleCompleteKey = new ModuleCompleteKey(completeKey);

        assertEquals(pluginKey, moduleCompleteKey.getPluginKey());
        assertEquals(moduleKey, moduleCompleteKey.getModuleKey());
    }

    @Test
    public void pluginKeyFromCompleteKeyNull() {
        assertThat(ModuleCompleteKey.pluginKeyFromCompleteKey(null), is(""));
    }

    @Test
    public void pluginKeyFromCompleteKeyNoSeparator() {
        assertThat(ModuleCompleteKey.pluginKeyFromCompleteKey("blargh"), is("blargh"));
    }

    @Test
    public void pluginKeyFromCompleteKeySeparatorMid() {
        assertThat(ModuleCompleteKey.pluginKeyFromCompleteKey("bla:rgh"), is("bla"));
    }

    @Test
    public void pluginKeyFromCompleteKeySeparatorEnd() {
        assertThat(ModuleCompleteKey.pluginKeyFromCompleteKey("blargh:"), is("blargh"));
    }

    @Test
    public void moduleKeyFromCompleteKeyNull() {
        assertThat(ModuleCompleteKey.moduleKeyFromCompleteKey(null), is(""));
    }

    @Test
    public void moduleKeyFromCompleteKeyNoSeparator() {
        assertThat(ModuleCompleteKey.moduleKeyFromCompleteKey("blargh"), is(""));
    }

    @Test
    public void moduleKeyFromCompleteKeySeparatorMid() {
        assertThat(ModuleCompleteKey.moduleKeyFromCompleteKey("bla:rgh"), is("rgh"));
    }

    @Test
    public void moduleKeyFromCompleteKeySeparatorEnd() {
        assertThat(ModuleCompleteKey.moduleKeyFromCompleteKey("blargh:"), is(""));
    }
}
