package com.atlassian.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.PluginDependencies.Type;

import static java.util.Arrays.asList;
import static java.util.Comparator.comparingInt;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;

public class TestPluginDependencies {

    private PluginDependencies deps;

    @Before
    public void setUp() throws Exception {
        deps = PluginDependencies.builder()
                .withDynamic("A")
                .withDynamic("B")
                .withDynamic("C", "D")
                .withOptional("E")
                .withOptional("F")
                .withOptional("G", "H")
                .withMandatory("I")
                .withMandatory("J")
                .withMandatory("K", "L")
                .withDynamic("X")
                .withMandatory("X")
                .withOptional("X")
                .build();
    }

    @Test
    public void testGetMandatory() {
        assertThat(deps.getMandatory(), is(equalTo((Set) Set.of("I", "J", "K", "L", "X"))));
    }

    @Test
    public void testGetOptional() {
        assertThat(deps.getOptional(), is(equalTo((Set) Set.of("E", "F", "G", "H", "X"))));
    }

    @Test
    public void testGetDynamic() {
        assertThat(deps.getDynamic(), is(equalTo((Set) Set.of("A", "B", "C", "D", "X"))));
    }

    @Test
    public void testGetAll() {
        assertThat(deps.getAll(), is(equalTo((Set)
                Set.of("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "X"))));
    }

    @Test
    public void testGetByPluginKeyNewAPI() {
        Map<String, SortedSet<Type>> actual = deps.getTypesByPluginKey();
        Map<String, SortedSet<Type>> expected = new HashMap<>();
        expected.put("A", treeSetOf(DYNAMIC));
        expected.put("B", treeSetOf(DYNAMIC));
        expected.put("C", treeSetOf(DYNAMIC));
        expected.put("D", treeSetOf(DYNAMIC));
        expected.put("E", treeSetOf(OPTIONAL));
        expected.put("F", treeSetOf(OPTIONAL));
        expected.put("G", treeSetOf(OPTIONAL));
        expected.put("H", treeSetOf(OPTIONAL));
        expected.put("I", treeSetOf(MANDATORY));
        expected.put("J", treeSetOf(MANDATORY));
        expected.put("K", treeSetOf(MANDATORY));
        expected.put("L", treeSetOf(MANDATORY));
        expected.put("X", treeSetOf(DYNAMIC, OPTIONAL, MANDATORY));

        assertSameElementsAnyOrder(actual.entrySet(), expected.entrySet());
    }

    private SortedSet<Type> treeSetOf(Type... types) {
        TreeSet<Type> typesTree = new TreeSet<>(comparingInt(Enum::ordinal));
        typesTree.addAll(asList(types));
        return typesTree;
    }

    private void assertSameElementsAnyOrder(
            Set<Map.Entry<String, SortedSet<Type>>> actual, Set<Map.Entry<String, SortedSet<Type>>> expected) {
        assertTrue("Actual does not contain all expected elements.", actual.containsAll(expected));
        assertTrue("Actual has additional elements to the expected.", expected.containsAll(actual));
    }
}
