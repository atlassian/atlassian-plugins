package com.atlassian.plugin;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPluginNameComparator {

    private final PluginNameComparator underTest = new PluginNameComparator();

    @Test
    public void testSecondGreater() {
        assertSecondGreater(mockPlugin("a"), mockPlugin("b"));
        assertSecondGreater(mockPlugin("Z"), mockPlugin("a"));
        assertSecondGreater(mockPlugin("1"), mockPlugin("2"));
        assertSecondGreater(mockPlugin("!"), mockPlugin("@"));
        assertSecondGreater(mockPlugin("a1!"), mockPlugin("b1!"));
        assertSecondGreater(mockPlugin("a1!"), mockPlugin("a1@"));
        assertSecondGreater(mockPlugin("a1!"), mockPlugin("a2!"));
    }

    @Test
    public void testBothAreEqual() {
        assertBothAreEqual(mockPlugin("a"), mockPlugin("a"));
        assertBothAreEqual(mockPlugin("Z"), mockPlugin("Z"));
        assertBothAreEqual(mockPlugin("1"), mockPlugin("1"));
        assertBothAreEqual(mockPlugin("!"), mockPlugin("!"));
        assertBothAreEqual(mockPlugin("a1!"), mockPlugin("a1!"));
    }

    private Plugin mockPlugin(final String name) {
        Plugin plugin = mock(Plugin.class);
        when(plugin.getName()).thenReturn(name);
        return plugin;
    }

    private void assertSecondGreater(final Plugin p1, final Plugin p2) {
        // check both are reflexive
        assertBothSame(p1, p1);
        assertBothSame(p2, p2);

        // check for symmetry
        assertTrue(p1 + " < " + p2, underTest.compare(p2, p1) > 0);
        assertTrue(p1 + " < " + p2, underTest.compare(p1, p2) < 0);
    }

    private void assertBothAreEqual(final Plugin p1, final Plugin p2) {
        // check both are reflexive
        assertBothSame(p1, p1);
        assertBothSame(p2, p2);

        // check both are equal
        assertBothSame(p1, p2);
        assertBothSame(p2, p1);
    }

    private void assertBothSame(final Plugin first, final Plugin second) {
        assertEquals(first + " == " + second, 0, underTest.compare(first, second));
    }
}
