package com.atlassian.plugin.event.events;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.ModuleDescriptor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class)
public class TestPluginModuleEvents {
    @Mock
    private ModuleDescriptor<?> moduleDescriptor;

    @Test
    public void pluginModuleDisablingEventGetters() {
        final PluginModuleDisablingEvent pluginModuleDisablingEvent =
                new PluginModuleDisablingEvent(moduleDescriptor, true);
        assertModuleDescriptor(pluginModuleDisablingEvent.getModule());
        assertThat(pluginModuleDisablingEvent.isPersistent(), is(true));
    }

    @Test
    public void pluginModuleAvailableEventGetters() {
        final PluginModuleAvailableEvent pluginModuleAvailableEvent = new PluginModuleAvailableEvent(moduleDescriptor);
        assertModuleDescriptor(pluginModuleAvailableEvent.getModule());
    }

    @Test
    public void pluginModuleDisabledEventGetters() {
        final PluginModuleDisabledEvent pluginModuleDisabledEvent =
                new PluginModuleDisabledEvent(moduleDescriptor, true);
        assertModuleDescriptor(pluginModuleDisabledEvent.getModule());
        assertThat(pluginModuleDisabledEvent.isPersistent(), is(true));
    }

    @Test
    public void pluginModuleEnabledEventGetters() {
        final PluginModuleEnabledEvent pluginModuleEnabledEvent = new PluginModuleEnabledEvent(moduleDescriptor);
        assertModuleDescriptor(pluginModuleEnabledEvent.getModule());
    }

    @Test
    public void pluginModuleUnavailableEventGetters() {
        final PluginModuleUnavailableEvent pluginModuleUnavailableEvent =
                new PluginModuleUnavailableEvent(moduleDescriptor);
        assertModuleDescriptor(pluginModuleUnavailableEvent.getModule());
    }

    private void assertModuleDescriptor(final ModuleDescriptor<?> module) {
        // The cast here works around problems with type matching
        assertThat(module, is((Object) moduleDescriptor));
    }
}
