package com.atlassian.plugin.metadata;

import org.junit.After;
import org.junit.Test;

import com.atlassian.plugin.util.PluginKeyStack;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class TestPluginKeyStack {

    @Test
    public void whenPluginKeyStackIsEmpty_AStackWillBeCreated() {
        PluginKeyStack.push("pluginKey");
        PluginKeyStack.push("pluginKey2");

        assertThat(PluginKeyStack.getPluginKeys(), contains("pluginKey", "pluginKey2"));
    }

    @Test
    public void whenPluginKeyStackNotEmpty_GetFirstPluginKeyReturnsTheFirstItem() {
        PluginKeyStack.push("pluginKey1");
        PluginKeyStack.push("pluginKey2");
        PluginKeyStack.push("pluginKey3");
        PluginKeyStack.push("pluginKey4");

        assertThat(PluginKeyStack.getFirstPluginKey(), equalTo("pluginKey1"));
    }

    @Test
    public void whenPluginKeyStackIsEmpty_TheStackWillBeRemoved() {
        PluginKeyStack.push("pluginKey");
        PluginKeyStack.push("pluginKey2");
        PluginKeyStack.pop();

        assertThat(PluginKeyStack.getPluginKeys(), contains("pluginKey"));
    }

    @After
    public void clearStack() {
        int stackLength = PluginKeyStack.getPluginKeys().size();

        while (stackLength > 0) {
            PluginKeyStack.pop();
            stackLength--;
        }
    }
}
