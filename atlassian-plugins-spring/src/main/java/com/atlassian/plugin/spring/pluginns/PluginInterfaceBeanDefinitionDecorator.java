package com.atlassian.plugin.spring.pluginns;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.xml.BeanDefinitionDecorator;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Node;

import com.atlassian.plugin.spring.PluginBeanDefinitionRegistry;

/**
 * Matches the &lt;plugin:interface&gt; element and registers it against the bean for later processing.
 */
public class PluginInterfaceBeanDefinitionDecorator implements BeanDefinitionDecorator {
    /**
     * Called when the Spring parser encounters an "interface" element.
     *
     * @param source The interface element
     * @param holder The containing bean definition
     * @param ctx    The parser context
     * @return The containing bean definition
     */
    @Nonnull
    @Override
    public BeanDefinitionHolder decorate(
            @Nonnull Node source, @Nonnull BeanDefinitionHolder holder, @Nonnull ParserContext ctx) {
        final String inf = source.getTextContent();
        if (inf != null) {
            new PluginBeanDefinitionRegistry(ctx.getRegistry()).addBeanInterface(holder.getBeanName(), inf.trim());
        }
        return holder;
    }
}
