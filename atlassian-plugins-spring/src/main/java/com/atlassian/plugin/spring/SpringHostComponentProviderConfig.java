package com.atlassian.plugin.spring;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy;

/**
 * Offers configurations for SpringHostComponentProvider.
 */
public class SpringHostComponentProviderConfig {
    /**
     * A set of bean names to make available to plugins
     */
    private Set<String> beanNames = Collections.emptySet();

    /**
     * Mapping of beanNames to the interfaces it should be exposed as. Note that if a bean name is present an no interface
     * is defined then all its interfaces should be 'exposed'.
     */
    private Map<String, Class[]> beanInterfaces = Collections.emptyMap();

    /**
     * Mapping of beanNames with their {@link com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy}.
     * Default value is {@link com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy#USE_HOST}.
     */
    private Map<String, ContextClassLoaderStrategy> beanContextClassLoaderStrategies = Collections.emptyMap();

    private Set<String> bundleTrackingBeans = Collections.emptySet();

    /**
     * Whether or not to scan for {@link com.atlassian.plugin.spring.AvailableToPlugins} annotations on beans defined in the bean factory, defaults to {@code false}.
     */
    private boolean useAnnotation = false;

    public Set<String> getBeanNames() {
        return beanNames;
    }

    @SuppressWarnings("unused")
    public void setBeanNames(Set<String> beanNames) {
        this.beanNames = beanNames;
    }

    public Map<String, Class[]> getBeanInterfaces() {
        return beanInterfaces;
    }

    @SuppressWarnings("unused")
    public void setBeanInterfaces(Map<String, Class[]> beanInterfaces) {
        this.beanInterfaces = beanInterfaces;
    }

    public Map<String, ContextClassLoaderStrategy> getBeanContextClassLoaderStrategies() {
        return beanContextClassLoaderStrategies;
    }

    @SuppressWarnings("unused")
    public void setBeanContextClassLoaderStrategies(
            Map<String, ContextClassLoaderStrategy> beanContextClassLoaderStrategies) {
        this.beanContextClassLoaderStrategies = beanContextClassLoaderStrategies;
    }

    public Set<String> getBundleTrackingBeans() {
        return bundleTrackingBeans;
    }

    @SuppressWarnings("unused")
    public void setBundleTrackingBeans(Set<String> bundleTrackingBeans) {
        this.bundleTrackingBeans = bundleTrackingBeans;
    }

    public void setUseAnnotation(boolean useAnnotation) {
        this.useAnnotation = useAnnotation;
    }

    public boolean isUseAnnotation() {
        return useAnnotation;
    }
}
