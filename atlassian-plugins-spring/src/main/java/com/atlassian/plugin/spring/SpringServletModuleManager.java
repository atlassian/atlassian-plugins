package com.atlassian.plugin.spring;

import javax.annotation.Nonnull;
import javax.servlet.ServletContext;

import org.springframework.web.context.ServletContextAware;

import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.servlet.DefaultServletModuleManager;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.util.ServletContextServletModuleManagerAccessor;

/**
 * A {@link ServletModuleManager} that has a {@link ServletContext} automatically injected
 */
public class SpringServletModuleManager extends DefaultServletModuleManager implements ServletContextAware {
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    @SuppressWarnings("deprecation")
    public SpringServletModuleManager(final PluginEventManager pluginEventManager) {
        super(pluginEventManager);
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    public void setServletContext(@Nonnull final ServletContext servletContext) {
        ServletContextServletModuleManagerAccessor.setServletModuleManager(servletContext, this);
    }
}
