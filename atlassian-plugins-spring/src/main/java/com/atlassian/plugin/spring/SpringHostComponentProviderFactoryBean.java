package com.atlassian.plugin.spring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanIsAbstractException;
import org.springframework.beans.factory.HierarchicalBeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.core.annotation.AnnotationUtils;
import org.apache.commons.lang3.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;

import static com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy.USE_HOST;
import static com.atlassian.plugin.util.Assertions.notNull;

public class SpringHostComponentProviderFactoryBean extends AbstractFactoryBean {
    private static final Logger log = LoggerFactory.getLogger(SpringHostComponentProviderFactoryBean.class);

    private SpringHostComponentProviderConfig springHostComponentProviderConfig;

    /**
     * A set of bean names to make available to plugins
     */
    private Set<String> beanNames = Collections.emptySet();

    /**
     * Mapping of beanNames to the interfaces it should be exposed as. Note that if a bean name is present an no interface
     * is defined then all its interfaces should be 'exposed'.
     */
    private Map<String, Class[]> beanInterfaces = Collections.emptyMap();

    /**
     * Mapping of beanNames with their {@link com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy}.
     * Default value is {@link com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy#USE_HOST}.
     */
    private Map<String, ContextClassLoaderStrategy> beanContextClassLoaderStrategies = Collections.emptyMap();

    private Set<String> bundleTrackingBeans = Collections.emptySet();

    public Class getObjectType() {
        return HostComponentProvider.class;
    }

    protected Object createInstance() {
        if (springHostComponentProviderConfig == null) {
            return new SpringHostComponentProvider(
                    getBeanFactory(),
                    beanNames,
                    beanInterfaces,
                    beanContextClassLoaderStrategies,
                    bundleTrackingBeans,
                    false);
        } else {
            // whatever defined in {@link SpringHostComponentProviderConfig} takes precedence
            return new SpringHostComponentProvider(
                    getBeanFactory(),
                    Sets.union(beanNames, springHostComponentProviderConfig.getBeanNames()),
                    ImmutableMap.<String, Class[]>builder()
                            .putAll(beanInterfaces)
                            .putAll(springHostComponentProviderConfig.getBeanInterfaces())
                            .build(),
                    ImmutableMap.<String, ContextClassLoaderStrategy>builder()
                            .putAll(beanContextClassLoaderStrategies)
                            .putAll(springHostComponentProviderConfig.getBeanContextClassLoaderStrategies())
                            .build(),
                    ImmutableSet.<String>builder()
                            .addAll(bundleTrackingBeans)
                            .addAll(springHostComponentProviderConfig.getBundleTrackingBeans())
                            .build(),
                    springHostComponentProviderConfig.isUseAnnotation());
        }
    }

    @Autowired(required = false)
    public void setSpringHostComponentProviderConfig(
            SpringHostComponentProviderConfig springHostComponentProviderConfig) {
        this.springHostComponentProviderConfig = springHostComponentProviderConfig;
    }

    // These beanNames, beanInterfaces, beanContextClassLoaderStrategies only exist for keeping plugin framework's
    // custom attributes during the parsing of spring context.
    public void setBeanNames(Set<String> beanNames) {
        this.beanNames = beanNames;
    }

    public void setBeanInterfaces(Map<String, Class[]> beanInterfaces) {
        this.beanInterfaces = beanInterfaces;
    }

    public void setBeanContextClassLoaderStrategies(
            Map<String, ContextClassLoaderStrategy> beanContextClassLoaderStrategies) {
        this.beanContextClassLoaderStrategies = beanContextClassLoaderStrategies;
    }

    public void setBundleTrackingBeans(Set<String> bundleTrackingBeans) {
        this.bundleTrackingBeans = bundleTrackingBeans;
    }

    private static class SpringHostComponentProvider implements HostComponentProvider {
        private final BeanFactory beanFactory;
        private boolean useAnnotation;
        private final Set<String> beanNames;
        private final Map<String, Class[]> beanInterfaces;
        private final Map<String, ContextClassLoaderStrategy> beanContextClassLoaderStrategies;
        private final Set<String> bundleTrackingBeans;

        public SpringHostComponentProvider(
                BeanFactory beanFactory,
                Set<String> beanNames,
                Map<String, Class[]> beanInterfaces,
                Map<String, ContextClassLoaderStrategy> beanContextClassLoaderStrategies,
                Set<String> bundleTrackingBeans,
                boolean useAnnotation) {
            this.beanFactory = notNull("beanFactory", beanFactory);
            this.useAnnotation = useAnnotation;
            this.beanNames = beanNames != null ? beanNames : new HashSet<>();
            this.beanInterfaces = beanInterfaces != null ? beanInterfaces : new HashMap<>();
            this.beanContextClassLoaderStrategies =
                    beanContextClassLoaderStrategies != null ? beanContextClassLoaderStrategies : new HashMap<>();
            this.bundleTrackingBeans = bundleTrackingBeans != null ? bundleTrackingBeans : new HashSet<>();
        }

        public void provide(ComponentRegistrar registrar) {
            final Set<String> beansToProvide = new HashSet<>(beanNames);
            final Map<String, Class[]> interfacesToProvide = new HashMap<>(beanInterfaces);
            final Map<String, ContextClassLoaderStrategy> contextClassLoaderStrategiesToProvide =
                    new HashMap<>(beanContextClassLoaderStrategies);
            final Set<String> bundleTrackingBeansToProvide = new HashSet<>(bundleTrackingBeans);

            if (useAnnotation) {
                scanForAnnotatedBeans(
                        beansToProvide,
                        interfacesToProvide,
                        contextClassLoaderStrategiesToProvide,
                        bundleTrackingBeansToProvide);
            }

            provideBeans(
                    registrar,
                    beansToProvide,
                    interfacesToProvide,
                    contextClassLoaderStrategiesToProvide,
                    bundleTrackingBeansToProvide);

            // make sure that host component providers we might have defined in parent bean factories also get a chance
            // to provide their beans.
            if (beanFactory instanceof HierarchicalBeanFactory) {
                final BeanFactory parentBeanFactory = ((HierarchicalBeanFactory) beanFactory).getParentBeanFactory();
                if (parentBeanFactory != null) {
                    try {
                        HostComponentProvider provider = (HostComponentProvider)
                                parentBeanFactory.getBean(PluginBeanDefinitionRegistry.HOST_COMPONENT_PROVIDER);
                        if (provider != null) {
                            provider.provide(registrar);
                        }
                    } catch (NoSuchBeanDefinitionException e) {
                        log.debug(
                                "Unable to find '{}' in the parent bean factory {}",
                                PluginBeanDefinitionRegistry.HOST_COMPONENT_PROVIDER,
                                parentBeanFactory);
                    }
                }
            }
        }

        private void provideBeans(
                ComponentRegistrar registrar,
                Set<String> beanNames,
                Map<String, Class[]> beanInterfaces,
                Map<String, ContextClassLoaderStrategy> beanContextClassLoaderStrategies,
                Set<String> bundleTrackingBeans) {
            for (String beanName : beanNames) {
                if (beanFactory.isSingleton(beanName)) {
                    final Object bean = beanFactory.getBean(beanName);
                    Class[] interfaces = beanInterfaces.get(beanName);
                    if (interfaces == null) {
                        interfaces = findInterfaces(getBeanClass(bean));
                    }
                    registrar
                            .register(interfaces)
                            .forInstance(bean)
                            .withName(beanName)
                            .withContextClassLoaderStrategy(
                                    beanContextClassLoaderStrategies.getOrDefault(beanName, USE_HOST))
                            .withTrackBundleEnabled(bundleTrackingBeans.contains(beanName));
                } else {
                    log.warn("Cannot register bean '{}' as it's scope is not singleton", beanName);
                }
            }
        }

        /**
         * The logic in this method is very similar to the logic in {@link AvailableToPluginsBeanDefinitionRegistryProcessor},
         * but where this class works off the AvailableToPlugins annotation object directly, that class has to work off the
         * lower-level MethodMetadata interface. If the logic in here is altered, then it should most likely be altered
         * there also.
         */
        private void scanForAnnotatedBeans(
                Set<String> beansToProvide,
                Map<String, Class[]> interfacesToProvide,
                Map<String, ContextClassLoaderStrategy> contextClassLoaderStrategiesToProvide,
                Set<String> bundleTrackingBeansToProvide) {
            if (beanFactory instanceof ListableBeanFactory) {
                for (String beanName : ((ListableBeanFactory) beanFactory).getBeanDefinitionNames()) {
                    try {
                        // Only singleton beans can be registered as service.
                        if (beanFactory.isSingleton(beanName)) {
                            final Class beanClass = getBeanClass(beanFactory.getBean(beanName));
                            final AvailableToPlugins annotation =
                                    AnnotationUtils.findAnnotation(beanClass, AvailableToPlugins.class);
                            if (annotation != null) {
                                beansToProvide.add(beanName);

                                // if interface(s) is defined in the annotation
                                if (annotation.value() != Void.class || annotation.interfaces().length != 0) {
                                    if (!interfacesToProvide.containsKey(beanName)) {
                                        List<Class> effectiveInterfaces = new ArrayList<>();
                                        if (annotation.value() != Void.class) {
                                            effectiveInterfaces.add(annotation.value());
                                        }

                                        if (annotation.interfaces().length != 0) {
                                            effectiveInterfaces.addAll(Arrays.asList(annotation.interfaces()));
                                        }

                                        interfacesToProvide.put(beanName, effectiveInterfaces.toArray(new Class[0]));
                                    } else {
                                        log.debug(
                                                "Interfaces for bean '{}' have been defined in XML or in a Module definition, ignoring the interface defined in the annotation",
                                                beanName);
                                    }
                                }

                                if (!contextClassLoaderStrategiesToProvide.containsKey(beanName)) {
                                    contextClassLoaderStrategiesToProvide.put(
                                            beanName, annotation.contextClassLoaderStrategy());
                                } else {
                                    log.debug(
                                            "Context class loader strategy for bean '{}' has been defined in XML or in a Module definition, ignoring the one defined in the annotation",
                                            beanName);
                                }

                                if (annotation.trackBundle()) {
                                    bundleTrackingBeansToProvide.add(beanName);
                                }
                            }
                        } else {
                            log.debug(
                                    "Bean: {} skipped during @AvailableToPlugins scanning since it's not a singleton bean",
                                    beanName);
                        }
                    } catch (BeanIsAbstractException ex) {
                        // skipping abstract beans (is there a better way to check for this?)
                    }
                }
            } else {
                log.warn(
                        "Could not scan bean factory for beans to make available to plugins, bean factory is not 'listable'");
            }
        }

        private Class[] findInterfaces(Class cls) {
            final List<Class> validInterfaces = new ArrayList<>();
            for (Class inf : getAllInterfaces(cls)) {
                if (!inf.getName().startsWith("org.springframework")) {
                    validInterfaces.add(inf);
                }
            }
            return validInterfaces.toArray(new Class[0]);
        }

        private List<Class<?>> getAllInterfaces(Class cls) {
            return ClassUtils.getAllInterfaces(cls);
        }

        private Class getBeanClass(Object bean) {
            return AopUtils.getTargetClass(bean);
        }
    }
}
