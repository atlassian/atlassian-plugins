package com.atlassian.plugin.spring;

import javax.annotation.Nonnull;
import javax.servlet.ServletContext;

import org.springframework.web.context.ServletContextAware;

import com.atlassian.plugin.osgi.container.impl.DefaultPackageScannerConfiguration;

/**
 * Spring-aware extension of the package scanner configuration that instructs spring to inject the servlet context
 */
public class SpringAwarePackageScannerConfiguration extends DefaultPackageScannerConfiguration
        implements ServletContextAware {
    public SpringAwarePackageScannerConfiguration() {
        super();
    }

    public SpringAwarePackageScannerConfiguration(String hostVersion) {
        super(hostVersion);
    }

    @Override
    public void setServletContext(@Nonnull ServletContext servletContext) {
        super.setServletContext(servletContext);
    }
}
