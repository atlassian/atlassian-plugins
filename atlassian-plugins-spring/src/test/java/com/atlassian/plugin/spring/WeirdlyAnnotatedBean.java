package com.atlassian.plugin.spring;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@AvailableToPlugins(
        value = Barable.class,
        interfaces = {Fooable.class, Serializable.class, Map.class})
public class WeirdlyAnnotatedBean extends HashMap implements Fooable, Serializable {
    public void sayHi() {
        System.out.println("hi");
    }
}
