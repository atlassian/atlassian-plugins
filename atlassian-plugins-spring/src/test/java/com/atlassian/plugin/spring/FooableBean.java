package com.atlassian.plugin.spring;

import java.io.Serializable;
import java.util.HashMap;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

@AvailableToPlugins
public class FooableBean extends HashMap implements Fooable, BeanFactoryAware, Serializable {
    public void sayHi() {
        System.out.println("hi");
    }

    public void setBeanFactory(BeanFactory beanFactory) {}
}
