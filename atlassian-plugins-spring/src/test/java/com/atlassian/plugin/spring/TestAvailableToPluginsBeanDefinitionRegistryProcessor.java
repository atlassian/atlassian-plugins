package com.atlassian.plugin.spring;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import static com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy.USE_HOST;
import static com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy.USE_PLUGIN;

@RunWith(MockitoJUnitRunner.class)
public class TestAvailableToPluginsBeanDefinitionRegistryProcessor {

    @Mock
    static PluginBeanDefinitionRegistry pluginBeanDefinitionRegistry;

    @Test
    public void beanMethodAnnotationsAreProcessed() {
        new AnnotationConfigApplicationContext(Config.class);

        verify(pluginBeanDefinitionRegistry).addBeanName("simplePluginAvailableBean");
        verify(pluginBeanDefinitionRegistry).addContextClassLoaderStrategy("simplePluginAvailableBean", USE_HOST);

        verify(pluginBeanDefinitionRegistry).addBeanName("complexPluginAvailableBean");
        verify(pluginBeanDefinitionRegistry).addBeanInterface("complexPluginAvailableBean", "java.util.Set");
        verify(pluginBeanDefinitionRegistry).addBeanInterface("complexPluginAvailableBean", "java.util.Map");
        verify(pluginBeanDefinitionRegistry).addBeanInterface("complexPluginAvailableBean", "java.util.List");
        verify(pluginBeanDefinitionRegistry).addContextClassLoaderStrategy("complexPluginAvailableBean", USE_PLUGIN);
        verify(pluginBeanDefinitionRegistry).addBundleTrackingBean("complexPluginAvailableBean");

        verify(pluginBeanDefinitionRegistry, never()).addBeanName("nonPluginAvailableBean");
        verify(pluginBeanDefinitionRegistry, never()).addBeanName("prototypeBean");
    }

    @Configuration
    public static class Config {
        @Bean
        public Object nonPluginAvailableBean() {
            return new Object();
        }

        @Bean
        @AvailableToPlugins
        public Object simplePluginAvailableBean() {
            return new Object();
        }

        @Bean
        @AvailableToPlugins(
                value = Set.class,
                interfaces = {List.class, Map.class},
                contextClassLoaderStrategy = ContextClassLoaderStrategy.USE_PLUGIN,
                trackBundle = true)
        public Object complexPluginAvailableBean() {
            return new Object();
        }

        @Bean
        @AvailableToPlugins
        @Scope("prototype")
        public Object prototypeBean() {
            return new Object();
        }

        @Bean
        public AvailableToPluginsBeanDefinitionRegistryProcessor availableToPluginsProcessor() {
            return new AvailableToPluginsBeanDefinitionRegistryProcessor((registry) -> pluginBeanDefinitionRegistry);
        }
    }
}
