package com.atlassian.plugin.test;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestPluginJarBuilder {

    @Test
    public void testBuild() throws Exception {
        File jar = new PluginJarBuilder("foo")
                .addJava("my.Foo", "package my; public class Foo { public String hi() {return \"hi\";}}")
                .addResource("foo.txt", "Some text")
                .addPluginInformation("someKey", "someName", "1.33")
                .build();
        assertNotNull(jar);

        URLClassLoader cl = new URLClassLoader(new URL[] {jar.toURI().toURL()}, null);
        Class<?> cls = cl.loadClass("my.Foo");
        assertNotNull(cls);
        Object foo = cls.getConstructor().newInstance();
        String result = (String) cls.getMethod("hi").invoke(foo);
        assertEquals("hi", result);
        assertEquals("Some text", IOUtils.toString(cl.getResourceAsStream("foo.txt")));
        assertNotNull(cl.getResource("META-INF/MANIFEST.MF"));

        String xml = IOUtils.toString(cl.getResourceAsStream("atlassian-plugin.xml"));
        assertTrue(xml.indexOf("someKey") > 0);
        assertTrue(xml.indexOf("someName") > 0);
        assertTrue(xml.indexOf("1.33") > 0);
    }
}
