package com.atlassian.plugin.test;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;

import org.apache.commons.io.FileUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PluginTestUtils {
    public static final String PROJECT_VERSION;
    public static final String SIMPLE_TEST_JAR;
    public static final String INNER1_TEST_JAR;
    public static final String INNER2_TEST_JAR;
    public static final String FILTER_TEST_JAR;

    static {
        PROJECT_VERSION = System.getProperty("project.version");
        SIMPLE_TEST_JAR = "atlassian-plugins-simpletest-" + PROJECT_VERSION + ".jar";
        INNER1_TEST_JAR = "atlassian-plugins-innerjarone-" + PROJECT_VERSION + ".jar";
        INNER2_TEST_JAR = "atlassian-plugins-innerjartwo-" + PROJECT_VERSION + ".jar";
        FILTER_TEST_JAR = "atlassian-plugins-filtertest-" + PROJECT_VERSION + ".jar";
    }

    public static File getFileForResource(final String resourceName) throws URISyntaxException {
        return new File(new URI(
                PluginTestUtils.class.getClassLoader().getResource(resourceName).toString()));
    }

    public static File createTempDirectory(Class<?> source) throws IOException {
        return createTempDirectory(source.getName());
    }

    private static Random random = new Random();

    /* Provide an empty directory */
    public static File createTempDirectory(String name) throws IOException {
        File tmpBase = new File("target", "tmp");

        File tmpDir = new File(tmpBase, name);

        if (tmpDir.exists()) {
            try {
                FileUtils.cleanDirectory(tmpDir);
            } catch (IOException ioe) {
                String unique = Long.toString(random.nextLong() >>> 1, 26);
                File movedAsideName = new File(tmpBase, name + "-" + unique);
                if (tmpDir.renameTo(movedAsideName)) {
                    tmpDir.mkdirs();
                } else {
                    throw new IOException("Unable to clean up temporary directory: " + tmpDir, ioe);
                }
            }
        } else {
            tmpDir.mkdirs();
        }

        assertTrue(tmpDir.exists());
        assertTrue(tmpDir.isDirectory());
        assertEquals(0, tmpDir.list().length);

        return tmpDir.getAbsoluteFile();
    }
}
