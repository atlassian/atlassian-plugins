package com.atlassian.plugin.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Appender;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.rules.ExternalResource;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.hasItem;

import static com.atlassian.plugin.test.Matchers.containsAllStrings;

/**
 * A JUnit Rule for capturing and verifying log messages from a given class.
 *
 * This implementation is Log4J specific, but the interface is framed in SLF4J terminology. The intent is that if we need to switch
 * to a different logger we should be able to preserve the interface, since it mirrors the SLF4J interface used by non test code.
 * The implementation uses a Log4j appender to capture log messages for verification.
 *
 * @since 3.2.16
 */
public class CapturedLogging extends ExternalResource {
    private final Class logSource;
    private Appender appender;
    private List<LoggingEvent> loggingEvents;
    private Logger logger;
    private Level savedLoggerLevel;
    private boolean savedLoggerAdditivity;

    public CapturedLogging(final Class logSource) {
        this.logSource = logSource;
    }

    public List<LoggingEvent> getLoggingEvents() {
        return loggingEvents;
    }

    @Override
    protected void before() throws Throwable {
        super.before();
        loggingEvents = new ArrayList<>();
        appender = new AppenderSkeleton() {
            @Override
            protected void append(final LoggingEvent event) {
                loggingEvents.add(event);
            }

            @Override
            public void close() {
                // Do nothing
            }

            @Override
            public boolean requiresLayout() {
                return false;
            }
        };
        logger = LogManager.getLogger(logSource);
        // Ensure we get all logs from the object under test, but stop them propagating
        savedLoggerLevel = logger.getLevel();
        savedLoggerAdditivity = logger.getAdditivity();
        logger.setLevel(Level.ALL);
        logger.setAdditivity(false);
        logger.addAppender(appender);
    }

    @Override
    protected void after() {
        logger.setLevel(savedLoggerLevel);
        logger.setAdditivity(savedLoggerAdditivity);
        logger.removeAppender(appender);
        super.after();
    }

    public static Matcher<CapturedLogging> didLog(final Matcher<LoggingEvent> loggingEventMatcher) {
        return new TypeSafeMatcher<CapturedLogging>() {
            @Override
            protected boolean matchesSafely(final CapturedLogging capturedLogging) {
                return hasItem(loggingEventMatcher).matches(capturedLogging.getLoggingEvents());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("some LoggingEvent that ");
                description.appendDescriptionOf(loggingEventMatcher);
            }
        };
    }

    public static Matcher<CapturedLogging> didLogError(final Matcher<String> messageMatcher) {
        return didLog(levelAndMessageMatch(Level.ERROR, messageMatcher));
    }

    public static Matcher<CapturedLogging> didLogError(final String... substrings) {
        return didLogError(containsAllStrings(substrings));
    }

    public static Matcher<CapturedLogging> didLogWarn(final Matcher<String> messageMatcher) {
        return didLog(levelAndMessageMatch(Level.WARN, messageMatcher));
    }

    public static Matcher<CapturedLogging> didLogWarn(final String... substrings) {
        return didLogWarn(containsAllStrings(substrings));
    }

    public static Matcher<CapturedLogging> didLogInfo(final Matcher<String> messageMatcher) {
        return didLog(levelAndMessageMatch(Level.INFO, messageMatcher));
    }

    public static Matcher<CapturedLogging> didLogInfo(final String... substrings) {
        return didLogInfo(containsAllStrings(substrings));
    }

    public static Matcher<CapturedLogging> didLogDebug(final Matcher<String> messageMatcher) {
        return didLog(levelAndMessageMatch(Level.DEBUG, messageMatcher));
    }

    public static Matcher<CapturedLogging> didLogDebug(final String... substrings) {
        return didLogDebug(containsAllStrings(substrings));
    }

    public static Matcher<LoggingEvent> levelIs(final Level level) {
        return new TypeSafeMatcher<LoggingEvent>() {
            @Override
            protected boolean matchesSafely(final LoggingEvent loggingEvent) {
                return level.equals(loggingEvent.getLevel());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("has level ");
                description.appendValue(level);
            }
        };
    }

    public static Matcher<LoggingEvent> messageMatches(final Matcher<String> stringMatcher) {
        return new TypeSafeMatcher<LoggingEvent>() {
            @Override
            protected boolean matchesSafely(final LoggingEvent loggingEvent) {
                return stringMatcher.matches(loggingEvent.getMessage());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("has message ");
                description.appendDescriptionOf(stringMatcher);
            }
        };
    }

    public static Matcher<LoggingEvent> throwableMatches(final Matcher<Throwable> throwableMatcher) {
        return new TypeSafeMatcher<LoggingEvent>() {
            @Override
            protected boolean matchesSafely(final LoggingEvent loggingEvent) {
                return Optional.ofNullable(loggingEvent.getThrowableInformation())
                        .map(ti -> throwableMatcher.matches(ti.getThrowable()))
                        .orElse(false);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("has throwable which ");
                description.appendDescriptionOf(throwableMatcher);
            }
        };
    }

    public String toString() {
        final List<String> loggingEventsAsString = loggingEvents.stream()
                .map(loggingEvent -> loggingEvent.getLevel() + ":" + loggingEvent.getMessage())
                .collect(Collectors.toList());
        return "CapturedLogging( " + loggingEventsAsString + ")";
    }

    public static Matcher<LoggingEvent> levelAndMessageMatch(final Level level, final Matcher<String> messageMatcher) {
        return allOf(levelIs(level), messageMatches(messageMatcher));
    }
}
