package com.atlassian.plugin;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugin.event.NotificationException;

/**
 * Augments the life-cycle of the plugin system with the ability to split {@link PluginSystemLifecycle#init()} for two phase
 * startup.
 *
 * @since 3.2.0
 */
@ExperimentalApi
public interface SplitStartupPluginSystemLifecycle extends PluginSystemLifecycle {
    /**
     * Perform the first part of startup.
     *
     * This initializes the plugin system, and installs and enables early plugins.
     *
     * @throws PluginParseException  If parsing the plugins failed.
     * @throws IllegalStateException if already initialized or already in the process of initialization.
     * @throws NotificationException If any of the Event Listeners throw an exception on the Framework startup events.
     * @see PluginSystemLifecycle#init
     */
    void earlyStartup();

    /**
     * Perform the second part of startup.
     *
     * This installs and enables late plugins, and performs any final validation steps. This may
     * only be called after {@link #earlyStartup}, and calling both earlyStartup and lateStartup is
     * equivalent to calling {@link PluginSystemLifecycle#init()}.
     *
     * @throws PluginParseException  If parsing the plugins failed.
     * @throws IllegalStateException if already initialized or already in the process of initialization.
     * @throws NotificationException If any of the Event Listeners throw an exception on the Framework startup events.
     * @see PluginSystemLifecycle#init
     */
    void lateStartup();
}
