package com.atlassian.plugin.predicate;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Predicate;

import com.atlassian.plugin.ModuleDescriptor;

import static com.atlassian.plugin.util.Assertions.notNull;

/**
 * A {@link Predicate} that matches modules for which their descriptor is an instance of one of the given {@link Class}.
 */
public class ModuleDescriptorOfClassPredicate<T> implements Predicate<ModuleDescriptor<T>> {
    private final Collection<Class<? extends ModuleDescriptor<? extends T>>> moduleDescriptorClasses;

    public ModuleDescriptorOfClassPredicate(
            final Class<? extends ModuleDescriptor<? extends T>> moduleDescriptorClass) {
        moduleDescriptorClasses = Collections.singleton(moduleDescriptorClass);
    }

    /**
     * @throws IllegalArgumentException if the moduleDescriptorClasses is <code>null</code>
     */
    public ModuleDescriptorOfClassPredicate(
            final Class<? extends ModuleDescriptor<? extends T>>[] moduleDescriptorClasses) {
        notNull("moduleDescriptorClasses", moduleDescriptorClasses);
        this.moduleDescriptorClasses = Arrays.asList(moduleDescriptorClasses);
    }

    public boolean test(final ModuleDescriptor<T> moduleDescriptor) {
        return moduleDescriptor != null
                && moduleDescriptorClasses != null
                && moduleDescriptorClasses.stream()
                        .anyMatch(descriptorClass ->
                                descriptorClass != null && descriptorClass.isInstance(moduleDescriptor));
    }
}
