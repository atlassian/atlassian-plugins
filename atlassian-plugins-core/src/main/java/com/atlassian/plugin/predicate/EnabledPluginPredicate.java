package com.atlassian.plugin.predicate;

import java.util.Set;
import java.util.function.Predicate;
import javax.annotation.Nonnull;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;

/**
 * A {@link Predicate} that matches enabled plugins.
 */
public class EnabledPluginPredicate implements Predicate<Plugin> {
    private final Set<Plugin> pluginsBeingEnabled;

    public EnabledPluginPredicate(final Set<Plugin> pluginsBeingEnabled) {
        this.pluginsBeingEnabled = pluginsBeingEnabled;
    }

    @Override
    public boolean test(@Nonnull final Plugin plugin) {
        return PluginState.ENABLED.equals(plugin.getPluginState()) && !pluginsBeingEnabled.contains(plugin);
    }
}
