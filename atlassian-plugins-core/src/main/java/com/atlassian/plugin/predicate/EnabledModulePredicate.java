package com.atlassian.plugin.predicate;

import java.util.function.Predicate;

import com.atlassian.plugin.ModuleDescriptor;

/**
 * A {@link Predicate} that matches enabled modules.
 */
public class EnabledModulePredicate implements Predicate<ModuleDescriptor<?>> {

    public boolean test(final ModuleDescriptor<?> moduleDescriptor) {
        return moduleDescriptor.isEnabled() && !moduleDescriptor.isBroken();
    }
}
