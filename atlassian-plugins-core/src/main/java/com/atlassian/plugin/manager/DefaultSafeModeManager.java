package com.atlassian.plugin.manager;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import javax.annotation.ParametersAreNonnullByDefault;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Supplier;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import com.atlassian.plugin.parsers.SafeModeCommandLineArguments;
import com.atlassian.plugin.parsers.SafeModeCommandLineArgumentsFactory;

import static com.google.common.base.Suppliers.memoize;
import static java.util.Collections.emptyList;

import static com.atlassian.plugin.manager.ApplicationDefinedPluginsProvider.NO_APPLICATION_PLUGINS;
import static com.atlassian.plugin.manager.PluginEnabledState.UNKNOWN_ENABLED_TIME;

/**
 * Default implementation of {@link SafeModeManager}
 * NB: The safe mode applies only in single node environment! The end application (e.g. JIRA) must implement
 * {@link ClusterEnvironmentProvider} to trigger the safe mode manager logic
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class DefaultSafeModeManager implements SafeModeManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSafeModeManager.class);
    private final SafeModeCommandLineArguments commandLineArguments;
    private final PluginMetadataManager pluginMetadataManager;
    private final ApplicationDefinedPluginsProvider appRelatedPluginsProvider;
    private final ClusterEnvironmentProvider clusterEnvironmentProvider;
    private final Supplier<Optional<String>> lastEnabledPluginKey = memoize(new Supplier<Optional<String>>() {
        @Override
        public Optional<String> get() {
            PluginPersistentState state = pluginPersistentStateStore.load();
            if (state.getStatesMap().isEmpty()) {
                return Optional.empty();
            }
            Optional<Map.Entry<String, PluginEnabledState>> mostRecentlyEnabled =
                    state.getStatesMap().entrySet().stream().max(Comparator.comparingLong(entry -> entry.getValue()
                            .getTimestamp()));
            if (mostRecentlyEnabled.isPresent()
                    && mostRecentlyEnabled.get().getValue().getTimestamp() == UNKNOWN_ENABLED_TIME) {
                return Optional.empty();
            }
            return (mostRecentlyEnabled.map(Map.Entry::getKey));
        }
    });

    private final boolean isInSafeMode;

    private final PluginPersistentStateStore pluginPersistentStateStore;

    public DefaultSafeModeManager(
            PluginMetadataManager pluginMetadataManager,
            ClusterEnvironmentProvider clusterEnvironmentProvider,
            SafeModeCommandLineArgumentsFactory safeModeCommandLineArgumentsFactory,
            PluginPersistentStateStore pluginPersistentStateStore) {
        this(
                pluginMetadataManager,
                NO_APPLICATION_PLUGINS,
                clusterEnvironmentProvider,
                safeModeCommandLineArgumentsFactory,
                pluginPersistentStateStore);
    }

    DefaultSafeModeManager(
            final PluginMetadataManager pluginMetadataManager,
            final ApplicationDefinedPluginsProvider appRelatedPluginsProvider,
            final ClusterEnvironmentProvider clusterEnvironmentProvider,
            final SafeModeCommandLineArgumentsFactory safeModeCommandLineArgumentsFactory,
            final PluginPersistentStateStore pluginPersistentStateStore) {
        this.pluginMetadataManager = pluginMetadataManager;
        this.commandLineArguments = safeModeCommandLineArgumentsFactory.get();
        this.appRelatedPluginsProvider = appRelatedPluginsProvider;
        this.clusterEnvironmentProvider = clusterEnvironmentProvider;
        this.pluginPersistentStateStore = pluginPersistentStateStore;
        this.isInSafeMode = !clusterEnvironmentProvider.isInCluster() && (commandLineArguments.isSafeMode());
        if (clusterEnvironmentProvider.isInCluster()
                && (commandLineArguments.isSafeMode()
                        || !commandLineArguments
                                .getDisabledPlugins()
                                .orElse(emptyList())
                                .isEmpty())) {
            LOGGER.warn(
                    "Add-ons disable options from '{}' are being ignored due to start up in clustered mode!",
                    commandLineArguments.getSafeModeArguments());
        }
    }

    @Override
    public boolean pluginShouldBeStarted(final Plugin plugin, final Iterable<ModuleDescriptor> descriptors) {
        return clusterEnvironmentProvider.isInCluster()
                || (!isPluginDisabledByDisableLastEnabled(plugin)
                        && !isPluginDisabledBySafeMode(plugin, descriptors)
                        && !commandLineArguments.isDisabledByParam(plugin.getKey()));
    }

    @Override
    public boolean isInSafeMode() {
        return isInSafeMode;
    }

    private boolean isPluginDisabledByDisableLastEnabled(Plugin plugin) {
        return (commandLineArguments.shouldLastEnabledBeDisabled()
                && plugin.getKey().equals(lastEnabledPluginKey.get().orElse(null)));
    }

    private boolean isPluginDisabledBySafeMode(Plugin plugin, Iterable<ModuleDescriptor> descriptors) {
        return (commandLineArguments.isSafeMode() && !isSystemPlugin(plugin, descriptors));
    }

    /**
     * Checks if the plugin is a required one
     *
     * @param plugin      plugin to be checked
     * @return true if the plugin is a system one
     */
    @Override
    public boolean isRequiredPlugin(Plugin plugin) {
        return !pluginMetadataManager.isOptional(plugin);
    }

    /**
     * Checks if the plugin is a system one (or is present within the application descriptors plugins).
     *
     * @param plugin      plugin to be checked
     * @param descriptors list of module descriptors to find the plugin references within
     * @return true if the plugin is a system one or is present within one of the application module descriptors
     */
    @SuppressWarnings("unchecked")
    private boolean isSystemPlugin(final Plugin plugin, Iterable<ModuleDescriptor> descriptors) {
        return pluginMetadataManager.isSystemProvided(plugin)
                || !pluginMetadataManager.isOptional(plugin)
                || appRelatedPluginsProvider.getPluginKeys(descriptors).contains(plugin.getKey());
    }
}
