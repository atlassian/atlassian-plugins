package com.atlassian.plugin.manager;

import static com.atlassian.util.profiling.Metrics.metric;

/**
 * Util class for emitting counter metrics to JMX.
 *
 * @since v5.7.2
 */
public final class PluginStateChangeCountEmitter {

    private static final String PLUGIN_ENABLED_METRIC_KEY = "plugin.enabled.counter";
    private static final String PLUGIN_DISABLED_METRIC_KEY = "plugin.disabled.counter";

    private PluginStateChangeCountEmitter() {}

    /**
     * Increments the plugin enabled counter metric when a plugin is enabled.
     */
    public static void emitPluginEnabledCounter() {
        emitPluginCounter(PLUGIN_ENABLED_METRIC_KEY);
    }

    /**
     * Increments the plugin disabled counter metric when a plugin is disabled.
     */
    public static void emitPluginDisabledCounter() {
        emitPluginCounter(PLUGIN_DISABLED_METRIC_KEY);
    }

    private static void emitPluginCounter(String metricKey) {
        metric(metricKey).withAnalytics().incrementCounter(1L);
    }
}
