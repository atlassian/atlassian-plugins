package com.atlassian.plugin.manager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.StoredPluginState;

import static java.util.stream.Collectors.toMap;

import static com.atlassian.plugin.manager.PluginEnabledState.UNKNOWN_ENABLED_TIME;
import static com.atlassian.plugin.manager.PluginEnabledState.getPluginEnabledStateWithCurrentTime;
import static com.atlassian.plugin.manager.PluginPersistentState.Util.RESTART_STATE_SEPARATOR;
import static com.atlassian.plugin.manager.PluginPersistentState.Util.buildStateKey;

/**
 * Interface that represents a configuration state for plugins and plugin modules. The configuration state (enabled
 * or disabled) is separate from the plugins and modules themselves because a plugin may have multiple
 * states depending on the context.
 *
 * @since 2.2.0
 */
public interface PluginPersistentState extends StoredPluginState {

    /**
     * @return a new builder
     * @since 5.0
     */
    static Builder builder() {
        return new Builder();
    }

    /**
     * @return a new builder that is initialized with the provided {@code state}
     * @since 5.0
     */
    static Builder builder(PluginPersistentState state) {
        return new Builder(state);
    }

    /**
     * Get the map of all states. Deprecated since 4.5.0
     * @deprecated please use {@link PluginPersistentState#getStatesMap} instead
     * @return The map that maps plugins and modules' keys to an actual enabled state (Boolean.True/Boolean.False).
     */
    @Deprecated
    default Map<String, Boolean> getMap() {
        return getStatesMap().entrySet().stream()
                .collect(toMap(Map.Entry::getKey, e -> e.getValue().isEnabled()));
    }

    /**
     * Get state map of the given plugin and its modules. Deprecated since 4.5.0
     *
     * @param plugin the plugin
     * @deprecated please use {@link PluginPersistentState#getPluginEnabledStateMap(Plugin)} instead
     * @return The map that maps the plugin and its modules' keys to plugin state (Boolean.TRUE/Boolean.FALSE).
     */
    @Deprecated
    default Map<String, Boolean> getPluginStateMap(final Plugin plugin) {
        return getPluginEnabledStateMap(plugin).entrySet().stream()
                .collect(toMap(Map.Entry::getKey, e -> e.getValue().isEnabled()));
    }

    /**
     * Builder for {@link PluginPersistentState} instances.
     * <p>
     * This class is <strong>not thread safe</strong>. It should
     * only be used in a method local context.
     *
     * @since 2.3.0
     */
    final class Builder {

        public static Builder create() {
            return new Builder();
        }

        public static Builder create(final PluginPersistentState state) {
            return new Builder(state);
        }

        private final Map<String, PluginEnabledState> map = new HashMap<>();

        Builder() {}

        Builder(final PluginPersistentState state) {
            map.putAll(state.getStatesMap());
        }

        public PluginPersistentState toState() {
            return new DefaultPluginPersistentState(map, true);
        }

        public Builder setEnabled(final ModuleDescriptor<?> pluginModule, final boolean isEnabled) {
            setEnabled(pluginModule.getCompleteKey(), isEnabled);
            return this;
        }

        public Builder setEnabled(final Plugin plugin, final boolean isEnabled) {
            setEnabled(plugin.getKey(), isEnabled);
            return this;
        }

        private Builder setEnabled(final String completeKey, final boolean isEnabled) {
            map.put(completeKey, getPluginEnabledStateWithCurrentTime(isEnabled));
            return this;
        }

        /**
         * reset all plugin's state.
         */
        public Builder setState(final PluginPersistentState state) {
            map.clear();
            map.putAll(state.getStatesMap());
            return this;
        }

        /**
         * Add the plugin state.
         */
        public Builder addPluginEnabledState(final Map<String, PluginEnabledState> state) {
            map.putAll(state);
            return this;
        }

        /**
         * Add the plugin state.
         * @deprecated in 4.5.0 for removal in 6.0. Use {@link #addPluginEnabledState(Map)} instead
         */
        @Deprecated
        public Builder addState(final Map<String, Boolean> state) {
            map.putAll(state.entrySet().stream()
                    .collect(
                            toMap(Map.Entry::getKey, e -> new PluginEnabledState(e.getValue(), UNKNOWN_ENABLED_TIME))));
            return this;
        }

        /**
         * Remove a plugin's state.
         */
        public Builder removeState(final String key) {
            map.remove(key);
            return this;
        }

        public Builder setPluginRestartState(final String pluginKey, final PluginRestartState state) {
            // Remove existing state, if any
            for (final PluginRestartState st : PluginRestartState.values()) {
                map.remove(buildStateKey(pluginKey, st));
            }

            if (state != PluginRestartState.NONE) {
                map.put(buildStateKey(pluginKey, state), getPluginEnabledStateWithCurrentTime(true));
            }
            return this;
        }

        public Builder clearPluginRestartState() {
            final Set<String> keys = new HashSet<>(map.keySet());
            for (final String key : keys) {
                if (key.contains(RESTART_STATE_SEPARATOR)) {
                    map.remove(key);
                }
            }
            return this;
        }
    }

    class Util {
        public static final String RESTART_STATE_SEPARATOR = "--";

        static String buildStateKey(final String pluginKey, final PluginRestartState state) {
            return state.name() + RESTART_STATE_SEPARATOR + pluginKey;
        }
    }
}
