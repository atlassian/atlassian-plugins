package com.atlassian.plugin.manager;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginController;

import static java.util.stream.StreamSupport.stream;

/**
 * Safely extracts the module instance from module descriptors,
 * handling exceptions and disabling broken plugins as appropriate.
 *
 * @since 4.0
 */
public class SafeModuleExtractor {
    private static final Logger log = LoggerFactory.getLogger(SafeModuleExtractor.class);

    private final PluginController pluginController;

    public SafeModuleExtractor(PluginController pluginController) {
        this.pluginController = pluginController;
    }

    /**
     * Safely extracts the module instance from the given module descriptors. This method will disable any plugin it
     * can't successfully extract the module instance from.
     */
    public <M> List<M> getModules(final Iterable<? extends ModuleDescriptor<M>> moduleDescriptors) {
        return stream(moduleDescriptors.spliterator(), false)
                .map(this::getModule)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Safely extracts the module instance from the given module descriptor. This method will disable any plugin it
     * can't successfully extract the module instance from.
     *
     * @since 5.0.0
     */
    <M> M getModule(ModuleDescriptor<M> descriptor) {
        if (descriptor == null || descriptor.isBroken()) {
            return null;
        }

        try {
            return descriptor.getModule();
        } catch (final RuntimeException ex) {
            final String pluginKey = descriptor.getPlugin().getKey();

            log.error(
                    "Exception when retrieving plugin module {}, disabling plugin {}",
                    descriptor.getCompleteKey(),
                    pluginKey,
                    ex);

            descriptor.setBroken();
            // Don't persist: don't want to create a snowflake config in Cloud instance,
            // and the module might work following restart if something has changed.
            pluginController.disablePluginWithoutPersisting(pluginKey);
            return null;
        }
    }
}
