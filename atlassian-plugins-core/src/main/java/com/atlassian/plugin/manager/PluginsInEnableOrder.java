package com.atlassian.plugin.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRegistry;

/**
 * A class to sort plugins in enable order
 *
 * @since v4.0
 */
final class PluginsInEnableOrder {
    private static Logger log = LoggerFactory.getLogger(PluginsInEnableOrder.class);
    // the list of plugins sorted by requirement order.
    final List<Plugin> sortedList;

    public PluginsInEnableOrder(final Collection<Plugin> pluginsToEnable, PluginRegistry.ReadOnly pluginRegistry) {
        this.sortedList = new ArrayList<>();
        final Set<Plugin> visited = new HashSet<>();

        for (final Plugin plugin : pluginsToEnable) {
            sortPluginForEnable(plugin, visited, pluginsToEnable, pluginRegistry);
        }
    }

    /**
     * If currentPlugin has not been visited, recurse on to each recognized plugin that it requires, and
     * then add it to the sorted list if it is allowed.
     *
     * @param currentPlugin  the plugin we are inspecting requirements of.
     * @param visited        the list of plugins we've already recursed into.
     * @param allowedPlugins the plugins which we can accumulate into sortedList.
     */
    private void sortPluginForEnable(
            final Plugin currentPlugin,
            final Set<Plugin> visited,
            final Collection<Plugin> allowedPlugins,
            final PluginRegistry.ReadOnly pluginRegistry) {
        if (!visited.add(currentPlugin)) {
            return;
        }

        log.debug("Candidate plugin for adding to sorted list: {}", currentPlugin.getKey());

        for (final String key : currentPlugin.getDependencies().getAll()) {
            final Plugin requiredPlugin = pluginRegistry.get(key);

            if (null != requiredPlugin) {
                log.debug(
                        "Candidate plugin {} suspended - adding dependency {}",
                        currentPlugin.getKey(),
                        requiredPlugin.getKey());
                sortPluginForEnable(requiredPlugin, visited, allowedPlugins, pluginRegistry);
            }
        }

        if (allowedPlugins.contains(currentPlugin)) {
            sortedList.add(currentPlugin);
            log.debug("Candidate plugin added to sorted list: {}", currentPlugin.getKey());
        }
    }

    public List<Plugin> get() {
        return Collections.unmodifiableList(sortedList);
    }
}
