package com.atlassian.plugin.manager;

import java.util.Set;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.scope.ScopeManager;

public class ProductPluginAccessor extends ForwardingPluginAccessor implements PluginAccessor {
    /**
     * A noop implementation of PluginController with only {@link PluginController#disablePluginWithoutPersisting(String)}
     * implemented as a noop and all other methods throwing {@link UnsupportedOperationException}
     * <p>
     * This is to decouple PluginAccessor from PluginController (see PLUG-304 for original coupling reasons)
     */
    private static PluginController noopDisablePluginPluginController = new PluginController() {
        @Override
        public void enablePlugins(final String... keys) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void disablePlugin(final String key) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void disablePluginWithoutPersisting(final String key) {
            // Do nothing
        }

        @Override
        public void enablePluginModule(final String completeKey) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void disablePluginModule(final String completeKey) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Set<String> installPlugins(final PluginArtifact... pluginArtifacts) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void uninstall(final Plugin plugin) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void revertRestartRequiredChange(final String pluginKey) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public int scanForNewPlugins() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ModuleDescriptor<?> addDynamicModule(final Plugin plugin, final Element module) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void removeDynamicModule(final Plugin plugin, final ModuleDescriptor<?> module) {
            throw new UnsupportedOperationException("Not implemented");
        }
    };

    public ProductPluginAccessor(
            final PluginRegistry.ReadOnly pluginRegistry,
            final PluginPersistentStateStore store,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager) {
        super(new EnabledModuleCachingPluginAccessor(
                new ProductPluginAccessorBase(pluginRegistry, store, moduleDescriptorFactory, pluginEventManager),
                pluginEventManager,
                noopDisablePluginPluginController));
    }

    /**
     * @deprecated in 5.0 for removal in 6.0 when {@link ScopeManager} will be removed. Use
     *             {@link ProductPluginAccessor(com.atlassian.plugin.PluginRegistry.ReadOnly,
     *             PluginPersistentStateStore, ModuleDescriptorFactory, PluginEventManager)} instead.
     */
    @Deprecated
    public ProductPluginAccessor(
            final PluginRegistry.ReadOnly pluginRegistry,
            final PluginPersistentStateStore store,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final ScopeManager ignored) {
        this(pluginRegistry, store, moduleDescriptorFactory, pluginEventManager);
    }
}
