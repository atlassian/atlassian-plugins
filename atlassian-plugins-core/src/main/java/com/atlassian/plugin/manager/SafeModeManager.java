package com.atlassian.plugin.manager;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;

/**
 * Controls what plugins are enabled when plugins are disabled from start-up using command-line options.
 * (NB: this is currently not the same as safe-mode as defined in UPM, as UPM allows the user to restore
 * the system to how it was before entering safe-mode)
 * {@link DefaultSafeModeManager} should be suitable for most implementations
 */
@PublicSpi
public interface SafeModeManager {
    /**
     * Default implementation of safe mode manager
     * Just a convenient way to start all the plugins skipping all checks
     */
    SafeModeManager START_ALL_PLUGINS = new SafeModeManager() {
        @Override
        public boolean pluginShouldBeStarted(Plugin plugin, Iterable<ModuleDescriptor> descriptors) {
            return true;
        }

        @Override
        public boolean isInSafeMode() {
            return false;
        }
    };

    /**
     * This checks if a plugin should be started when safe-mode is on
     *
     * @param plugin      to be checked if it should be started
     * @param descriptors list of module descriptors to find the plugin references that must be started in safe mode
     *                    (this is kinda tricky way to do such search, but the only available at the startup phase)
     * @return true if plugin should be started when the product starts up or false otherwise
     */
    boolean pluginShouldBeStarted(Plugin plugin, Iterable<ModuleDescriptor> descriptors);

    boolean isInSafeMode();

    default boolean isRequiredPlugin(Plugin plugin) {
        return false;
    }
}
