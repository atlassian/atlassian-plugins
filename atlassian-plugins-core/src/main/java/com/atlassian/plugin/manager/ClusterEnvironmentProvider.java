package com.atlassian.plugin.manager;

import com.atlassian.annotations.PublicSpi;

/**
 * Allows to check if the application (e.g. JIRA) is running in the cluster environment
 */
@PublicSpi
public interface ClusterEnvironmentProvider {
    /**
     * Default single node implementation (always false) just for convenience
     * This value must be used for single node environments only
     */
    ClusterEnvironmentProvider SINGLE_NODE = () -> false;

    /**
     * @return true if the application is running within the cluster environment
     */
    boolean isInCluster();
}
