package com.atlassian.plugin.manager;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginRestartState;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Simple forwarding delegate for a {@link PluginAccessor}.
 *
 * @since 2.7.0
 */
abstract class ForwardingPluginAccessor implements PluginAccessor {
    protected final PluginAccessor delegate;

    ForwardingPluginAccessor(final PluginAccessor delegate) {
        this.delegate = checkNotNull(delegate);
    }

    public ClassLoader getClassLoader() {
        return delegate.getClassLoader();
    }

    public InputStream getDynamicResourceAsStream(final String resourcePath) {
        return delegate.getDynamicResourceAsStream(resourcePath);
    }

    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz) {
        return delegate.getEnabledModuleDescriptorsByClass(descriptorClazz);
    }

    @Override
    @Deprecated
    public <D extends ModuleDescriptor<?>> List<D> getActiveModuleDescriptorsByClass(Class<D> descriptorClazz) {
        return delegate.getActiveModuleDescriptorsByClass(descriptorClazz);
    }

    public <M> List<M> getEnabledModulesByClass(final Class<M> moduleClass) {
        return delegate.getEnabledModulesByClass(moduleClass);
    }

    public Plugin getEnabledPlugin(final String pluginKey) {
        return delegate.getEnabledPlugin(pluginKey);
    }

    public ModuleDescriptor<?> getEnabledPluginModule(final String completeKey) {
        return delegate.getEnabledPluginModule(completeKey);
    }

    public Collection<Plugin> getEnabledPlugins() {
        return delegate.getEnabledPlugins();
    }

    public <M> Collection<ModuleDescriptor<M>> getModuleDescriptors(
            final Predicate<ModuleDescriptor<M>> moduleDescriptorPredicate) {
        return delegate.getModuleDescriptors(moduleDescriptorPredicate);
    }

    public <M> Collection<M> getModules(final Predicate<ModuleDescriptor<M>> moduleDescriptorPredicate) {
        return delegate.getModules(moduleDescriptorPredicate);
    }

    public Plugin getPlugin(final String key) {
        return delegate.getPlugin(key);
    }

    public ModuleDescriptor<?> getPluginModule(final String completeKey) {
        return delegate.getPluginModule(completeKey);
    }

    public PluginRestartState getPluginRestartState(final String key) {
        return delegate.getPluginRestartState(key);
    }

    public Collection<Plugin> getPlugins() {
        return delegate.getPlugins();
    }

    public Collection<Plugin> getPlugins(final Predicate<Plugin> pluginPredicate) {
        return delegate.getPlugins(pluginPredicate);
    }

    public boolean isPluginEnabled(final String key) {
        return delegate.isPluginEnabled(key);
    }

    public boolean isPluginModuleEnabled(final String completeKey) {
        return delegate.isPluginModuleEnabled(completeKey);
    }

    public boolean isSystemPlugin(final String key) {
        return delegate.isSystemPlugin(key);
    }

    public Iterable<ModuleDescriptor<?>> getDynamicModules(final Plugin plugin) {
        return delegate.getDynamicModules(plugin);
    }
}
