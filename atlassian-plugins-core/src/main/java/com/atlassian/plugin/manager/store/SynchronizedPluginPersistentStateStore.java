package com.atlassian.plugin.manager.store;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.atlassian.util.concurrent.ManagedLock;
import io.atlassian.util.concurrent.ManagedLocks;

import com.atlassian.annotations.Internal;
import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;

import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.plugin.util.EnumUtils.enumValueFromProperty;

/**
 * A wrapper to add exclusion and logging to a {@link PluginPersistentStateStore}.
 *
 * This wrapper is intended as an interim measure to investigate and experiment with locking to avoid the worst symptoms of
 * PLUG-1106. It's not designed to fix the underlying causes of the concurrency issues.
 *
 * @since 3.2.12
 */
public class SynchronizedPluginPersistentStateStore implements PluginPersistentStateStore {
    private static final Logger log = LoggerFactory.getLogger(SynchronizedPluginPersistentStateStore.class);

    @Internal
    public enum LockMode {
        UNLOCKED {
            @Override
            ReadWriteLock getReadWriteLock() {
                return new CommonReadWriteLock(new NoOpLock());
            }
        },
        SIMPLE {
            @Override
            ReadWriteLock getReadWriteLock() {
                return new CommonReadWriteLock(new ReentrantLock());
            }
        },
        FAIRSIMPLE {
            @Override
            ReadWriteLock getReadWriteLock() {
                return new CommonReadWriteLock(new ReentrantLock(true));
            }
        },
        READWRITE {
            @Override
            ReadWriteLock getReadWriteLock() {
                return new ReentrantReadWriteLock();
            }
        },
        FAIRREADWRITE {
            @Override
            ReadWriteLock getReadWriteLock() {
                return new ReentrantReadWriteLock(true);
            }
        };

        private static final String PROPERTY_NAME =
                SynchronizedPluginPersistentStateStore.class.getName() + ".lockMode";

        static LockMode current() {
            return enumValueFromProperty(PROPERTY_NAME, LockMode.values(), LockMode.READWRITE);
        }

        abstract ReadWriteLock getReadWriteLock();
    }

    @Internal
    public static String getLockModeProperty() {
        return LockMode.PROPERTY_NAME;
    }

    private final PluginPersistentStateStore delegate;
    private final ManagedLock.ReadWrite lock;
    private final AtomicInteger loadConcurrency = new AtomicInteger(0);
    private final AtomicInteger saveConcurrency = new AtomicInteger(0);

    public SynchronizedPluginPersistentStateStore(final PluginPersistentStateStore delegate) {
        this(delegate, LockMode.current().getReadWriteLock());
    }

    public SynchronizedPluginPersistentStateStore(final PluginPersistentStateStore delegate, final ReadWriteLock lock) {
        this.delegate = checkNotNull(delegate);
        this.lock = ManagedLocks.manageReadWrite(checkNotNull(lock));
    }

    @Override
    public void save(final PluginPersistentState state) {
        final int writes = saveConcurrency.incrementAndGet();
        log.debug("save concurrency {}", writes);
        lock.write().withLock(() -> delegate.save(state));
        saveConcurrency.decrementAndGet();
    }

    @Override
    public PluginPersistentState load() {
        final int reads = loadConcurrency.incrementAndGet();
        log.debug("load concurrency {}", reads);
        final PluginPersistentState pluginPersistentState =
                lock.read().withLock((Supplier<PluginPersistentState>) delegate::load);
        loadConcurrency.decrementAndGet();
        return pluginPersistentState;
    }

    static class NoOpLock implements Lock {
        @Override
        public void lock() {
            // Do nothing
        }

        @Override
        public void lockInterruptibly() {
            // Do nothing
        }

        @Override
        public boolean tryLock() {
            return true;
        }

        @Override
        public boolean tryLock(final long time, final TimeUnit unit) {
            return true;
        }

        @Override
        public void unlock() {
            // Do nothing
        }

        @Override
        public Condition newCondition() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    static class CommonReadWriteLock implements ReadWriteLock {
        private final Lock lock;

        public CommonReadWriteLock(final Lock lock) {
            this.lock = lock;
        }

        @Override
        public Lock readLock() {
            return lock;
        }

        @Override
        public Lock writeLock() {
            return lock;
        }
    }
}
