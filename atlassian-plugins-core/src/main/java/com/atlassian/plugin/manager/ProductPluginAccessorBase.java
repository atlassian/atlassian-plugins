package com.atlassian.plugin.manager;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.classloader.PluginsClassLoader;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.predicate.EnabledModulePredicate;
import com.atlassian.plugin.predicate.ModuleOfClassPredicate;
import com.atlassian.plugin.scope.ScopeManager;

import static com.atlassian.plugin.util.Assertions.notNull;

public class ProductPluginAccessorBase implements PluginAccessor {
    private static final Logger log = LoggerFactory.getLogger(ProductPluginAccessorBase.class);

    private final PluginRegistry.ReadOnly pluginRegistry;
    private final ModuleDescriptorFactory moduleDescriptorFactory;
    private final ClassLoader classLoader;
    private final PluginPersistentStateStore store;

    public ProductPluginAccessorBase(
            final PluginRegistry.ReadOnly pluginRegistry,
            final PluginPersistentStateStore store,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager) {
        this.pluginRegistry = pluginRegistry;
        this.moduleDescriptorFactory = notNull("ModuleDescriptorFactory", moduleDescriptorFactory);
        this.classLoader = new PluginsClassLoader(null, this, pluginEventManager);
        this.store = notNull("PluginPersistentStateStore", store);
    }

    /**
     * @deprecated in 5.0 for removal in 6.0 when {@link ScopeManager} will be removed.
     * Use
     * {@link com.atlassian.plugin.manager.ProductPluginAccessor#ProductPluginAccessor(com.atlassian.plugin.PluginRegistry.ReadOnly,
     * com.atlassian.plugin.manager.PluginPersistentStateStore,
     * com.atlassian.plugin.ModuleDescriptorFactory,
     * com.atlassian.plugin.event.PluginEventManager)} instead.
     */
    @Deprecated
    public ProductPluginAccessorBase(
            final PluginRegistry.ReadOnly pluginRegistry,
            final PluginPersistentStateStore store,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final ScopeManager ignored) {
        this(pluginRegistry, store, moduleDescriptorFactory, pluginEventManager);
    }

    @Override
    public Collection<Plugin> getPlugins() {
        return pluginRegistry.getAll();
    }

    @Override
    public Collection<Plugin> getPlugins(final Predicate<Plugin> pluginPredicate) {
        notNull("pluginPredicate", pluginPredicate);
        return getPlugins().stream().filter(pluginPredicate).collect(Collectors.toList());
    }

    @Override
    public Collection<Plugin> getEnabledPlugins() {
        return getPlugins((Predicate<Plugin>) p -> PluginState.ENABLED.equals(p.getPluginState()));
    }

    /**
     * Get the modules of all the given descriptor. If any of the getModule() calls fails, the error is recorded in the
     * logs
     *
     * @param moduleDescriptors the collection of module descriptors to get the modules from.
     * @return a {@link Collection} modules that can be any type of object. This collection will not contain any null
     * value.
     */
    private <M> Stream<M> getModules(final Stream<ModuleDescriptor<M>> moduleDescriptors) {
        return moduleDescriptors
                .filter(Objects::nonNull)
                .map(md -> {
                    try {
                        return md.getModule();
                    } catch (final RuntimeException ex) {
                        log.error("Exception when retrieving plugin module {}", md.getCompleteKey(), ex);
                        md.setBroken();
                        return null;
                    }
                })
                .filter(Objects::nonNull);
    }

    /**
     * Get the all the module descriptors from the given collection of plugins, filtered by the predicate.
     * <p>
     * Be careful, your predicate must filter ModuleDescriptors that are not M, this method does not guarantee that the
     * descriptors are of the correct type by itself.
     *
     * @param plugins a collection of {@link Plugin}s
     * @return a stream of {@link ModuleDescriptor descriptors}
     */
    private <M> Stream<ModuleDescriptor<M>> getModuleDescriptors(
            final Collection<Plugin> plugins, final Predicate<ModuleDescriptor<M>> predicate) {
        return plugins.stream()
                .flatMap(p -> p.getModuleDescriptors().stream())
                .map(m -> {
                    // hack way to get typed descriptors from plugin and
                    // keep generics happy
                    @SuppressWarnings("unchecked")
                    final ModuleDescriptor<M> cast = (ModuleDescriptor<M>) m;
                    return cast;
                })
                .filter(predicate);
    }

    @Override
    public <M> Collection<M> getModules(final Predicate<ModuleDescriptor<M>> predicate) {
        notNull("moduleDescriptorPredicate", predicate);
        return getModules(getModuleDescriptors(getPlugins(), predicate)).collect(Collectors.toList());
    }

    @Override
    public <M> Collection<ModuleDescriptor<M>> getModuleDescriptors(final Predicate<ModuleDescriptor<M>> predicate) {
        notNull("moduleDescriptorPredicate", predicate);
        return getModuleDescriptors(getPlugins(), predicate).collect(Collectors.toList());
    }

    @Override
    public Plugin getPlugin(final String key) {
        return pluginRegistry.get(notNull("Plugin key ", key));
    }

    @Override
    public Plugin getEnabledPlugin(final String pluginKey) {
        if (!isPluginEnabled(pluginKey)) {
            return null;
        }
        return getPlugin(pluginKey);
    }

    private ModuleDescriptor<?> getPluginModule(final ModuleCompleteKey key) {
        final Plugin plugin = getPlugin(key.getPluginKey());
        if (plugin == null) {
            return null;
        }
        return plugin.getModuleDescriptor(key.getModuleKey());
    }

    @Override
    public ModuleDescriptor<?> getPluginModule(@Nullable final String completeKey) {
        return getPluginModule(new ModuleCompleteKey(completeKey));
    }

    private boolean isPluginModuleEnabled(final ModuleCompleteKey key) {
        if (!isPluginEnabled(key.getPluginKey())) {
            return false;
        }
        final ModuleDescriptor<?> pluginModule = getPluginModule(key);
        return (pluginModule != null) && pluginModule.isEnabled();
    }

    @Override
    public ModuleDescriptor<?> getEnabledPluginModule(@Nullable final String completeKey) {
        final ModuleCompleteKey key = new ModuleCompleteKey(completeKey);

        // If it's disabled, return null
        if (!isPluginModuleEnabled(key)) {
            return null;
        }

        return getEnabledPlugin(key.getPluginKey()).getModuleDescriptor(key.getModuleKey());
    }

    @Override
    public boolean isPluginEnabled(final String key) {
        final Plugin plugin = pluginRegistry.get(notNull("Plugin key", key));

        return plugin != null && plugin.getPluginState() == PluginState.ENABLED;
    }

    @Override
    public boolean isPluginModuleEnabled(@Nullable final String completeKey) {
        return (completeKey != null) && isPluginModuleEnabled(new ModuleCompleteKey(completeKey));
    }

    /**
     * Get all module descriptor that are enabled and for which the module is an instance of the given class.
     *
     * @param moduleClass the class of the module within the module descriptor.
     * @return a stream of {@link ModuleDescriptor}s
     */
    private <M> Stream<ModuleDescriptor<M>> getEnabledModuleDescriptorsByModuleClass(final Class<M> moduleClass) {
        final ModuleOfClassPredicate<M> ofType = new ModuleOfClassPredicate<>(moduleClass);
        final EnabledModulePredicate enabled = new EnabledModulePredicate();
        return getModuleDescriptors(getEnabledPlugins(), ofType.and(enabled));
    }

    @Override
    public <M> List<M> getEnabledModulesByClass(final Class<M> moduleClass) {
        return getModules(getEnabledModuleDescriptorsByModuleClass(moduleClass)).collect(Collectors.toList());
    }

    @Override
    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz) {
        notNull("Descriptor class", descriptorClazz);
        return getEnabledPlugins().stream()
                .flatMap(p -> p.getModuleDescriptors().stream())
                .filter(descriptorClazz::isInstance)
                .filter(new EnabledModulePredicate())
                .map(descriptorClazz::cast)
                .collect(Collectors.toList());
    }

    @Override
    public InputStream getDynamicResourceAsStream(final String resourcePath) {
        return getClassLoader().getResourceAsStream(resourcePath);
    }

    @Override
    public ClassLoader getClassLoader() {
        return classLoader;
    }

    @Override
    public boolean isSystemPlugin(final String key) {
        final Plugin plugin = getPlugin(key);
        return (plugin != null) && plugin.isSystemPlugin();
    }

    @Override
    public PluginRestartState getPluginRestartState(final String key) {
        return store.load().getPluginRestartState(key);
    }

    @Override
    public Iterable<ModuleDescriptor<?>> getDynamicModules(final Plugin plugin) {
        // check the type
        if (plugin instanceof PluginInternal) {
            return ((PluginInternal) plugin).getDynamicModuleDescriptors();
        }
        throw new IllegalArgumentException(plugin + " does not implement com.atlassian.plugin.PluginInternal it is a "
                + plugin.getClass().getCanonicalName());
    }
}
