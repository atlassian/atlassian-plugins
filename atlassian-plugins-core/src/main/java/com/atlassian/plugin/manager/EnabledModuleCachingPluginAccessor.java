package com.atlassian.plugin.manager;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.instrumentation.PluginSystemInstrumentation;
import com.atlassian.plugin.instrumentation.Timer;
import com.atlassian.plugin.predicate.EnabledModulePredicate;
import com.atlassian.plugin.predicate.ModuleOfClassPredicate;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.cache.CacheBuilder.newBuilder;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * A caching decorator which caches {@link #getEnabledModuleDescriptorsByClass(Class)} and {@link
 * #getEnabledModulesByClass(Class)} on {@link com.atlassian.plugin.PluginAccessor} interface.
 */
public final class EnabledModuleCachingPluginAccessor extends ForwardingPluginAccessor implements PluginAccessor {
    private static final long DESCRIPTOR_TIMEOUT_SEC =
            Long.getLong("com.atlassian.plugin.descriptor.class.cache.timeout.sec", 30 * 60);
    private static final long MODULE_TIMEOUT_SEC =
            Long.getLong("com.atlassian.plugin.module.class.cache.timeout.sec", 30 * 60);

    private final SafeModuleExtractor safeModuleExtractor;

    private final LoadingCache<Class<ModuleDescriptor<Object>>, List<ModuleDescriptor<Object>>> cacheByDescriptorClass =
            newBuilder()
                    .expireAfterAccess(DESCRIPTOR_TIMEOUT_SEC, SECONDS)
                    .build(new ByModuleDescriptorClassCacheLoader());

    private final LoadingCache<Class<?>, List<ModuleDescriptor>> cacheByModuleClass =
            newBuilder().expireAfterAccess(MODULE_TIMEOUT_SEC, SECONDS).build(new ByModuleClassCacheLoader());

    public EnabledModuleCachingPluginAccessor(
            final PluginAccessor delegate,
            final PluginEventManager pluginEventManager,
            final PluginController pluginController) {
        super(delegate);
        checkNotNull(pluginEventManager);

        this.safeModuleExtractor = new SafeModuleExtractor(pluginController);

        // Strictly speaking a bad idea to register in constructor, but probably unlikely to get events during startup
        // https://extranet.atlassian.com/display/CONF/2015/04/28/Publishing+unconstructed+objects+into+main+memory+considered+harmful constructor may not be safe
        pluginEventManager.register(this);
    }

    /**
     * Clears the enabled module cache when any plugin is disabled. This ensures old modules are never returned from
     * disabled plugins.
     *
     * @param event The plugin disabled event
     */
    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginDisable(PluginDisabledEvent event) {
        // Safer not to assume we also receive module disable events for each individual module.
        invalidateAll();
    }

    /**
     * Clears the cache when any plugin is enabled.
     */
    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginEnable(PluginEnabledEvent event) {
        // PLUG-840 Don't assume we also receive module enable events for each individual module
        invalidateAll();
    }

    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginModuleEnabled(final PluginModuleEnabledEvent event) {
        // We invalidate from here and let cache loader reconstruct the state,
        // rather than having the cache entries be their own event listeners,
        // because if the cache entry is being constructed, we have a race condition: it may already have been told
        // the module is disabled, but not yet have registered to receive this enable event.

        // Just invalidate everything: entries for module/descriptor superclasses and interfaces all need invalidating,
        // this is simpler, and the cache mainly benefits web requests during post-startup steady state of plugin
        // system.
        invalidateAll();
    }

    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginModuleDisabled(final PluginModuleDisabledEvent event) {
        // We invalidate from here and let cache loader reconstruct the state,
        // rather than having the cache entries be their own event listeners,
        // because if the cache entry is being constructed, we have a race condition: it may already have been told
        // the module is enabled, but not yet have registered to receive this disable event.

        // Just invalidate everything: entries for module/descriptor superclasses and interfaces all need invalidating,
        // this is simpler, and the cache mainly benefits web requests during post-startup steady state of plugin
        // system.
        invalidateAll();
    }

    @SuppressWarnings("unused")
    @PluginEventListener
    public void onPluginFrameworkShutdown(final PluginFrameworkShutdownEvent event) {
        invalidateAll();
    }

    private void invalidateAll() {
        cacheByDescriptorClass.invalidateAll();
        cacheByModuleClass.invalidateAll();
    }

    /**
     * Cache loader for module descriptor cache
     */
    private class ByModuleDescriptorClassCacheLoader
            extends CacheLoader<Class<ModuleDescriptor<Object>>, List<ModuleDescriptor<Object>>> {
        public List<ModuleDescriptor<Object>> load(
                @Nonnull final Class<ModuleDescriptor<Object>> moduleDescriptorClass) {
            return delegate.getEnabledModuleDescriptorsByClass(moduleDescriptorClass);
        }
    }

    /**
     * Cache loader for module class cache
     */
    private class ByModuleClassCacheLoader extends CacheLoader<Class, List<ModuleDescriptor>> {
        @SuppressWarnings("unchecked")
        public List<ModuleDescriptor> load(@Nonnull final Class moduleClass) {
            return getEnabledModuleDescriptorsByModuleClass(moduleClass);
        }
    }

    /**
     * This method overrides the same method on PluginAccessor from the plugin system. We keep an up to date cache
     * of (module descriptor class M -> collection of module descriptors of descriptor class M) so we can avoid the expensive
     * call to the plugin system.
     *
     * @param descriptorClazz The module descriptor class you wish to find all enabled instances of
     * @return A list of all instances of enabled plugin module descriptors of the specified descriptor class
     */
    @SuppressWarnings("unchecked")
    @Override
    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz) {
        try (Timer ignored = PluginSystemInstrumentation.instance().pullTimer("getEnabledModuleDescriptorsByClass")) {
            final List<ModuleDescriptor<Object>> descriptors =
                    cacheByDescriptorClass.getUnchecked((Class<ModuleDescriptor<Object>>) descriptorClazz);
            return (List<D>) descriptors;
        }
    }

    /**
     * This method overrides the same method on PluginAccessor from the plugin system. We keep an up to date cache
     * of (module class M -> collection of module descriptors that have module class M) so we can avoid the expensive
     * call to the plugin system.
     *
     * @param moduleClass The module class you wish to find all instances of
     * @return A list of all instances of enabled plugin modules of the specified class
     */
    @Override
    public <M> List<M> getEnabledModulesByClass(final Class<M> moduleClass) {
        try (Timer ignored = PluginSystemInstrumentation.instance().pullTimer("getEnabledModulesByClass")) {
            List<?> descriptors = cacheByModuleClass.getUnchecked(moduleClass);
            //noinspection unchecked
            return safeModuleExtractor.getModules((List<ModuleDescriptor<M>>) descriptors);
        }
    }

    /**
     * Creates a collection of module descriptors that all share the same given module class
     *
     * @param moduleClass The module class we are interested in
     * @param <M>         The module class we are interested in
     * @return A collection of all module descriptors in the system with module class M
     */
    private <M> List<ModuleDescriptor<M>> getEnabledModuleDescriptorsByModuleClass(final Class<M> moduleClass) {
        final ModuleOfClassPredicate<M> ofType = new ModuleOfClassPredicate<>(moduleClass);
        final EnabledModulePredicate enabled = new EnabledModulePredicate();
        return getModuleDescriptors(delegate.getEnabledPlugins(), ofType.and(enabled));
    }

    /**
     * Builds an iterable of module descriptors for the given module predicate out of a collection of plugins.
     *
     * @param plugins   A collection of plugins to search through
     * @param predicate A predicate to filter the module descriptors by
     * @param <M>       The module class type
     * @return An Iterable of module descriptors of module class M
     */
    private <M> List<ModuleDescriptor<M>> getModuleDescriptors(
            final Collection<Plugin> plugins, final Predicate<ModuleDescriptor<M>> predicate) {
        //noinspection unchecked
        return plugins.stream()
                .flatMap(plugin -> plugin.getModuleDescriptors().stream())
                .map(moduleDescriptor -> (ModuleDescriptor<M>) moduleDescriptor)
                .filter(predicate)
                .collect(Collectors.toList());
    }
}
