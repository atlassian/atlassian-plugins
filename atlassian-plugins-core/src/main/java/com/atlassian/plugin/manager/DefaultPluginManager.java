package com.atlassian.plugin.manager;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.stream.Stream;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.instrumentation.operations.OpTimer;
import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginInstaller;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.RevertablePluginInstaller;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.classloader.PluginsClassLoader;
import com.atlassian.plugin.descriptors.CannotDisable;
import com.atlassian.plugin.descriptors.UnloadableModuleDescriptor;
import com.atlassian.plugin.descriptors.UnloadableModuleDescriptorFactory;
import com.atlassian.plugin.event.NotificationException;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginContainerUnavailableEvent;
import com.atlassian.plugin.event.events.PluginDependentsChangedEvent;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginDisablingEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginEnablingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkDelayedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkResumingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShuttingDownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkWarmRestartedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkWarmRestartingEvent;
import com.atlassian.plugin.event.events.PluginInstalledEvent;
import com.atlassian.plugin.event.events.PluginInstallingEvent;
import com.atlassian.plugin.event.events.PluginModuleAvailableEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisablingEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnablingEvent;
import com.atlassian.plugin.event.events.PluginModuleUnavailableEvent;
import com.atlassian.plugin.event.events.PluginRefreshedEvent;
import com.atlassian.plugin.event.events.PluginUninstalledEvent;
import com.atlassian.plugin.event.events.PluginUninstallingEvent;
import com.atlassian.plugin.event.events.PluginUpgradedEvent;
import com.atlassian.plugin.event.events.PluginUpgradingEvent;
import com.atlassian.plugin.exception.NoOpPluginExceptionInterception;
import com.atlassian.plugin.exception.PluginExceptionInterception;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.impl.UnloadablePluginFactory;
import com.atlassian.plugin.instrumentation.PluginSystemInstrumentation;
import com.atlassian.plugin.instrumentation.Timer;
import com.atlassian.plugin.internal.util.PluginUtils;
import com.atlassian.plugin.loaders.DiscardablePluginLoader;
import com.atlassian.plugin.loaders.DynamicPluginLoader;
import com.atlassian.plugin.loaders.PermissionCheckingPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.metadata.ClasspathFilePluginMetadata;
import com.atlassian.plugin.metadata.DefaultRequiredPluginValidator;
import com.atlassian.plugin.metadata.RequiredPluginValidator;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.predicate.EnabledModulePredicate;
import com.atlassian.plugin.predicate.EnabledPluginPredicate;
import com.atlassian.plugin.predicate.ModuleOfClassPredicate;
import com.atlassian.plugin.scope.ScopeManager;
import com.atlassian.plugin.util.VersionStringComparator;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Maps.filterKeys;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.max;
import static java.util.Collections.reverse;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.Collections.unmodifiableMap;
import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.StreamSupport.stream;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;
import static com.atlassian.plugin.impl.AbstractPlugin.cleanVersionString;
import static com.atlassian.plugin.manager.SafeModeManager.START_ALL_PLUGINS;
import static com.atlassian.plugin.util.Assertions.notNull;

/**
 * This implementation delegates the initiation and classloading of plugins to a
 * list of {@link com.atlassian.plugin.loaders.PluginLoader}s and records the
 * state of plugins in a
 * {@link com.atlassian.plugin.manager.PluginPersistentStateStore}.
 * <p>
 * This class is responsible for enabling and disabling plugins and plugin
 * modules and reflecting these state changes in the PluginPersistentStateStore.
 * <p>
 * An interesting quirk in the design is that
 * {@link #installPlugins(com.atlassian.plugin.PluginArtifact[])} explicitly stores
 * the plugin via a {@link com.atlassian.plugin.PluginInstaller}, whereas
 * {@link #uninstall(Plugin)} relies on the underlying
 * {@link com.atlassian.plugin.loaders.PluginLoader} to remove the plugin if
 * necessary.
 */
public class DefaultPluginManager implements PluginController, PluginAccessor, SplitStartupPluginSystemLifecycle {
    private static final Logger log = LoggerFactory.getLogger(DefaultPluginManager.class);

    @Internal
    public static String getStartupOverrideFileProperty() {
        return DefaultPluginManager.class.getName() + ".startupOverrideFile";
    }

    @Internal
    public static String getLateStartupEnableRetryProperty() {
        return DefaultPluginManager.class.getName() + ".lateStartupEnableRetry";
    }

    @Internal
    public static String getMinimumPluginVersionsFileProperty() {
        return DefaultPluginManager.class.getName() + ".minimumPluginVersionsFile";
    }

    private final List<DiscardablePluginLoader> pluginLoaders;
    private final PluginPersistentStateModifier persistentStateModifier;
    private final ModuleDescriptorFactory moduleDescriptorFactory;
    private final PluginEventManager pluginEventManager;

    private final PluginRegistry.ReadWrite pluginRegistry;
    private final PluginsClassLoader classLoader;
    private final PluginEnabler pluginEnabler;
    private final StateTracker tracker;
    private final boolean verifyRequiredPlugins;

    /**
     * Predicate for identify plugins which are not loaded at initialization.
     * <p>
     * This can be used to support two-phase "tenant aware" startup.
     */
    private final Predicate<Plugin> delayLoadOf;

    /**
     * Installer used for storing plugins. Used by
     * {@link #installPlugins(PluginArtifact[])}.
     */
    private RevertablePluginInstaller pluginInstaller;

    /**
     * Stores {@link Plugin}s as a key and {@link PluginLoader} as a value.
     */
    private final Map<Plugin, PluginLoader> installedPluginsToPluginLoader;

    /**
     * A map to pass information from {@link #earlyStartup} to {@link #addPlugins} without an API change.
     * <p>
     * This map allows earlyStartup to specify a plugin-specific loader for the list of plugins provided to addPlugins. It is only
     * valid when addPlugins is called from earlyStartup or lateStartup. Products may override addPlugins (to add clustering
     * behaviour for example), so fixing the API here is a more involved change.
     */
    private final Map<Plugin, DiscardablePluginLoader> candidatePluginsToPluginLoader;

    /**
     * A list of plugins to be re-enabled when adding plugins.
     * <p>
     * As with {@link #candidatePluginsToPluginLoader}, this is passing data from {@link #earlyStartup} and {@link #lateStartup} to
     * {@link #addPlugins} without an API change.
     */
    private final Collection<Plugin> additionalPluginsToEnable;

    private final DefaultPluginManagerJmxBridge defaultPluginManagerJmxBridge;

    /**
     * The list of plugins whose load was delayed.
     */
    private final List<Plugin> delayedPlugins;

    /**
     * A map of plugins which need to be removed during lateStartup.
     * <p>
     * The plugins which need to be removed on restart are discovered during earlyStartup so we can avoid installing them
     * when not required, but cannot be removed until persistence is available during late startup.
     */
    private final Map<Plugin, DiscardablePluginLoader> delayedPluginRemovalsToLoader;

    private final SafeModuleExtractor safeModuleExtractor;
    private final SafeModeManager safeModeManager;
    private final PluginTransactionContext pluginTransactionContext;

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager));
    }

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final PluginExceptionInterception pluginExceptionInterception) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withPluginExceptionInterception(pluginExceptionInterception));
    }

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final boolean verifyRequiredPlugins) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withVerifyRequiredPlugins(verifyRequiredPlugins));
    }

    @ExperimentalApi
    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final Predicate<Plugin> delayLoadOf) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withDelayLoadOf(delayLoadOf));
    }

    @ExperimentalApi
    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final PluginExceptionInterception pluginExceptionInterception,
            final Predicate<Plugin> delayLoadOf) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withPluginExceptionInterception(pluginExceptionInterception)
                .withDelayLoadOf(delayLoadOf));
    }

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final PluginExceptionInterception pluginExceptionInterception,
            final boolean verifyRequiredPlugins) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withPluginExceptionInterception(pluginExceptionInterception)
                .withVerifyRequiredPlugins(verifyRequiredPlugins));
    }

    public DefaultPluginManager(
            final PluginPersistentStateStore store,
            final List<PluginLoader> pluginLoaders,
            final ModuleDescriptorFactory moduleDescriptorFactory,
            final PluginEventManager pluginEventManager,
            final PluginExceptionInterception pluginExceptionInterception,
            final boolean verifyRequiredPlugins,
            final Predicate<Plugin> delayLoadOf) {
        this(newBuilder()
                .withStore(store)
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withPluginExceptionInterception(pluginExceptionInterception)
                .withVerifyRequiredPlugins(verifyRequiredPlugins)
                .withDelayLoadOf(delayLoadOf));
    }

    protected DefaultPluginManager(Builder<? extends Builder> builder) {
        this.safeModeManager = builder.safeModeManager;
        this.pluginLoaders = toPermissionCheckingPluginLoaders(notNull("Plugin Loaders list", builder.pluginLoaders));
        this.persistentStateModifier =
                new PluginPersistentStateModifier(notNull("PluginPersistentStateStore", builder.store));
        this.moduleDescriptorFactory = notNull("ModuleDescriptorFactory", builder.moduleDescriptorFactory);
        this.pluginEventManager = notNull("PluginEventManager", builder.pluginEventManager);
        this.pluginEnabler = new PluginEnabler(
                this, this, notNull("PluginExceptionInterception", builder.pluginExceptionInterception));
        this.verifyRequiredPlugins = builder.verifyRequiredPlugins;
        this.delayLoadOf = wrapDelayPredicateWithOverrides(builder.delayLoadOf);
        this.pluginRegistry = builder.pluginRegistry;
        this.classLoader = builder.pluginAccessor
                .map(pa -> PluginsClassLoader.class.cast(pa.getClassLoader()))
                .orElseGet(() -> new PluginsClassLoader(null, this, this.pluginEventManager));
        this.tracker = new StateTracker();
        this.pluginInstaller = new NoOpRevertablePluginInstaller(new UnsupportedPluginInstaller());
        this.installedPluginsToPluginLoader = new HashMap<>();
        this.candidatePluginsToPluginLoader = new HashMap<>();
        this.additionalPluginsToEnable = new ArrayList<>();
        this.delayedPlugins = new ArrayList<>();
        this.delayedPluginRemovalsToLoader = new HashMap<>();

        this.pluginEventManager.register(this);
        this.defaultPluginManagerJmxBridge = new DefaultPluginManagerJmxBridge(this);
        this.safeModuleExtractor = new SafeModuleExtractor(this);
        this.pluginTransactionContext = new PluginTransactionContext(this.pluginEventManager);
    }

    @SuppressWarnings("unchecked")
    public static class Builder<T extends Builder<?>> {
        private PluginPersistentStateStore store;
        private List<PluginLoader> pluginLoaders = new ArrayList<>();
        private ModuleDescriptorFactory moduleDescriptorFactory;
        private PluginEventManager pluginEventManager;
        private PluginExceptionInterception pluginExceptionInterception =
                NoOpPluginExceptionInterception.NOOP_INTERCEPTION;
        private boolean verifyRequiredPlugins = false;
        private Predicate<Plugin> delayLoadOf = p -> false;
        private PluginRegistry.ReadWrite pluginRegistry = new PluginRegistryImpl();
        private Optional<PluginAccessor> pluginAccessor = Optional.empty();
        private SafeModeManager safeModeManager = START_ALL_PLUGINS;

        public T withSafeModeManager(final SafeModeManager safeModeManager) {
            this.safeModeManager = safeModeManager;
            return (T) this;
        }

        public T withStore(final PluginPersistentStateStore store) {
            this.store = store;
            return (T) this;
        }

        public T withPluginLoaders(final List<PluginLoader> pluginLoaders) {
            this.pluginLoaders.addAll(pluginLoaders);
            return (T) this;
        }

        public T withPluginLoader(final PluginLoader pluginLoader) {
            this.pluginLoaders.add(pluginLoader);
            return (T) this;
        }

        public T withModuleDescriptorFactory(final ModuleDescriptorFactory moduleDescriptorFactory) {
            this.moduleDescriptorFactory = moduleDescriptorFactory;
            return (T) this;
        }

        public T withPluginEventManager(final PluginEventManager pluginEventManager) {
            this.pluginEventManager = pluginEventManager;
            return (T) this;
        }

        public T withPluginExceptionInterception(final PluginExceptionInterception pluginExceptionInterception) {
            this.pluginExceptionInterception = pluginExceptionInterception;
            return (T) this;
        }

        public T withVerifyRequiredPlugins(final boolean verifyRequiredPlugins) {
            this.verifyRequiredPlugins = verifyRequiredPlugins;
            return (T) this;
        }

        public T withDelayLoadOf(final Predicate<Plugin> delayLoadOf) {
            this.delayLoadOf = delayLoadOf;
            return (T) this;
        }

        public T withPluginRegistry(final PluginRegistry.ReadWrite pluginRegistry) {
            this.pluginRegistry = pluginRegistry;
            return (T) this;
        }

        public T withPluginAccessor(PluginAccessor pluginAccessor) {
            this.pluginAccessor = Optional.of(pluginAccessor);
            return (T) this;
        }

        /**
         * @deprecated in 5.0 for removal in 6.0 when {@link ScopeManager} will be removed.
         */
        @Deprecated
        public T withScopeManager(ScopeManager ignored) {
            return (T) this;
        }

        public DefaultPluginManager build() {
            return new DefaultPluginManager(this);
        }
    }

    public static Builder<? extends Builder<?>> newBuilder() {
        return new Builder<>();
    }

    private static Iterable<String> toPluginKeys(Iterable<Plugin> plugins) {
        return stream(plugins.spliterator(), false).map(Plugin::getKey).collect(toList());
    }

    private List<DiscardablePluginLoader> toPermissionCheckingPluginLoaders(final List<PluginLoader> fromIterable) {
        return fromIterable.stream().map(PermissionCheckingPluginLoader::new).collect(toList());
    }

    private Predicate<Plugin> wrapDelayPredicateWithOverrides(final Predicate<Plugin> pluginPredicate) {
        final Map<String, String> startupOverridesMap = parseFileNamedByPropertyAsMap(getStartupOverrideFileProperty());

        return new Predicate<Plugin>() {
            @Override
            public boolean test(final Plugin plugin) {
                final String pluginKey = plugin.getKey();

                // Check startup overrides for plugin startup declaration
                final String stringFromFile = startupOverridesMap.get(pluginKey);
                final Optional<Boolean> parsedFromFile =
                        parseStartupToDelay(stringFromFile, pluginKey, "override file");
                if (parsedFromFile.isPresent()) {
                    return parsedFromFile.get();
                }

                // Check PluginInformation for plugin startup declaration
                final PluginInformation pluginInformation = plugin.getPluginInformation();
                final String stringFromInformation =
                        (null != pluginInformation) ? pluginInformation.getStartup() : null;
                final Optional<Boolean> parsedFromInformation =
                        parseStartupToDelay(stringFromInformation, pluginKey, "PluginInformation");
                // If no plugin startup information, use product supplied predicate
                return parsedFromInformation.orElseGet(() -> pluginPredicate.test(plugin));
            }

            private Optional<Boolean> parseStartupToDelay(
                    final String startup, final String pluginKey, final String source) {
                if (null != startup) {
                    if ("early".equals(startup)) {
                        return Optional.of(Boolean.FALSE);
                    }
                    if ("late".equals(startup)) {
                        return Optional.of(Boolean.TRUE);
                    }

                    log.warn("Unknown startup '{}' for plugin '{}' from {}", startup, pluginKey, source);
                    // and fall through
                }

                return Optional.empty();
            }
        };
    }

    private Map<String, String> parseFileNamedByPropertyAsMap(final String property) {
        final Properties properties = new Properties();
        final String fileName = System.getProperty(property);
        if (null != fileName) {
            try (FileInputStream inStream = new FileInputStream(fileName)) {
                properties.load(inStream);
            } catch (final IOException eio) {
                log.warn("Failed to load file named by property {}, that is '{}'.", property, fileName, eio);
            }
        }
        return unmodifiableMap(propertiesToMap(properties));
    }

    private Map<String, String> propertiesToMap(final Properties properties) {
        final Map<String, String> propertiesMap = new HashMap<>();
        properties.forEach((key, value) -> propertiesMap.put((String) key, (String) value));
        return propertiesMap;
    }

    @Override
    public void init() {
        pluginTransactionContext.wrap(() -> {
            earlyStartup();
            lateStartup();
        });
    }

    @ExperimentalApi
    @Override
    public void earlyStartup() {
        pluginTransactionContext.wrap(() -> {
            try (Timer timer = PluginSystemInstrumentation.instance().pullSingleTimer("earlyStartup")) {
                log.info("Plugin system earlyStartup begun");
                tracker.setState(StateTracker.State.STARTING);

                defaultPluginManagerJmxBridge.register();
                broadcastIgnoreError(new PluginFrameworkStartingEvent(this, this));
                pluginInstaller.clearBackups();
                final PluginPersistentState pluginPersistentState = getState();
                final Map<String, List<Plugin>> candidatePluginKeyToVersionedPlugins = new TreeMap<>();
                for (final DiscardablePluginLoader loader : pluginLoaders) {
                    if (loader == null) {
                        continue;
                    }

                    final Iterable<Plugin> possiblePluginsToLoad = loader.loadAllPlugins(moduleDescriptorFactory);

                    if (log.isDebugEnabled()) {
                        log.debug(
                                "Found {} plugins to possibly load: {}",
                                stream(possiblePluginsToLoad.spliterator(), false)
                                        .count(),
                                toPluginKeys(possiblePluginsToLoad));
                    }

                    for (final Plugin plugin : possiblePluginsToLoad) {
                        if (pluginPersistentState.getPluginRestartState(plugin.getKey()) == PluginRestartState.REMOVE) {
                            log.info("Plugin {} was marked to be removed on restart. Removing now.", plugin);
                            // We need to remove the plugin and clear its state, but we don't want to do any persistence
                            // until
                            // late startup. We may as well delay PluginLoader#removePlugin also, as it doesn't hurt,
                            // and it makes fewer assumptions about how the product persists the plugins themselves.
                            delayedPluginRemovalsToLoader.put(plugin, loader);
                        } else {
                            // We need the loaders for installed plugins so we can issue removePlugin() when
                            // the plugin is unloaded. So anything we didn't remove above, we put into the
                            // candidatePluginsToPluginLoader map. All of these need either removePlugin()
                            // or discardPlugin() for resource management.
                            candidatePluginsToPluginLoader.put(plugin, loader);
                            List<Plugin> plugins = candidatePluginKeyToVersionedPlugins.computeIfAbsent(
                                    plugin.getKey(), key -> new ArrayList<>());
                            plugins.add(plugin);
                        }
                    }
                }

                final List<Plugin> pluginsToInstall = new ArrayList<>();
                for (final List<Plugin> plugins : candidatePluginKeyToVersionedPlugins.values()) {
                    final Plugin plugin = max(plugins, Comparator.naturalOrder());
                    if (plugins.size() > 1) {
                        log.debug(
                                "Plugin {} contained multiple versions. installing version {}.",
                                plugin.getKey(),
                                plugin.getPluginInformation().getVersion());
                    }
                    pluginsToInstall.add(plugin);
                }

                // Partition pluginsToInstall into immediatePlugins that we install now, and delayedPlugins
                // that we install when instructed by a call to lateStartup.
                // In later versions of Guava, ImmutableListMultimap.index is another way to slice this if
                // you convert the PluginPredicate to a function. Whether or this is cleaner is a bit moot
                // since we can't use it yet anyway.
                final List<Plugin> immediatePlugins = new ArrayList<>();
                for (final Plugin plugin : pluginsToInstall) {
                    if (delayLoadOf.test(plugin)) {
                        delayedPlugins.add(plugin);
                    } else {
                        immediatePlugins.add(plugin);
                    }
                }

                // Install the non-delayed plugins
                addPlugins(null, immediatePlugins);

                // For each immediatePlugins, addPlugins has either called removePlugin()/discardPlugin()
                // for its loader, or is tracking it in installedPluginsToPluginLoader for a removePlugin()
                // when it is uninstalled (either via upgrade or shutdown).
                for (final Plugin plugin : immediatePlugins) {
                    candidatePluginsToPluginLoader.remove(plugin);
                }

                if (Boolean.getBoolean(getLateStartupEnableRetryProperty())) {
                    for (final Plugin plugin : immediatePlugins) {
                        // For each plugin that didn't enable but should have, make a note so we can try them again
                        // later. It's a little
                        // bit vexing that we are checking the persistent state here again just after addPlugins did it,
                        // but refactoring this
                        // stuff is fraught with API danger, so this can wait. PLUG-1116 seems like a time this might be
                        // worth revisiting.
                        if ((PluginState.ENABLED != plugin.getPluginState())
                                && pluginPersistentState.isEnabled(plugin)) {
                            additionalPluginsToEnable.add(plugin);
                        }
                    }
                    if (!additionalPluginsToEnable.isEmpty()) {
                        // Let people know we're going to retry them, so there is information in the logs about this
                        // near the resolution errors
                        log.warn(
                                "Failed to enable some ({}) early plugins, will fallback during lateStartup. Plugins: {}",
                                additionalPluginsToEnable.size(),
                                additionalPluginsToEnable);
                    }
                }

                // We need to keep candidatePluginsToPluginLoader populated with the delayedPlugins so that
                // addPlugins can do the right thing when called by lateStartup. However, we want to
                // discardPlugin anything we don't need so that loaders can release resources. So move what
                // we need later from candidatePluginsToPluginLoader to delayedPluginsLoaders.
                final Map<Plugin, DiscardablePluginLoader> delayedPluginsLoaders = new HashMap<>();
                for (final Plugin plugin : delayedPlugins) {
                    final DiscardablePluginLoader loader = candidatePluginsToPluginLoader.remove(plugin);
                    delayedPluginsLoaders.put(plugin, loader);
                }
                // Now candidatePluginsToPluginLoader contains exactly Plugins returned by loadAllPlugins
                // for which we didn't removePlugin() above, didn't pass on addPlugins(), and won't handle
                // in loadDelayedPlugins. So loaders can release resources, we discardPlugin() these.
                for (final Map.Entry<Plugin, DiscardablePluginLoader> entry :
                        candidatePluginsToPluginLoader.entrySet()) {
                    final Plugin plugin = entry.getKey();
                    final DiscardablePluginLoader loader = entry.getValue();
                    loader.discardPlugin(plugin);
                }
                // Finally, make candidatePluginsToPluginLoader contain what loadDelayedPlugins needs.
                candidatePluginsToPluginLoader.clear();
                candidatePluginsToPluginLoader.putAll(delayedPluginsLoaders);
                tracker.setState(StateTracker.State.DELAYED);

                logTime(timer, "Plugin system earlyStartup ended");

                broadcastIgnoreError(new PluginFrameworkDelayedEvent(this, this));
            }
        });
    }

    @ExperimentalApi
    @Override
    public void lateStartup() {
        pluginTransactionContext.wrap(() -> {
            try (Timer timer = PluginSystemInstrumentation.instance().pullSingleTimer("lateStartup")) {
                log.info("Plugin system lateStartup begun");

                tracker.setState(StateTracker.State.RESUMING);
                broadcastIgnoreError(new PluginFrameworkResumingEvent(this, this));

                addPlugins(null, delayedPlugins);
                delayedPlugins.clear();
                candidatePluginsToPluginLoader.clear();

                persistentStateModifier.clearPluginRestartState();
                for (final Map.Entry<Plugin, DiscardablePluginLoader> entry :
                        delayedPluginRemovalsToLoader.entrySet()) {
                    final Plugin plugin = entry.getKey();
                    final DiscardablePluginLoader loader = entry.getValue();
                    // Remove the plugin from the loader, and discard saved state (see PLUG-13).
                    loader.removePlugin(plugin);
                    persistentStateModifier.removeState(plugin);
                }
                delayedPluginRemovalsToLoader.clear();

                logTime(timer, "Plugin system lateStartup ended");

                tracker.setState(StateTracker.State.STARTED);
                if (verifyRequiredPlugins) {
                    validateRequiredPlugins();
                }
                broadcastIgnoreError(new PluginFrameworkStartedEvent(this, this));
            }
        });
    }

    private void validateRequiredPlugins() {
        final RequiredPluginValidator validator =
                new DefaultRequiredPluginValidator(this, new ClasspathFilePluginMetadata());
        final Collection<String> errors = validator.validate();
        if (!errors.isEmpty()) {
            log.error("Unable to validate required plugins or modules - plugin system shutting down");
            log.error("Failures:");
            for (final String error : errors) {
                log.error("\t{}", error);
            }
            shutdown();
            throw new PluginException("Unable to validate required plugins or modules");
        }
    }

    /**
     * @param timer the timer
     * @param message Message to log as info
     */
    private void logTime(Timer timer, String message) {
        Optional<OpTimer> opTimer = timer.getOpTimer();
        if (opTimer.isPresent()) {
            long elapsedSeconds = opTimer.get().snapshot().getElapsedTotalTime(TimeUnit.SECONDS);
            log.info("{} in {}s", message, elapsedSeconds);
        } else {
            log.info(message);
        }
    }

    /**
     * Fires the shutdown event
     *
     * @throws IllegalStateException if already shutdown or already in the
     *                               process of shutting down.
     * @since 2.0.0
     */
    @Override
    public void shutdown() {
        pluginTransactionContext.wrap(() -> {
            try (Timer ignored = PluginSystemInstrumentation.instance().pullSingleTimer("shutdown")) {
                tracker.setState(StateTracker.State.SHUTTING_DOWN);

                log.info("Preparing to shut down the plugin system");
                broadcastIgnoreError(
                        new PluginFrameworkShuttingDownEvent(DefaultPluginManager.this, DefaultPluginManager.this));

                log.info("Shutting down the plugin system");
                broadcastIgnoreError(
                        new PluginFrameworkShutdownEvent(DefaultPluginManager.this, DefaultPluginManager.this));

                pluginRegistry.clear();
                pluginEventManager.unregister(this);
                tracker.setState(StateTracker.State.SHUTDOWN);
                defaultPluginManagerJmxBridge.unregister();
            }
        });
    }

    @Override
    public final void warmRestart() {
        pluginTransactionContext.wrap(() -> {
            tracker.setState(StateTracker.State.WARM_RESTARTING);
            log.info("Initiating a warm restart of the plugin system");
            broadcastIgnoreError(
                    new PluginFrameworkWarmRestartingEvent(DefaultPluginManager.this, DefaultPluginManager.this));

            // Make sure we reload plugins in order
            final List<Plugin> restartedPlugins = new ArrayList<>();
            final List<PluginLoader> loaders = new ArrayList<>(pluginLoaders);
            reverse(loaders);
            for (final PluginLoader loader : pluginLoaders) {
                for (final Map.Entry<Plugin, PluginLoader> entry : installedPluginsToPluginLoader.entrySet()) {
                    if (entry.getValue() == loader) {
                        final Plugin plugin = entry.getKey();
                        if (isPluginEnabled(plugin.getKey())) {
                            disablePluginModules(plugin);
                            restartedPlugins.add(plugin);
                        }
                    }
                }
            }

            // then enable them in reverse order
            reverse(restartedPlugins);
            for (final Plugin plugin : restartedPlugins) {
                enableConfiguredPluginModules(plugin);
            }

            broadcastIgnoreError(
                    new PluginFrameworkWarmRestartedEvent(DefaultPluginManager.this, DefaultPluginManager.this));
            tracker.setState(StateTracker.State.STARTED);
        });
    }

    @PluginEventListener
    public void onPluginModuleAvailable(final PluginModuleAvailableEvent event) {
        pluginTransactionContext.wrap(
                () -> enableConfiguredPluginModule(event.getModule().getPlugin(), event.getModule(), new HashSet<>()));
    }

    @PluginEventListener
    public void onPluginModuleUnavailable(final PluginModuleUnavailableEvent event) {
        pluginTransactionContext.wrap(() -> disablePluginModuleNoPersist(event.getModule()));
    }

    @PluginEventListener
    public void onPluginContainerUnavailable(final PluginContainerUnavailableEvent event) {
        pluginTransactionContext.wrap(() -> disablePluginWithoutPersisting(event.getPluginKey()));
    }

    @PluginEventListener
    public void onPluginRefresh(final PluginRefreshedEvent event) {
        pluginTransactionContext.wrap(() -> {
            final Plugin plugin = event.getPlugin();

            disablePluginModules(plugin);

            // It would be nice to fire this earlier, but doing it earlier than the disable of the plugin modules seems
            // too early.
            // We should probably hook methods on NonValidatingOsgiBundleXmlApplicationContext (such as prepareRefresh
            // ?) to
            // move this and the disable earlier if it makes sense.
            broadcastIgnoreError(new PluginEnablingEvent(plugin));

            if (enableConfiguredPluginModules(plugin)) {
                broadcastPluginEnabled(plugin);
            }
        });
    }

    /**
     * Set the plugin installation strategy for this manager
     *
     * @param pluginInstaller the plugin installation strategy to use
     * @see PluginInstaller
     */
    public void setPluginInstaller(final PluginInstaller pluginInstaller) {
        if (pluginInstaller instanceof RevertablePluginInstaller) {
            this.pluginInstaller = (RevertablePluginInstaller) pluginInstaller;
        } else {
            this.pluginInstaller = new NoOpRevertablePluginInstaller(pluginInstaller);
        }
    }

    @Override
    public Set<String> installPlugins(final PluginArtifact... pluginArtifacts) {
        final Map<String, PluginArtifact> validatedArtifacts = new LinkedHashMap<>();
        pluginTransactionContext.wrap(() -> {
            try {
                for (final PluginArtifact pluginArtifact : pluginArtifacts) {
                    validatedArtifacts.put(validatePlugin(pluginArtifact), pluginArtifact);
                }
            } catch (final PluginParseException ex) {
                throw new PluginParseException("All plugins could not be validated", ex);
            }

            for (final Map.Entry<String, PluginArtifact> entry : validatedArtifacts.entrySet()) {
                pluginInstaller.installPlugin(entry.getKey(), entry.getValue());
            }

            scanForNewPlugins();
        });
        return validatedArtifacts.keySet();
    }

    /**
     * Validate a plugin jar. Looks through all plugin loaders for ones that can
     * load the plugin and extract the plugin key as proof.
     *
     * @param pluginArtifact the jar file representing the plugin
     * @return The plugin key
     * @throws PluginParseException if the plugin cannot be parsed
     * @throws NullPointerException if <code>pluginJar</code> is null.
     */
    String validatePlugin(final PluginArtifact pluginArtifact) {
        boolean foundADynamicPluginLoader = false;
        for (final PluginLoader loader : pluginLoaders) {
            if (loader.isDynamicPluginLoader()) {
                foundADynamicPluginLoader = true;
                final String key = ((DynamicPluginLoader) loader).canLoad(pluginArtifact);
                if (key != null) {
                    return key;
                }
            }
        }

        if (!foundADynamicPluginLoader) {
            throw new IllegalStateException("Should be at least one DynamicPluginLoader in the plugin loader list");
        }
        throw new PluginParseException("Jar " + pluginArtifact.getName() + " is not a valid plugin!");
    }

    @Override
    public int scanForNewPlugins() {
        final StateTracker.State state = tracker.get();
        checkState(
                (StateTracker.State.RESUMING == state) || (StateTracker.State.STARTED == state),
                "Cannot scanForNewPlugins in state %s",
                state);

        final AtomicInteger numberFound = new AtomicInteger(0);
        pluginTransactionContext.wrap(() -> {
            for (final PluginLoader loader : pluginLoaders) {
                if (loader != null && loader.supportsAddition()) {
                    final List<Plugin> pluginsToAdd = new ArrayList<>();
                    for (Plugin plugin : loader.loadFoundPlugins(moduleDescriptorFactory)) {
                        final Plugin oldPlugin = pluginRegistry.get(plugin.getKey());
                        // Only actually install the plugin if its module
                        // descriptors support it. Otherwise, mark it as
                        // unloadable.
                        if (!(plugin instanceof UnloadablePlugin)) {
                            if (PluginUtils.doesPluginRequireRestart(plugin)) {
                                if (oldPlugin == null) {
                                    markPluginInstallThatRequiresRestart(plugin);

                                    final UnloadablePlugin unloadablePlugin =
                                            UnloadablePluginFactory.createUnloadablePlugin(plugin);
                                    unloadablePlugin.setErrorText("Plugin requires a restart of the application due "
                                            + "to the following modules: "
                                            + PluginUtils.getPluginModulesThatRequireRestart(plugin));
                                    plugin = unloadablePlugin;
                                } else {
                                    // If a plugin has been installed but is waiting for restart then we do not want to
                                    // put the plugin into the update state, we want to keep it in the install state.
                                    if (!PluginRestartState.INSTALL.equals(getPluginRestartState(plugin.getKey()))) {
                                        markPluginUpgradeThatRequiresRestart(plugin);
                                    }
                                    continue;
                                }
                            }
                            // If the new plugin does not require restart we need to check what the restart state of
                            // the old plugin was and act accordingly
                            else if (oldPlugin != null && PluginUtils.doesPluginRequireRestart(oldPlugin)) {
                                // If you have installed the plugin that requires restart and before restart you have
                                // reinstalled a version of that plugin that does not require restart then you should
                                // just go ahead and install that plugin. This means reverting the previous install
                                // and letting the plugin fall into the plugins to add list
                                if (PluginRestartState.INSTALL.equals(getPluginRestartState(oldPlugin.getKey()))) {
                                    revertRestartRequiredChange(oldPlugin.getKey());
                                } else {
                                    markPluginUpgradeThatRequiresRestart(plugin);
                                    continue;
                                }
                            }
                            pluginsToAdd.add(plugin);
                        }
                    }
                    addPlugins(loader, pluginsToAdd);
                    numberFound.addAndGet(pluginsToAdd.size());
                }
            }
        });
        return numberFound.get();
    }

    private void markPluginInstallThatRequiresRestart(final Plugin plugin) {
        log.info(
                "Installed plugin '{}' requires a restart due to the following modules: {}",
                plugin,
                PluginUtils.getPluginModulesThatRequireRestart(plugin));
        updateRequiresRestartState(plugin.getKey(), PluginRestartState.INSTALL);
    }

    private void markPluginUpgradeThatRequiresRestart(final Plugin plugin) {
        log.info(
                "Upgraded plugin '{}' requires a restart due to the following modules: {}",
                plugin,
                PluginUtils.getPluginModulesThatRequireRestart(plugin));
        updateRequiresRestartState(plugin.getKey(), PluginRestartState.UPGRADE);
    }

    private void markPluginUninstallThatRequiresRestart(final Plugin plugin) {
        log.info(
                "Uninstalled plugin '{}' requires a restart due to the following modules: {}",
                plugin,
                PluginUtils.getPluginModulesThatRequireRestart(plugin));
        updateRequiresRestartState(plugin.getKey(), PluginRestartState.REMOVE);
    }

    private void updateRequiresRestartState(final String pluginKey, final PluginRestartState pluginRestartState) {
        persistentStateModifier.setPluginRestartState(pluginKey, pluginRestartState);
        onUpdateRequiresRestartState(pluginKey, pluginRestartState);
    }

    @SuppressWarnings("UnusedParameters")
    protected void onUpdateRequiresRestartState(final String pluginKey, final PluginRestartState pluginRestartState) {
        // nothing to do in this implementation
    }

    /**
     * Uninstalls the given plugin, emitting disabled and uninstalled events as it does so.
     *
     * @param plugin the plugin to uninstall.
     * @throws PluginException If the plugin or loader doesn't support uninstallation
     */
    @Override
    public void uninstall(final Plugin plugin) {
        pluginTransactionContext.wrap(() -> uninstallPlugins(singletonList(plugin)));
    }

    @Override
    public void uninstallPlugins(Collection<Plugin> plugins) {
        pluginTransactionContext.wrap(() -> {
            Map<Boolean, Set<Plugin>> requireRestart =
                    plugins.stream().collect(partitioningBy(PluginUtils::doesPluginRequireRestart, toSet()));

            // Plugins that require application restart will be uninstalled on the next application start
            // (see com.atlassian.plugin.descriptors.RequiresRestart).
            requireRestart.get(true).forEach(plugin -> {
                ensurePluginAndLoaderSupportsUninstall(plugin);
                markPluginUninstallThatRequiresRestart(plugin);
            });

            Set<Plugin> pluginsToDisable = requireRestart.get(false);

            if (!pluginsToDisable.isEmpty()) {
                final DependentPlugins disabledPlugins = disablePluginsAndTheirDependencies(
                        pluginsToDisable.stream().map(Plugin::getKey).collect(toList()),
                        unmodifiableSet(new HashSet<>(asList(MANDATORY, OPTIONAL, DYNAMIC))));
                disabledPlugins.getPluginsByTypes(singleton(MANDATORY), true).forEach(persistentStateModifier::disable);

                pluginsToDisable.forEach(p -> broadcastIgnoreError(new PluginUninstallingEvent(p)));

                pluginsToDisable.forEach(this::uninstallNoEvent);

                pluginsToDisable.forEach(p -> broadcastIgnoreError(new PluginUninstalledEvent(p)));

                reenableDependent(pluginsToDisable, disabledPlugins, PluginState.UNINSTALLED);
            }
        });
    }

    /**
     * Preforms an uninstallation without broadcasting the uninstallation event.
     *
     * @param plugin The plugin to uninstall
     * @since 2.5.0
     */
    protected void uninstallNoEvent(final Plugin plugin) {
        unloadPlugin(plugin);

        // PLUG-13: Plugins should not save state across uninstalls.
        persistentStateModifier.removeState(plugin);
    }

    /**
     * @param pluginKey The plugin key to revert
     * @throws PluginException If the revert cannot be completed
     */
    @Override
    public void revertRestartRequiredChange(final String pluginKey) {
        pluginTransactionContext.wrap(() -> {
            notNull("pluginKey", pluginKey);
            final PluginRestartState restartState = getState().getPluginRestartState(pluginKey);
            if (restartState == PluginRestartState.UPGRADE) {
                pluginInstaller.revertInstalledPlugin(pluginKey);
            } else if (restartState == PluginRestartState.INSTALL) {
                pluginInstaller.revertInstalledPlugin(pluginKey);
                pluginRegistry.remove(pluginKey);
            }
            updateRequiresRestartState(pluginKey, PluginRestartState.NONE);
        });
    }

    protected void removeStateFromStore(final PluginPersistentStateStore stateStore, final Plugin plugin) {
        new PluginPersistentStateModifier(stateStore).removeState(plugin);
    }

    /**
     * Unload a plugin. Called when plugins are added locally, or remotely in a
     * clustered application.
     *
     * @param plugin the plugin to remove
     * @throws PluginException if the plugin cannot be uninstalled
     */
    protected void unloadPlugin(final Plugin plugin) {
        pluginTransactionContext.wrap(() -> {
            final PluginLoader loader = ensurePluginAndLoaderSupportsUninstall(plugin);

            if (isPluginEnabled(plugin.getKey())) {
                notifyPluginDisabled(plugin);
            }

            notifyUninstallPlugin(plugin);
            if (loader != null) {
                removePluginFromLoader(plugin);
            }

            pluginRegistry.remove(plugin.getKey());
        });
    }

    private PluginLoader ensurePluginAndLoaderSupportsUninstall(final Plugin plugin) {
        if (!plugin.isUninstallable()) {
            throw new PluginException("Plugin is not uninstallable: " + plugin);
        }

        final PluginLoader loader = installedPluginsToPluginLoader.get(plugin);

        if ((loader != null) && !loader.supportsRemoval()) {
            throw new PluginException("Not uninstalling plugin - loader doesn't allow removal. Plugin: " + plugin);
        }
        return loader;
    }

    private void removePluginFromLoader(final Plugin plugin) {
        if (plugin.isUninstallable()) {
            final PluginLoader pluginLoader = installedPluginsToPluginLoader.get(plugin);
            pluginLoader.removePlugin(plugin);
        }

        installedPluginsToPluginLoader.remove(plugin);
    }

    protected void notifyUninstallPlugin(final Plugin plugin) {
        classLoader.notifyUninstallPlugin(plugin);

        for (final ModuleDescriptor<?> descriptor : plugin.getModuleDescriptors()) {
            descriptor.destroy();
        }
    }

    protected PluginPersistentState getState() {
        return persistentStateModifier.getState();
    }

    /**
     * Update the local plugin state and enable state aware modules.
     * <p>
     * If there is an existing plugin with the same key, the version strings of
     * the existing plugin and the plugin provided to this method will be parsed
     * and compared. If the installed version is newer than the provided
     * version, it will not be changed. If the specified plugin's version is the
     * same or newer, the existing plugin state will be saved and the plugin
     * will be unloaded before the provided plugin is installed. If the existing
     * plugin cannot be unloaded a {@link PluginException} will be thrown.
     *
     * @param loader           the loader used to load this plugin. This should only be null when called
     *                         internally from init(), in which case the loader is looked up per plugin in
     *                         candidatePluginsToPluginLoader.
     * @param pluginsToInstall the plugins to add
     * @throws PluginParseException if the plugin cannot be parsed
     * @since 2.0.2
     */
    protected void addPlugins(@Nullable final PluginLoader loader, final Collection<Plugin> pluginsToInstall) {
        pluginTransactionContext.wrap(() -> {
            final List<Plugin> pluginsToEnable = new ArrayList<>();
            final Set<PluginDependentsChangedEvent> dependentsChangedEvents = new HashSet<>();
            final Map<String, String> minimumPluginVersions =
                    parseFileNamedByPropertyAsMap(getMinimumPluginVersionsFileProperty());

            // Install plugins, looking for upgrades and duplicates
            for (final Plugin plugin : new TreeSet<>(pluginsToInstall)) {
                boolean pluginUpgraded = false;
                // testing to make sure plugin keys are unique
                final String pluginKey = plugin.getKey();
                final Plugin existingPlugin = pluginRegistry.get(pluginKey);
                if (!pluginVersionIsAcceptable(plugin, minimumPluginVersions)) {
                    log.info(
                            "Unacceptable plugin {} found - version less than minimum '{}'",
                            plugin,
                            minimumPluginVersions.get(pluginKey));
                    // We're not going to install it, so notify loader to release resources
                    discardPlugin(loader, plugin);

                    // Don't install the unacceptable plugin
                    continue;

                } else if (null == existingPlugin) {
                    broadcastIgnoreError(new PluginInstallingEvent(plugin));
                } else if (plugin.compareTo(existingPlugin) >= 0) {
                    // upgrading a plugin
                    try {
                        // disable mandatory, optional and dynamic dependencies, enabling them afterwards
                        final DependentPlugins disabledPlugins = disableOnlyPluginDependencies(
                                singletonList(plugin.getKey()),
                                unmodifiableSet(new HashSet<>(asList(MANDATORY, OPTIONAL, DYNAMIC))));
                        final List<Plugin> disabledPluginsList = disabledPlugins.getPlugins(false);
                        pluginsToEnable.addAll(disabledPluginsList);

                        if (!disabledPluginsList.isEmpty()) {
                            log.info(
                                    "Found mandatory, optional and dynamically dependent plugins to re-enable after plugin upgrade '{}': {}. Enabling...",
                                    plugin,
                                    toPluginKeys(disabledPluginsList));
                        }

                        // Fire upgrading event for outgoing plugin. We do this before updatePlugin which removes the
                        // old plugin,
                        // but after disabling dependent plugins. This latter ordering is a bit of a judgement call, but
                        // this
                        // gives the plugin as much freedom to move as possible (knowing that clients have been
                        // disabled).
                        broadcastIgnoreError(new PluginUpgradingEvent(existingPlugin));
                        updatePlugin(existingPlugin, plugin);
                        pluginsToEnable.remove(existingPlugin);
                        pluginUpgraded = true;

                        if (!disabledPluginsList.isEmpty()) {
                            dependentsChangedEvents.add(new PluginDependentsChangedEvent(
                                    plugin, PluginState.INSTALLED, emptyList(), disabledPluginsList));
                        }
                    } catch (final PluginException e) {
                        throw new PluginParseException(
                                "Duplicate plugin found (installed version is the same or older) and"
                                        + " could not be unloaded: '" + pluginKey + "'",
                                e);
                    }
                } else {
                    // If we find an older plugin, don't error (PLUG-12) ...
                    log.debug("Duplicate plugin found (installed version is newer): '{}'", pluginKey);
                    // ... just discard the older (unused) plugin
                    discardPlugin(loader, plugin);

                    // Don't install the older plugin
                    continue;
                }

                plugin.install();

                final boolean isPluginEnabledInSafeMode = tracker.get() == StateTracker.State.STARTED
                        || isPluginEnabledInSafeMode(plugin, pluginsToInstall);
                if (getState().isEnabled(plugin) && isPluginEnabledInSafeMode) {
                    log.debug("Plugin '{}' is to be enabled.", pluginKey);
                    pluginsToEnable.add(plugin);
                } else if (!isPluginEnabledInSafeMode) {
                    log.warn("Plugin '{}' is disabled due to startup options!", pluginKey);
                } else {
                    if (plugin.isSystemPlugin()) {
                        log.warn("System Plugin '{}' is disabled.", pluginKey);
                    } else {
                        log.debug("Plugin '{}' is disabled.", pluginKey);
                    }
                }

                if (pluginUpgraded) {
                    broadcastIgnoreError(new PluginUpgradedEvent(plugin));
                } else {
                    broadcastIgnoreError(new PluginInstalledEvent(plugin));
                }

                pluginRegistry.put(plugin);
                if (loader == null) {
                    installedPluginsToPluginLoader.put(plugin, candidatePluginsToPluginLoader.get(plugin));
                } else {
                    installedPluginsToPluginLoader.put(plugin, loader);
                }
            }

            // Include any additionalPluginsToEnable which are hanging over from a previous enable attempt. This is a
            // safety net
            // to workaround issues where early plugins are failing to enabled because of newly added dependencies on
            // late plugins.
            // The code in question is broken, and we warn below, but this is causing enough pain we're special casing
            // for now.
            pluginsToEnable.addAll(additionalPluginsToEnable);

            // bounce mandatory, optional and dynamic dependencies
            enableDependentPlugins(pluginsToEnable);

            // broadcast dependency changes following an upgrade
            for (PluginDependentsChangedEvent event : dependentsChangedEvents) {
                broadcastIgnoreError(event);
            }
        });
    }

    private boolean isPluginEnabledInSafeMode(Plugin plugin, Collection<Plugin> pluginsToInstall) {
        return safeModeManager.pluginShouldBeStarted(
                plugin,
                getModuleDescriptors(pluginsToInstall, moduleDescriptor -> true).collect(toList()));
    }

    private void enableDependentPlugins(final Collection<Plugin> pluginsToEnable) {
        pluginTransactionContext.wrap(() -> {
            if (pluginsToEnable.isEmpty()) {
                log.debug("No dependent plugins found to enable.");
                return;
            }

            final List<Plugin> pluginsInEnableOrder = new PluginsInEnableOrder(pluginsToEnable, pluginRegistry).get();

            if (log.isDebugEnabled()) {
                log.debug(
                        "Found {} plugins to enable: {}",
                        pluginsInEnableOrder.size(),
                        toPluginKeys(pluginsInEnableOrder));
            }

            for (final Plugin plugin : pluginsInEnableOrder) {
                broadcastIgnoreError(new PluginEnablingEvent(plugin));
            }

            pluginEnabler.enable(pluginsInEnableOrder); // enable all plugins, waiting a time period for them to enable

            for (final Plugin plugin : additionalPluginsToEnable) {
                if (PluginState.ENABLED == plugin.getPluginState()) {
                    log.warn(
                            "Plugin {} was early but failed to enable, but was fallback enabled in lateStartup."
                                    + " It likely has dependencies on plugins which are late, in which case you should fix those plugins and"
                                    + " make them early, or as a last resort make the offending plugin late",
                            plugin);
                }
            }
            additionalPluginsToEnable.clear();

            // handle the plugins that were able to be successfully enabled
            for (final Plugin plugin : pluginsInEnableOrder) {
                if (plugin.getPluginState() == PluginState.ENABLED && enableConfiguredPluginModules(plugin)) {
                    broadcastPluginEnabled(plugin);
                }
            }
        });
    }

    private void discardPlugin(final @Nullable PluginLoader loader, final Plugin plugin) {
        if (null == loader) {
            // This happens if we're discarding a plugin during init() due to version restrictions
            candidatePluginsToPluginLoader.get(plugin).discardPlugin(plugin);
        } else if (loader instanceof DiscardablePluginLoader) {
            // This happens if we're discarding a plugin because it would be a downgrade. The loader is one of our
            // loaders from
            // pluginLoaders which was wrapped with PermissionCheckingPluginLoader which is a DiscardablePluginLoader.
            ((DiscardablePluginLoader) loader).discardPlugin(plugin);
        } else {
            // This is only reachable if the caller has supplied their own PluginLoader, since all of ours are wrapped
            // in a
            // PermissionCheckingPluginLoader which implements DiscardablePluginLoader. We ignore the discard, but we
            // log because
            // (in the short term) this is more likely to be a PluginLoader which should really be a
            // DiscardablePluginLoader then
            // one which is legitimately ignoring discardPlugin.
            log.debug(
                    "Ignoring discardPlugin({}, version {}) as delegate is not a DiscardablePluginLoader",
                    plugin.getKey(),
                    plugin.getPluginInformation().getVersion());
        }
    }

    private boolean pluginVersionIsAcceptable(final Plugin plugin, final Map<String, String> minimumPluginVersions) {
        final String pluginKey = plugin.getKey();
        final String rawMinimumVersion = minimumPluginVersions.get(pluginKey);
        if (null == rawMinimumVersion) {
            // fine, no minimum given
            return true;
        }

        final String cleanMinimumVersion = cleanVersionString(rawMinimumVersion);

        // Rather than replicate some of the validation login in VersionStringComparator, let it validate and handle
        // errors
        try {
            // Actually compare versions

            final PluginInformation pluginInformation = plugin.getPluginInformation();
            final String pluginVersion =
                    cleanVersionString((pluginInformation != null) ? pluginInformation.getVersion() : null);
            final VersionStringComparator versionStringComparator = new VersionStringComparator();
            return versionStringComparator.compare(pluginVersion, cleanMinimumVersion) >= 0;
        } catch (IllegalArgumentException e_ia) {
            log.warn(
                    "Cannot compare minimum version '{}' for plugin {}: {}",
                    rawMinimumVersion,
                    plugin,
                    e_ia.getMessage());
            // accept the plugin if something goes wrong
            return true;
        }
    }

    private DependentPlugins disablePluginsAndTheirDependencies(
            Collection<String> pluginKeys, Set<PluginDependencies.Type> dependencyTypes) {
        return disablePlugins(pluginKeys, dependencyTypes, true);
    }

    private DependentPlugins disableOnlyPluginDependencies(
            Collection<String> pluginKeys, Set<PluginDependencies.Type> dependencyTypes) {
        return disablePlugins(pluginKeys, dependencyTypes, false);
    }

    /**
     * Disable all plugins which require, possibly indirectly, a given plugin specified by key.
     * <p>
     * The plugin with given key is not disabled.
     * <p>
     * This prevents a dependent plugin trying to access, indirectly, the felix global lock, which is held by the
     * PackageAdmin while refreshing. See https://ecosystem.atlassian.net/browse/PLUG-582 for details.
     *
     * @param rootPluginKeys   Plugin keys to disable dependents of, and optionally the plugins themselves (see {@code disableRoots})
     * @param dependencyTypes  only disable these types
     * @param disableRoots     if the plugins in {@code rootPluginKeys} should also be disabled, or only their dependents
     * @return dependent plugins plugins that were disabled.
     */
    private DependentPlugins disablePlugins(
            final Collection<String> rootPluginKeys,
            final Set<PluginDependencies.Type> dependencyTypes,
            boolean disableRoots) {
        final DependentPlugins dependentPlugins =
                new DependentPlugins(rootPluginKeys, getEnabledPlugins(), dependencyTypes);
        pluginTransactionContext.wrap(() -> {
            final List<Plugin> pluginsToDisable =
                    disableRoots ? dependentPlugins.getPlugins(true) : dependentPlugins.getPlugins(false);

            if (!pluginsToDisable.isEmpty()) {
                log.info(
                        "To disable plugins '{}', we need to first disable all dependent enabled plugins: {}",
                        rootPluginKeys,
                        dependentPlugins.toStringList());

                for (Plugin p : pluginsToDisable) {
                    broadcastPluginDisabling(p);
                }

                for (Plugin p : pluginsToDisable) {
                    disablePluginWithModuleEvents(p);
                }

                for (Plugin p : pluginsToDisable) {
                    broadcastPluginDisabled(p);
                }
            }
        });
        return dependentPlugins;
    }

    /**
     * Replace an already loaded plugin with another version. Relevant stored
     * configuration for the plugin will be preserved.
     *
     * @param oldPlugin Plugin to replace
     * @param newPlugin New plugin to install
     * @throws PluginException if the plugin cannot be updated
     */
    protected void updatePlugin(final Plugin oldPlugin, final Plugin newPlugin) {
        pluginTransactionContext.wrap(() -> {
            if (!oldPlugin.getKey().equals(newPlugin.getKey())) {
                throw new IllegalArgumentException(
                        "New plugin '" + newPlugin + "' must have the same key as the old plugin '" + oldPlugin + "'");
            }

            if (log.isInfoEnabled()) {
                // We know the keys are the same here, so log the versions (if available)
                final PluginInformation oldInformation = oldPlugin.getPluginInformation();
                final String oldVersion = (oldInformation == null) ? "?" : oldInformation.getVersion();
                final PluginInformation newInformation = newPlugin.getPluginInformation();
                final String newVersion = (newInformation == null) ? "?" : newInformation.getVersion();
                log.info("Updating plugin '{}' from version '{}' to version '{}'", oldPlugin, oldVersion, newVersion);
            }

            // Preserve the old plugin configuration - uninstall changes it (as
            // disable is called on all modules) and then
            // removes it
            final Map<String, PluginEnabledState> oldPluginState =
                    new HashMap<>(getState().getPluginEnabledStateMap(oldPlugin));

            log.debug("Uninstalling old plugin: {}", oldPlugin);
            uninstallNoEvent(oldPlugin);
            log.debug("Plugin uninstalled '{}', preserving old state", oldPlugin);

            // Build a set of module keys from the new plugin version
            final Set<String> newModuleKeys = new HashSet<>();
            newModuleKeys.add(newPlugin.getKey());
            for (final ModuleDescriptor<?> moduleDescriptor : newPlugin.getModuleDescriptors()) {
                newModuleKeys.add(moduleDescriptor.getCompleteKey());
            }

            // for removing any keys from the old plugin state that do not exist in
            // the new version
            Map<String, PluginEnabledState> states = filterKeys(oldPluginState, newModuleKeys::contains);
            persistentStateModifier.addPluginEnabledState(states);
        });
    }

    public Collection<Plugin> getPlugins() {
        return pluginRegistry.getAll();
    }

    /**
     * @see PluginAccessor#getPlugins(Predicate)
     * @since 0.17
     */
    public Collection<Plugin> getPlugins(final Predicate<Plugin> pluginPredicate) {
        return getPlugins().stream()
                .filter(pluginPredicate)
                .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }

    /**
     * @see PluginAccessor#getEnabledPlugins()
     */
    public Collection<Plugin> getEnabledPlugins() {
        return getPlugins(new EnabledPluginPredicate(pluginEnabler.getPluginsBeingEnabled()));
    }

    /**
     * @see PluginAccessor#getModules(Predicate)
     * @since 0.17
     */
    public <M> Collection<M> getModules(final Predicate<ModuleDescriptor<M>> moduleDescriptorPredicate) {
        return getModuleDescriptors(getPlugins(), moduleDescriptorPredicate)
                .map(safeModuleExtractor::getModule)
                .filter(Objects::nonNull)
                .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }

    /**
     * @see PluginAccessor#getModuleDescriptors(Predicate)
     * @since 0.17
     */
    public <M> Collection<ModuleDescriptor<M>> getModuleDescriptors(
            final Predicate<ModuleDescriptor<M>> moduleDescriptorPredicate) {
        return getModuleDescriptors(getPlugins(), moduleDescriptorPredicate)
                .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }

    /**
     * Get the all the module descriptors from the given collection of plugins,
     * filtered by the predicate.
     * <p>
     * Be careful, your predicate must filter ModuleDescriptors that are not M,
     * this method does not guarantee that the descriptors are of the correct
     * type by itself.
     *
     * @param plugins a collection of {@link Plugin}s
     * @return a collection of {@link ModuleDescriptor descriptors}
     */
    private <M> Stream<ModuleDescriptor<M>> getModuleDescriptors(
            final Collection<Plugin> plugins, final Predicate<ModuleDescriptor<M>> predicate) {
        //noinspection unchecked
        return plugins.stream()
                .flatMap(plugin -> plugin.getModuleDescriptors().stream())
                .map(descriptor -> (ModuleDescriptor<M>) descriptor)
                .filter(predicate);
    }

    public Plugin getPlugin(final String key) {
        return pluginRegistry.get(notNull("The plugin key must be specified", key));
    }

    public Plugin getEnabledPlugin(final String pluginKey) {
        if (!isPluginEnabled(pluginKey)) {
            return null;
        }
        return getPlugin(pluginKey);
    }

    public ModuleDescriptor<?> getPluginModule(final String completeKey) {
        return getPluginModule(new ModuleCompleteKey(completeKey));
    }

    private ModuleDescriptor<?> getPluginModule(final ModuleCompleteKey key) {
        final Plugin plugin = getPlugin(key.getPluginKey());
        if (plugin == null) {
            return null;
        }
        return plugin.getModuleDescriptor(key.getModuleKey());
    }

    public ModuleDescriptor<?> getEnabledPluginModule(final String completeKey) {
        final ModuleCompleteKey key = new ModuleCompleteKey(completeKey);

        // If it's disabled, return null
        if (!isPluginModuleEnabled(key)) {
            return null;
        }

        return getEnabledPlugin(key.getPluginKey()).getModuleDescriptor(key.getModuleKey());
    }

    /**
     * @see PluginAccessor#getEnabledModulesByClass(Class)
     */
    public <M> List<M> getEnabledModulesByClass(final Class<M> moduleClass) {
        return getEnabledModuleDescriptorsByModuleClass(moduleClass)
                .map(safeModuleExtractor::getModule)
                .filter(Objects::nonNull)
                .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }

    /**
     * Get all module descriptor that are enabled and for which the module is an
     * instance of the given class.
     *
     * @param moduleClass the class of the module within the module descriptor.
     * @return a collection of {@link ModuleDescriptor}s
     */
    private <M> Stream<ModuleDescriptor<M>> getEnabledModuleDescriptorsByModuleClass(final Class<M> moduleClass) {
        final ModuleOfClassPredicate<M> ofType = new ModuleOfClassPredicate<>(moduleClass);
        final EnabledModulePredicate enabled = new EnabledModulePredicate();
        return getModuleDescriptors(getEnabledPlugins(), ofType.and(enabled));
    }

    /**
     * This method has been reverted to pre PLUG-40 to fix performance issues
     * that were encountered during load testing. This should be reverted to the
     * state it was in at 54639 when the fundamental issue leading to this
     * slowdown has been corrected (that is, slowness of PluginClassLoader).
     *
     * @see PluginAccessor#getEnabledModuleDescriptorsByClass(Class)
     */
    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz) {
        // TODO Avoid moving elements into a List until we call something other than iterator()
        // Most of the time getEnabledModuleDescriptorsByClass is called within a foreach loop so would only call
        // iterator()
        return getEnabledPlugins().stream()
                .flatMap(plugin -> plugin.getModuleDescriptors().stream())
                .filter(descriptorClazz::isInstance)
                .filter(new EnabledModulePredicate())
                .map(descriptorClazz::cast)
                .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }

    /**
     * Enable a set of plugins by key. This will implicitly and recursively
     * enable all dependent plugins.
     *
     * @param keys The plugin keys. Must not be null.
     * @since 2.5.0
     */
    @Override
    public void enablePlugins(final String... keys) {
        pluginTransactionContext.wrap(() -> {
            final Collection<Plugin> pluginsToEnable = new ArrayList<>(keys.length);

            for (final String key : keys) {
                if (key == null) {
                    throw new IllegalArgumentException("Keys passed to enablePlugins must be non-null");
                }

                final Plugin plugin = pluginRegistry.get(key);
                if (plugin == null) {
                    final Plugin delayedPlugin = findDelayedPlugin(key);
                    if (delayedPlugin == null) {
                        log.info("No plugin was found for key '{}'. Not enabling.", key);
                    } else {
                        persistentStateModifier.enable(delayedPlugin);
                    }
                    continue;
                }

                if (!plugin.getPluginInformation().satisfiesMinJavaVersion()) {
                    log.error(
                            "Minimum Java version of '{}' was not satisfied for module '{}'. Not enabling.",
                            plugin.getPluginInformation().getMinJavaVersion(),
                            key);
                    continue;
                }

                // Do not enable if already enabled, which in turn will prevent the notify
                if (plugin.getPluginState() != PluginState.ENABLED) {
                    pluginsToEnable.add(plugin);
                }
            }
            for (final Plugin plugin : pluginsToEnable) {
                broadcastIgnoreError(new PluginEnablingEvent(plugin));
            }

            final Collection<Plugin> enabledPlugins = pluginEnabler.enableAllRecursively(pluginsToEnable);

            for (final Plugin plugin : enabledPlugins) {
                persistentStateModifier.enable(plugin);
                if (enableConfiguredPluginModules(plugin)) {
                    broadcastPluginEnabled(plugin);
                }
            }
        });
    }

    /**
     * For each module in the plugin, call the module descriptor's enabled()
     * method if the module is StateAware and enabled.
     * <p>
     * If any modules fail to enable then the plugin is replaced by an
     * UnloadablePlugin, and this method will return {@code false}.
     *
     * @param plugin the plugin to enable
     * @return true if the modules were all enabled correctly, false otherwise.
     */
    private boolean enableConfiguredPluginModules(final Plugin plugin) {
        return pluginTransactionContext.wrap(() -> {
            final Set<ModuleDescriptor<?>> enabledDescriptors = new HashSet<>();
            for (final ModuleDescriptor<?> descriptor : plugin.getModuleDescriptors()) {
                if (!enableConfiguredPluginModule(plugin, descriptor, enabledDescriptors)) {
                    return false;
                }
            }
            return true;
        });
    }

    private boolean enableConfiguredPluginModule(
            final Plugin plugin,
            final ModuleDescriptor<?> descriptor,
            final Set<ModuleDescriptor<?>> enabledDescriptors) {
        return pluginTransactionContext.wrap(() -> {
            try {
                // This can happen if the plugin available event is fired as part of the plugin initialization process
                if (pluginEnabler.isPluginBeingEnabled(plugin)) {
                    log.debug(
                            "The plugin is currently being enabled, so we won't bother trying to enable the '{}' module",
                            descriptor.getKey());
                    return true;
                }

                // We only want to re-enable modules that weren't explicitly
                // disabled by the user.
                if (!isPluginEnabled(descriptor.getPluginKey()) || !getState().isEnabled(descriptor)) {
                    log.debug(
                            "Plugin module '{}' is explicitly disabled (or so by default), so not re-enabling.",
                            descriptor.getDisplayName());
                    return true;
                }
                notifyModuleEnabled(descriptor);
                enabledDescriptors.add(descriptor);

                return true;
            } catch (final Throwable enableException) {
                // catch any errors and insert an UnloadablePlugin (PLUG-7)
                log.error(
                        "There was an error loading the descriptor '{}' of plugin '{}'. Disabling.",
                        descriptor.getDisplayName(),
                        plugin,
                        enableException);

                // Disable all previously enabled descriptors
                for (final ModuleDescriptor<?> descriptorToDisable : enabledDescriptors) {
                    try {
                        notifyModuleDisabled(descriptorToDisable);
                    } catch (final Exception disableException) {
                        log.error(
                                "Could not notify previously enabled descriptor {} of module disabled in plugin {}",
                                descriptorToDisable.getDisplayName(),
                                plugin,
                                disableException);
                    }
                }

                // use the original exception
                replacePluginWithUnloadablePlugin(plugin, descriptor, enableException);
                return false;
            }
        });
    }

    @Override
    public void disablePlugin(final String key) {
        pluginTransactionContext.wrap(() -> {
            if (isPluginEnabled(key)) {
                disablePluginInternal(key, true);
            } else {
                log.debug("Plugin {} already disabled", key);
            }
        });
    }

    @Override
    public void disablePluginWithoutPersisting(final String key) {
        pluginTransactionContext.wrap(() -> disablePluginInternal(key, false));
    }

    protected void disablePluginInternal(final String key, final boolean persistDisabledState) {
        if (key == null) {
            throw new IllegalArgumentException("You must specify a plugin key to disable.");
        }
        pluginTransactionContext.wrap(() -> {
            final Plugin plugin = pluginRegistry.get(key);
            if (plugin == null) {
                final Plugin delayedPlugin = findDelayedPlugin(key);
                if (delayedPlugin == null) {
                    log.info("No plugin was found for key '{}'. Not disabling.", key);
                } else {
                    if (persistDisabledState) {
                        persistentStateModifier.disable(delayedPlugin);
                    }
                }
                return;
            }
            if (safeModeManager.isRequiredPlugin(plugin)) {
                log.warn("Trying to disable bundled plugin: {}, skipping.", plugin.getName());
                return;
            }
            // Do not disable if the plugin is already disabled
            if (plugin.getPluginState() != PluginState.DISABLED) {
                final DependentPlugins disabledPlugins = disablePluginsAndTheirDependencies(
                        singletonList(plugin.getKey()), unmodifiableSet(new HashSet<>(asList(MANDATORY, OPTIONAL))));

                if (persistDisabledState) {
                    disabledPlugins
                            .getPluginsByTypes(singleton(MANDATORY), true)
                            .forEach(persistentStateModifier::disable);
                }

                reenableDependent(singletonList(plugin), disabledPlugins, PluginState.DISABLED);
            }
        });
    }

    // re-enable plugins dependent on a plugin depending on the final state
    // - for uninstalled enable optional and dynamic
    // - for disabled enable only optional
    private void reenableDependent(
            final Collection<Plugin> plugins, final DependentPlugins disabledPlugins, final PluginState state) {
        pluginTransactionContext.wrap(() -> {
            final Set<PluginDependencies.Type> cycledTypes = EnumSet.of(OPTIONAL);
            if (state == PluginState.UNINSTALLED) {
                cycledTypes.add(DYNAMIC);
            } else if (state != PluginState.DISABLED) {
                throw new IllegalArgumentException("State must be one of (UNINSTALLED,DISABLED)");
            }

            final List<Plugin> cycled = disabledPlugins.getPluginsByTypes(cycledTypes, false);
            if (!cycled.isEmpty()) {
                log.info(
                        "Found optional/dynamic dependent plugins to re-enable after plugins {} '{}': {}. Enabling...",
                        state,
                        plugins,
                        disabledPlugins.toStringList(cycledTypes));
                enableDependentPlugins(cycled);
            }

            final List<Plugin> disabled = disabledPlugins.getPluginsByTypes(singleton(MANDATORY), false);
            if (!disabled.isEmpty() || !cycled.isEmpty()) {
                // extra crap here//
                plugins.forEach(
                        p -> broadcastIgnoreError(new PluginDependentsChangedEvent(p, state, disabled, cycled)));
            }
        });
    }

    private Plugin findDelayedPlugin(final String key) {
        return delayedPlugins.stream()
                .filter(plugin -> plugin.getKey().equals(key))
                .findFirst()
                .orElse(null);
    }

    /**
     * Disable a plugin without broadcasting PluginDisabl(ing|ed) events
     * <p>
     * It is expected that {@link #broadcastPluginDisabling(Plugin)} will be called for the plugin before
     * and {@link #broadcastPluginDisabled(Plugin)} will be called after the method call
     * <p>
     * Note: all plugin's modules will be disabled in this method and corresponding PluginModule* events
     * will still be broadcasted
     *
     * @param plugin the plugin to disable
     */
    private void disablePluginWithModuleEvents(final Plugin plugin) {
        if (plugin.getPluginState() == PluginState.DISABLED) {
            return;
        }

        disablePluginModules(plugin);

        // This needs to happen after modules are disabled to prevent errors
        plugin.disable();
    }

    private void broadcastPluginDisabling(final Plugin plugin) {
        pluginTransactionContext.wrap(() -> {
            log.info("Disabling {}", plugin);
            broadcastIgnoreError(new PluginDisablingEvent(plugin));
        });
    }

    private void broadcastPluginDisabled(final Plugin plugin) {
        pluginTransactionContext.wrap(() -> {
            broadcastIgnoreError(new PluginDisabledEvent(plugin));
            PluginStateChangeCountEmitter.emitPluginDisabledCounter();
        });
    }

    private void broadcastPluginEnabled(final Plugin plugin) {
        pluginTransactionContext.wrap(() -> {
            broadcastIgnoreError(new PluginEnabledEvent(plugin));
            PluginStateChangeCountEmitter.emitPluginEnabledCounter();
        });
    }

    /**
     * @param plugin the plugin being disabled.
     */
    private void notifyPluginDisabled(final Plugin plugin) {
        pluginTransactionContext.wrap(() -> {
            broadcastPluginDisabling(plugin);

            disablePluginWithModuleEvents(plugin);

            broadcastPluginDisabled(plugin);
        });
    }

    private void disablePluginModules(final Plugin plugin) {
        pluginTransactionContext.wrap(() -> {
            final List<ModuleDescriptor<?>> moduleDescriptors = new ArrayList<>(plugin.getModuleDescriptors());
            reverse(moduleDescriptors); // disable in reverse order

            for (final ModuleDescriptor<?> module : moduleDescriptors) {
                // don't actually disable the module, just fire the events because
                // its plugin is being disabled
                // if the module was actually disabled, you'd have to reenable each
                // one when enabling the plugin

                disablePluginModuleNoPersist(module);
            }
        });
    }

    private void disablePluginModuleNoPersist(final ModuleDescriptor<?> module) {
        if (isPluginModuleEnabled(module.getCompleteKey())) {
            publishModuleDisabledEvents(module, false);
        }
    }

    @Override
    public void disablePluginModule(final String completeKey) {
        pluginTransactionContext.wrap(() -> {
            if (completeKey == null) {
                throw new IllegalArgumentException("You must specify a plugin module key to disable.");
            }

            final ModuleDescriptor<?> module = getPluginModule(completeKey);

            if (module == null) {
                log.info("Returned module for key '{}' was null. Not disabling.", completeKey);
                return;
            }
            if (module.getClass().isAnnotationPresent(CannotDisable.class)) {
                log.info(
                        "Plugin module '{}' cannot be disabled; it is annotated with {}",
                        completeKey,
                        CannotDisable.class.getName());
                return;
            }
            persistentStateModifier.disable(module);
            notifyModuleDisabled(module);
        });
    }

    protected void notifyModuleDisabled(final ModuleDescriptor<?> module) {
        publishModuleDisabledEvents(module, true);
    }

    private void publishModuleDisabledEvents(final ModuleDescriptor<?> module, final boolean persistent) {
        pluginTransactionContext.wrap(() -> {
            log.debug("Disabling {}", module.getKey());
            broadcastIgnoreError(new PluginModuleDisablingEvent(module, persistent));

            if (module instanceof StateAware) {
                ((StateAware) module).disabled();
            }

            broadcastIgnoreError(new PluginModuleDisabledEvent(module, persistent));
        });
    }

    @Override
    public void enablePluginModule(final String completeKey) {
        pluginTransactionContext.wrap(() -> {
            if (completeKey == null) {
                throw new IllegalArgumentException("You must specify a plugin module key to disable.");
            }

            final ModuleDescriptor<?> module = getPluginModule(completeKey);

            if (module == null) {
                log.info("Returned module for key '{}' was null. Not enabling.", completeKey);
                return;
            }

            if (!module.satisfiesMinJavaVersion()) {
                log.error(
                        "Minimum Java version of '{}' was not satisfied for module '{}'. Not enabling.",
                        module.getMinJavaVersion(),
                        completeKey);
                return;
            }

            persistentStateModifier.enable(module);
            notifyModuleEnabled(module);
        });
    }

    protected void notifyModuleEnabled(final ModuleDescriptor<?> module) {
        pluginTransactionContext.wrap(() -> {
            log.debug("Enabling {}", module.getKey());
            broadcastIgnoreError(new PluginModuleEnablingEvent(module));
            if (module instanceof StateAware) {
                ((StateAware) module).enabled();
            }
            broadcastIgnoreError(new PluginModuleEnabledEvent(module));
        });
    }

    public boolean isPluginModuleEnabled(final String completeKey) {
        // completeKey may be null
        return (completeKey != null) && isPluginModuleEnabled(new ModuleCompleteKey(completeKey));
    }

    private boolean isPluginModuleEnabled(final ModuleCompleteKey key) {
        if (!isPluginEnabled(key.getPluginKey())) {
            return false;
        }
        final ModuleDescriptor<?> pluginModule = getPluginModule(key);
        return (pluginModule != null) && pluginModule.isEnabled();
    }

    /**
     * This method checks to see if the plugin is enabled based on the state
     * manager and the plugin.
     *
     * @param key The plugin key
     * @return True if the plugin is enabled
     */
    public boolean isPluginEnabled(final String key) {
        final Plugin plugin = pluginRegistry.get(notNull("The plugin key must be specified", key));

        return plugin != null && plugin.getPluginState() == PluginState.ENABLED;
    }

    public InputStream getDynamicResourceAsStream(final String name) {
        return getClassLoader().getResourceAsStream(name);
    }

    public Class<?> getDynamicPluginClass(final String className) throws ClassNotFoundException {
        return getClassLoader().loadClass(className);
    }

    public PluginsClassLoader getClassLoader() {
        return classLoader;
    }

    /**
     * Disables and replaces a plugin currently loaded with an UnloadablePlugin.
     *
     * @param plugin     the plugin to be replaced
     * @param descriptor the descriptor which caused the problem
     * @param throwable  the problem caught when enabling the descriptor
     */
    private void replacePluginWithUnloadablePlugin(
            final Plugin plugin, final ModuleDescriptor<?> descriptor, final Throwable throwable) {
        final UnloadableModuleDescriptor unloadableDescriptor =
                UnloadableModuleDescriptorFactory.createUnloadableModuleDescriptor(plugin, descriptor, throwable);
        final UnloadablePlugin unloadablePlugin =
                UnloadablePluginFactory.createUnloadablePlugin(plugin, unloadableDescriptor);

        // Add the error text at the plugin level as well. This is useful for logging.
        unloadablePlugin.setErrorText(unloadableDescriptor.getErrorText());
        pluginRegistry.put(unloadablePlugin);
    }

    public boolean isSystemPlugin(final String key) {
        final Plugin plugin = getPlugin(key);
        return (plugin != null) && plugin.isSystemPlugin();
    }

    public PluginRestartState getPluginRestartState(final String key) {
        return getState().getPluginRestartState(key);
    }

    private void broadcastIgnoreError(final Object event) {
        try {
            pluginEventManager.broadcast(event);
            pluginTransactionContext.addEvent(event);
        } catch (final NotificationException ex) {
            log.warn("Error broadcasting '{}'. Continuing anyway.", event, ex);
            for (Throwable throwable : ex.getAllCauses()) {
                log.debug("Cause:", throwable);
            }
        }
    }

    @Override
    public ModuleDescriptor<?> addDynamicModule(final Plugin maybePluginInternal, final Element module) {
        final AtomicReference<ModuleDescriptor> moduleDescriptorRef = new AtomicReference<>();
        pluginTransactionContext.wrap(() -> {
            final PluginInternal plugin = checkPluginInternal(maybePluginInternal);

            // identify the loader that loaded this plugin
            final PluginLoader pluginLoader = installedPluginsToPluginLoader.get(plugin);
            if (pluginLoader == null) {
                throw new PluginException("cannot locate PluginLoader that created plugin '" + plugin + "'");
            }

            // attempt to create the module
            final ModuleDescriptor moduleDescriptor =
                    pluginLoader.createModule(plugin, module, moduleDescriptorFactory);
            moduleDescriptorRef.set(moduleDescriptor);
            if (moduleDescriptor == null) {
                throw new PluginException("cannot add dynamic module of type '" + module.getName() + "' to plugin '"
                        + plugin + "' as the PluginLoader does not know how to create the module");
            }

            // check for duplicate module keys within the plugin; this is a safety-check as it should have been verified
            // during module init
            if (plugin.getModuleDescriptor(moduleDescriptor.getKey()) != null) {
                throw new PluginException(
                        "duplicate module key '" + moduleDescriptor.getKey() + "' for plugin '" + plugin + "'");
            }

            // add to the plugin
            if (!plugin.addDynamicModuleDescriptor(moduleDescriptor)) {
                throw new PluginException("cannot add dynamic module '" + moduleDescriptor.getKey() + "' to plugin '"
                        + plugin + "' as it is already present");
            }

            // enable the module only if the plugin is enabled and the module wasn't explicitly disabled by the user
            if (plugin.getPluginState() == PluginState.ENABLED && getState().isEnabled(moduleDescriptor)) {
                notifyModuleEnabled(moduleDescriptor);
            }
        });
        return moduleDescriptorRef.get();
    }

    @Override
    public Iterable<ModuleDescriptor<?>> getDynamicModules(final Plugin maybePluginInternal) {
        final PluginInternal plugin = checkPluginInternal(maybePluginInternal);

        return plugin.getDynamicModuleDescriptors();
    }

    @Override
    public void removeDynamicModule(final Plugin maybePluginInternal, final ModuleDescriptor<?> module) {
        pluginTransactionContext.wrap(() -> {
            final PluginInternal plugin = checkPluginInternal(maybePluginInternal);

            // remove from the plugin
            if (!plugin.removeDynamicModuleDescriptor(module)) {
                throw new PluginException("cannot remove dynamic module '" + module.getKey() + "' from plugin '"
                        + plugin + "' as it wasn't added by addDynamicModule");
            }

            // disable it
            persistentStateModifier.disable(module);
            notifyModuleDisabled(module);

            // destroy it
            module.destroy();
        });
    }

    @VisibleForTesting
    PluginInternal checkPluginInternal(final Plugin maybePluginInternal) {
        // check the type
        if (!(maybePluginInternal instanceof PluginInternal)) {
            throw new IllegalArgumentException(
                    maybePluginInternal + " does not implement com.atlassian.plugin.PluginInternal it is a "
                            + maybePluginInternal.getClass().getCanonicalName());
        }

        return (PluginInternal) maybePluginInternal;
    }
}
