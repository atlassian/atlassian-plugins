package com.atlassian.plugin.manager;

import java.util.Map;

import io.atlassian.fugue.Effect;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRestartState;

import static com.atlassian.plugin.manager.DefaultPluginPersistentState.getPluginEnabledStateMap;

public class PluginPersistentStateModifier {
    private final PluginPersistentStateStore store;

    public PluginPersistentStateModifier(final PluginPersistentStateStore store) {
        this.store = store;
    }

    public PluginPersistentState getState() {
        return store.load();
    }

    public synchronized void apply(Effect<PluginPersistentState.Builder> effect) {
        PluginPersistentState.Builder builder = PluginPersistentState.Builder.create(store.load());
        effect.apply(builder);
        store.save(builder.toState());
    }

    public void setEnabled(final Plugin plugin, final boolean enabled) {
        apply(builder -> builder.setEnabled(plugin, enabled));
    }

    public void disable(final Plugin plugin) {
        setEnabled(plugin, false);
    }

    public void enable(final Plugin plugin) {
        setEnabled(plugin, true);
    }

    public void setEnabled(final ModuleDescriptor<?> module, final boolean enabled) {
        apply(builder -> builder.setEnabled(module, enabled));
    }

    public void disable(final ModuleDescriptor<?> module) {
        setEnabled(module, false);
    }

    public void enable(final ModuleDescriptor<?> module) {
        setEnabled(module, true);
    }

    public void clearPluginRestartState() {
        apply(PluginPersistentState.Builder::clearPluginRestartState);
    }

    public void setPluginRestartState(final String pluginKey, final PluginRestartState pluginRestartState) {
        apply(builder -> builder.setPluginRestartState(pluginKey, pluginRestartState));
    }

    /**
     * @deprecated in 4.5.0 for removal in 6.0. Use {@link #addPluginEnabledState(Map)} instead
     */
    @Deprecated
    public void addState(final Map<String, Boolean> state) {
        apply(builder -> builder.addPluginEnabledState(getPluginEnabledStateMap(state)));
    }

    public void addPluginEnabledState(final Map<String, PluginEnabledState> state) {
        apply(builder -> builder.addPluginEnabledState(state));
    }

    public void removeState(final Plugin plugin) {
        apply(builder -> {
            builder.removeState(plugin.getKey());
            for (final ModuleDescriptor<?> moduleDescriptor : plugin.getModuleDescriptors()) {
                builder.removeState(moduleDescriptor.getCompleteKey());
            }
        });
    }
}
