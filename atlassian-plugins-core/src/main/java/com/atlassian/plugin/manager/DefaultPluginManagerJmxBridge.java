package com.atlassian.plugin.manager;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.jmx.AbstractJmxBridge;
import com.atlassian.plugin.jmx.JmxUtil;
import com.atlassian.plugin.jmx.PluginManagerMXBean;

/**
 * Expose a {@link DefaultPluginManager} as a JMX {@link PluginManagerMXBean}.
 * <p>
 * This implementation has concurrency issues, because it traverses unprotected data structures. It does not update them (except
 * where such problems already exist, such as in {@link DefaultPluginManager#scanForNewPlugins()}, so the likely fall out here is
 * simply a failed request. We should address this when we have a firm plan to managing asynchrony in the plugin system generally.
 * Replacing the collections here with snapshots is really just going to shrink the race window a little and not close it, and
 * doesn't feel worth the effort unless it causes problems in expected usage.
 *
 * @since v3.0.24
 */
class DefaultPluginManagerJmxBridge extends AbstractJmxBridge<PluginManagerMXBean> implements PluginManagerMXBean {
    /**
     * Counter for uniqueifying jmx object names, since we have no uniqueness in our scope to fall back on.
     */
    private static final AtomicInteger nextJmxInstance = new AtomicInteger();

    private final DefaultPluginManager defaultPluginManager;

    DefaultPluginManagerJmxBridge(final DefaultPluginManager defaultPluginManager) {
        super(JmxUtil.objectName(nextJmxInstance, "PluginManager"), PluginManagerMXBean.class);
        this.defaultPluginManager = defaultPluginManager;
    }

    @Override
    protected PluginManagerMXBean getMXBean() {
        return this;
    }

    @Override
    public PluginData[] getPlugins() {
        return defaultPluginManager.getPlugins().stream()
                .map(plugin -> new PluginData() {
                    @Override
                    public String getKey() {
                        return plugin.getKey();
                    }

                    @Override
                    public String getVersion() {
                        final PluginInformation pluginInformation = plugin.getPluginInformation();
                        // It's unclear whether getPluginInformation can return null, but it is handled in a few places
                        // (such as
                        // version comparison code), so i'm being defensive about it here.
                        return (null == pluginInformation) ? null : pluginInformation.getVersion();
                    }

                    @Override
                    public String getLocation() {
                        final PluginArtifact pluginArtifact = plugin.getPluginArtifact();

                        return (null == pluginArtifact)
                                ? null
                                : pluginArtifact.toFile().getAbsolutePath();
                    }

                    @Override
                    public Long getDateLoaded() {
                        final Date dateLoaded = plugin.getDateLoaded();

                        return (null == dateLoaded) ? null : dateLoaded.getTime();
                    }

                    @Override
                    public Long getDateInstalled() {
                        final Date dateInstalled = plugin.getDateInstalled();

                        return (null == dateInstalled) ? null : dateInstalled.getTime();
                    }

                    @Override
                    public boolean isEnabled() {
                        return defaultPluginManager.isPluginEnabled(plugin.getKey());
                    }

                    @Override
                    public boolean isEnabledByDefault() {
                        return plugin.isEnabledByDefault();
                    }

                    @Override
                    public boolean isBundledPlugin() {
                        return plugin.isBundledPlugin();
                    }
                })
                .toArray(PluginData[]::new);
    }

    @Override
    public int scanForNewPlugins() {
        return defaultPluginManager.scanForNewPlugins();
    }
}
