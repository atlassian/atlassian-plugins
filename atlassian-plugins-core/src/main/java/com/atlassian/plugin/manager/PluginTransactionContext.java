package com.atlassian.plugin.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;

import com.atlassian.plugin.event.NotificationException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginTransactionEndEvent;
import com.atlassian.plugin.event.events.PluginTransactionStartEvent;

/**
 * Utility for wrapping an action triggering sending plugin events with {@link PluginTransactionStartEvent} and
 * {@link PluginTransactionEndEvent}. A "plugin transaction" exists in the context of a single thread.
 * See {@link DefaultPluginManager}
 */
public class PluginTransactionContext {

    private static final Logger log = LoggerFactory.getLogger(PluginTransactionContext.class);

    private static ThreadLocal<AtomicInteger> level = ThreadLocal.withInitial(() -> new AtomicInteger(0));
    private static ThreadLocal<List<Object>> events = ThreadLocal.withInitial(ArrayList::new);

    private final PluginEventManager pluginEventManager;

    public PluginTransactionContext(final PluginEventManager pluginEventManager) {
        this.pluginEventManager = pluginEventManager;
    }

    void start() {
        if (level.get().getAndIncrement() == 0) {
            if (log.isTraceEnabled()) {
                log.trace("Starting plugin event transaction.", new Throwable());
            }
            broadcastIgnoreError(new PluginTransactionStartEvent());
        }
    }

    public void addEvent(final Object event) {
        if (level.get().get() > 0) {
            events.get().add(event);
        }
    }

    void stop() {
        if (level.get().decrementAndGet() == 0) {
            if (log.isTraceEnabled()) {
                log.trace("Stopping plugin event transaction.", new Throwable());
            }
            final List<Object> eventsInTransaction = events.get();
            events.remove();
            broadcastIgnoreError(new PluginTransactionEndEvent(eventsInTransaction));
        }
    }

    public void wrap(final Runnable runnable) {
        start();
        try {
            runnable.run();
        } finally {
            stop();
        }
    }

    public <T> T wrap(final Supplier<T> supplier) {
        start();
        try {
            return supplier.get();
        } finally {
            stop();
        }
    }

    private void broadcastIgnoreError(final Object event) {
        try {
            pluginEventManager.broadcast(event);
        } catch (final NotificationException e) {
            log.warn("Error broadcasting '{}': {}. Continuing anyway.", event, e, e);
        }
    }

    int getLevel() {
        return level.get().get();
    }

    List<Object> getEvents() {
        return ImmutableList.copyOf(events.get());
    }
}
