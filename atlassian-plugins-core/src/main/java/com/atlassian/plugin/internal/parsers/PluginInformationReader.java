package com.atlassian.plugin.internal.parsers;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Element;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.Plugin;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableSet.copyOf;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

import static com.atlassian.plugin.internal.parsers.PluginDescriptorReader.elements;

/**
 * Reads plugin information from a plugin descriptor.
 *
 * @see PluginDescriptorReader#getPluginInformationReader()
 * @since 3.0.0
 */
public final class PluginInformationReader {
    static final String PLUGIN_INFO = "plugin-info";
    /**
     * The default scan-folder
     */
    static final String DEFAULT_SCAN_FOLDER = "META-INF/atlassian";

    private final Element pluginInfo;
    private final Set<Application> applications;
    private final int pluginsVersion;

    PluginInformationReader(Element pluginInfo, Set<Application> applications, int pluginsVersion) {
        this.pluginsVersion = pluginsVersion;
        this.pluginInfo = pluginInfo;
        this.applications = copyOf(checkNotNull(applications));
    }

    public Optional<String> getDescription() {
        return getDescriptionElement().map(Element::getTextTrim);
    }

    public Optional<String> getDescriptionKey() {
        return getDescriptionElement().map(description -> description.attributeValue("key"));
    }

    public Optional<String> getVersion() {
        return childElement("version").map(Element::getTextTrim);
    }

    private Optional<Element> childElement(final String name) {
        return pluginInfo == null ? empty() : ofNullable(pluginInfo.element(name));
    }

    private Stream<Element> childElements(final String name) {
        return pluginInfo == null ? Stream.empty() : pluginInfo.elements(name).stream();
    }

    public Optional<String> getVendorName() {
        return getVendorElement().map(vendor -> vendor.attributeValue("name"));
    }

    public Optional<String> getVendorUrl() {
        return getVendorElement().map(vendor -> vendor.attributeValue("url"));
    }

    /**
     * @deprecated in 5.0 for removal in 6.0 when support for scopes is removed
     */
    @Deprecated
    public Optional<String> getScopeKey() {
        final Optional<String> scopeKey = getScopeElement().map(el -> el.attributeValue("key"));

        checkArgument(!scopeKey.map(String::isEmpty).orElse(false), "Value of scope key can't be blank");
        return scopeKey;
    }

    public Map<String, String> getParameters() {
        return Collections.unmodifiableMap(getParamElements()
                .collect(Collectors.toMap(
                        param -> param.attribute("name").getData().toString(), Element::getText)));
    }

    public Optional<Float> getMinVersion() {
        return getApplicationVersionElement()
                .flatMap(new GetAttributeFunction("min"))
                .map(new ParseAttributeValueAsFloatFunction());
    }

    public Optional<Float> getMaxVersion() {
        return getApplicationVersionElement()
                .flatMap(new GetAttributeFunction("max"))
                .map(new ParseAttributeValueAsFloatFunction());
    }

    public Optional<Float> getMinJavaVersion() {
        return childElement("java-version")
                .flatMap(new GetAttributeFunction("min"))
                .map(new ParseAttributeValueAsFloatFunction());
    }

    public Map<String, Optional<String>> getPermissions() {
        return Collections.unmodifiableMap(getPermissionElements()
                .collect(Collectors.toMap(
                        Element::getTextTrim, perm -> ofNullable(perm.attributeValue("installation-mode")))));
    }

    public boolean hasAllPermissions() {
        return getPermissions().isEmpty() && pluginsVersion < Plugin.VERSION_3;
    }

    public Set<String> getPermissions(final InstallationMode installationMode) {
        return copyOf(Maps.filterValues(getPermissions(), permInstallMode -> permInstallMode
                        .flatMap(InstallationMode::of)
                        .map(installMode -> installMode.equals(installationMode))
                        .orElse(true))
                .keySet());
    }

    public Optional<String> getStartup() {
        return childElement("startup").map(Element::getTextTrim);
    }

    public Iterable<String> getModuleScanFolders() {
        final Set<String> scanFolders = Sets.newLinkedHashSet();
        return childElement("scan-modules")
                .map((Function<Element, Iterable<Element>>) scanModules -> {
                    List<Element> elements = elements(scanModules, "folder");
                    if (elements.isEmpty()) {
                        scanFolders.add(DEFAULT_SCAN_FOLDER);
                    }
                    return elements;
                })
                .map((Function<Iterable<Element>, Iterable<String>>) folders -> {
                    for (Element folder : folders) {
                        scanFolders.add(folder.getTextTrim());
                    }
                    return scanFolders;
                })
                .orElseGet(Collections::emptyList);
    }

    private Stream<Element> getPermissionElements() {
        return streamOptional(childElement("permissions"))
                .flatMap(permissions -> elements(permissions, "permission").stream())
                .filter(new ElementWithForApplicationsPredicate(applications))
                .filter(element -> StringUtils.isNotBlank(element.getTextTrim()));
    }

    private Optional<Element> getApplicationVersionElement() {
        return childElement("application-version");
    }

    private Stream<Element> getParamElements() {
        return childElements("param").filter(param -> param.attribute("name") != null);
    }

    private Optional<Element> getVendorElement() {
        return childElement("vendor");
    }

    private Optional<Element> getScopeElement() {
        return childElement("scope");
    }

    private Optional<Element> getDescriptionElement() {
        return childElement("description");
    }

    private static <T> Stream<T> streamOptional(Optional<T> value) {
        return value.map(Stream::of).orElseGet(Stream::empty);
    }

    private static final class ParseAttributeValueAsFloatFunction implements Function<Attribute, Float> {
        @Override
        public Float apply(Attribute attr) {
            return Float.parseFloat(attr.getValue());
        }
    }

    private static final class GetAttributeFunction implements Function<Element, Optional<Attribute>> {
        private final String name;

        private GetAttributeFunction(String name) {
            this.name = name;
        }

        @Override
        public Optional<Attribute> apply(Element applicationVersion) {
            return ofNullable(applicationVersion.attribute(name));
        }
    }

    private static final class ElementWithForApplicationsPredicate implements Predicate<Element> {
        private final Set<Application> applications;

        private ElementWithForApplicationsPredicate(Set<Application> applications) {
            this.applications = checkNotNull(applications);
        }

        @Override
        public boolean test(final Element el) {
            final String appName = el.attributeValue("application");
            return appName == null
                    || applications.stream().anyMatch(app -> app != null && appName.equals(app.getKey()));
        }
    }
}
