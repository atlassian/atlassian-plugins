package com.atlassian.plugin.internal.parsers;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Namespace;
import org.dom4j.VisitorSupport;
import org.dom4j.tree.DefaultElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor;
import com.atlassian.plugin.module.Element;

import static com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptorFactory.createUnrecognisedModuleDescriptor;

/**
 * @since 3.0.0
 */
public final class XmlDescriptorParserUtils {
    private static final Logger log = LoggerFactory.getLogger(XmlDescriptorParserUtils.class);

    public static Document removeAllNamespaces(Document doc) {
        doc.accept(new NamespaceCleaner());
        return doc;
    }

    public static ModuleDescriptor<?> addModule(
            final ModuleDescriptorFactory moduleDescriptorFactory, final Plugin plugin, final Element module) {
        // create a new module descriptor
        final ModuleDescriptor<?> moduleDescriptor = newModuleDescriptor(plugin, module, moduleDescriptorFactory);

        // load it and add to the plugin
        moduleDescriptor.init(plugin, module);

        return moduleDescriptor;
    }

    static ModuleDescriptor<?> newModuleDescriptor(
            final Plugin plugin, final Element element, final ModuleDescriptorFactory moduleDescriptorFactory) {
        final String name = element.getName();

        final ModuleDescriptor<?> moduleDescriptor;

        // Try to retrieve the module descriptor
        try {
            moduleDescriptor = moduleDescriptorFactory.getModuleDescriptor(name);
        }
        // When there's a problem loading a module, return an UnrecognisedModuleDescriptor with error
        catch (final Throwable e) {
            final UnrecognisedModuleDescriptor unrecognisedModuleDescriptor =
                    createUnrecognisedModuleDescriptor(plugin, element, e, moduleDescriptorFactory);

            log.error(
                    "There were problems loading the module '{}' in plugin '{}'. The module has been disabled.",
                    name,
                    plugin.getName());
            log.error(unrecognisedModuleDescriptor.getErrorText(), e);

            return unrecognisedModuleDescriptor;
        }

        return moduleDescriptor;
    }

    private static final class NamespaceCleaner extends VisitorSupport {
        @Override
        public void visit(Document document) {
            ((DefaultElement) document.getRootElement()).setNamespace(Namespace.NO_NAMESPACE);
            document.getRootElement().additionalNamespaces().clear();
        }

        @Override
        public void visit(Namespace namespace) {
            namespace.detach();
        }

        @Override
        public void visit(Attribute node) {
            if (node.toString().contains("xmlns") || node.toString().contains("xsi:")) {
                node.detach();
            }
        }

        @Override
        public void visit(org.dom4j.Element node) {
            if (node instanceof DefaultElement) {
                ((DefaultElement) node).setNamespace(Namespace.NO_NAMESPACE);
            }
        }
    }
}
