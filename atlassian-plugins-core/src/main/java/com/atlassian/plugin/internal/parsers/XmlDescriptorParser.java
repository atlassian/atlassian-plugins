package com.atlassian.plugin.internal.parsers;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.DOMReader;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginPermission;
import com.atlassian.plugin.descriptors.UnloadableModuleDescriptor;
import com.atlassian.plugin.factories.XmlDynamicPluginFactory;
import com.atlassian.plugin.impl.UnloadablePluginFactory;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.atlassian.security.xml.SecureXmlParserFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

import static com.atlassian.plugin.InstallationMode.LOCAL;
import static com.atlassian.plugin.descriptors.UnloadableModuleDescriptorFactory.createUnloadableModuleDescriptor;
import static com.atlassian.plugin.internal.parsers.XmlDescriptorParserUtils.newModuleDescriptor;

/**
 * Provides access to the descriptor information retrieved from an XML InputStream.
 * <p>
 * Uses the dom4j {@link SAXReader} to parse the XML stream into a document
 * when the parser is constructed.
 *
 * @see XmlDescriptorParserFactory
 */
public class XmlDescriptorParser implements DescriptorParser {
    private static final Logger log = LoggerFactory.getLogger(XmlDescriptorParser.class);

    private final PluginDescriptorReader descriptorReader;

    /**
     * Constructs a parser with an already-constructed document
     *
     * @param source       the source document
     * @param applications the application key to filter modules with, null for all unspecified
     * @throws PluginParseException if there is a problem reading the descriptor from the XML {@link InputStream}.
     * @since 2.2.0
     */
    public XmlDescriptorParser(final Document source, final Set<Application> applications) {
        this.descriptorReader = new PluginDescriptorReader(
                checkNotNull(source, "XML descriptor source document cannot be null"), checkNotNull(applications));
    }

    /**
     * Constructs a parser with a stream of an XML document for a specific application
     *
     * @param source       The descriptor stream
     * @param applications the application key to filter modules with, null for all unspecified
     * @throws PluginParseException if there is a problem reading the descriptor from the XML {@link InputStream}.
     * @since 2.2.0
     */
    public XmlDescriptorParser(final InputStream source, final Set<Application> applications) {
        this(createDocument(checkNotNull(source, "XML descriptor source cannot be null")), applications);
    }

    /**
     * Constructs a parser with a stream of an XML document for a specific application
     *
     * @param source              The descriptor stream
     * @param supplementalSources a collection of streams containing supplemental ModuleDescriptors
     * @param applications        the application key to filter modules with, null for all unspecified
     * @throws PluginParseException if there is a problem reading the descriptor from the XML {@link InputStream}.
     * @since 3.2.16
     */
    public XmlDescriptorParser(
            final InputStream source,
            final Iterable<InputStream> supplementalSources,
            final Set<Application> applications) {
        checkNotNull(source, "XML descriptor source cannot be null");
        checkNotNull(supplementalSources, "Supplemental XML descriptors cannot be null");
        Document mainDescriptor = createDocument(source);
        final Iterable<Document> supplementalDocs =
                Iterables.transform(supplementalSources, XmlDescriptorParser::createDocument);
        mainDescriptor = mergeDocuments(mainDescriptor, supplementalDocs);
        descriptorReader = new PluginDescriptorReader(mainDescriptor, checkNotNull(applications));
    }

    protected static Document createDocument(final InputStream source) {
        final DocumentBuilder documentBuilder = SecureXmlParserFactory.newNamespaceAwareDocumentBuilder();
        // Don't log parsing errors, because they are a part of normal flow.
        documentBuilder.setErrorHandler(NoopErrorHandler.INSTANCE);
        try {
            org.w3c.dom.Document document = documentBuilder.parse(new InputSource(source));
            document.normalize();
            return (new DOMReader()).read(document);
        } catch (final IOException | SAXException e) {
            throw new PluginParseException("Cannot parse XML plugin descriptor", e);
        }
    }

    protected static Document mergeDocuments(
            final Document mainDocument, final Iterable<Document> supplementalDocuments) {
        org.dom4j.Element mainRootElement = mainDocument.getRootElement();
        for (Document supplementalDocument : supplementalDocuments) {
            org.dom4j.Element supplementaryRoot = supplementalDocument.getRootElement();
            for (Iterator<Node> iter = supplementaryRoot.content().iterator(); iter.hasNext(); ) {
                Node node = iter.next();
                iter.remove();
                mainRootElement.add(node);
            }
        }
        return mainDocument;
    }

    protected Document getDocument() {
        return descriptorReader.getDescriptor();
    }

    public Plugin configurePlugin(final ModuleDescriptorFactory moduleDescriptorFactory, final Plugin plugin) {
        plugin.setName(descriptorReader.getPluginName());
        plugin.setKey(getKey());
        plugin.setPluginsVersion(getPluginsVersion());
        plugin.setSystemPlugin(isSystemPlugin());
        plugin.setI18nNameKey(descriptorReader.getI18nPluginNameKey().orElseGet(plugin::getI18nNameKey));

        if (plugin.getKey().indexOf(':') > 0) {
            throw new PluginParseException("Plugin keys cannot contain ':'. Key is '" + plugin.getKey() + "'");
        }

        plugin.setEnabledByDefault(descriptorReader.isEnabledByDefault());
        plugin.setResources(descriptorReader.getResources());
        plugin.setPluginInformation(createPluginInformation());

        for (org.dom4j.Element module : descriptorReader.getModules(plugin.getInstallationMode())) {
            final ModuleDescriptor<?> moduleDescriptor =
                    createModuleDescriptor(plugin, new Dom4jDelegatingElement(module), moduleDescriptorFactory);

            // If we're not loading the module descriptor, null is returned, so we skip it
            if (moduleDescriptor == null) {
                continue;
            }

            if (plugin.getModuleDescriptor(moduleDescriptor.getKey()) != null) {
                throw new PluginParseException("Found duplicate key '" + moduleDescriptor.getKey() + "' within plugin '"
                        + plugin.getKey() + "'");
            }

            plugin.addModuleDescriptor(moduleDescriptor);

            // If we have any unloadable modules, also create an unloadable plugin, which will make it clear that there
            // was a problem
            if (moduleDescriptor instanceof UnloadableModuleDescriptor) {
                log.error(
                        "There were errors loading the plugin '{}' of version '{}'. The plugin has been disabled.",
                        plugin.getName(),
                        plugin.getPluginInformation().getVersion());
                return UnloadablePluginFactory.createUnloadablePlugin(plugin);
            }
        }
        return plugin;
    }

    @Override
    public ModuleDescriptor<?> addModule(
            final ModuleDescriptorFactory moduleDescriptorFactory, final Plugin plugin, final Element module) {
        return XmlDescriptorParserUtils.addModule(moduleDescriptorFactory, plugin, module);
    }

    @Override
    public List<Element> parseChildModulesFromSource() {
        return stream(descriptorReader.getModules(LOCAL).spliterator(), false)
                .map(Dom4jDelegatingElement::new)
                .collect(toList());
    }

    protected ModuleDescriptor<?> createModuleDescriptor(
            final Plugin plugin,
            final com.atlassian.plugin.module.Element element,
            final ModuleDescriptorFactory moduleDescriptorFactory) {
        final String name = element.getName();

        final ModuleDescriptor<?> moduleDescriptor = newModuleDescriptor(plugin, element, moduleDescriptorFactory);

        // When the module descriptor has been excluded, null is returned (PLUG-5)
        if (moduleDescriptor == null) {
            log.info(
                    "The module '{}' in plugin '{}' is in the list of excluded module descriptors, so not enabling.",
                    name,
                    plugin.getName());
            return null;
        }

        // Once we have the module descriptor, create it using the given information
        try {
            moduleDescriptor.init(plugin, element);
        }
        // If it fails, return a dummy module that contains the error
        catch (final Exception e) {
            final UnloadableModuleDescriptor descriptor =
                    createUnloadableModuleDescriptor(plugin, element, e, moduleDescriptorFactory);
            log.error(
                    "There were problems loading the module '{}'. The module and its plugin ('{}' of version '{}') have been disabled.",
                    name,
                    plugin.getName(),
                    plugin.getPluginInformation().getVersion());
            log.error(descriptor.getErrorText(), e);

            return descriptor;
        }

        return moduleDescriptor;
    }

    protected PluginInformation createPluginInformation() {
        final PluginInformationReader pluginInformationReader = descriptorReader.getPluginInformationReader();

        final PluginInformation pluginInfo = new PluginInformation();
        pluginInfo.setDescription(pluginInformationReader.getDescription().orElseGet(pluginInfo::getDescription));
        pluginInfo.setDescriptionKey(
                pluginInformationReader.getDescriptionKey().orElseGet(pluginInfo::getDescriptionKey));
        pluginInfo.setVersion(pluginInformationReader.getVersion().orElseGet(pluginInfo::getVersion));
        pluginInfo.setVendorName(pluginInformationReader.getVendorName().orElseGet(pluginInfo::getVendorName));
        pluginInfo.setVendorUrl(pluginInformationReader.getVendorUrl().orElseGet(pluginInfo::getVendorUrl));
        pluginInfo.setScopeKey(pluginInformationReader.getScopeKey());

        for (Map.Entry<String, String> param :
                pluginInformationReader.getParameters().entrySet()) {
            pluginInfo.addParameter(param.getKey(), param.getValue());
        }
        pluginInfo.setMinJavaVersion(
                pluginInformationReader.getMinJavaVersion().orElseGet(pluginInfo::getMinJavaVersion));
        pluginInfo.setStartup(pluginInformationReader.getStartup().orElseGet(pluginInfo::getStartup));
        pluginInfo.setModuleScanFolders(pluginInformationReader.getModuleScanFolders());

        final Map<String, Optional<String>> readPermissions = pluginInformationReader.getPermissions();
        if (pluginInformationReader.hasAllPermissions()) {
            pluginInfo.setPermissions(ImmutableSet.of(PluginPermission.ALL));
        } else {
            final ImmutableSet.Builder<PluginPermission> permissions = ImmutableSet.builder();
            for (Map.Entry<String, Optional<String>> permission : readPermissions.entrySet()) {
                final String permissionKey = permission.getKey();
                final Optional<String> readInstallationMode = permission.getValue();
                final Optional<InstallationMode> installationMode = readInstallationMode.flatMap(InstallationMode::of);
                if (StringUtils.isNotBlank(readInstallationMode.orElse(null)) && !installationMode.isPresent()) {
                    log.warn(
                            "The parsed installation mode '{}' for permission '{}' didn't match any of the valid values: {}",
                            readInstallationMode,
                            permission.getKey(),
                            transform(copyOf(InstallationMode.values()), InstallationMode::getKey));
                }

                permissions.add(new PluginPermission(permissionKey, installationMode.orElse(null)));
            }
            pluginInfo.setPermissions(permissions.build());
        }

        return pluginInfo;
    }

    public String getKey() {
        return descriptorReader.getPluginKey();
    }

    public int getPluginsVersion() {
        return descriptorReader.getPluginsVersion();
    }

    public PluginInformation getPluginInformation() {
        return createPluginInformation();
    }

    public boolean isSystemPlugin() {
        return descriptorReader.isSystemPlugin();
    }

    /**
     * This error handler suppresses all logging because the default SAX handler
     * {@link com.sun.org.apache.xerces.internal.util.DefaultErrorHandler} writes directly to stderr.
     * <p>
     * In this class we use XML parsing errors as a part of control flow and errors are expected, that's why we
     * suppress all logging to stderr. If someone wants to see parsing errors, they can use DEBUG logging level
     * for {@link XmlDynamicPluginFactory}.
     */
    private static class NoopErrorHandler implements ErrorHandler {

        static final NoopErrorHandler INSTANCE = new NoopErrorHandler();

        @Override
        public void warning(SAXParseException exception) {
            // Do nothing
        }

        @Override
        public void error(SAXParseException exception) {
            // Do nothing
        }

        @Override
        public void fatalError(SAXParseException exception) {
            // Do nothing
        }
    }
}
