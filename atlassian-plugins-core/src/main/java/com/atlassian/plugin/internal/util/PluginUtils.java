package com.atlassian.plugin.internal.util;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.descriptors.RequiresRestart;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * General plugin utility methods
 *
 * @since 2.1
 */
public class PluginUtils {
    private static final Logger logger = LoggerFactory.getLogger(PluginUtils.class);

    public static final String ATLASSIAN_DEV_MODE = "atlassian.dev.mode";

    /**
     * System property for storing and retrieving the time the plugin system will wait for the enabling of a plugin in
     * seconds.
     *
     * @see #DEFAULT_ATLASSIAN_PLUGINS_ENABLE_WAIT_SECONDS
     * @since 2.3.6
     */
    public static final String ATLASSIAN_PLUGINS_ENABLE_WAIT = "atlassian.plugins.enable.wait";

    /**
     * The default number of seconds that a plugin should wait for its dependencies to become enabled. Currently 300s.
     *
     * @see #ATLASSIAN_PLUGINS_ENABLE_WAIT
     * @since 3.1.0
     */
    public static final String DEFAULT_ATLASSIAN_PLUGINS_ENABLE_WAIT_SECONDS = "300";

    /**
     * Used to customise the size of the LRU cache for batch web resources.
     * This effectively controls how many files will be created
     * by the file cache. Providing a negative number results in undefined behaviour.
     *
     * @since 2.13.0
     */
    public static final String WEBRESOURCE_FILE_CACHE_SIZE = new String("atlassian.webresource.file.cache.size");

    /**
     * Used to disable the file cache should that be desired. Setting this value to true will disable the
     * file caching completely for all places it is used.
     *
     * @since 2.13.0
     */
    public static final String WEBRESOURCE_DISABLE_FILE_CACHE = new String("atlassian.webresource.file.cache.disable");

    /**
     * Determines if a plugin requires a restart after being installed at runtime. Looks for the annotation
     * {@link RequiresRestart} on the plugin's module descriptors.
     *
     * @param plugin The plugin that was just installed at runtime, but not yet enabled
     * @return True if a restart is required
     * @since 2.1
     */
    public static boolean doesPluginRequireRestart(final Plugin plugin) {
        // PLUG-451: When in dev mode, plugins should not require a restart.
        if (isAtlassianDevMode()) {
            return false;
        }

        for (final ModuleDescriptor<?> descriptor : plugin.getModuleDescriptors()) {
            if (descriptor.getClass().getAnnotation(RequiresRestart.class) != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets a list of all the module keys in a plugin that require restart. Looks for the annotation
     * {@link RequiresRestart} on the plugin's module descriptors.
     *
     * @param plugin The plugin
     * @return A unique set of module keys
     * @since 2.5.0
     */
    public static Set<String> getPluginModulesThatRequireRestart(final Plugin plugin) {
        final Set<String> keys = new HashSet<>();
        for (final ModuleDescriptor<?> descriptor : plugin.getModuleDescriptors()) {
            if (descriptor.getClass().getAnnotation(RequiresRestart.class) != null) {
                keys.add(descriptor.getKey());
            }
        }
        return keys;
    }

    /**
     * Determines if a module element applies to the current application by matching the 'application' attribute
     * to the set of applications. If the application is specified, but isn't in the set, we return false
     *
     * @param element      The module element
     * @param applications The set of application applications
     * @return True if it should apply, false otherwise
     * @since 2.2.0
     */
    public static boolean doesModuleElementApplyToApplication(
            final Element element, final Set<Application> applications, final InstallationMode installationMode) {
        checkNotNull(element);
        checkNotNull(applications);

        final ModuleRestricts restricts = ModuleRestricts.parse(element);
        final boolean valid = restricts.isValidFor(applications, installationMode);
        if (!valid && logger.isDebugEnabled()) {
            logger.debug(
                    "Module '{}' with key '{}' is restricted to the following "
                            + "applications {} and therefore does not apply to applications {}",
                    element.getName(),
                    element.attributeValue("key"),
                    restricts,
                    asString(applications));
        }
        return valid;
    }

    private static String asString(final Set<Application> applications) {
        return "["
                + Joiner.on(",")
                        .join(applications.stream()
                                .map(app -> MoreObjects.toStringHelper(app.getKey())
                                        .add("version", app.getVersion())
                                        .add("build", app.getBuildNumber())
                                        .toString())
                                .collect(Collectors.toList()))
                + "]";
    }

    /**
     * @return The default enabling waiting period in seconds
     * @since 2.3.6
     */
    public static int getDefaultEnablingWaitPeriod() {
        return Integer.parseInt(
                System.getProperty(ATLASSIAN_PLUGINS_ENABLE_WAIT, DEFAULT_ATLASSIAN_PLUGINS_ENABLE_WAIT_SECONDS));
    }

    public static boolean isAtlassianDevMode() {
        return Boolean.getBoolean(ATLASSIAN_DEV_MODE);
    }
}
