package com.atlassian.plugin.internal.util;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;
import com.google.common.base.MoreObjects;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Objects.requireNonNull;

/**
 * Represents a list of {@link ModuleRestrict}. This represents the restricts
 * as used in the plugin descriptor:
 * <code><br>
 * &nbsp;&nbsp;&lt;module key="module-key"><br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;restrict ...>...&lt;/restrict><br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;restrict ...>...&lt;/restrict><br>
 * &nbsp;&nbsp;&lt;/module>
 * </code>
 *
 * @since 3.0
 */
final class ModuleRestricts {
    final Iterable<ModuleRestrict> restricts;

    private ModuleRestricts() {
        this(ImmutableList.of());
    }

    private ModuleRestricts(Iterable<ModuleRestrict> restricts) {
        this.restricts = ImmutableList.copyOf(restricts);
    }

    static ModuleRestricts parse(Element moduleElement) {
        final String applicationKeys = moduleElement.attributeValue("application");
        if (applicationKeys != null) {
            return parseApplicationsFromAttribute(applicationKeys);
        } else if (!moduleElement.elements("restrict").isEmpty()) {
            @SuppressWarnings("unchecked")
            final List<Element> restrict = moduleElement.elements("restrict");
            return parseApplicationsFromRestrictElements(restrict);
        } else {
            return new ModuleRestricts();
        }
    }

    private static ModuleRestricts parseApplicationsFromRestrictElements(List<Element> restrictElements) {
        return new ModuleRestricts(Iterables.transform(restrictElements, restrictElement -> {
            final String application = restrictElement.attributeValue("application");
            checkState(application != null, "No application defined for 'restrict' element.");
            return new ModuleRestrict(
                    application,
                    parseInstallationMode(restrictElement).orElse(null),
                    parseVersionRange(restrictElement));
        }));
    }

    private static Optional<InstallationMode> parseInstallationMode(Element restrictElement) {
        return InstallationMode.of(restrictElement.attributeValue("mode"));
    }

    private static VersionRange parseVersionRange(Element restrictElement) {
        final String version = restrictElement.attributeValue("version");
        if (version != null) {
            return VersionRange.parse(version);
        } else {
            final List<Element> versionElements = restrictElement.elements("version");
            if (!versionElements.isEmpty()) {
                VersionRange range = VersionRange.empty();
                for (Element versionElement : versionElements) {
                    range = range.or(VersionRange.parse(versionElement.getText()));
                }
                return range;
            } else {
                return VersionRange.all();
            }
        }
    }

    private static ModuleRestricts parseApplicationsFromAttribute(String applicationKeys) {
        final String[] keys = applicationKeys.split("\\s*,[,\\s]*");
        final Iterable<ModuleRestrict> restricts =
                transform(filter(newArrayList(keys), new IsNotBlankPredicate()), ModuleRestrict::new);

        return new ModuleRestricts(restricts);
    }

    public boolean isValidFor(Set<Application> applications, InstallationMode mode) {
        if (Iterables.isEmpty(restricts)) {
            return true;
        }

        for (Application application : applications) {
            if (Iterables.any(restricts, new RestrictMatchesApplication(application, mode))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return restricts.toString();
    }

    static final class ModuleRestrict {
        final String application;
        final InstallationMode mode;
        final VersionRange version;

        ModuleRestrict(String application) {
            this(application, null);
        }

        ModuleRestrict(String application, InstallationMode mode) {
            this(application, mode, VersionRange.all());
        }

        ModuleRestrict(String application, InstallationMode mode, VersionRange version) {
            this.application = requireNonNull(application, "application");
            this.mode = mode;
            this.version = requireNonNull(version);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper("restrict")
                    .add("application", application)
                    .add("mode", mode)
                    .add("range", version)
                    .toString();
        }
    }

    private static final class IsNotBlankPredicate implements Predicate<String> {
        @Override
        public boolean apply(String input) {
            return StringUtils.isNotBlank(input);
        }
    }

    private static final class RestrictMatchesApplication implements Predicate<ModuleRestrict> {
        private final Application app;
        private final InstallationMode installationMode;

        public RestrictMatchesApplication(Application app, InstallationMode installationMode) {
            this.app = checkNotNull(app);
            this.installationMode = checkNotNull(installationMode);
        }

        @Override
        public boolean apply(ModuleRestrict restrict) {
            return restrict.application.equals(app.getKey())
                    && isInstallModeValid(restrict.mode)
                    && restrict.version.isInRange(app.getVersion());
        }

        private boolean isInstallModeValid(InstallationMode mode) {
            return mode == null || mode.equals(installationMode);
        }
    }
}
