package com.atlassian.plugin.internal.parsers;

import java.io.InputStream;
import java.util.Set;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.atlassian.plugin.parsers.DescriptorParserFactory;

/**
 * Creates XML descriptor parser instances.
 *
 * @see XmlDescriptorParser
 * @see DescriptorParserFactory
 */
public class XmlDescriptorParserFactory implements DescriptorParserFactory {
    /**
     * @param source       the stream of data which represents the descriptor. The stream will
     *                     only be read once, so it need not be resettable.
     * @param applications the identifier of the current application to use to match modules, if specified. Null to
     *                     match only modules with no application key.
     * @return
     * @throws PluginParseException
     */
    public DescriptorParser getInstance(InputStream source, Set<Application> applications) {
        return new XmlDescriptorParser(source, applications);
    }
}
