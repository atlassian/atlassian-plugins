package com.atlassian.plugin.exception;

import com.atlassian.plugin.Plugin;

/**
 * The equivalent of the old plugin system behaviour
 */
public class NoOpPluginExceptionInterception implements PluginExceptionInterception {
    public static final PluginExceptionInterception NOOP_INTERCEPTION = new NoOpPluginExceptionInterception();

    private NoOpPluginExceptionInterception() {}

    @Override
    public boolean onEnableException(final Plugin plugin, final Exception pluginException) {
        return true;
    }
}
