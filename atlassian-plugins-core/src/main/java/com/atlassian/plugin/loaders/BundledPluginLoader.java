package com.atlassian.plugin.loaders;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.DefaultPluginArtifactFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import com.atlassian.plugin.loaders.classloading.EmptyScanner;
import com.atlassian.plugin.loaders.classloading.ForwardingScanner;
import com.atlassian.plugin.loaders.classloading.Scanner;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import static com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE;

/**
 * A Plugin loader that manages a set of bundled plugins, meaning that they can can be upgraded, but
 * not deleted.
 * <p>
 * This loader can source plugins from:
 * <p>
 * <ul>
 * <li>A directory containing Plugin artifacts.
 * <li>A file with a {@link #getListSuffix()} suffix, where each line of that
 * file gives the path to a Plugin artifact.
 * <li>A URL identifying a zip file, and a path to explode the zip into, and the exploded
 * contents of the zip are used as the plugin artifacts.
 * </ul>
 */
public class BundledPluginLoader extends ScanningPluginLoader {
    private static final Logger log = LoggerFactory.getLogger(BundledPluginLoader.class);

    /**
     * The suffix used for bundled plugin list files.
     */
    public static String getListSuffix() {
        return ".list";
    }

    private BundledPluginLoader(
            final Scanner scanner, final List<PluginFactory> pluginFactories, final PluginEventManager eventManager) {
        super(
                new NonRemovingScanner(scanner),
                pluginFactories,
                new DefaultPluginArtifactFactory(PERMIT_REFERENCE),
                eventManager);
    }

    /**
     * Construct a bundled plugin loader for a directory or list file source.
     *
     * @param source          a directory of containing plugin artifacts, or a file with suffix
     *                        {@link #getListSuffix()} containing a list of paths to plugin artifacts.
     * @param pluginFactories as per {@link ScanningPluginLoader}.
     * @param eventManager    as per {@link ScanningPluginLoader}.
     * @since 3.0.16
     */
    public BundledPluginLoader(
            final File source, final List<PluginFactory> pluginFactories, final PluginEventManager eventManager) {
        this(buildSourceScanner(source), pluginFactories, eventManager);
    }

    /**
     * Construct a bundled plugin loader for a zip source.
     * <p>
     * For backwards compatibility, if the zipUrl is in fact a file url (acccording to
     * {@link FileUtils#toFile}), this constructor has the same semantics as
     * {@link #BundledPluginLoader(File, List, PluginEventManager)} where the first argument
     * is the file referred to by zipUrl, and the pluginPath argument is ignored. That
     * constructor should be used directly in such cases.
     * <p>
     *
     * @param zipUrl          a url to a zipFile containing plugin artifacts, or a file url to a directory or
     *                        list file.
     * @param pluginPath      path to the directory to expand zipUrl.
     * @param pluginFactories as per {@link ScanningPluginLoader}.
     * @param eventManager    as per {@link ScanningPluginLoader}.
     */
    public BundledPluginLoader(
            final URL zipUrl,
            final File pluginPath,
            final List<PluginFactory> pluginFactories,
            final PluginEventManager eventManager) {
        this(buildZipScanner(zipUrl, pluginPath), pluginFactories, eventManager);
    }

    @Override
    protected Plugin postProcess(final Plugin plugin) {
        if (plugin instanceof PluginInternal) {
            ((PluginInternal) plugin).setBundledPlugin(true);
        } else {
            log.warn(
                    "unable to set bundled attribute on plugin '{}' as it is of class {}",
                    plugin,
                    plugin.getClass().getCanonicalName());
        }
        return plugin;
    }

    private static Scanner buildScannerCommon(final File file) {
        if (file.isDirectory()) {
            // file points directly to a directory of jars
            return new DirectoryScanner(file);
        } else if (file.isFile() && file.getName().endsWith(getListSuffix())) {
            // file contains a list of jars.
            final List<File> files = readListFile(file);
            return new FileListScanner(files);
        } else {
            // unknown - let caller figure out fallback
            return null;
        }
    }

    private static Scanner buildSourceScanner(final File source) {
        checkNotNull(source, "Source must not be null");
        final Scanner scanner = buildScannerCommon(source);
        if (null == scanner) {
            log.error("Cannot build a scanner for source '{}'", source);
            // This is approximately what the code used to do - produce a scanner which would have
            // no entries (unless it accidentally had a random collection from a prior boot).
            return new EmptyScanner();
        } else {
            return scanner;
        }
    }

    private static Scanner buildZipScanner(final URL url, final File pluginPath) {
        // checkArgument used to preserve historical behaviour of throwing IllegalArgumentException
        checkArgument(null != url, "Bundled plugins url cannot be null");

        Scanner scanner = null;

        // Legacy behaviour - treat file:// urls as per buildSourceScanner, but we don't
        // want the error or the empty scanner.
        final File file = FileUtils.toFile(url);
        if (null != file) {
            scanner = buildScannerCommon(file);
        }

        if (null == scanner) {
            // Not handled by file:// urls, so it's a zip url
            com.atlassian.plugin.util.FileUtils.conditionallyExtractZipFile(url, pluginPath);
            scanner = new DirectoryScanner(pluginPath);
        }

        return scanner;
    }

    private static List<File> readListFile(final File file) {
        try {
            final List<String> fnames = Files.readAllLines(file.toPath());
            final List<File> files = new ArrayList<>();
            for (final String fname : fnames) {
                files.add(new File(fname));
            }
            return files;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read list from " + file, e);
        }
    }

    /**
     * A forwarding scanner which suppresses remove operation.
     * <p>
     * Bundled plugins are never actually removed from the system, so we wrap the scanner to
     * suppress removal.
     */
    private static class NonRemovingScanner extends ForwardingScanner {
        NonRemovingScanner(final Scanner scanner) {
            super(scanner);
        }

        @Override
        public void remove(final DeploymentUnit unit) {
            // Suppressed - see class documentation.
        }
    }
}
