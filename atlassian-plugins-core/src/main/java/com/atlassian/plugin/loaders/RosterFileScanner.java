package com.atlassian.plugin.loaders;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import com.atlassian.plugin.loaders.classloading.Scanner;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A Scanner which polls a file containing a list of file names to define its content.
 *
 * @since 3.0.24
 */
public class RosterFileScanner implements Scanner {
    private static final Logger log = LoggerFactory.getLogger(RosterFileScanner.class);

    private final File rosterFile;
    private Map<String, DeploymentUnit> deploymentUnits;
    private long lastModified;

    /**
     * Create a roster file scanner which loads the given roster file to locate plugins.
     * <p>
     * The provided roster file format is determined by its file suffix. Currently supported suffixes are
     * - {@link #getListSuffix()}: a list of file paths, one per line
     * If the format is not recognized, an {@link IllegalArgumentException} is thrown. This can be checked before construction
     * using {@link #isKnownRosterFileFormat(File)}.
     *
     * @param rosterFile the file to load the plugin roster from.
     * @throws IllegalArgumentException if the suffix of rosterFile is not a supported file format
     */
    public RosterFileScanner(final File rosterFile) {
        checkNotNull(rosterFile);
        checkArgument(
                isKnownRosterFileFormat(rosterFile),
                "Roster file '%s' does not end with '%s'",
                rosterFile,
                getListSuffix());
        this.rosterFile = rosterFile;
        this.deploymentUnits = Collections.emptyMap();
    }

    @Override
    public Collection<DeploymentUnit> scan() {
        try {
            final List<DeploymentUnit> scanned = new ArrayList<>();
            final long updatedLastModified = rosterFile.lastModified();
            if ((updatedLastModified != 0) && (updatedLastModified != lastModified)) {
                // File exists and has changed, reload to implement the scan
                // These semantics are by analogy with DirectoryScanner, that we forget plugins that drop out of the
                // list
                // FileUtils is not generic safe
                //noinspection unchecked
                final List<String> filePaths = (List<String>) FileUtils.readLines(rosterFile);
                final Map<String, DeploymentUnit> updatedDeploymentUnits = new HashMap<>(filePaths.size());
                for (final String filePath : filePaths) {
                    final DeploymentUnit priorUnit = deploymentUnits.get(filePath);
                    if (null == priorUnit) {
                        // It's new, make a new DeploymentUnit and include it in the scan.
                        final File file = new File(filePath);
                        final File absoluteFile =
                                file.isAbsolute() ? file : new File(rosterFile.getParentFile(), filePath);
                        final DeploymentUnit deploymentUnit = new DeploymentUnit(absoluteFile);
                        updatedDeploymentUnits.put(filePath, deploymentUnit);
                        scanned.add(deploymentUnit);
                    } else {
                        // Keep the deployment unit from the previous run
                        updatedDeploymentUnits.put(filePath, priorUnit);
                    }
                }
                deploymentUnits = updatedDeploymentUnits;
                lastModified = updatedLastModified;
                return scanned;
            }
        } catch (final IOException eio) {
            log.warn("Cannot read roster file '{}': {}", rosterFile.getAbsolutePath(), eio.getMessage());
        }
        // unreadable, unchanged or error - report nothing new
        return Collections.emptyList();
    }

    @Override
    public Collection<DeploymentUnit> getDeploymentUnits() {
        return Collections.unmodifiableCollection(deploymentUnits.values());
    }

    @Override
    public void reset() {
        deploymentUnits = Collections.emptyMap();
        lastModified = 0;
    }

    @Override
    public void remove(final DeploymentUnit deploymentUnit) {
        // Remove is called during the plugin upgrade process, but since we don't own the files, we don't actually
        // remove them.
        // In fact, they're in our software tree and hence read-only anyway, so we can't remove them.
        // We just ignore remove - while we could in principle track what we'd seen, that's a chunk of logic for not
        // much gain.
    }

    /**
     * The suffix for a roster file containing a simple plugin list.
     * <p>
     * Roster files with this suffix have one literal path per line. Relative paths are resolved relative to the directory
     * containing the list file. Entries are considered distinct or not based on the path used in the file - no canonicalization
     * is forced.
     *
     * @return the suffix used for roster files containing just a list of plugin jar files.
     */
    public static String getListSuffix() {
        return ".list";
    }

    /**
     * Check whether a proposed roster file is in a known format.
     *
     * @param rosterFile the roster file to check.
     * @return true if the format of the roster file is known.
     * @see RosterFileScanner
     */
    public static boolean isKnownRosterFileFormat(final File rosterFile) {
        return rosterFile.getName().endsWith(getListSuffix());
    }
}
