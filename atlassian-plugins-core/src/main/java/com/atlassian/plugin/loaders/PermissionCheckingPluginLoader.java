package com.atlassian.plugin.loaders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Function;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.impl.UnloadablePlugin;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

public final class PermissionCheckingPluginLoader extends ForwardingPluginLoader {
    private static final Logger logger = LoggerFactory.getLogger(PermissionCheckingPluginLoader.class);

    public PermissionCheckingPluginLoader(PluginLoader delegate) {
        super(delegate);
    }

    @Override
    public Iterable<Plugin> loadAllPlugins(ModuleDescriptorFactory moduleDescriptorFactory) {
        return copyOf(
                transform(delegate().loadAllPlugins(moduleDescriptorFactory), new CheckPluginPermissionFunction()));
    }

    @Override
    public Iterable<Plugin> loadFoundPlugins(ModuleDescriptorFactory moduleDescriptorFactory) {
        return copyOf(
                transform(delegate().loadFoundPlugins(moduleDescriptorFactory), new CheckPluginPermissionFunction()));
    }

    @Override
    public void removePlugin(Plugin plugin) {
        if (!(plugin instanceof UnloadablePlugin)) {
            super.removePlugin(plugin);
        } else {
            logger.debug("Detected an unloadable plugin '{}', so skipping removal", plugin.getKey());
        }
    }

    @Override
    public void discardPlugin(Plugin plugin) {
        if (!(plugin instanceof UnloadablePlugin)) {
            super.discardPlugin(plugin);
        } else {
            logger.debug("Detected an unloadable plugin '{}', so skipping discard", plugin.getKey());
        }
    }

    private final class CheckPluginPermissionFunction implements Function<Plugin, Plugin> {
        @Override
        public Plugin apply(Plugin p) {
            // we don't need to check if all permissions are allowed
            if (p.hasAllPermissions()) {
                return p;
            }

            if (p instanceof PluginInternal) {
                return checkPlugin((PluginInternal) p);
            }
            return p;
        }

        private Plugin checkPlugin(PluginInternal p) {
            final PluginArtifact pluginArtifact = p.getPluginArtifact();
            if (pluginArtifact != null) {

                if (pluginArtifact.containsJavaExecutableCode()
                        && !p.getActivePermissions().contains(Permissions.EXECUTE_JAVA)) {
                    UnloadablePlugin unloadablePlugin = new UnloadablePlugin(
                            "Plugin doesn't require '" + Permissions.EXECUTE_JAVA
                                    + "' permission yet references some java executable code. "
                                    + "This could be either embedded java classes, embedded java libraries, spring context files or bundle activator.");
                    unloadablePlugin.setKey(p.getKey());
                    unloadablePlugin.setName(p.getName());

                    logger.warn(
                            "Plugin '{}' only requires permission {} which doesn't include '{}', yet has some java code "
                                    + "(classes, libs, spring context, etc), making it un-loadable.",
                            p.getKey(),
                            p.getActivePermissions(),
                            Permissions.EXECUTE_JAVA);

                    // This will go via super to the delegate
                    discardPlugin(p);

                    return unloadablePlugin;
                } else if (hasSystemModules(p)
                        && !p.getActivePermissions().contains(Permissions.CREATE_SYSTEM_MODULES)) {
                    UnloadablePlugin unloadablePlugin =
                            new UnloadablePlugin("Plugin doesn't require '" + Permissions.CREATE_SYSTEM_MODULES
                                    + "' permission yet declared " + "modules use the 'system' attribute. ");
                    unloadablePlugin.setKey(p.getKey());
                    unloadablePlugin.setName(p.getName());

                    logger.warn(
                            "Plugin '{}' only requires permission {} which doesn't include '{}', yet has system modules "
                                    + ", making it un-loadable.",
                            p.getKey(),
                            p.getActivePermissions(),
                            Permissions.CREATE_SYSTEM_MODULES);

                    // This will go via super to the delegate
                    discardPlugin(p);

                    return unloadablePlugin;
                }
            }
            return p;
        }

        private boolean hasSystemModules(Plugin plugin) {
            for (ModuleDescriptor descriptor : plugin.getModuleDescriptors()) {
                if (descriptor.isSystemModule()) {
                    return true;
                }
            }
            return false;
        }
    }
}
