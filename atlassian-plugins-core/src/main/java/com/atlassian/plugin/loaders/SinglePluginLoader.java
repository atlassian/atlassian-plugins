package com.atlassian.plugin.loaders;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.impl.UnloadablePluginFactory;
import com.atlassian.plugin.internal.parsers.XmlDescriptorParserFactory;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.atlassian.plugin.parsers.DescriptorParserFactory;
import com.atlassian.plugin.util.ClassLoaderUtils;

import static com.atlassian.plugin.internal.parsers.XmlDescriptorParserUtils.addModule;
import static com.atlassian.plugin.util.Assertions.notNull;

/**
 * Loads a single plugin from the descriptor provided, which can either be an InputStream
 * or a resource on the classpath. The classes used by the plugin must already be available
 * on the classpath because this plugin loader does <b>not</b> load any classes.
 *
 * @see PluginLoader
 * @see ClassPathPluginLoader
 */
public class SinglePluginLoader implements PluginLoader {
    protected Collection<Plugin> plugins;

    /**
     * to load the Stream from the classpath.
     */
    private final String resource;

    /**
     * to load the Stream directly.
     */
    private final URL url;

    private final DescriptorParserFactory descriptorParserFactory = new XmlDescriptorParserFactory();

    private static final Logger log = LoggerFactory.getLogger(SinglePluginLoader.class);

    public SinglePluginLoader(final String resource) {
        this.resource = notNull("resource", resource);
        url = null;
    }

    public SinglePluginLoader(final URL url) {
        this.url = notNull("url", url);
        resource = null;
    }

    public Iterable<Plugin> loadAllPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) {
        if (plugins == null) {
            Plugin plugin;
            try {
                plugin = loadPlugin(moduleDescriptorFactory);
            } catch (RuntimeException ex) {
                String id = getIdentifier();
                log.error("Error loading plugin or descriptor: " + id, ex);
                plugin = new UnloadablePlugin(id + ": " + ex);
                plugin.setKey(id);
            }
            plugins = Collections.singleton(plugin);
        }
        return ImmutableList.copyOf(plugins);
    }

    public boolean supportsRemoval() {
        return false;
    }

    public boolean supportsAddition() {
        return false;
    }

    public Iterable<Plugin> loadFoundPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) {
        throw new UnsupportedOperationException("This PluginLoader does not support addition.");
    }

    public void removePlugin(final Plugin plugin) {
        throw new PluginException("This PluginLoader does not support removal.");
    }

    @Override
    public boolean isDynamicPluginLoader() {
        return false;
    }

    @Override
    public ModuleDescriptor<?> createModule(
            final Plugin plugin, final Element module, final ModuleDescriptorFactory moduleDescriptorFactory) {
        if (plugins.contains(plugin)) {
            return addModule(moduleDescriptorFactory, plugin, module);
        } else {
            return null;
        }
    }

    protected Plugin loadPlugin(final ModuleDescriptorFactory moduleDescriptorFactory) {
        final InputStream source = getSource();
        if (source == null) {
            throw new PluginParseException("Invalid resource or inputstream specified to load plugins from.");
        }

        Plugin plugin;
        try {
            final DescriptorParser parser = descriptorParserFactory.getInstance(source, ImmutableSet.of());
            plugin = parser.configurePlugin(moduleDescriptorFactory, getNewPlugin());
            if (plugin.getPluginsVersion() == 2) {
                UnloadablePlugin unloadablePlugin = UnloadablePluginFactory.createUnloadablePlugin(plugin);
                final StringBuilder errorText = new StringBuilder(
                        "OSGi plugins cannot be deployed via the classpath, which is usually WEB-INF/lib.");
                if (resource != null) {
                    errorText.append("\n Resource is: ").append(resource);
                }
                if (url != null) {
                    errorText.append("\n URL is: ").append(url);
                }
                unloadablePlugin.setErrorText(errorText.toString());
                plugin = unloadablePlugin;
            }
        } catch (final PluginParseException e) {
            throw new PluginParseException("Unable to load plugin resource: " + resource + " - " + e.getMessage(), e);
        }

        return plugin;
    }

    private String getIdentifier() {
        if (resource != null) {
            return resource;
        }
        if (url != null) {
            return url.getPath();
        }
        return null;
    }

    protected StaticPlugin getNewPlugin() {
        return new StaticPlugin();
    }

    protected InputStream getSource() {
        if (resource != null) {
            return ClassLoaderUtils.getResourceAsStream(resource, this.getClass());
        }

        if (url != null) {
            try {
                return url.openConnection().getInputStream();
            } catch (IOException e) {
                throw new PluginParseException(e);
            }
        }

        throw new IllegalStateException("No defined method for getting an input stream.");
    }
}
