package com.atlassian.plugin.loaders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.module.Element;

import static com.google.common.base.Preconditions.checkNotNull;

public class ForwardingPluginLoader implements DynamicPluginLoader, DiscardablePluginLoader {
    private static final Logger log = LoggerFactory.getLogger(ForwardingPluginLoader.class);

    private final PluginLoader delegate;

    public ForwardingPluginLoader(PluginLoader delegate) {
        this.delegate = checkNotNull(delegate);
    }

    protected final PluginLoader delegate() {
        return delegate;
    }

    @Override
    public Iterable<Plugin> loadAllPlugins(ModuleDescriptorFactory moduleDescriptorFactory) {
        return delegate().loadAllPlugins(moduleDescriptorFactory);
    }

    @Override
    public Iterable<Plugin> loadFoundPlugins(ModuleDescriptorFactory moduleDescriptorFactory) {
        return delegate().loadFoundPlugins(moduleDescriptorFactory);
    }

    @Override
    public boolean supportsAddition() {
        return delegate().supportsAddition();
    }

    @Override
    public boolean supportsRemoval() {
        return delegate().supportsRemoval();
    }

    @Override
    public void removePlugin(Plugin plugin) {
        delegate().removePlugin(plugin);
    }

    @Override
    public boolean isDynamicPluginLoader() {
        return delegate().isDynamicPluginLoader();
    }

    @Override
    public ModuleDescriptor<?> createModule(
            final Plugin plugin, final Element module, final ModuleDescriptorFactory moduleDescriptorFactory) {
        return delegate().createModule(plugin, module, moduleDescriptorFactory);
    }

    @Override
    public String canLoad(PluginArtifact pluginArtifact) {
        if (isDynamicPluginLoader()) {
            return ((DynamicPluginLoader) delegate()).canLoad(pluginArtifact);
        } else {
            throw new IllegalStateException("Should not call on non-dynamic plugin loader");
        }
    }

    @Override
    public void discardPlugin(final Plugin plugin) {
        final PluginLoader delegate = delegate();
        if (delegate instanceof DiscardablePluginLoader) {
            ((DiscardablePluginLoader) delegate).discardPlugin(plugin);
        } else {
            // We ignore the discard, but we log because (in the short term) this is more likely to
            // be a PluginLoader which should really be a DiscardablePluginLoader then one which is
            // legitimately ignoring discardPlugin.
            log.debug(
                    "Ignoring discardPlugin({}, version {}) as delegate is not a DiscardablePluginLoader",
                    plugin.getKey(),
                    plugin.getPluginInformation().getVersion());
        }
    }

    @Override
    public String toString() {
        return delegate.toString();
    }
}
