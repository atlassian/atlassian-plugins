package com.atlassian.plugin.loaders;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import com.atlassian.plugin.loaders.classloading.Scanner;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Scans the filesystem for changed or added plugin files and stores a map of the currently known ones. Files beginning
 * with "." are ignored.
 *
 * @since 2.1.0
 */
public class DirectoryScanner implements Scanner {

    private static Logger log = LoggerFactory.getLogger(DirectoryScanner.class);

    /**
     * Tracks the classloading
     */
    private final File pluginsDirectory;

    /**
     * A Map of {@link String} absolute file paths to {@link DeploymentUnit}s.
     */
    private final Map<String, DeploymentUnit> scannedDeploymentUnits = new TreeMap<>();

    /**
     * Constructor for scanner.
     *
     * @param pluginsDirectory the directory that the scanner should monitor for plugins
     */
    public DirectoryScanner(final File pluginsDirectory) {
        this.pluginsDirectory = checkNotNull(pluginsDirectory);
    }

    private DeploymentUnit createAndStoreDeploymentUnit(final File file) {
        if (isScanned(file)) return null;

        final DeploymentUnit unit = new DeploymentUnit(file);
        scannedDeploymentUnits.put(file.getAbsolutePath(), unit);

        return unit;
    }

    /**
     * Given a file, finds the deployment unit for it if one has already been scanned.
     *
     * @param file a jar file.
     * @return the stored deploymentUnit matching the file or null if none exists.
     */
    public DeploymentUnit locateDeploymentUnit(final File file) {
        return scannedDeploymentUnits.get(file.getAbsolutePath());
    }

    /**
     * Finds whether the given file has been scanned already.
     */
    private boolean isScanned(final File file) {
        return locateDeploymentUnit(file) != null;
    }

    /**
     * Tells the Scanner to forget about a file it has loaded so that it will reload it
     * next time it scans.
     *
     * @param file a file that may have already been scanned.
     */
    public void clear(final File file) {
        scannedDeploymentUnits.remove(file.getAbsolutePath());
    }

    /**
     * Scans for all files and directories that have been added or modified since the
     * last call to scan. This will ignore all files or directories starting with
     * the '.' character.
     *
     * @return Collection of {@link DeploymentUnit}s that describe newly added files or directories.
     */
    public Collection<DeploymentUnit> scan() {
        // Checks to see if we have deleted any of the deployment units.
        final List<File> removedFiles = new ArrayList<>();
        for (final DeploymentUnit unit : scannedDeploymentUnits.values()) {
            if (!unit.getPath().exists() || !unit.getPath().canRead()) {
                removedFiles.add(unit.getPath());
            }
        }
        clear(removedFiles);

        // Checks for new files that don't start in '.'
        final Collection<DeploymentUnit> result = new ArrayList<>();
        final File files[] = pluginsDirectory.listFiles((dir, name) -> !name.startsWith("."));

        if (files == null) {
            log.error("listFiles returned null for directory {}", pluginsDirectory.getAbsolutePath());
            return result;
        }

        Arrays.sort(files); // sorts by filename for deterministic load order
        for (final File file : files) {
            if (isScanned(file) && isModified(file)) {
                clear(file);
                final DeploymentUnit unit = createAndStoreDeploymentUnit(file);
                if (unit != null) result.add(unit);
            } else if (!isScanned(file)) {
                final DeploymentUnit unit = createAndStoreDeploymentUnit(file);
                if (unit != null) result.add(unit);
            }
        }
        return result;
    }

    private boolean isModified(final File file) {
        final DeploymentUnit unit = locateDeploymentUnit(file);
        return file.lastModified() > unit.lastModified();
    }

    private void clear(final List<File> toUndeploy) {
        for (final File aToUndeploy : toUndeploy) {
            clear(aToUndeploy);
        }
    }

    /**
     * Retrieve all the {@link DeploymentUnit}s currently stored.
     *
     * @return the complete unmodifiable list of scanned {@link DeploymentUnit}s.
     */
    public Collection<DeploymentUnit> getDeploymentUnits() {
        return Collections.unmodifiableCollection(scannedDeploymentUnits.values());
    }

    /**
     * Clears the list of scanned deployment units.
     */
    public void reset() {
        scannedDeploymentUnits.clear();
    }

    public void remove(final DeploymentUnit unit) {
        final File file = unit.getPath();
        try {
            // Rather than using File.delete(), which returns a boolean without giving much information on
            // what actually failed when false is returned, use Files.delete(Path) so that exceptions will
            // be thrown, allowing them to be handled accordingly.
            // Note that exceptions, like AccessDeniedException and NoSuchFileException, are documented as
            // "optional specific exceptions" that the underlying FileSystemProvider _can_ throw; they're
            // not mandated by Files.delete(Path). In practice, however, these exceptions tend to be quite
            // reliable.
            Files.delete(file.toPath());
        } catch (final AccessDeniedException e) {
            log.info(
                    "Plugin file <{}> exists but we do not have permission to remove it. Ignoring.",
                    file.getAbsolutePath());
        } catch (final FileNotFoundException | NoSuchFileException e) {
            log.debug("Plugin file <{}> doesn't exist to delete. Ignoring.", file.getAbsolutePath());
        } catch (final IOException e) {
            throw new PluginException("Unable to delete plugin file: " + file.getAbsolutePath());
        }

        clear(file);
    }
}
