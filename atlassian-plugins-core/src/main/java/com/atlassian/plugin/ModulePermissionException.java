package com.atlassian.plugin;

import java.util.HashSet;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;
import static java.util.Objects.requireNonNull;

/**
 * This exception is thrown by {@link ModuleDescriptor module descriptors} when a set of their required permissions is
 * not met by the plugin declaring them.
 *
 * @since 3.0
 */
public final class ModulePermissionException extends PluginException {
    private final String moduleKey;
    private final Set<String> permissions;

    public ModulePermissionException(String moduleKey, Set<String> permissions) {
        super("Could not load module " + moduleKey + ". The plugin is missing the following permissions: "
                + permissions);
        this.moduleKey = requireNonNull(moduleKey);
        this.permissions = unmodifiableSet(new HashSet<>(requireNonNull(permissions)));
    }

    public String getModuleKey() {
        return moduleKey;
    }

    public Set<String> getPermissions() {
        return permissions;
    }
}
