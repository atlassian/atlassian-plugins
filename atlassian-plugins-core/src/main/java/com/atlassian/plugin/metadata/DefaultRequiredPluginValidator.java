package com.atlassian.plugin.metadata;

import java.util.Collection;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.PluginAccessor;

public class DefaultRequiredPluginValidator implements RequiredPluginValidator {
    private static final Logger log = LoggerFactory.getLogger(DefaultRequiredPluginValidator.class);
    private final PluginAccessor pluginAccessor;
    private final RequiredPluginProvider requiredPluginProvider;
    private final Collection<String> errors;

    public DefaultRequiredPluginValidator(
            final PluginAccessor pluginAccessor, final RequiredPluginProvider requiredPluginProvider) {
        this.pluginAccessor = pluginAccessor;
        this.requiredPluginProvider = requiredPluginProvider;
        errors = new HashSet<>();
    }

    @Override
    public Collection<String> validate() {
        for (String key : requiredPluginProvider.getRequiredPluginKeys()) {
            if (!pluginAccessor.isPluginEnabled(key)) {
                log.error("Plugin Not Enabled: {}", key);
                errors.add(key);
            }
        }

        for (String key : requiredPluginProvider.getRequiredModuleKeys()) {
            if (!pluginAccessor.isPluginModuleEnabled(key)) {
                log.error("Plugin Module Not Enabled: {}", key);
                errors.add(key);
            }
        }

        return errors;
    }
}
