package com.atlassian.plugin.impl;

import java.io.InputStream;
import java.net.URL;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.classloader.PluginClassLoader;
import com.atlassian.plugin.loaders.classloading.DeploymentUnit;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A dynamically loaded plugin is loaded through the plugin class loader.
 */
public class DefaultDynamicPlugin extends AbstractPlugin {
    private final PluginClassLoader loader;

    public DefaultDynamicPlugin(final DeploymentUnit deploymentUnit, final PluginClassLoader loader) {
        this(new JarPluginArtifact(deploymentUnit.getPath()), loader);
    }

    public DefaultDynamicPlugin(final PluginArtifact pluginArtifact, final PluginClassLoader loader) {
        super(checkNotNull(pluginArtifact));
        this.loader = checkNotNull(loader);
    }

    public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
        @SuppressWarnings("unchecked")
        final Class<T> result = (Class<T>) loader.loadClass(clazz);
        return result;
    }

    public boolean isUninstallable() {
        return true;
    }

    public URL getResource(final String name) {
        return loader.getResource(name);
    }

    public InputStream getResourceAsStream(final String name) {
        return loader.getResourceAsStream(name);
    }

    public ClassLoader getClassLoader() {
        return loader;
    }

    /**
     * This plugin is dynamically loaded, so returns true.
     *
     * @return true
     */
    public boolean isDynamicallyLoaded() {
        return true;
    }

    public boolean isDeleteable() {
        return true;
    }

    @Override
    protected void uninstallInternal() {
        loader.close();
    }
}
