package com.atlassian.plugin.util.zip;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UrlUnzipper extends AbstractUnzipper {
    private URL zipUrl;

    public UrlUnzipper(URL zipUrl, File destDir) {
        this.zipUrl = zipUrl;
        this.destDir = destDir;
    }

    public void unzip() throws IOException {
        try (ZipInputStream zis = new ZipInputStream(zipUrl.openStream())) {

            ZipEntry zipEntry;
            while ((zipEntry = zis.getNextEntry()) != null) {
                saveEntry(zis, zipEntry);
            }
        }
    }

    public File unzipFileInArchive(String fileName) {
        throw new UnsupportedOperationException("Feature not implemented.");
    }

    public ZipEntry[] entries() throws IOException {
        return entries(new ZipInputStream(zipUrl.openStream()));
    }
}
