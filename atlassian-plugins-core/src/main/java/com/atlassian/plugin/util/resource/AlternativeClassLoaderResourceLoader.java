package com.atlassian.plugin.util.resource;

import java.io.InputStream;
import java.net.URL;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Loads resources from the given class classloader.
 *
 * @since 3.0.0
 */
public final class AlternativeClassLoaderResourceLoader implements AlternativeResourceLoader {
    private final Class<?> clazz;

    public AlternativeClassLoaderResourceLoader() {
        this(AlternativeClassLoaderResourceLoader.class);
    }

    public AlternativeClassLoaderResourceLoader(final Class<?> clazz) {
        this.clazz = checkNotNull(clazz);
    }

    @Override
    public URL getResource(final String path) {
        return clazz.getResource(path);
    }

    @Override
    public InputStream getResourceAsStream(final String name) {
        return clazz.getResourceAsStream(name);
    }
}
