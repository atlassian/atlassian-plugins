package com.atlassian.plugin.util.validation;

import java.util.List;
import java.util.stream.Collectors;

import com.atlassian.plugin.PluginParseException;

import static java.util.Objects.requireNonNull;

/**
 * Exception for a validation error parsing DOM4J nodes
 *
 * @since 2.2.0
 */
public class ValidationException extends PluginParseException {
    private final List<String> errors;

    public ValidationException(String msg, List<String> errors) {
        super(msg);
        this.errors = List.copyOf(requireNonNull(errors));
    }

    public ValidationException(List<String> errors) {
        this("There were validation errors:", errors);
    }

    /**
     * @return a list of the original errors
     */
    public List<String> getErrors() {
        return errors;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + " " + getErrors().stream().collect(Collectors.joining(", ", "[", "]"));
    }
}
