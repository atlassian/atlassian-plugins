package com.atlassian.plugin.util.resource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Splitter;

import com.atlassian.plugin.internal.util.PluginUtils;

import static java.util.Collections.emptyList;

/**
 * Loads resources from directories configured via the system property {@code plugin.resource.directories}, which should
 * be a comma-delimited list of file paths that contain resources to load.
 *
 * @since 2.2.0
 */
public class AlternativeDirectoryResourceLoader implements AlternativeResourceLoader {
    private static final Logger log = LoggerFactory.getLogger(AlternativeDirectoryResourceLoader.class);
    private static final Splitter splitter = Splitter.on(',').trimResults().omitEmptyStrings();

    public static final String PLUGIN_RESOURCE_DIRECTORIES = "plugin.resource.directories";

    private volatile String pluginResourceSystemProperty = "";
    private volatile List<File> resourceDirectories = emptyList();

    public AlternativeDirectoryResourceLoader() {
        getPluginResourceDirs();
    }

    private List<File> getPluginResourceDirs() {
        if (!PluginUtils.isAtlassianDevMode()) {
            return emptyList();
        }

        String dirs = System.getProperty(PLUGIN_RESOURCE_DIRECTORIES, "");
        //
        // has the system property changed since we last read it
        if (!dirs.equals(pluginResourceSystemProperty)) {
            List<File> tmp = new ArrayList<>();
            for (String dir : splitter.split(dirs)) {
                File file = new File(dir);
                if (file.exists()) {
                    log.debug("Found alternative resource directory {}", dir);
                    tmp.add(file);
                } else {
                    log.warn("Resource directory {}, which resolves to {} does not exist", dir, file.getAbsolutePath());
                }
            }
            // caching the system property means we don't do the file lookup every time
            // but we can tell then it changes on next read
            pluginResourceSystemProperty = dirs;
            resourceDirectories = tmp;
        }
        return resourceDirectories;
    }

    /**
     * Retrieve the URL of the resource from the directories.
     *
     * @param path the name of the resource to be loaded
     *
     * @return The URL to the resource, or null if the resource is not found
     */
    public URL getResource(String path) {
        for (File dir : getPluginResourceDirs()) {
            File file = new File(dir, path);
            if (file.exists()) {
                try {
                    return file.toURI().toURL();
                } catch (MalformedURLException e) {
                    log.error("Malformed URL: " + file.toString(), e);
                }
            } else {
                log.debug("File {} not found, ignoring", file);
            }
        }
        return null;
    }

    /**
     * Load a given resource from the directories.
     *
     * @param name The name of the resource to be loaded.
     *
     * @return An InputStream for the resource, or null if the resource is not found.
     */
    public InputStream getResourceAsStream(String name) {
        URL url = getResource(name);
        if (url != null) {
            try {
                return url.openStream();
            } catch (IOException e) {
                log.error("Unable to open URL " + url, e);
            }
        }
        return null;
    }

    /**
     * @return the current list of alternate resource directories
     */
    public List<File> getResourceDirectories() {
        return getPluginResourceDirs();
    }
}
