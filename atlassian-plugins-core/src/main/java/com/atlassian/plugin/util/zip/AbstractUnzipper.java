package com.atlassian.plugin.util.zip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;

import static java.util.stream.Collectors.toMap;

public abstract class AbstractUnzipper implements Unzipper {
    protected static Logger log = LoggerFactory.getLogger(FileUnzipper.class);
    protected File destDir;

    protected File saveEntry(InputStream is, ZipEntry entry) throws IOException {
        final File file = new File(destDir, normaliseAndVerify(entry.getName()));

        if (entry.isDirectory()) {
            file.mkdirs();
        } else {
            final File dir = new File(file.getParent());
            dir.mkdirs();

            try (FileOutputStream fos = new FileOutputStream(file)) {
                IOUtils.copy(is, fos);
                fos.flush();
            } catch (FileNotFoundException fnfe) {
                log.error(
                        "Error extracting a file to '{}{}{}'. This destination is invalid for writing an extracted "
                                + "file stream to.",
                        destDir,
                        File.separator,
                        entry.getName());
                return null;
            }
        }
        file.setLastModified(entry.getTime());

        return file;
    }

    protected ZipEntry[] entries(ZipInputStream zis) throws IOException {
        List<ZipEntry> entries = new ArrayList<>();
        try {
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                entries.add(zipEntry);
                zis.closeEntry();
                zipEntry = zis.getNextEntry();
            }
        } finally {
            IOUtils.closeQuietly(zis);
        }

        return entries.toArray(new ZipEntry[0]);
    }

    public void conditionalUnzip() throws IOException {
        Map<String, Long> zipContentsAndLastModified =
                Arrays.stream(entries()).collect(toMap(ZipEntry::getName, ZipEntry::getTime));

        // If the jar contents of the directory does not match the contents of the zip
        // The we will nuke the bundled plugins directory and re-extract.
        Map<String, Long> targetDirContents = getContentsOfTargetDir(destDir);
        if (!targetDirContents.equals(zipContentsAndLastModified)) {
            // Note: clean, not delete, as destdir may be a symlink (PLUG-606).
            if (destDir.exists()) {
                FileUtils.cleanDirectory(destDir);
            }
            unzip();
        } else {
            log.debug("Target directory contents match zip contents. Do nothing.");
        }
    }

    /**
     * Normalises the supplied path name, by removing all intermediate {@code ..} constructs
     * and ensures that resulting the path name is not a relative path.
     * <p>
     * Examples are:
     * <ul>
     * <li>Passing {@code foo} will return {@code foo}
     * <li>Passing {@code dir/../foo} will return {@code foo}
     * <li>Passing {@code dir/../../foo} will throw an IllegalArgumentException
     * </ul>
     *
     * @param name the name to be normalised and verified
     * @return the normalised name, that is not relative.
     */
    @VisibleForTesting
    static String normaliseAndVerify(String name) {
        final String normalised = FilenameUtils.normalizeNoEndSeparator(name);
        if (StringUtils.isBlank(normalised)) {
            throw new IllegalArgumentException("Path name " + name + " is illegal");
        }
        return normalised;
    }

    private Map<String, Long> getContentsOfTargetDir(File dir) {
        if (!dir.isDirectory()) {
            return Collections.emptyMap();
        }

        File[] files = dir.listFiles();
        if (files == null) {
            return Collections.emptyMap();
        }

        Map<String, Long> targetDirContents = new HashMap<>();
        for (File child : files) {
            if (log.isDebugEnabled()) {
                log.debug("Examining entry in zip: {}", child);
            }
            targetDirContents.put(child.getName(), child.lastModified());
        }

        return targetDirContents;
    }
}
