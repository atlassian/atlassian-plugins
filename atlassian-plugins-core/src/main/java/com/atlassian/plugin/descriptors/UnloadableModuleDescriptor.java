package com.atlassian.plugin.descriptors;

import com.atlassian.plugin.Plugin;

/**
 * Instances of this class represent a module which <i>could not be loaded</i>, not a module
 * which <i>can be unloaded</i>.
 */
public final class UnloadableModuleDescriptor extends AbstractNoOpModuleDescriptor<Void> {

    @Override
    protected void loadClass(Plugin plugin, String clazz) {
        // don't try to load the class -- we are possibly here because it doesn't exist
    }
}
