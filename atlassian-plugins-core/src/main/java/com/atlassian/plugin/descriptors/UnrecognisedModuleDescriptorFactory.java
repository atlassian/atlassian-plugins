package com.atlassian.plugin.descriptors;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;

import static com.atlassian.plugin.descriptors.UnloadableModuleDescriptorFactory.initNoOpModuleDescriptor;

/**
 * Utility class to create UnloadableModuleDescriptor instances when there are problems
 */
public final class UnrecognisedModuleDescriptorFactory {

    /**
     * Creates a new UnrecognisedModuleDescriptor, for when a problem occurs during the retrieval
     * of the ModuleDescriptor itself.
     * <p>
     * This instance has the same information as the original ModuleDescriptor, but also contains
     * an error message that reports the error.
     *
     * @param plugin                  the Plugin the ModuleDescriptor belongs to
     * @param element                 the XML Element used to construct the ModuleDescriptor
     * @param e                       the Throwable
     * @param moduleDescriptorFactory a ModuleDescriptorFactory used to retrieve ModuleDescriptor instances
     * @return a new UnrecognisedModuleDescriptor instance
     * @throws com.atlassian.plugin.PluginParseException if there was a problem constructing the UnrecognisedModuleDescriptor
     */
    public static UnrecognisedModuleDescriptor createUnrecognisedModuleDescriptor(
            final Plugin plugin,
            final com.atlassian.plugin.module.Element element,
            final Throwable e,
            final ModuleDescriptorFactory moduleDescriptorFactory) {
        return initNoOpModuleDescriptor(
                new UnrecognisedModuleDescriptor(), plugin, element, e, moduleDescriptorFactory);
    }
}
