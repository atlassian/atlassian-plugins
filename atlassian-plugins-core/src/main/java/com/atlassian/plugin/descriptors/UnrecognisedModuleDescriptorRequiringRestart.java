package com.atlassian.plugin.descriptors;

@RequiresRestart
public final class UnrecognisedModuleDescriptorRequiringRestart extends UnrecognisedModuleDescriptor {}
