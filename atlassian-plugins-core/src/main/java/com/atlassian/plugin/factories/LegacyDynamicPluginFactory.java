package com.atlassian.plugin.factories;

import java.io.File;
import java.io.InputStream;
import java.util.function.Predicate;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.classloader.PluginClassLoader;
import com.atlassian.plugin.impl.DefaultDynamicPlugin;
import com.atlassian.plugin.internal.parsers.XmlDescriptorParserFactory;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.parsers.DescriptorParser;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.util.Collections.emptySet;

/**
 * Deploys version 1.0 plugins into the legacy custom classloader structure that gives each plugin its own classloader.
 *
 * @since 2.0.0
 */
public final class LegacyDynamicPluginFactory extends AbstractPluginFactory {

    private static final Predicate<Integer> IS_PLUGIN_1 = input -> input != null && input <= Plugin.VERSION_1;

    private final String pluginDescriptorFileName;
    private final File tempDirectory;

    public LegacyDynamicPluginFactory(String pluginDescriptorFileName) {
        this(
                pluginDescriptorFileName,
                new File(System.getProperty("java.io.tmpdir")),
                new XmlDescriptorParserFactory());
    }

    public LegacyDynamicPluginFactory(String pluginDescriptorFileName, File tempDirectory) {
        this(pluginDescriptorFileName, tempDirectory, new XmlDescriptorParserFactory());
    }

    public LegacyDynamicPluginFactory(
            String pluginDescriptorFileName,
            File tempDirectory,
            XmlDescriptorParserFactory xmlDescriptorParserFactory) {
        super(xmlDescriptorParserFactory, emptySet());
        this.tempDirectory = checkNotNull(tempDirectory);
        this.pluginDescriptorFileName = checkNotNull(pluginDescriptorFileName);
        checkState(StringUtils.isNotBlank(pluginDescriptorFileName), "Plugin descriptor name cannot be null or blank");
    }

    @Override
    protected InputStream getDescriptorInputStream(PluginArtifact pluginArtifact) {
        return pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
    }

    @Override
    protected Predicate<Integer> isValidPluginsVersion() {
        return IS_PLUGIN_1;
    }

    /**
     * Deploys the plugin artifact
     *
     * @param pluginArtifact          the plugin artifact to deploy
     * @param moduleDescriptorFactory The factory for plugin modules
     * @return The instantiated and populated plugin
     * @throws PluginParseException If the descriptor cannot be parsed
     * @since 2.2.0
     */
    public Plugin create(PluginArtifact pluginArtifact, ModuleDescriptorFactory moduleDescriptorFactory) {
        checkNotNull(pluginArtifact, "The deployment unit must not be null");
        checkNotNull(moduleDescriptorFactory, "The module descriptor factory must not be null");

        final File file = pluginArtifact.toFile();
        Plugin plugin = null;
        InputStream pluginDescriptor = null;
        PluginClassLoader loader = null;
        try {
            pluginDescriptor = pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
            if (pluginDescriptor == null) {
                throw new PluginParseException("No descriptor found in classloader for : " + file);
            }

            // The plugin we get back may not be the same (in the case of an UnloadablePlugin), so add what gets
            // returned, rather than the original
            DescriptorParser parser = descriptorParserFactory.getInstance(pluginDescriptor, emptySet());
            loader = new PluginClassLoader(file, Thread.currentThread().getContextClassLoader(), tempDirectory);
            plugin = parser.configurePlugin(moduleDescriptorFactory, createPlugin(pluginArtifact, loader));
        }
        // Under normal conditions, the deployer would be closed when the plugins are undeployed. However,
        // these are not normal conditions, so we need to make sure that we close them explicitly.
        catch (PluginParseException e) {
            if (loader != null) loader.close();
            throw e;
        } catch (RuntimeException e) {
            if (loader != null) loader.close();
            throw new PluginParseException(e);
        } catch (Error e) {
            if (loader != null) loader.close();
            throw e;
        } finally {
            IOUtils.closeQuietly(pluginDescriptor);
        }
        return plugin;
    }

    @Override
    public ModuleDescriptor<?> createModule(
            final Plugin plugin, final Element module, final ModuleDescriptorFactory moduleDescriptorFactory) {
        if (plugin instanceof DefaultDynamicPlugin) {
            InputStream pluginDescriptor = null;
            try {
                final PluginArtifact pluginArtifact = plugin.getPluginArtifact();
                pluginDescriptor = pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
                final DescriptorParser parser = descriptorParserFactory.getInstance(pluginDescriptor, emptySet());

                return parser.addModule(moduleDescriptorFactory, plugin, module);
            } finally {
                IOUtils.closeQuietly(pluginDescriptor);
            }
        }
        return null;
    }

    /**
     * Creates the plugin. Override to use a different Plugin class
     *
     * @param pluginArtifact The plugin artifact
     * @param loader         The plugin loader
     * @return The plugin instance
     * @since 2.2.0
     */
    protected Plugin createPlugin(PluginArtifact pluginArtifact, PluginClassLoader loader) {
        return new DefaultDynamicPlugin(pluginArtifact, loader);
    }
}
