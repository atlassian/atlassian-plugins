package com.atlassian.plugin.classloader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;

/**
 * A class loader that delegates to another class loader.
 */
public class DelegationClassLoader extends ClassLoader {
    private static final Logger log = LoggerFactory.getLogger(DelegationClassLoader.class);

    private ClassLoader delegateClassLoader = DelegationClassLoader.class.getClassLoader();

    public void setDelegateClassLoader(ClassLoader delegateClassLoader) {
        this.delegateClassLoader = requireNonNull(delegateClassLoader, "Can't set the delegation target to null");
        if (log.isDebugEnabled()) {
            log.debug(
                    "Update class loader delegation from [{}] to [{}]", this.delegateClassLoader, delegateClassLoader);
        }
    }

    @Override
    public Class loadClass(String name) throws ClassNotFoundException {
        return delegateClassLoader.loadClass(name);
    }

    @Override
    public URL getResource(String name) {
        return delegateClassLoader.getResource(name);
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        return delegateClassLoader.getResources(name);
    }

    @Override
    public InputStream getResourceAsStream(String name) {
        return delegateClassLoader.getResourceAsStream(name);
    }

    @Override
    public synchronized void setDefaultAssertionStatus(boolean enabled) {
        delegateClassLoader.setDefaultAssertionStatus(enabled);
    }

    @Override
    public synchronized void setPackageAssertionStatus(String packageName, boolean enabled) {
        delegateClassLoader.setPackageAssertionStatus(packageName, enabled);
    }

    @Override
    public synchronized void setClassAssertionStatus(String className, boolean enabled) {
        delegateClassLoader.setClassAssertionStatus(className, enabled);
    }

    @Override
    public synchronized void clearAssertionStatus() {
        delegateClassLoader.clearAssertionStatus();
    }
}
