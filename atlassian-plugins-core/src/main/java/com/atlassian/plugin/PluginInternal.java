package com.atlassian.plugin;

/**
 * Methods that are necessary for the plugin framework, however should not be exposed via public API.
 *
 * @since v4.0
 */
public interface PluginInternal extends Plugin {
    /**
     * Alters whether the plugin is a "bundled" plugin that can't be removed.
     *
     * @param bundledPlugin {@code true} if this plugin is a "bundled" plugin.
     * @since 4.0
     */
    void setBundledPlugin(final boolean bundledPlugin);

    /**
     * Add a module descriptor.
     *
     * @param module to add
     * @return true if added, false if already present
     * @since 4.0
     */
    boolean addDynamicModuleDescriptor(final ModuleDescriptor<?> module);

    /**
     * @return current set of dynamic module descriptors.
     * @since 4.0
     */
    Iterable<ModuleDescriptor<?>> getDynamicModuleDescriptors();

    /**
     * Remove a module descriptor.
     *
     * @param module to remove
     * @return true if removed, false if not present
     * @since 4.0
     */
    boolean removeDynamicModuleDescriptor(final ModuleDescriptor<?> module);
}
