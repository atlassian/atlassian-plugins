package com.atlassian.plugin.descriptors;

import org.junit.Test;

import com.atlassian.plugin.ModuleDescriptor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Responsible for testing that the <code>equals</code> contract defined by
 * {@link com.atlassian.plugin.ModuleDescriptor#equals(Object)} is fulfilled by the
 * {@link ModuleDescriptors.EqualsBuilder} class.
 *
 * @since 2.8.0
 */
public class TestModuleDescriptorsEqualsBuilder {

    @Test
    public void testAModuleDescriptorMustNotBeEqualToNull() {
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);

        assertFalse(new ModuleDescriptors.EqualsBuilder().descriptor(descriptor).isEqualTo(null));
    }

    @Test
    public void testModuleDescriptorsShouldBeEqualIfTheyHaveTheSameCompleteKey() {
        final ModuleDescriptor descriptor1 = mock(ModuleDescriptor.class);
        final ModuleDescriptor descriptor2 = mock(ModuleDescriptor.class);

        when(descriptor1.getCompleteKey()).thenReturn("abc:xyz");
        when(descriptor2.getCompleteKey()).thenReturn("abc:xyz");

        assertTrue(new ModuleDescriptors.EqualsBuilder().descriptor(descriptor1).isEqualTo(descriptor2));
    }

    @Test
    public void testModuleDescriptorsShouldBeUnEqualIfTheyDoNotHaveTheSameCompleteKey() {
        final ModuleDescriptor descriptor1 = mock(ModuleDescriptor.class);
        final ModuleDescriptor descriptor2 = mock(ModuleDescriptor.class);

        when(descriptor1.getCompleteKey()).thenReturn("abc:xyz");
        when(descriptor2.getCompleteKey()).thenReturn("def:xyz");

        assertFalse(
                new ModuleDescriptors.EqualsBuilder().descriptor(descriptor1).isEqualTo(descriptor2));
    }

    @Test
    public void testAModuleDescriptorMustNotBeEqualToAnObjectThatIsNotAModuleDescriptor() {
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        final Object anObjectThatIsNotAModuleDescriptor = new Object();

        assertFalse(new ModuleDescriptors.EqualsBuilder()
                .descriptor(descriptor)
                .isEqualTo(anObjectThatIsNotAModuleDescriptor));
    }

    @Test
    public void testAModuleDescriptorShouldBeEqualToItSelf() {
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);

        assertTrue(new ModuleDescriptors.EqualsBuilder().descriptor(descriptor).isEqualTo(descriptor));
    }

    @Test
    public void testEqualsImplementationIsReflexiveForTwoModuleDescriptorsWithTheSameCompleteKey() {
        final ModuleDescriptor descriptor1 = mock(ModuleDescriptor.class);
        final ModuleDescriptor descriptor2 = mock(ModuleDescriptor.class);

        when(descriptor1.getCompleteKey()).thenReturn("abc:xyz");
        when(descriptor2.getCompleteKey()).thenReturn("abc:xyz");

        assertTrue(new ModuleDescriptors.EqualsBuilder().descriptor(descriptor1).isEqualTo(descriptor2));
        assertTrue(new ModuleDescriptors.EqualsBuilder().descriptor(descriptor2).isEqualTo(descriptor1));
    }

    @Test
    public void testEqualsImplementationIsTransitiveForThreeDescriptorsWithTheSameCompleteKey() {
        final ModuleDescriptor descriptor1 = mock(ModuleDescriptor.class);
        final ModuleDescriptor descriptor2 = mock(ModuleDescriptor.class);
        final ModuleDescriptor descriptor3 = mock(ModuleDescriptor.class);

        when(descriptor1.getCompleteKey()).thenReturn("abc:xyz");
        when(descriptor2.getCompleteKey()).thenReturn("abc:xyz");
        when(descriptor3.getCompleteKey()).thenReturn("abc:xyz");

        assertTrue(new ModuleDescriptors.EqualsBuilder().descriptor(descriptor1).isEqualTo(descriptor2));
        assertTrue(new ModuleDescriptors.EqualsBuilder().descriptor(descriptor2).isEqualTo(descriptor3));
        assertTrue(new ModuleDescriptors.EqualsBuilder().descriptor(descriptor1).isEqualTo(descriptor3));
    }
}
