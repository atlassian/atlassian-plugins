package com.atlassian.plugin.descriptors;

import org.junit.Test;

import com.atlassian.plugin.ModuleDescriptor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Responsible for testing that the <code>hashCode</code> contract defined by
 * {@link com.atlassian.plugin.ModuleDescriptor#hashCode()} is fulfilled by the
 * {@link ModuleDescriptors.HashCodeBuilder} class.
 *
 * @since 2.8.0
 */
public class TestModuleDescriptorsHashCodeBuilder {

    @Test
    public void testTheHashCodeOfADescriptorWithANullCompleteKeyShouldBeZero() {
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        when(descriptor.getCompleteKey()).thenReturn(null);

        assertEquals(
                0,
                new ModuleDescriptors.HashCodeBuilder().descriptor(descriptor).toHashCode());
    }

    @Test
    public void testTheHashCodeOfADescriptorWithANonNullCompleteKeyIsEqualToTheHashCodeOfTheCompleteKey() {
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        when(descriptor.getCompleteKey()).thenReturn("test-plugin:test-key");

        assertEquals(
                new ModuleDescriptors.HashCodeBuilder().descriptor(descriptor).toHashCode(),
                descriptor.getCompleteKey().hashCode());
    }
}
