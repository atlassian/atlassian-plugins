package com.atlassian.plugin.internal.module;

import java.util.Collections;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Element;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Dom4jDelegatingElementTest {

    private final Element dom4jElement;
    private final Element childDom4jElement;
    private final Attribute attribute;
    private final Dom4jDelegatingElement elementWithDom4j;

    public Dom4jDelegatingElementTest() {
        dom4jElement = mock(Element.class);
        childDom4jElement = mock(Element.class);
        attribute = mock(Attribute.class);
        elementWithDom4j = new Dom4jDelegatingElement(dom4jElement);
    }

    @Test
    public void givenGetAttributeValueWithKey_whenDom4jElementIsNotNull_thenReturnsAttributeValueFromDom4JElement() {
        when(dom4jElement.attributeValue("key")).thenReturn("value");

        String attributeValue = elementWithDom4j.attributeValue("key");

        assertEquals("value", attributeValue);
    }

    @Test
    public void givenGetAttributeValueWithKey_whenDom4jElementIsNull_thenReturnNull() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        String attributeValue = elementWithNullDelegate.attributeValue("key");

        assertNull(attributeValue);
    }

    @Test
    public void
            givenGetAttributeValueWithKeyAndDefaultValuewhenDom4jElementIsNotNull_thenReturnsAttributeValueFromDom4JElementNotDefault() {
        when(dom4jElement.attributeValue("key")).thenReturn("value");

        String attributeValue = elementWithDom4j.attributeValue("key", "defaultValue");

        assertEquals("value", attributeValue);
    }

    @Test
    public void givenGetAttributeValueWithKeyAndDefaultValuewhenDom4jElementIsNull_thenReturnDefaultValue() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        String attributeValue = elementWithNullDelegate.attributeValue("key", "defaultValue");

        assertEquals("defaultValue", attributeValue);
    }

    @Test
    public void givenGetAttributeValueWithKeyAndDefaultValue_whenDom4jElementReturnsNull_thenReturnDefaultValue() {
        String attributeValue = elementWithDom4j.attributeValue("key", "defaultValue");

        assertEquals("defaultValue", attributeValue);
    }

    @Test
    public void givenGetElementByName_whenDom4jElementIsNotNull_thenReturnsElementFromDom4jWrapped() {
        when(dom4jElement.element("name")).thenReturn(childDom4jElement);
        when(childDom4jElement.getName()).thenReturn("name");

        com.atlassian.plugin.module.Element child = elementWithDom4j.element("name");

        assertEquals("name", child.getName());
    }

    @Test
    public void givenGetElementByName_whenDom4jElementIsNull_thenReturnNullElement() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        com.atlassian.plugin.module.Element child = elementWithNullDelegate.element("name");

        assertNull(child);
    }

    @Test
    public void givenGetElementByName_whenDom4jElementReturnsNullElement_thenReturnNullElement() {
        com.atlassian.plugin.module.Element child = elementWithDom4j.element("name");

        assertNull(child);
    }

    @Test
    public void givenGetElementsByName_whenDom4jElementIsNotNull_thenReturnsElementsFromDom4j() {
        when(dom4jElement.elements("name")).thenReturn(Collections.singletonList(childDom4jElement));

        List<com.atlassian.plugin.module.Element> children = elementWithDom4j.elements("name");

        assertFalse(children.isEmpty());
        assertEquals(1, children.size());
    }

    @Test
    public void givenGetElementsByName_whenDom4jElementIsNull_thenReturnEmptyListForElementsWithName() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        List<com.atlassian.plugin.module.Element> children = elementWithNullDelegate.elements("name");

        assertNotNull(children);
        assertTrue(children.isEmpty());
    }

    @Test
    public void givenGetTextTrim_whenDom4jElementIsNotNull_thenReturnsTextTrimFromDom4JElement() {
        when(dom4jElement.getTextTrim()).thenReturn("trimmed");

        String textTrim = elementWithDom4j.getTextTrim();

        assertEquals("trimmed", textTrim);
    }

    @Test
    public void givenGetTextTrim_whenDom4jElementIsNull_thenReturnsEmptyString() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        String textTrim = elementWithNullDelegate.getTextTrim();

        assertNotNull(textTrim);
        assertTrue(textTrim.isEmpty());
    }

    @Test
    public void givenGetElementTextTrimByName_whenDom4jElementIsNotNull_thenReturnsTextTrimFromDom4JElement() {
        when(dom4jElement.elementTextTrim("name")).thenReturn("trimmed");

        String textTrim = elementWithDom4j.elementTextTrim("name");

        assertEquals("trimmed", textTrim);
    }

    @Test
    public void givenGetElementTextTrimByName_whenDom4jElementIsNull_thenReturnsNull() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        String textTrim = elementWithNullDelegate.elementTextTrim("name");

        assertNull(textTrim);
    }

    @Test
    public void givenGetElementText_whenDom4jElementIsNotNull_thenReturnsTextTrimFromDom4JElement() {
        when(dom4jElement.getText()).thenReturn("text");

        String textTrim = elementWithDom4j.getText();

        assertEquals("text", textTrim);
    }

    @Test
    public void givenGetElementText_whenDom4jElementIsNull_thenReturnsEmptyString() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        String textTrim = elementWithNullDelegate.getText();

        assertNotNull(textTrim);
        assertTrue(textTrim.isEmpty());
    }

    @Test
    public void givenGetName_whenDom4jElementIsNotNull_thenReturnsNameFromDom4JElement() {
        when(dom4jElement.getName()).thenReturn("name");

        String name = elementWithDom4j.getName();

        assertEquals("name", name);
    }

    @Test
    public void givenGetName_whenDom4jElementIsNull_thenReturnsNull() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        String name = elementWithNullDelegate.getName();

        assertNull(name);
    }

    @Test
    public void givenGetAllElements_whenDom4jElementIsNotNull_thenReturnsNameFromDom4JElement() {
        when(dom4jElement.elements()).thenReturn(Collections.singletonList(childDom4jElement));

        List<com.atlassian.plugin.module.Element> children = elementWithDom4j.elements();

        assertFalse(children.isEmpty());
        assertEquals(1, children.size());
    }

    @Test
    public void givenGetAllElements_whenDom4jElementIsNull_thenReturnsEmptyList() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        List<com.atlassian.plugin.module.Element> children = elementWithNullDelegate.elements();

        assertNotNull(children);
        assertTrue(children.isEmpty());
    }

    @Test
    public void givenGetAttributeNames_whenDom4jElementIsNotNull_thenReturnsAttributeNamesFromDom4JElement() {
        when(attribute.getName()).thenReturn("attributeName");
        when(dom4jElement.attributes()).thenReturn(Collections.singletonList(attribute));

        List<String> attributeNames = elementWithDom4j.attributeNames();

        assertFalse(attributeNames.isEmpty());
        assertEquals(1, attributeNames.size());
        assertEquals("attributeName", attributeNames.get(0));
    }

    @Test
    public void givenGetAttributeNames_whenDom4jElementIsNull_thenReturnsEmptyList() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);

        List<String> attributeNames = elementWithNullDelegate.attributeNames();

        assertNotNull(attributeNames);
        assertTrue(attributeNames.isEmpty());
    }

    @Test
    public void givenGetDelegate_whenDom4jElementIsNull_thenReturnsNull() {
        Dom4jDelegatingElement elementWithNullDelegate = new Dom4jDelegatingElement(null);
        Element delegate = elementWithNullDelegate.getDelegate();
        assertNull(delegate);
    }

    @Test
    public void givenGetDelegate_whenDom4jElementIsNotNull_thenReturnsSameElement() {
        Element delegate = elementWithDom4j.getDelegate();
        assertSame(dom4jElement, delegate);
    }
}
