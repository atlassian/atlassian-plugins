package com.atlassian.plugin.factories;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.dom4j.Element;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.impl.DefaultDynamicPlugin;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.internal.parsers.XmlDescriptorParserFactory;
import com.atlassian.plugin.parsers.DescriptorParser;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestLegacyDynamicPluginFactory {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    private LegacyDynamicPluginFactory legacyDynamicPluginFactory;

    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;

    @Mock
    private XmlDescriptorParserFactory xmlDescriptorParserFactory;

    @Mock
    private Element module;

    @Mock
    private PluginArtifact pluginArtifact;

    @Mock
    private InputStream pluginDescriptor;

    @Mock
    private DescriptorParser descriptorParser;

    @Mock
    private ModuleDescriptor moduleDescriptor;

    @Before
    public void before() {
        legacyDynamicPluginFactory = new LegacyDynamicPluginFactory(
                PluginAccessor.Descriptor.FILENAME,
                new File(System.getProperty("java.io.tmpdir")),
                xmlDescriptorParserFactory);
    }

    @Test
    public void createCorruptJar() {
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        when(pluginArtifact.getResourceAsStream(anyString())).thenReturn(null);
        when(pluginArtifact.toFile()).thenReturn(new File("sadfasdf"));

        expectedException.expect(PluginParseException.class);

        legacyDynamicPluginFactory.create(pluginArtifact, moduleDescriptorFactory);
    }

    @Test
    public void createModule() throws IOException {
        final DefaultDynamicPlugin plugin = mock(DefaultDynamicPlugin.class);
        com.atlassian.plugin.module.Element element = mock(com.atlassian.plugin.module.Element.class);
        when(plugin.getPluginArtifact()).thenReturn(pluginArtifact);
        when(pluginArtifact.getResourceAsStream(PluginAccessor.Descriptor.FILENAME))
                .thenReturn(pluginDescriptor);
        when(xmlDescriptorParserFactory.getInstance(pluginDescriptor, ImmutableSet.of()))
                .thenReturn(descriptorParser);
        when(descriptorParser.addModule(moduleDescriptorFactory, plugin, element))
                .thenReturn(moduleDescriptor);

        assertThat(
                legacyDynamicPluginFactory.createModule(plugin, element, moduleDescriptorFactory),
                is(moduleDescriptor));

        verify(pluginDescriptor).close();
    }

    @Test
    public void creatModuleIncorrectPluginType() {
        final Plugin plugin = mock(Plugin.class);

        assertThat(
                legacyDynamicPluginFactory.createModule(
                        plugin, new Dom4jDelegatingElement(module), moduleDescriptorFactory),
                nullValue());
    }
}
