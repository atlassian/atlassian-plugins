package com.atlassian.plugin;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @since 2.3.0
 */
public class MockPluginAccessor implements PluginAccessor {
    private Collection<Plugin> allPlugins = new ArrayList<>();

    public Collection<Plugin> getPlugins() {
        return allPlugins;
    }

    public void addPlugin(Plugin plugin) {
        allPlugins.add(plugin);
    }

    public Collection<Plugin> getPlugins(final Predicate<Plugin> predicate) {
        throw new UnsupportedOperationException();
    }

    public Collection<Plugin> getEnabledPlugins() {
        return allPlugins.stream()
                .filter(plugin -> plugin.getPluginState() == PluginState.ENABLED)
                .collect(Collectors.toList());
    }

    public <M> Collection<M> getModules(final Predicate<ModuleDescriptor<M>> predicate) {
        throw new UnsupportedOperationException();
    }

    public <M> Collection<ModuleDescriptor<M>> getModuleDescriptors(final Predicate<ModuleDescriptor<M>> predicate) {
        throw new UnsupportedOperationException();
    }

    public Plugin getPlugin(final String key) {
        for (Plugin plugin : allPlugins) {
            if (key.equals(plugin.getKey())) {
                return plugin;
            }
        }
        return null;
    }

    public Plugin getEnabledPlugin(final String pluginKey) {
        throw new UnsupportedOperationException();
    }

    public ModuleDescriptor<?> getPluginModule(final String completeKey) {
        throw new UnsupportedOperationException();
    }

    public ModuleDescriptor<?> getEnabledPluginModule(final String completeKey) {
        throw new UnsupportedOperationException();
    }

    public boolean isPluginEnabled(final String key) {
        return getPlugin(key).getPluginState() == PluginState.ENABLED;
    }

    public boolean isPluginModuleEnabled(final String completeKey) {
        return false;
    }

    public <M> List<M> getEnabledModulesByClass(final Class<M> moduleClass) {
        throw new UnsupportedOperationException();
    }

    public <M> List<M> getEnabledModulesByClassAndDescriptor(
            final Class<ModuleDescriptor<M>>[] descriptorClazz, final Class<M> moduleClass) {
        throw new UnsupportedOperationException();
    }

    public <M> List<M> getEnabledModulesByClassAndDescriptor(
            final Class<ModuleDescriptor<M>> descriptorClass, final Class<M> moduleClass) {
        throw new UnsupportedOperationException();
    }

    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <D extends ModuleDescriptor<?>> List<D> getActiveModuleDescriptorsByClass(Class<D> descriptorClazz) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public <M> List<ModuleDescriptor<M>> getEnabledModuleDescriptorsByType(final String type) {
        throw new UnsupportedOperationException();
    }

    public InputStream getDynamicResourceAsStream(final String resourcePath) {
        throw new UnsupportedOperationException();
    }

    public InputStream getPluginResourceAsStream(final String pluginKey, final String resourcePath) {
        throw new UnsupportedOperationException();
    }

    public Class<?> getDynamicPluginClass(final String className) {
        throw new UnsupportedOperationException();
    }

    public ClassLoader getClassLoader() {
        throw new UnsupportedOperationException();
    }

    public boolean isSystemPlugin(final String key) {
        return false;
    }

    public PluginRestartState getPluginRestartState(final String key) {
        throw new UnsupportedOperationException();
    }

    public Iterable<ModuleDescriptor<?>> getDynamicModules(final Plugin plugin) {
        throw new UnsupportedOperationException();
    }
}
