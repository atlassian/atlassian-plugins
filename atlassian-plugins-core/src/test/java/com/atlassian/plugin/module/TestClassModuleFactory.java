package com.atlassian.plugin.module;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.hostcontainer.HostContainer;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestClassModuleFactory {

    private ModuleFactory moduleCreator;

    @Mock
    private ModuleDescriptor<Object> moduleDescriptor;

    private HostContainer hostContainer;

    @Before
    public void setUp() throws Exception {
        hostContainer = mock(HostContainer.class);
        moduleCreator = new ClassPrefixModuleFactory(hostContainer);
    }

    @Test
    public void testCreateBeanUsingHostContainer() throws Exception {
        final Plugin plugin = mock(Plugin.class);
        when(plugin.loadClass(eq("myBean"), any())).thenReturn(Object.class);

        when(moduleDescriptor.getPlugin()).thenReturn(plugin);
        final Object object = new Object();
        when(hostContainer.create(Object.class)).thenReturn(object);

        final Object bean = moduleCreator.createModule("myBean", moduleDescriptor);
        assertEquals(object, bean);
    }

    @Test
    public void testCreateBeanUsingPluginContainer() {
        final ContainerAccessor containerAccessor = mock(ContainerAccessor.class);
        final Plugin plugin = new MockContainerManagedPlugin(containerAccessor);

        when(moduleDescriptor.getPlugin()).thenReturn(plugin);
        final Object beanObject = new Object();
        when(containerAccessor.createBean(Object.class)).thenReturn(beanObject);
        final Object bean = moduleCreator.createModule("java.lang.Object", moduleDescriptor);
        assertEquals(beanObject, bean);
    }
}
