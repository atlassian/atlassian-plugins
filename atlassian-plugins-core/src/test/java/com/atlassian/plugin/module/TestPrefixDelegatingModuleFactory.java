package com.atlassian.plugin.module;

import java.util.Collections;
import java.util.function.Consumer;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestPrefixDelegatingModuleFactory {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    private ModuleDescriptor<Object> moduleDescriptor;

    @Test
    public void testCreateBean() {
        PrefixModuleFactory moduleFactory = mock(PrefixModuleFactory.class);
        when(moduleFactory.getPrefix()).thenReturn("jira");
        Object bean = new Object();
        when(moduleFactory.createModule("doSomething", moduleDescriptor)).thenReturn(bean);

        Object returnedBean = new PrefixDelegatingModuleFactory(Collections.singleton(moduleFactory))
                .createModule("jira:doSomething", moduleDescriptor);

        assertEquals(bean, returnedBean);
    }

    @Test
    public void testCreateBeanWithDynamicModuleFactory() {
        PrefixModuleFactory moduleFactory = mock(PrefixModuleFactory.class);
        when(moduleFactory.getPrefix()).thenReturn("jira");

        Object bean = new Object();
        ContainerAccessor containerAccessor = mock(ContainerAccessor.class);
        ContainerManagedPlugin plugin = mock(ContainerManagedPlugin.class);
        when(plugin.getContainerAccessor()).thenReturn(containerAccessor);
        when(moduleDescriptor.getPlugin()).thenReturn(plugin);
        when(containerAccessor.getBeansOfType(PrefixModuleFactory.class))
                .thenReturn(Collections.singleton(moduleFactory));
        when(moduleFactory.createModule("doSomething", moduleDescriptor)).thenReturn(bean);

        Object returnedBean = new PrefixDelegatingModuleFactory(Collections.emptySet())
                .createModule("jira:doSomething", moduleDescriptor);

        assertEquals(bean, returnedBean);
    }

    @Test
    public void testCreateBeanThrowsNoClassDefFoundError() {
        _testCreateWithThrowableCausingErrorLogMessage(
                new NoClassDefFoundError(), log -> verify(log).error(anyString(), any(), any(), any()));
    }

    @Test
    public void testCreateBeanThrowsUnsatisfiedDependencyException() {
        _testCreateWithThrowableCausingErrorLogMessage(
                new UnsatisfiedDependencyException(), log -> verify(log).error(anyString()));
    }

    @Test
    public void testCreateBeanThrowsLinkageError() {
        _testCreateWithThrowableCausingErrorLogMessage(
                new LinkageError(), log -> verify(log).error(anyString(), any(), any(), any()));
    }

    @Test
    public void testCreateBeanFailed() {
        PrefixModuleFactory moduleFactory = mock(PrefixModuleFactory.class);
        when(moduleFactory.getPrefix()).thenReturn("bob");
        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage("Failed to create a module. Prefix 'jira' not supported");

        new PrefixDelegatingModuleFactory(Collections.singleton(moduleFactory))
                .createModule("jira:doSomething", moduleDescriptor);

        verify(moduleFactory, never()).createModule("doSomething", moduleDescriptor);
    }

    private void _testCreateWithThrowableCausingErrorLogMessage(Throwable throwable, Consumer<Logger> logVerifier) {
        PrefixModuleFactory moduleFactory = mock(PrefixModuleFactory.class);
        when(moduleFactory.getPrefix()).thenReturn("jira");
        Logger log = mock(Logger.class);

        Plugin plugin = mock(Plugin.class);
        when(moduleDescriptor.getPlugin()).thenReturn(plugin);
        when(moduleFactory.createModule("doSomething", moduleDescriptor)).thenThrow(throwable);

        PrefixDelegatingModuleFactory prefixDelegatingModuleFactory =
                new PrefixDelegatingModuleFactory(Collections.singleton(moduleFactory));
        prefixDelegatingModuleFactory.log = log;
        expectedException.expect(equalTo(throwable));

        try {
            prefixDelegatingModuleFactory.createModule("jira:doSomething", moduleDescriptor);
        } finally {
            logVerifier.accept(log);
        }
    }

    private static class UnsatisfiedDependencyException extends RuntimeException {}
}
