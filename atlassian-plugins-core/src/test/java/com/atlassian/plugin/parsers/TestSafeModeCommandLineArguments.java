package com.atlassian.plugin.parsers;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import com.google.common.collect.ImmutableList;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugin.parsers.SafeModeCommandLineArguments.PLUGIN_LIST_SEPARATOR;

public class TestSafeModeCommandLineArguments {
    private String testDisablePluginParameterLinux = "--disable-addons=";
    private String testDisablePluginParameterWindows = "/disableaddons=";
    private String pluginKeyWithSlashes = "com.atlassian.test.plugin/adsfasdf";
    private String pluginKeyWithSpaces = "asdf.asdf adsfasdf";
    private String ordinaryPluginKey = "com.atlassian.test.plugin";
    private String pluginKeyWithEmojis = "asdf.🙈.plugin";
    private String pluginKeyWithKanji = "佀佁佂佃佄佅但佇佈佉佊佋佌位低住";

    @Test
    public void disablePluginParameterContainingKanjiShouldDisablePlugin() {
        testDisablePlugins(singletonList(pluginKeyWithKanji), testDisablePluginParameterWindows, false);
        testDisablePlugins(singletonList(pluginKeyWithKanji), testDisablePluginParameterLinux, false);
    }

    @Test
    public void disablePluginParameterContainingSlashesShouldDisablePlugin() {
        testDisablePlugins(singletonList(pluginKeyWithSlashes), testDisablePluginParameterWindows, false);
        testDisablePlugins(singletonList(pluginKeyWithSlashes), testDisablePluginParameterLinux, false);
    }

    @Test
    public void disablePluginParameterContainingEmojisShouldDisablePlugin() {
        testDisablePlugins(singletonList(pluginKeyWithEmojis), testDisablePluginParameterWindows, false);
        testDisablePlugins(singletonList(pluginKeyWithEmojis), testDisablePluginParameterLinux, false);
    }

    @Test
    public void disablePluginParameterContainingSpacesAndQuotesShouldDisablePlugin() {
        testDisablePlugins(singletonList(pluginKeyWithSpaces), testDisablePluginParameterWindows, true);
        testDisablePlugins(singletonList(pluginKeyWithSpaces), testDisablePluginParameterLinux, true);
    }

    @Test
    public void disablePluginParameterWithJustASCIICharactersShouldDisablePluginWithQuotes() {
        testDisablePlugins(singletonList(ordinaryPluginKey), testDisablePluginParameterWindows, true);
        testDisablePlugins(singletonList(ordinaryPluginKey), testDisablePluginParameterLinux, true);
    }

    @Test
    public void disableMultiplePluginsWithQuotesShouldDisablePlugin() {
        List<String> plugins = ImmutableList.of(ordinaryPluginKey, pluginKeyWithSlashes, pluginKeyWithSpaces);
        testDisablePlugins(plugins, testDisablePluginParameterWindows, true);
        testDisablePlugins(plugins, testDisablePluginParameterLinux, true);
    }

    @Test
    public void disablePluginParameterWithJustASCIICharactersShouldDisablePlugin() {
        testDisablePlugins(singletonList(ordinaryPluginKey), testDisablePluginParameterWindows, false);
        testDisablePlugins(singletonList(ordinaryPluginKey), testDisablePluginParameterLinux, false);
    }

    @Test
    public void disableParameterWithNoArgumentsPresent() {
        SafeModeCommandLineArguments safeModeCommandLineArguments =
                new SafeModeCommandLineArguments("--disable-addons");
        assertEquals(
                Collections.emptyList(),
                safeModeCommandLineArguments.getDisabledPlugins().orElse(null));
        assertFalse(safeModeCommandLineArguments.isSafeMode());
    }

    @Test
    public void disableParameterWithEqualsSignNoArgumentsPresent() {
        SafeModeCommandLineArguments safeModeCommandLineArguments =
                new SafeModeCommandLineArguments("--disable-addons=");
        assertEquals(
                Collections.emptyList(),
                safeModeCommandLineArguments.getDisabledPlugins().orElse(null));
        assertFalse(safeModeCommandLineArguments.isSafeMode());
    }

    @Test
    public void noParametersPresent() {
        SafeModeCommandLineArguments safeModeCommandLineArguments = new SafeModeCommandLineArguments("");
        assertFalse(safeModeCommandLineArguments.getDisabledPlugins().isPresent());
        assertFalse(safeModeCommandLineArguments.isSafeMode());
        assertFalse(safeModeCommandLineArguments.shouldLastEnabledBeDisabled());
    }

    @Test
    public void disableLastEnabledParameterPresentWindows() {
        SafeModeCommandLineArguments safeModeCommandLineArguments =
                new SafeModeCommandLineArguments("/disablelastenabled");
        assertTrue(safeModeCommandLineArguments.shouldLastEnabledBeDisabled());
    }

    @Test
    public void disableLastEnabledParameterPresentLinux() {
        SafeModeCommandLineArguments safeModeCommandLineArguments =
                new SafeModeCommandLineArguments("--disable-last-enabled");
        assertTrue(safeModeCommandLineArguments.shouldLastEnabledBeDisabled());
    }

    private void testDisablePlugins(List<String> plugins, String testParameter, boolean quotes) {
        String pluginsList = "";

        for (String plugin : plugins) {
            pluginsList = pluginsList.concat(PLUGIN_LIST_SEPARATOR).concat(plugin);
        }

        if (quotes) {
            pluginsList =
                    testParameter.concat("\"".concat(pluginsList.substring(1))); // strip off the leading : and add a "
        } else {
            pluginsList = testParameter.concat(pluginsList.substring(1)); // strip off the leading :
        }

        if (quotes) {
            pluginsList = pluginsList.concat("\"");
        }
        SafeModeCommandLineArguments parser = new SafeModeCommandLineArguments(pluginsList);

        plugins.forEach(p -> assertTrue(parser.isDisabledByParam(p)));
    }
}
