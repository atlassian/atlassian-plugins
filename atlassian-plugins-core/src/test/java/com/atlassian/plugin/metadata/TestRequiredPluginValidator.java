package com.atlassian.plugin.metadata;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.PluginAccessor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestRequiredPluginValidator {

    private static final String PLUGIN_KEY = "com.atlassian.req.plugin.1";
    private static final Set<String> REQ_PLUGINS = new HashSet<String>() {
        {
            add(PLUGIN_KEY);
            add("com.atlassian.req.plugin.2");
            add("com.atlassian.req.plugin.3");
        }
    };
    private static final Set<String> REQ_PLUGIN_MODULES = new HashSet<String>() {
        {
            add("com.atlassian.req.plugin.4:module1");
            add("com.atlassian.req.plugin.4:module2");
            add("com.atlassian.req.plugin.4:module3");
        }
    };

    private RequiredPluginProvider provider;

    @Mock
    private PluginAccessor pluginAccessor;

    @Before
    public void setUp() {
        provider = new RequiredPluginProvider() {
            public Set<String> getRequiredPluginKeys() {
                return REQ_PLUGINS;
            }

            public Set<String> getRequiredModuleKeys() {
                return REQ_PLUGIN_MODULES;
            }
        };
    }

    @Test
    public void testValidate() {
        when(pluginAccessor.isPluginEnabled(anyString())).thenReturn(true);
        when(pluginAccessor.isPluginModuleEnabled(anyString())).thenReturn(true);
        RequiredPluginValidator validator = new DefaultRequiredPluginValidator(pluginAccessor, provider);
        assertEquals(0, validator.validate().size());
    }

    @Test
    public void testValidateFails() {
        when(pluginAccessor.isPluginEnabled(anyString())).thenReturn(true);
        when(pluginAccessor.isPluginEnabled(PLUGIN_KEY)).thenReturn(false);
        when(pluginAccessor.isPluginModuleEnabled(anyString())).thenReturn(true);
        RequiredPluginValidator validator = new DefaultRequiredPluginValidator(pluginAccessor, provider);
        Collection<String> errors = validator.validate();
        assertEquals(1, errors.size());
        assertTrue(errors.contains(PLUGIN_KEY));
    }
}
