package com.atlassian.plugin.metadata;

import org.junit.Test;

import com.atlassian.plugin.MockModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.descriptors.CannotDisable;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestDefaultPluginMetadataManager {

    @Test
    public void testIsUserInstalledPlugin() {
        final Plugin plugin = new MockPlugin("my.plugin");
        assertTrue(new DefaultPluginMetadataManager(new EmptyPluginMetadata()).isUserInstalled(plugin));
    }

    @Test
    public void testIsNotUserInstalledPluginIfApplicationSupplied() {
        final Plugin plugin = new MockPlugin("my.plugin");
        assertFalse(new DefaultPluginMetadataManager(new ApplicationPluginMetadata()).isUserInstalled(plugin));
    }

    @Test
    public void testPluginIsNotUserInstalledBecauseItIsBundled() {
        final Plugin plugin = new MockBundledPlugin("my.plugin");
        assertFalse(new DefaultPluginMetadataManager(new EmptyPluginMetadata()).isUserInstalled(plugin));
    }

    @Test
    public void testPluginIsRequired() {
        final Plugin plugin = new MockPlugin("my.plugin");
        assertFalse(new DefaultPluginMetadataManager(new RequiredPluginMetadata()).isOptional(plugin));
    }

    @Test
    public void testPluginIsOptional() {
        final Plugin plugin = new MockPlugin("my.plugin");
        assertTrue(new DefaultPluginMetadataManager(new EmptyPluginMetadata()).isOptional(plugin));
    }

    @Test
    public void testPluginWithModulesIsOptional() {
        final Plugin plugin = new MockPlugin("my.plugin");
        final ModuleDescriptor<?> moduleDescriptor = new MockModuleDescriptor<>(plugin, "my.plugin.c-mod3", null);
        plugin.addModuleDescriptor(moduleDescriptor);
        assertTrue(new DefaultPluginMetadataManager(new EmptyPluginMetadata()).isOptional(plugin));
    }

    @Test
    public void testPluginIsRequiredBecauseOfRequiredModule() {
        final Plugin plugin = new MockPlugin("my.plugin");
        final ModuleDescriptor<?> moduleDescriptor = new MockModuleDescriptor<>(plugin, "my.plugin.c-mod3", null);
        plugin.addModuleDescriptor(moduleDescriptor);
        assertFalse(new DefaultPluginMetadataManager(new ModuleRequiredPluginMetadata()).isOptional(plugin));
    }

    @Test
    public void testModuleIsRequired() {
        final ModuleDescriptor<?> moduleDescriptor = new MockModuleDescriptor<>(null, "my.plugin.c-mod3", null);
        assertFalse(new DefaultPluginMetadataManager(new ModuleRequiredPluginMetadata()).isOptional(moduleDescriptor));
    }

    @Test
    public void testModuleIsRequiredBecauseParentPluginIsRequired() {
        final Plugin plugin = new MockPlugin("my.plugin");
        final ModuleDescriptor<?> moduleDescriptor = new MockModuleDescriptor<>(plugin, "my.plugin.c-mod3", null);
        plugin.addModuleDescriptor(moduleDescriptor);
        assertFalse(new DefaultPluginMetadataManager(new RequiredPluginMetadata()).isOptional(moduleDescriptor));
    }

    @Test
    public void testModuleIsNotMadeRequiredBecauseSiblingModuleIsRequired() {
        final Plugin plugin = new MockPlugin("my.plugin");
        final MockModuleDescriptor<Object> required = new MockModuleDescriptor<>(plugin, "required", null);
        plugin.addModuleDescriptor(required);
        final ModuleDescriptor<?> moduleDescriptor = new MockModuleDescriptor<>(plugin, "not-required", null);
        plugin.addModuleDescriptor(moduleDescriptor);
        assertTrue(new DefaultPluginMetadataManager(new EmptyPluginMetadata() {
                    @Override
                    public boolean required(final ModuleDescriptor<?> descriptor) {
                        return descriptor == required;
                    }
                })
                .isOptional(moduleDescriptor));
    }

    @Test
    public void testModuleIsRequiredTypeMarkedByAnnotation() {
        final Plugin plugin = new MockPlugin("my.plugin");
        // Looked a bit, do not think mockito can create a mock such that it has a class-level annotation
        final ModuleDescriptor<?> moduleDescriptor = new CannotDisableModuleDescriptorType(plugin, "my.plugin.c-mod3");

        assertFalse(new DefaultPluginMetadataManager(new EmptyPluginMetadata()).isOptional(moduleDescriptor));
    }

    @Test
    public void testModuleIsOptional() {
        final Plugin plugin = new MockPlugin("my.plugin");
        final ModuleDescriptor<?> moduleDescriptor = new MockModuleDescriptor<>(plugin, "my.plugin.c-mod3", null);
        plugin.addModuleDescriptor(moduleDescriptor);
        assertTrue(new DefaultPluginMetadataManager(new EmptyPluginMetadata()).isOptional(moduleDescriptor));
    }

    @Test(expected = NullPointerException.class)
    public void testIsOptionalPluginNullPlugin() {
        new DefaultPluginMetadataManager(new EmptyPluginMetadata()).isOptional((Plugin) null);
    }

    @Test(expected = NullPointerException.class)
    public void testIsOptionalModuleNullModule() {
        new DefaultPluginMetadataManager(new EmptyPluginMetadata()).isOptional((ModuleDescriptor<?>) null);
    }

    @Test(expected = NullPointerException.class)
    public void testIsUserInstalledPluginNullPlugin() {
        new DefaultPluginMetadataManager(new EmptyPluginMetadata()).isUserInstalled(null);
    }

    class MockPlugin extends com.atlassian.plugin.MockPlugin {
        MockPlugin(final String key) {
            super(key, TestDefaultPluginMetadataManager.this.getClass().getClassLoader());
        }
    }

    class MockBundledPlugin extends MockPlugin {
        MockBundledPlugin(final String key) {
            super(key);
        }

        @Override
        public boolean isBundledPlugin() {
            return true;
        }
    }

    @CannotDisable
    class CannotDisableModuleDescriptorType extends MockModuleDescriptor<Object> implements ModuleDescriptor<Object> {
        CannotDisableModuleDescriptorType(final Plugin plugin, final String completeKey) {
            super(plugin, completeKey, null);
        }
    }

    class EmptyPluginMetadata implements PluginMetadata {
        public boolean applicationProvided(final Plugin plugin) {
            return false;
        }

        public boolean required(final Plugin plugin) {
            return false;
        }

        public boolean required(final ModuleDescriptor<?> descriptor) {
            return false;
        }
    }

    class ApplicationPluginMetadata extends EmptyPluginMetadata {
        @Override
        public boolean applicationProvided(final Plugin plugin) {
            return true;
        }
    }

    class RequiredPluginMetadata extends EmptyPluginMetadata {
        @Override
        public boolean required(final Plugin plugin) {
            return true;
        }
    }

    class ModuleRequiredPluginMetadata extends EmptyPluginMetadata {
        @Override
        public boolean required(final ModuleDescriptor<?> descriptor) {
            return true;
        }
    }
}
