package com.atlassian.plugin.loaders;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Function;
import javax.annotation.Nonnull;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils;
import com.atlassian.plugin.loaders.classloading.Scanner;
import com.atlassian.plugin.test.PluginTestUtils;

import static org.apache.commons.lang3.SystemUtils.IS_OS_WINDOWS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeFalse;

import static com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils.PADDINGTON_JAR;
import static com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils.POOH_JAR;

public class TestDirectoryScanner {

    private File pluginsTestDir;

    @Before
    public void createFillAndCleanTempPluginDirectory() throws IOException {
        final DirectoryPluginLoaderUtils.ScannerDirectories directories =
                DirectoryPluginLoaderUtils.createFillAndCleanTempPluginDirectory();
        pluginsTestDir = directories.pluginsTestDir;
    }

    @Test
    public void testNormalOperation() {
        File pluginsDirectory = pluginsTestDir;
        Scanner scanner = new DirectoryScanner(pluginsDirectory);
        scanner.scan();
        Collection<DeploymentUnit> deployedUnits = scanner.getDeploymentUnits();
        assertEquals(2, deployedUnits.size());

        // units should be returned ordered alphabetically
        Iterator iterator = deployedUnits.iterator();
        DeploymentUnit unit = (DeploymentUnit) iterator.next();
        assertEquals(PADDINGTON_JAR, unit.getPath().getName());

        unit = (DeploymentUnit) iterator.next();
        assertEquals(POOH_JAR, unit.getPath().getName());
    }

    @Test
    public void testSkipDot() throws Exception {
        File pluginsDirectory = pluginsTestDir;
        assertNotNull(File.createTempFile(".asdf", ".jar", pluginsDirectory));
        Scanner scanner = new DirectoryScanner(pluginsDirectory);
        scanner.scan();
        Collection<DeploymentUnit> deployedUnits = scanner.getDeploymentUnits();
        assertEquals(2, deployedUnits.size());

        // units should be returned ordered alphabetically
        Iterator iterator = deployedUnits.iterator();
        DeploymentUnit unit = (DeploymentUnit) iterator.next();
        assertEquals(PADDINGTON_JAR, unit.getPath().getName());

        unit = (DeploymentUnit) iterator.next();
        assertEquals(POOH_JAR, unit.getPath().getName());
    }

    @Test
    public void testRemove() {
        File pluginsDirectory = pluginsTestDir;
        File paddington = new File(pluginsDirectory, PADDINGTON_JAR);

        assertTrue(paddington.exists());

        DirectoryScanner scanner = new DirectoryScanner(pluginsDirectory);
        scanner.scan();
        assertEquals(2, scanner.getDeploymentUnits().size());
        DeploymentUnit paddingtonUnit = scanner.locateDeploymentUnit(paddington);
        scanner.remove(paddingtonUnit);

        assertFalse(paddington.exists());
        assertEquals(1, scanner.getDeploymentUnits().size());
    }

    @Test
    public void testRemoveFromReadOnlySource() {
        // Windows does not support read only directories
        assumeFalse(IS_OS_WINDOWS);

        final File pluginsDirectory = pluginsTestDir;
        final File paddington = new File(pluginsDirectory, PADDINGTON_JAR);
        try {
            // Disallow deleting of the file.
            setWritable(pluginsDirectory, false);
            // Check that the plugins directory is actually read-only, otherwise skip the test, as it will fail.
            assumeFalse(
                    "Can write to " + pluginsDirectory
                            + ", even though we made it read-only. Maybe we're running as root?",
                    pluginsDirectory.canWrite());
            final DirectoryScanner scanner = new DirectoryScanner(pluginsDirectory);
            scanner.scan();
            assertEquals(2, scanner.getDeploymentUnits().size());
            final DeploymentUnit paddingtonUnit = scanner.locateDeploymentUnit(paddington);
            assertTrue(PADDINGTON_JAR + " should exist before remove() is called", paddington.exists());

            scanner.remove(paddingtonUnit);

            assertTrue(PADDINGTON_JAR + " should exist after remove() is called", paddington.exists());
            assertEquals(1, scanner.getDeploymentUnits().size());
        } finally {
            // Reset to writable when we are done
            setWritable(pluginsDirectory, true);
        }
    }

    @Test
    public void testRemoveNoErrorWhenNotExist() {
        File pluginsDirectory = pluginsTestDir;
        File paddington = new File(pluginsDirectory, PADDINGTON_JAR);

        assertTrue(paddington.exists());

        DirectoryScanner scanner = new DirectoryScanner(pluginsDirectory);
        scanner.scan();
        assertEquals(2, scanner.getDeploymentUnits().size());
        DeploymentUnit paddingtonUnit = scanner.locateDeploymentUnit(paddington);
        assertTrue(paddington.exists());
        assertTrue(paddington.delete());
        scanner.remove(paddingtonUnit);

        assertFalse(paddington.exists());
        assertEquals(1, scanner.getDeploymentUnits().size());
    }

    @Test
    public void testAddAndRemoveJarFromOutsideScanner() throws Exception {
        File pluginsDirectory = pluginsTestDir;
        File paddington = new File(pluginsDirectory, PADDINGTON_JAR);
        File duplicate = new File(pluginsDirectory, "duplicate-test-plugin.jar");

        // make sure it's not there already from a bad test
        if (duplicate.canRead()) {
            delete(duplicate);
        }

        // should be 2 to start with
        Scanner scanner = new DirectoryScanner(pluginsDirectory);
        scanner.scan();
        assertEquals(2, scanner.getDeploymentUnits().size());

        // copy and scan - should have 3 files
        FileUtils.copyFile(paddington, duplicate);
        scanner.scan();
        assertEquals(3, scanner.getDeploymentUnits().size());

        // delete and we should have 2!
        delete(duplicate);
        scanner.scan();
        assertEquals(2, scanner.getDeploymentUnits().size());
    }

    @Test
    public void testModifyJar() throws Exception {
        // Note windows is a piece of shit (can't modify files in the classpath) so
        // we need to create a temporary directory outside the classpath where we can modify files freely.
        File pluginsDirectory = pluginsTestDir;
        File paddington = new File(pluginsDirectory, PADDINGTON_JAR);

        File testTempDirectory = PluginTestUtils.createTempDirectory(TestDirectoryScanner.class);

        File newPaddington = new File(testTempDirectory, PADDINGTON_JAR);
        FileUtils.copyFile(paddington, newPaddington);

        // set the original mod date - must be a multiple of 1000 on most (all?) file systems
        long originalModification = System.currentTimeMillis();
        originalModification = originalModification - (originalModification % 1000) - 3000;
        setLastModified(newPaddington, originalModification);
        assertEquals(originalModification, newPaddington.lastModified());

        // should be 2 to start with
        DirectoryScanner scanner = new DirectoryScanner(testTempDirectory);
        scanner.scan();
        assertEquals(1, scanner.getDeploymentUnits().size());

        DeploymentUnit newPaddingtonUnit = scanner.locateDeploymentUnit(newPaddington);
        assertEquals(originalModification, newPaddingtonUnit.lastModified());

        // modify the JAR file
        long secondModification = originalModification + 2000;
        setLastModified(newPaddington, secondModification);
        scanner.scan();

        newPaddingtonUnit = scanner.locateDeploymentUnit(newPaddington);
        assertEquals(secondModification, newPaddingtonUnit.lastModified());
    }

    private static void delete(@Nonnull final File file) {
        confirmModifyFile(file, File::delete);
    }

    private static void setLastModified(@Nonnull final File file, final long time) {
        confirmModifyFile(file, f -> f.setLastModified(time));
    }

    private static void setWritable(@Nonnull final File file, final boolean writable) {
        confirmModifyFile(file, f -> f.setWritable(writable));
    }

    private static void confirmModifyFile(@Nonnull final File file, final Function<File, Boolean> operation) {
        final String message = String.format("Couldn't modify file %s", file);
        assertTrue(message, operation.apply(file));
    }
}
