package com.atlassian.plugin.loaders;

import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.junit.Test;

import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;

import static org.junit.Assert.assertEquals;

public class TestLoaderUtils {

    @Test
    public void testMultipleParameters() throws DocumentException {
        Document document = DocumentHelper.parseText("<foo>" + "<param name=\"colour\">green</param>"
                + "<param name=\"size\" value=\"large\" />"
                + "</foo>");

        Map params = LoaderUtils.getParams(new Dom4jDelegatingElement(document.getRootElement()));
        assertEquals(2, params.size());

        assertEquals("green", params.get("colour"));
        assertEquals("large", params.get("size"));
    }
}
