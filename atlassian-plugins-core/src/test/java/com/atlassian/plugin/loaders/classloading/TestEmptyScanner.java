package com.atlassian.plugin.loaders.classloading;

import java.io.File;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;

public class TestEmptyScanner {
    private EmptyScanner emptyScanner;

    @Before
    public void setUp() {
        emptyScanner = new EmptyScanner();
    }

    @After
    public void tearDown() {
        emptyScanner = null;
    }

    @Test
    public void scanReturnsEmpty() {
        final Collection<DeploymentUnit> deploymentUnits = emptyScanner.scan();
        assertThat(deploymentUnits, empty());
    }

    @Test
    public void getDeploymentUnitsReturnsEmpty() {
        final Collection<DeploymentUnit> deploymentUnits = emptyScanner.getDeploymentUnits();
        assertThat(deploymentUnits, empty());
    }

    @Test
    public void resetDoesNotThrow() {
        emptyScanner.reset();
    }

    @Test
    public void removeDoesNotThrow() {
        final DeploymentUnit deploymentUnit = new DeploymentUnit(new File("unit"));
        emptyScanner.remove(deploymentUnit);
    }
}
