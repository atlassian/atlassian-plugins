package com.atlassian.plugin.loaders.classloading;

import java.io.File;
import java.io.IOException;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDeploymentUnit {

    @Test
    public void testCompareTo() throws IOException {
        File tmp = File.createTempFile("testDeploymentUnit", ".txt");
        DeploymentUnit unit1 = new DeploymentUnit(tmp);
        DeploymentUnit unit2 = new DeploymentUnit(tmp);
        assertEquals(0, unit1.compareTo(unit2));

        tmp.setLastModified(System.currentTimeMillis() + 1000);
        unit2 = new DeploymentUnit(tmp);
        assertEquals(-1, unit1.compareTo(unit2));
    }

    public static Matcher<DeploymentUnit> deploymentUnitWithPath(final Matcher<File> pathMatcher) {
        return new TypeSafeMatcher<DeploymentUnit>() {
            @Override
            protected boolean matchesSafely(final DeploymentUnit deploymentUnit) {
                return pathMatcher.matches(deploymentUnit.getPath());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Deployment unit with path ");
                pathMatcher.describeTo(description);
            }
        };
    }
}
