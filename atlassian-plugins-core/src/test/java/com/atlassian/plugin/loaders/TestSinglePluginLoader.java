package com.atlassian.plugin.loaders;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockMineralModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.util.ClassLoaderUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSinglePluginLoader {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    private SinglePluginLoader loader;

    @Mock
    private Plugin plugin;

    @Mock
    private Element module;

    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;

    @Mock
    private ModuleDescriptor moduleDescriptor;

    @Before
    public void before() {
        loader = new SinglePluginLoader(
                ClassLoaderUtils.getResource("test-disabled-plugin.xml", SinglePluginLoader.class));
    }

    @Test
    public void pluginByUrl() {
        // URL created should be reentrant and create a different stream each
        // time
        assertThat(loader.getSource(), not(sameInstance(loader.getSource())));
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        assertThat(plugins, Matchers.<Plugin>iterableWithSize(1));
        assertThat(plugins.iterator().next().isEnabledByDefault(), is(false));
    }

    @Test
    public void createModule() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        loader.plugins = ImmutableList.of(plugin);
        ArgumentCaptor<Element> captor = ArgumentCaptor.forClass(Element.class);
        when(moduleDescriptorFactory.getModuleDescriptor(any())).thenReturn(moduleDescriptor);

        assertThat(loader.createModule(plugin, module, moduleDescriptorFactory), is(moduleDescriptor));

        verify(moduleDescriptor).init(eq(plugin), captor.capture());
        assertSame(module, captor.getValue());
    }

    @Test
    public void createModuleNoFactory() {
        loader.plugins = ImmutableList.of();

        assertThat(loader.createModule(plugin, module, moduleDescriptorFactory), nullValue());
    }
}
