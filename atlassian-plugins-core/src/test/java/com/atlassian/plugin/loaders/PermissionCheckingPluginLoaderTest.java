package com.atlassian.plugin.loaders;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.impl.UnloadablePlugin;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public final class PermissionCheckingPluginLoaderTest {

    private PermissionCheckingPluginLoader permissionCheckingPluginLoader;

    @Mock
    private PluginLoader pluginLoader;

    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;

    @Before
    public void setUp() {
        permissionCheckingPluginLoader = new PermissionCheckingPluginLoader(pluginLoader);
    }

    @Test
    public void loadAllPluginsWithEmptyCollectionOfPlugins() {
        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of());
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);
        assertTrue(Iterables.isEmpty(plugins));
    }

    @Test
    public void loadFoundPluginsWithEmptyCollectionOfPlugins() {
        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of());
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);
        assertTrue(Iterables.isEmpty(plugins));
    }

    @Test
    public void loadFoundPluginsWithPluginWithoutExecuteJavaPermissionAndNoJava() {
        final Plugin plugin = newPlugin(ImmutableSet.of(), false, false);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithoutExecuteJavaPermissionAndNoJava() {
        final Plugin plugin = newPlugin(ImmutableSet.of(), false, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadFoundPluginsWithPluginWithoutExecuteJavaPermissionAndSomeJava() {
        final Plugin plugin = newPlugin(ImmutableSet.of(), true, false);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertTrue(Iterables.get(plugins, 0) instanceof UnloadablePlugin);
    }

    @Test
    public void loadAllPluginsWithPluginWithoutExecuteJavaPermissionAndSomeJava() {
        final Plugin plugin = newPlugin(ImmutableSet.of(), true, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertTrue(Iterables.get(plugins, 0) instanceof UnloadablePlugin);
    }

    @Test
    public void loadAllPluginsWithUnloadablePluginThenRemoved() {
        final Plugin plugin = newPlugin(ImmutableSet.of(), true, false);

        doThrow(new PluginException("cannot find plugin")).when(pluginLoader).removePlugin(any(Plugin.class));
        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        Plugin loadedPlugin = Iterables.get(plugins, 0);
        assertTrue(loadedPlugin instanceof UnloadablePlugin);
        permissionCheckingPluginLoader.removePlugin(loadedPlugin);
    }

    @Test
    public void loadAllPluginsWithGoodPluginThenDiscarded() throws Exception {
        // We need to replace the object under test with a discardable loader. This could be structured better.
        final DiscardablePluginLoader discardablePluginLoader = mock(DiscardablePluginLoader.class);
        permissionCheckingPluginLoader = new PermissionCheckingPluginLoader(discardablePluginLoader);

        final Plugin plugin = newPlugin(ImmutableSet.of(), true, true);

        when(discardablePluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);
        permissionCheckingPluginLoader.discardPlugin(plugin);
        verify(discardablePluginLoader).discardPlugin(plugin);
    }

    @Test
    public void loadAllPluginsWithUnloadablePluginThenDiscarded() {
        // We need to replace the object under test with a discardable loader. This could be structured better.
        final DiscardablePluginLoader discardablePluginLoader = mock(DiscardablePluginLoader.class);
        permissionCheckingPluginLoader = new PermissionCheckingPluginLoader(discardablePluginLoader);

        final Plugin containsJavaButNoPermission = newPlugin(ImmutableSet.of(), true, false);
        final Plugin hasSystemButNoPermission = newPlugin(ImmutableSet.of(), false, false);
        addModuleDescriptor(hasSystemButNoPermission, true);

        final Iterable<Plugin> pluginsToLoad = ImmutableList.of(containsJavaButNoPermission, hasSystemButNoPermission);
        when(discardablePluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(pluginsToLoad);

        final Iterable<Plugin> loadedPlugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);
        // The actual plugin should be discarded when loadAllPlugins ignores it
        for (final Plugin plugin : pluginsToLoad) {
            verify(discardablePluginLoader).discardPlugin(plugin);
        }

        assertEquals(Iterables.size(loadedPlugins), Iterables.size(pluginsToLoad));
        for (final Plugin plugin : loadedPlugins) {
            assertThat(plugin, instanceOf(UnloadablePlugin.class));
            permissionCheckingPluginLoader.discardPlugin(plugin);
        }
        // I know .times() is frowned on, but i can't find another good way to do this in Mockito - i want to say
        // the only discard calls were from the loadAllPlugins() above.
        verify(discardablePluginLoader, times(Iterables.size(pluginsToLoad))).discardPlugin(any(Plugin.class));
    }

    @Test
    public void loadFoundPluginsWithGoodPluginThenDiscarded() throws Exception {
        // We need to replace the object under test with a discardable loader. This could be structured better.
        final DiscardablePluginLoader discardablePluginLoader = mock(DiscardablePluginLoader.class);
        permissionCheckingPluginLoader = new PermissionCheckingPluginLoader(discardablePluginLoader);

        final Plugin plugin = newPlugin(ImmutableSet.of(), true, true);

        when(discardablePluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);
        permissionCheckingPluginLoader.discardPlugin(plugin);
        verify(discardablePluginLoader).discardPlugin(plugin);
    }

    @Test
    public void loadFoundPluginsWithUnloadablePluginThenDiscarded() {
        // We need to replace the object under test with a discardable loader. This could be structured better.
        final DiscardablePluginLoader discardablePluginLoader = mock(DiscardablePluginLoader.class);
        permissionCheckingPluginLoader = new PermissionCheckingPluginLoader(discardablePluginLoader);

        final Plugin containsJavaButNoPermission = newPlugin(ImmutableSet.of(), true, false);
        final Plugin hasSystemButNoPermission = newPlugin(ImmutableSet.of(), false, false);
        addModuleDescriptor(hasSystemButNoPermission, true);

        final Iterable<Plugin> pluginsToLoad = ImmutableList.of(containsJavaButNoPermission, hasSystemButNoPermission);
        when(discardablePluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(pluginsToLoad);

        final Iterable<Plugin> loadedPlugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);
        // The actual plugin should be discarded when loadFoundPlugins ignores it
        for (final Plugin plugin : pluginsToLoad) {
            verify(discardablePluginLoader).discardPlugin(plugin);
        }

        assertEquals(Iterables.size(loadedPlugins), Iterables.size(pluginsToLoad));
        for (final Plugin plugin : loadedPlugins) {
            assertThat(plugin, instanceOf(UnloadablePlugin.class));
            permissionCheckingPluginLoader.discardPlugin(plugin);
        }
        // I know .times() is frowned on, but i can't find another good way to do this in Mockito - i want to say
        // the only discard calls were from the loadFoundPlugins() above.
        verify(discardablePluginLoader, times(Iterables.size(pluginsToLoad))).discardPlugin(any(Plugin.class));
    }

    @Test
    public void loadAllPluginsWithPluginWithNoPermissionsAndSystemModules() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(), false, false);

        addModuleDescriptor(plugin, true);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertTrue(Iterables.get(plugins, 0) instanceof UnloadablePlugin);
    }

    @Test
    public void loadAllPluginsWithPluginWithNoPermissionsAndNoSystemModules() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(), false, false);

        addModuleDescriptor(plugin, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadFoundPluginsWithPluginWithJavaPermissionAndSomeJava() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(Permissions.EXECUTE_JAVA), true, false);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithCreateSystemModulePermissionsAndSystemModules() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(Permissions.CREATE_SYSTEM_MODULES), false, false);

        addModuleDescriptor(plugin, true);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithCreateSystemModulePermissionsAndNoSystemModules() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(Permissions.CREATE_SYSTEM_MODULES), false, false);

        addModuleDescriptor(plugin, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadFoundPluginsWithPluginWithJavaPermissionAndNoJava() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(Permissions.EXECUTE_JAVA), false, false);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithJavaPermissionAndNoJava() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(Permissions.EXECUTE_JAVA), false, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadFoundPluginsWithPluginWithAllPermissionsAndSomeJava() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(), true, true);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithAllPermissionsAndSomeJava() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(), true, true);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithAllPermissionsAndSystemModules() throws Exception {
        final Plugin plugin = newPlugin(ImmutableSet.of(), false, true);
        addModuleDescriptor(plugin, true);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    private void addModuleDescriptor(Plugin plugin, boolean system) {
        ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        when(descriptor.getKey()).thenReturn("foo");
        when(descriptor.isSystemModule()).thenReturn(system);
        when(plugin.getModuleDescriptors()).thenReturn(ImmutableList.of(descriptor));
    }

    private Plugin newPlugin(Set<String> permissions, boolean hasJava, boolean hasAllPermissions) {
        final PluginInternal plugin = mockPlugin(PluginInternal.class);
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        when(plugin.hasAllPermissions()).thenReturn(hasAllPermissions);
        when(plugin.getActivePermissions()).thenReturn(permissions);
        when(plugin.getPluginArtifact()).thenReturn(pluginArtifact);
        when(pluginArtifact.containsJavaExecutableCode()).thenReturn(hasJava);
        return plugin;
    }

    private <P extends Plugin> P mockPlugin(Class<P> type) {
        P mock = mock(type);
        when(mock.getKey()).thenReturn("test-plugin-key");
        when(mock.getName()).thenReturn("Test Plugin");
        PluginInformation pluginInformation = mock(PluginInformation.class);
        when(mock.getPluginInformation()).thenReturn(pluginInformation);
        return mock;
    }
}
