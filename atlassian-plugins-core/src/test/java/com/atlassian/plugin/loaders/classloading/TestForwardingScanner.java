package com.atlassian.plugin.loaders.classloading;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestForwardingScanner {
    private ForwardingScanner forwardingScanner;

    @Mock
    private Scanner mockScanner;

    final DeploymentUnit unitA = new DeploymentUnit(new File("unitA"));

    final DeploymentUnit unitB = new DeploymentUnit(new File("unitB"));

    @Before
    public void setUp() {
        forwardingScanner = new ForwardingScanner(mockScanner);
    }

    @After
    public void tearDown() {
        forwardingScanner = null;
    }

    @Test
    public void scanIsForwarded() {
        when(mockScanner.scan()).thenReturn(Arrays.asList(unitA));
        final Collection<DeploymentUnit> deploymentUnits = forwardingScanner.scan();
        verify(mockScanner).scan();
        assertEquals(deploymentUnits, Arrays.asList(unitA));
    }

    @Test
    public void getDeploymentUnitsIsForwarded() {
        when(mockScanner.getDeploymentUnits()).thenReturn(Arrays.asList(unitA, unitB));
        final Collection<DeploymentUnit> deploymentUnits = forwardingScanner.getDeploymentUnits();
        verify(mockScanner).getDeploymentUnits();
        assertEquals(deploymentUnits, Arrays.asList(unitA, unitB));
    }

    @Test
    public void resetIsForwarded() {
        forwardingScanner.reset();
        verify(mockScanner).reset();
    }

    @Test
    public void removeIsForwarded() {
        forwardingScanner.remove(unitA);
        verify(mockScanner).remove(unitA);
    }
}
