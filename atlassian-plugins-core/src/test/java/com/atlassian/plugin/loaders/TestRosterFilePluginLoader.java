package com.atlassian.plugin.loaders;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginArtifactFactory;
import com.atlassian.plugin.ReferenceMode;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import static com.atlassian.plugin.loaders.RosterFilePluginLoader.getReferenceModePropertyName;
import static com.atlassian.plugin.loaders.TestRosterFileScanner.backdateFile;
import static com.atlassian.plugin.loaders.TestRosterFileScanner.writeRosterFile;
import static com.atlassian.plugin.test.Matchers.uriWithPath;
import static com.atlassian.plugin.test.PluginTestUtils.createTempDirectory;

@RunWith(MockitoJUnitRunner.class)
public class TestRosterFilePluginLoader {
    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Mock
    private PluginFactory pluginFactory;

    @Mock
    private PluginArtifactFactory pluginArtifactFactory;

    @Mock
    private PluginEventManager pluginEventManager;

    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;

    @Captor
    private ArgumentCaptor<PluginArtifact> pluginArtifactCaptor;

    private File temporaryDirectory;
    private File rosterFile;
    private List<PluginFactory> pluginFactories;

    RosterFilePluginLoader rosterFilePluginLoader;

    @Before
    public void setUp() throws Exception {
        temporaryDirectory = createTempDirectory(TestRosterFilePluginLoader.class);
        rosterFile = new File(temporaryDirectory, "rosterFile.list");
        pluginFactories = ImmutableList.of(pluginFactory);
        rosterFilePluginLoader =
                new RosterFilePluginLoader(rosterFile, pluginFactories, pluginArtifactFactory, pluginEventManager);
    }

    @After
    public void tearDown() {
        deleteQuietly(temporaryDirectory);
    }

    @Test
    public void rosterFileUsedToLoadAllPlugins() throws Exception {
        final String pluginKey = "someKey";
        final String jarName = "somePlugin.jar";
        final File jarFile = getOnlyElement(writeRosterFile(rosterFile, jarName));
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        final Plugin plugin = mock(Plugin.class);

        final ArgumentCaptor<URI> uriCaptor = ArgumentCaptor.forClass(URI.class);
        when(pluginArtifactFactory.create(uriCaptor.capture())).thenReturn(pluginArtifact);
        configurePluginFactory(pluginKey, plugin, pluginArtifact);

        final Iterable<Plugin> plugins = rosterFilePluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertThat(plugins, contains(plugin));
        final URI uri = uriCaptor.getValue();
        assertThat(uri, uriWithPath(jarFile.getPath()));
        verify(pluginFactory).canCreate(pluginArtifact);
    }

    @Test
    public void removePluginResultsInPluginUninstall() throws Exception {
        final String pluginKey = "someKey";
        final String oldJarName = "oldPlugin.jar";
        final File oldJarFile = getOnlyElement(writeRosterFile(rosterFile, oldJarName));
        // Since we need to scan again, backdate the file to ensure it appears new when rewritten
        backdateFile(rosterFile);
        final PluginArtifact oldPluginArtifact = mock(PluginArtifact.class);
        final Plugin oldPlugin = mock(Plugin.class);
        when(oldPlugin.isUninstallable()).thenReturn(true);

        when(pluginArtifactFactory.create(argThat(uriWithPath(oldJarFile.getPath()))))
                .thenReturn(oldPluginArtifact);
        configurePluginFactory(pluginKey, oldPlugin, oldPluginArtifact);

        rosterFilePluginLoader.loadAllPlugins(moduleDescriptorFactory);

        final String newJarName = "newPlugin.jar";
        writeRosterFile(rosterFile, newJarName);

        // The plugin system calls PluginLoader#removePlugin when upgrading plugins, so it has to work in this scenario
        rosterFilePluginLoader.loadFoundPlugins(moduleDescriptorFactory);
        rosterFilePluginLoader.removePlugin(oldPlugin);

        verify(oldPlugin).uninstall();
    }

    @Test
    public void defaultRosterFilePluginLoaderForbidsReference() throws Exception {
        final JarPluginArtifact jarPluginArtifact = obtainPluginArtifactFromRosterFilesDefaultFactory();

        assertThat(jarPluginArtifact.getReferenceMode(), is(ReferenceMode.FORBID_REFERENCE));
    }

    @Test
    public void propertyCanSetRosterFilePluginLoaderToAllowReference() throws Exception {
        System.setProperty(getReferenceModePropertyName(), "PERMIT_REFERENCE");
        final JarPluginArtifact jarPluginArtifact = obtainPluginArtifactFromRosterFilesDefaultFactory();

        assertThat(jarPluginArtifact.getReferenceMode(), is(ReferenceMode.PERMIT_REFERENCE));
    }

    private JarPluginArtifact obtainPluginArtifactFromRosterFilesDefaultFactory() throws IOException {
        final String pluginKey = "someKey";
        final String jarName = "oldPlugin.jar";
        final Plugin plugin = mock(Plugin.class);

        writeRosterFile(rosterFile, jarName);
        when(pluginFactory.canCreate(any(PluginArtifact.class))).thenReturn(pluginKey);
        when(pluginFactory.create(pluginArtifactCaptor.capture(), same(moduleDescriptorFactory)))
                .thenReturn(plugin);

        rosterFilePluginLoader = new RosterFilePluginLoader(rosterFile, pluginFactories, pluginEventManager);
        rosterFilePluginLoader.loadAllPlugins(moduleDescriptorFactory);

        final PluginArtifact pluginArtifact = pluginArtifactCaptor.getValue();
        assertThat(pluginArtifact, instanceOf(JarPluginArtifact.class));
        return (JarPluginArtifact) pluginArtifact;
    }

    private void configurePluginFactory(
            final String pluginKey, final Plugin plugin, final PluginArtifact pluginArtifact) {
        when(pluginFactory.canCreate(pluginArtifact)).thenReturn(pluginKey);
        when(pluginFactory.create(pluginArtifact, moduleDescriptorFactory)).thenReturn(plugin);
    }
}
