package com.atlassian.plugin.classloader;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;

import com.atlassian.plugin.MockPlugin;
import com.atlassian.plugin.MockPluginAccessor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TestPluginsClassLoader {

    private static final String TEST_RESOURCE = "log4j.properties";

    private PluginsClassLoader pluginsClassLoader;
    private Mock mockPluginAccessor;
    private Mock mockPlugin;
    private static final String PLUGIN_KEY = "aPluginKey";
    private static final String TEST_CLASS = "java.lang.String";

    @Before
    public void setUp() throws Exception {
        mockPluginAccessor = new Mock(PluginAccessor.class);
        pluginsClassLoader = new PluginsClassLoader((PluginAccessor) mockPluginAccessor.proxy());

        mockPlugin = new Mock(Plugin.class);
    }

    @After
    public void tearDown() {
        mockPluginAccessor.verify();
        mockPlugin.verify();

        mockPluginAccessor = null;
        pluginsClassLoader = null;
    }

    @Test
    public void testFindResourceWhenIndexed() {
        final StubClassLoader stubClassLoader = new StubClassLoader();
        loadPluginResource(stubClassLoader);
        assertTrue(stubClassLoader.getFindResourceNames().contains(TEST_RESOURCE));
        stubClassLoader.clear();

        mockPlugin.expectAndReturn("getKey", PLUGIN_KEY);
        mockPluginAccessor.matchAndReturn("isPluginEnabled", C.args(C.eq(PLUGIN_KEY)), Boolean.TRUE);
        mockPlugin.expectAndReturn("getClassLoader", stubClassLoader);
        pluginsClassLoader.findResource(TEST_RESOURCE);

        assertTrue(stubClassLoader.getFindResourceNames().contains(TEST_RESOURCE));
    }

    @Test
    public void testFindResourceWhenNotIndexed() {
        final StubClassLoader stubClassLoader = new StubClassLoader();
        loadPluginResource(stubClassLoader);

        assertTrue(stubClassLoader.getFindResourceNames().contains(TEST_RESOURCE));
    }

    @Test
    public void testFindResourceWhenIndexedAndPluginDisabled() {
        final StubClassLoader stubClassLoader = new StubClassLoader();
        loadPluginResource(stubClassLoader);
        assertTrue(stubClassLoader.getFindResourceNames().contains(TEST_RESOURCE));
        stubClassLoader.clear();

        mockPluginAccessor.expectAndReturn("getEnabledPlugins", Collections.emptyList());
        mockPlugin.expectAndReturn("getKey", PLUGIN_KEY);
        mockPluginAccessor.matchAndReturn("isPluginEnabled", C.args(C.eq(PLUGIN_KEY)), Boolean.FALSE);
        pluginsClassLoader.findResource(TEST_RESOURCE);

        assertFalse(stubClassLoader.getFindResourceNames().contains(TEST_RESOURCE));
    }

    @Test
    public void testFindClassWhenIndexed() throws Exception {
        final StubClassLoader stubClassLoader = new StubClassLoader();
        loadPluginClass(stubClassLoader);
        assertTrue(stubClassLoader.getFindClassNames().contains(TEST_CLASS));
        stubClassLoader.clear();

        mockPlugin.expectAndReturn("getKey", PLUGIN_KEY);
        mockPluginAccessor.matchAndReturn("isPluginEnabled", C.args(C.eq(PLUGIN_KEY)), Boolean.TRUE);
        mockPlugin.expectAndReturn("getClassLoader", stubClassLoader);
        pluginsClassLoader.findClass(TEST_CLASS);

        assertTrue(stubClassLoader.getFindClassNames().contains(TEST_CLASS));
    }

    @Test
    public void testFindClassWhenNotIndexed() throws Exception {
        final StubClassLoader stubClassLoader = new StubClassLoader();
        loadPluginClass(stubClassLoader);

        assertTrue(stubClassLoader.getFindClassNames().contains(TEST_CLASS));
    }

    @Test(expected = ClassNotFoundException.class)
    public void testFindClassWhenIndexedAndPluginDisabled() throws Exception {
        final StubClassLoader stubClassLoader = new StubClassLoader();
        loadPluginClass(stubClassLoader);
        assertTrue(stubClassLoader.getFindClassNames().contains(TEST_CLASS));
        stubClassLoader.clear();

        mockPluginAccessor.expectAndReturn("getEnabledPlugins", Collections.emptyList());
        mockPlugin.expectAndReturn("getKey", PLUGIN_KEY);
        mockPluginAccessor.matchAndReturn("isPluginEnabled", C.args(C.eq(PLUGIN_KEY)), Boolean.FALSE);

        pluginsClassLoader.findClass(TEST_CLASS);
    }

    @Test
    public void testGetPluginForClass() {
        final MockPluginAccessor mockPluginAccessor = new MockPluginAccessor();
        PluginsClassLoader pluginsClassLoader = new PluginsClassLoader(mockPluginAccessor);
        // Set up plugin A
        MockClassLoader mockClassLoaderA = new MockClassLoader();
        mockClassLoaderA.register("com.acme.Ant", String.class);
        mockClassLoaderA.register("com.acme.Clash", String.class);
        MockPlugin pluginA = new MockPlugin("A", mockClassLoaderA);
        mockPluginAccessor.addPlugin(pluginA);
        // Set up plugin B
        MockClassLoader mockClassLoaderB = new MockClassLoader();
        mockClassLoaderB.register("com.acme.Bat", String.class);
        mockClassLoaderB.register("com.acme.Clash", String.class);
        MockPlugin pluginB = new MockPlugin("B", mockClassLoaderB);
        mockPluginAccessor.addPlugin(pluginB);

        // With both plugins disabled, we should get Clash from no-one
        assertNull(pluginsClassLoader.getPluginForClass("com.acme.Ant"));
        assertNull(pluginsClassLoader.getPluginForClass("com.acme.Bat"));
        assertNull(pluginsClassLoader.getPluginForClass("com.acme.Clash"));
        assertNull(pluginsClassLoader.getPluginForClass("java.lang.String"));

        // Enable PluginB and it should give us Bat and Clash from pluginB
        pluginB.enable();
        pluginsClassLoader.notifyPluginOrModuleEnabled();
        assertNull(pluginsClassLoader.getPluginForClass("com.acme.Ant"));
        assertEquals(pluginB, pluginsClassLoader.getPluginForClass("com.acme.Bat"));
        assertEquals(pluginB, pluginsClassLoader.getPluginForClass("com.acme.Clash"));
        assertNull(pluginsClassLoader.getPluginForClass("java.lang.String"));

        // Enable PluginA and it should give us Clash from pluginB (because it is cached).
        pluginA.enable();
        pluginsClassLoader.notifyPluginOrModuleEnabled();
        assertEquals(pluginA, pluginsClassLoader.getPluginForClass("com.acme.Ant"));
        assertEquals(pluginB, pluginsClassLoader.getPluginForClass("com.acme.Bat"));
        assertEquals(pluginB, pluginsClassLoader.getPluginForClass("com.acme.Clash"));
        assertNull(pluginsClassLoader.getPluginForClass("java.lang.String"));

        // flush the cache and we get Clash from plugin A instead (because it is earlier in the list).
        pluginsClassLoader.notifyUninstallPlugin(pluginB);
        assertEquals(pluginA, pluginsClassLoader.getPluginForClass("com.acme.Ant"));
        assertEquals(pluginB, pluginsClassLoader.getPluginForClass("com.acme.Bat"));
        assertEquals(pluginA, pluginsClassLoader.getPluginForClass("com.acme.Clash"));
        assertNull(pluginsClassLoader.getPluginForClass("java.lang.String"));
    }

    private void loadPluginResource(ClassLoader stubClassLoader) {
        mockPluginAccessor.expectAndReturn("getEnabledPlugins", Collections.singleton(mockPlugin.proxy()));
        mockPlugin.expectAndReturn("getClassLoader", stubClassLoader);
        pluginsClassLoader.findResource(TEST_RESOURCE);
    }

    private void loadPluginClass(ClassLoader stubClassLoader) throws ClassNotFoundException {
        mockPluginAccessor.expectAndReturn("getEnabledPlugins", Collections.singleton(mockPlugin.proxy()));
        mockPlugin.expectAndReturn("getClassLoader", stubClassLoader);
        pluginsClassLoader.findClass(TEST_CLASS);
    }

    private static final class StubClassLoader extends AbstractClassLoader {
        private final Collection<String> findResourceNames = new LinkedList<>();
        private final Collection<String> findClassNames = new LinkedList<>();

        Collection getFindClassNames() {
            return findClassNames;
        }

        StubClassLoader() {
            super(null); // no parent classloader needed for tests
        }

        protected URL findResource(String name) {
            findResourceNames.add(name);
            try {
                return new URL("file://" + name);
            } catch (MalformedURLException e) {
                // ignore
                return null;
            }
        }

        /**
         * override the default behavior to bypass the system class loader
         * for tests
         */
        public Class loadClass(String name) {
            return findClass(name);
        }

        protected Class findClass(String className) {
            findClassNames.add(className);
            return String.class;
        }

        Collection getFindResourceNames() {
            return findResourceNames;
        }

        void clear() {
            findResourceNames.clear();
            findClassNames.clear();
        }
    }
}
