package com.atlassian.plugin;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests that the plugin manager properly notifies StateAware plugin modules of state
 * transitions.
 */
@RunWith(MockitoJUnitRunner.class)
public class TestStateAware {
    private Combination mockEnabling;
    private Combination mockDisabled;
    private ModuleDescriptor mockThwarted;
    private com.atlassian.plugin.manager.DefaultPluginManager manager;
    private Plugin plugin1;

    @Mock
    private PluginLoader pluginLoader;

    interface Combination extends StateAware, ModuleDescriptor {}

    @Before
    public void setUp() throws Exception {
        // FIXME - the next line is here to prevent a null pointer exception caused by a debug logging
        // a variable in the lifecycle is not initialized, which is fine for testing, but a debug logging causes an NPE

        Logger.getRootLogger().setLevel(Level.INFO);
        mockEnabling = makeMockModule(Combination.class, "key1", "enabling", true);
        mockDisabled = makeMockModule(Combination.class, "key1", "disabled", false);
        mockThwarted = makeMockModule(ModuleDescriptor.class, "key1", "thwarted", true);

        plugin1 = new StaticPlugin();
        plugin1.setPluginInformation(new PluginInformation());
        plugin1.setKey("key1");
        plugin1.enable();

        ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin1));

        manager = new com.atlassian.plugin.manager.DefaultPluginManager(
                new MemoryPluginPersistentStateStore(),
                ImmutableList.of(pluginLoader),
                moduleDescriptorFactory,
                new DefaultPluginEventManager());
    }

    /**
     * Any StateAware plugin module that is active when the plugin manager is initialised should
     * recieve an enabled message
     */
    @Test
    public void stateAwareOnInit() {
        plugin1.addModuleDescriptor(mockEnabling);
        plugin1.addModuleDescriptor(mockThwarted);
        plugin1.addModuleDescriptor(mockDisabled);
        manager.init();
        verify(mockEnabling).enabled();
    }

    /**
     * Any StateAware plugin moudle that is explicitly enabled or disabled through the plugin manager
     * should receive the appropriate message
     */
    @Test
    public void stateAwareOnPluginModule() {
        plugin1.addModuleDescriptor(mockDisabled);
        manager.init();

        when(mockDisabled.satisfiesMinJavaVersion()).thenReturn(true);
        manager.enablePluginModule(mockDisabled.getCompleteKey());
        verify(mockDisabled).enabled();

        manager.disablePluginModule(mockDisabled.getCompleteKey());
        verify(mockDisabled).disabled();
    }

    /**
     * If a plugin is disabled, any modules that are currently enabled should be sent the disabled
     * message
     */
    @Test
    public void stateAwareOnPluginDisable() {
        plugin1.addModuleDescriptor(mockEnabling);
        plugin1.addModuleDescriptor(mockDisabled);

        manager.init();
        verify(mockEnabling).enabled();

        manager.disablePlugin(plugin1.getKey());
        verify(mockEnabling).disabled();
    }

    /**
     * If a plugin is enabled, any modules that are currently enabled should be sent the enabled
     * message, but modules which are disabled should not.
     */
    @Test
    public void disabledModuleDescriptorsAreEnabled() {
        plugin1.addModuleDescriptor(mockEnabling);
        plugin1.addModuleDescriptor(mockDisabled);
        plugin1.setEnabledByDefault(false);

        manager.init();

        manager.enablePlugins(plugin1.getKey());
        verify(mockEnabling).enabled();
    }

    private <T extends ModuleDescriptor> T makeMockModule(
            Class<T> moduleClass, String pluginKey, String moduleKey, boolean enabledByDefault) {
        ModuleDescriptor mock = Mockito.mock(moduleClass);
        when(mock.getPluginKey()).thenReturn(pluginKey);
        when(mock.getKey()).thenReturn(moduleKey);
        when(mock.getCompleteKey()).thenReturn(pluginKey + ":" + moduleKey);
        when(mock.isEnabledByDefault()).thenReturn(enabledByDefault);
        when(mock.isEnabled()).thenReturn(true);
        return (T) mock;
    }
}
