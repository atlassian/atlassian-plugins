package com.atlassian.plugin.elements;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import com.google.common.collect.ImmutableMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ResourceLocationTest {
    @Test
    public void paramsAreCopied() {
        Map<String, String> params = new HashMap<>();
        params.put("a", "original");

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        params.put("a", "modified");

        assertEquals("original", rl.getParams().get("a"));
    }

    @Test
    public void paramsAreImmutable() {
        Map<String, String> params = new HashMap<>();
        params.put("a", "original");

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);

        try {
            rl.getParams().put("a", "modified");
        } catch (UnsupportedOperationException e) {
            // Okay
        }

        assertEquals("original", rl.getParams().get("a"));
    }

    @Test(expected = NullPointerException.class)
    public void constructionWithNullParamsFails() {
        new ResourceLocation("", "", "", "", "", null);
    }

    @Test
    public void constructionWithNullValuesSucceeds() {
        Map<String, String> params = Collections.singletonMap("key", null);

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        assertNull(rl.getParameter("key"));
    }

    @Test
    public void constructionWithNullValuesOmitsParam() {
        Map<String, String> params = Collections.singletonMap("key", null);

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        assertEquals(Collections.emptyMap(), rl.getParams());
    }

    @Test
    public void constructionWithMixedValuesIncludesNonNullParam() {
        Map<String, String> params = new HashMap<>();
        params.put("null", null);
        params.put("notnull", "value");

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        assertEquals(ImmutableMap.of("notnull", "value"), rl.getParams());
    }

    @Test
    public void constructionWithNullKeyOmitsParam() {
        Map<String, String> params = Collections.singletonMap(null, "value");

        ResourceLocation rl = new ResourceLocation("", "", "", "", "", params);
        assertEquals(Collections.emptyMap(), rl.getParams());
    }
}
