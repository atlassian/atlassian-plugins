package com.atlassian.plugin.util.resource;

import java.io.File;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TemporaryFolder;

import com.atlassian.plugin.internal.util.PluginUtils;
import com.atlassian.plugin.test.CapturedLogging;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import static com.atlassian.plugin.test.CapturedLogging.didLogDebug;
import static com.atlassian.plugin.test.CapturedLogging.didLogWarn;

public class TestAlternativeDirectoryResourceLoader {
    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder(new File("target"));

    @Rule
    public final CapturedLogging capturedLogging = new CapturedLogging(AlternativeDirectoryResourceLoader.class);

    private File base;
    private File classes;
    private File com;

    @Before
    public void setUp() throws Exception {
        base = temporaryFolder.getRoot();
        com = temporaryFolder.newFolder("classes", "com");
        classes = com.getParentFile();
        temporaryFolder.newFile("kid.txt");
        System.setProperty(PluginUtils.ATLASSIAN_DEV_MODE, TRUE.toString());
    }

    /**
     * Otherwise setting this property (e.g. via QuickReload) could lead to adverse affects in production
     */
    @Test
    public void givenDevModeIsEnabled_whenFindingResources_thenNoAlternatesShouldBeProvided() {
        // setup
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, base.getAbsolutePath());
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();

        // given
        System.setProperty(PluginUtils.ATLASSIAN_DEV_MODE, FALSE.toString());

        assertThat(
                loader.getResource("classes"), // when
                nullValue()); // then
    }

    @Test
    public void testGetResource() throws Exception {
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, base.getAbsolutePath());
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResource("classes"), equalTo(classes.toURI().toURL()));
        assertThat(loader.getResource("com"), nullValue());
        assertThat(loader.getResource("asdfasdfasf"), nullValue());
    }

    @Test
    public void testGetResourceWithTwoDefined() throws Exception {
        System.setProperty(
                AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES,
                base.getAbsolutePath() + "," + classes.getAbsolutePath());
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResource("classes"), equalTo(classes.toURI().toURL()));
        assertThat(loader.getResource("com"), equalTo(com.toURI().toURL()));
        assertThat(loader.getResource("asdfasdfasf"), nullValue());
    }

    @Test
    public void testChangesInSystemPropertyAreDynamic() throws Exception {
        //
        // PLUG-1188 - didn't do dynamic setting of alternate resource directories
        //
        // with no property its zero sized
        final AlternativeDirectoryResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResourceDirectories().size(), equalTo(0));

        // just one directory to start with
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, base.getAbsolutePath());
        assertThat(loader.getResourceDirectories().size(), equalTo(1));

        // add another one and see how it detects that
        System.setProperty(
                AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES,
                base.getAbsolutePath() + "," + classes.getAbsolutePath());
        assertThat(loader.getResourceDirectories().size(), equalTo(2));

        // and if we set the same value it stays stable
        System.setProperty(
                AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES,
                base.getAbsolutePath() + "," + classes.getAbsolutePath());
        assertThat(loader.getResourceDirectories().size(), equalTo(2));
    }

    @Test
    public void testGetResourceWithWhitespace() throws Exception {
        System.setProperty(
                AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES,
                "\n" + "         "
                        + base.getAbsolutePath() + ",\n" + "         "
                        + classes.getAbsolutePath() + "\n" + "         ");
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResource("classes"), equalTo(classes.toURI().toURL()));
        assertThat(loader.getResource("com"), equalTo(com.toURI().toURL()));
        assertThat(loader.getResource("asdfasdfasf"), nullValue());
    }

    @Test
    public void testGetResourceNoProperty() throws Exception {
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResource("classes"), nullValue());
        assertThat(loader.getResource("asdfasdfasf"), nullValue());
    }

    @Test
    public void testGetResourceAsStream() throws Exception {
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, base.getAbsolutePath());
        final AlternativeResourceLoader loader = new AlternativeDirectoryResourceLoader();
        InputStream kidInputStream = null;
        InputStream junkInputStream = null;
        try {
            kidInputStream = loader.getResourceAsStream("kid.txt");
            junkInputStream = loader.getResourceAsStream("asdfasdfasf");
            assertThat(kidInputStream, notNullValue());
            assertThat(junkInputStream, nullValue());
        } finally {
            IOUtils.closeQuietly(kidInputStream);
            IOUtils.closeQuietly(junkInputStream);
        }
    }

    @Test
    public void constructionLogsFoundAsDebugAndNotFoundAsWarning() {
        final File classes = new File(base, "classes");
        final File nonexistent = new File(base, "non-existent");
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, nonexistent + "," + classes);
        final AlternativeDirectoryResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResourceDirectories(), contains(classes));
        assertThat(capturedLogging, didLogDebug(classes.getPath(), "Found"));
        assertThat(capturedLogging, didLogWarn(nonexistent.getPath(), nonexistent.getAbsolutePath(), "does not exist"));
    }

    @Test
    public void emptyNamePluginResourceDirectoriesDoNotLogWarnings() {
        final File classes = new File(base, "classes");
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, "," + classes);
        final AlternativeDirectoryResourceLoader loader = new AlternativeDirectoryResourceLoader();
        assertThat(loader.getResourceDirectories(), contains(classes));
        // We should get no warnings at all
        assertThat(capturedLogging, not(didLogWarn()));
    }
}
