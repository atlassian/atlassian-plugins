package com.atlassian.plugin.util;

import java.net.URISyntaxException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

public class TestClassLoaderStack {

    private ClassLoader mainLoader;

    @Before
    public void setUp() throws Exception {
        mainLoader = Thread.currentThread().getContextClassLoader();
    }

    @After
    public void tearDown() throws Exception {
        Thread.currentThread().setContextClassLoader(mainLoader);
    }

    @Test
    public void testThreadClassLoaderIsReplacedAndRestored() throws URISyntaxException {
        ClassLoader pluginLoader1 = new MockClassLoader();
        ClassLoader pluginLoader2 = new MockClassLoader();

        ClassLoaderStack.push(pluginLoader1);
        assertSame(pluginLoader1, Thread.currentThread().getContextClassLoader());
        ClassLoaderStack.push(pluginLoader2);
        assertSame(pluginLoader2, Thread.currentThread().getContextClassLoader());
        ClassLoaderStack.pop();
        assertSame(pluginLoader1, Thread.currentThread().getContextClassLoader());
        ClassLoaderStack.pop();
        assertSame(mainLoader, Thread.currentThread().getContextClassLoader());
    }

    @Test
    public void testPopReturnsPreviousContextClassLoader() throws Exception {
        ClassLoader pluginLoader1 = new MockClassLoader();
        ClassLoader pluginLoader2 = new MockClassLoader();

        ClassLoaderStack.push(pluginLoader1);
        assertSame(pluginLoader1, Thread.currentThread().getContextClassLoader());
        ClassLoaderStack.push(pluginLoader2);
        assertSame(pluginLoader2, Thread.currentThread().getContextClassLoader());
        ClassLoader previous = ClassLoaderStack.pop();
        assertSame(pluginLoader2, previous);
        assertSame(pluginLoader1, Thread.currentThread().getContextClassLoader());
        previous = ClassLoaderStack.pop();
        assertSame(pluginLoader1, previous);
        assertSame(mainLoader, Thread.currentThread().getContextClassLoader());
    }

    @Test
    public void testPushAndPopHandleNull() throws Exception {
        // popping with empty stack should return null
        assertNull(ClassLoaderStack.pop());
        // pushing null should be a no-op
        ClassLoaderStack.push(null);
        assertSame(mainLoader, Thread.currentThread().getContextClassLoader());
        assertNull(ClassLoaderStack.pop());
    }

    public static class MockClassLoader extends ClassLoader {}
}
