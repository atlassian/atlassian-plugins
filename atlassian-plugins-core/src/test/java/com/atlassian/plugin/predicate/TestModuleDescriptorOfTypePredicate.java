package com.atlassian.plugin.predicate;

import java.util.function.Predicate;

import org.junit.Test;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testing {@link ModuleDescriptorOfTypePredicate}
 */
public class TestModuleDescriptorOfTypePredicate {

    @Test
    public void testMatchesModuleWithModuleDescriptorMatchingType() {
        final Mock mockModuleDescriptorFactory = new Mock(ModuleDescriptorFactory.class);
        mockModuleDescriptorFactory.matchAndReturn("getModuleDescriptorClass", C.ANY_ARGS, ModuleDescriptorStubA.class);

        final Predicate<ModuleDescriptor<Object>> moduleDescriptorPredicate = new ModuleDescriptorOfTypePredicate<>(
                (ModuleDescriptorFactory) mockModuleDescriptorFactory.proxy(), "test-module-type");
        assertTrue(moduleDescriptorPredicate.test(new ModuleDescriptorStubB()));
    }

    @Test
    public void testDoesNotMatchModuleWithModuleDescriptorNotMatchingType() {
        final Mock mockModuleDescriptorFactory = new Mock(ModuleDescriptorFactory.class);
        mockModuleDescriptorFactory.matchAndReturn("getModuleDescriptorClass", C.ANY_ARGS, ModuleDescriptorStubB.class);

        final Predicate<ModuleDescriptor<Object>> moduleDescriptorPredicate = new ModuleDescriptorOfTypePredicate<>(
                (ModuleDescriptorFactory) mockModuleDescriptorFactory.proxy(), "test-module-type");
        assertFalse(moduleDescriptorPredicate.test(
                new AbstractModuleDescriptor<Object>(ModuleFactory.LEGACY_MODULE_FACTORY) {
                    @Override
                    public Object getModule() {
                        throw new UnsupportedOperationException();
                    }
                }));
    }

    private static class ModuleDescriptorStubA extends AbstractModuleDescriptor<Object> {
        ModuleDescriptorStubA() {
            super(ModuleFactory.LEGACY_MODULE_FACTORY);
        }

        @Override
        public Object getModule() {
            return null;
        }
    }

    private static class ModuleDescriptorStubB extends ModuleDescriptorStubA {}
}
