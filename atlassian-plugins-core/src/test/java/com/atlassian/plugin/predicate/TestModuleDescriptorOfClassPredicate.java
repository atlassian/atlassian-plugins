package com.atlassian.plugin.predicate;

import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.MockUnusedModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testing {@link ModuleDescriptorOfClassPredicate}
 */
public class TestModuleDescriptorOfClassPredicate {

    private Predicate<ModuleDescriptor<?>> moduleDescriptorPredicate;

    @Before
    public void setUp() throws Exception {
        moduleDescriptorPredicate = new ModuleDescriptorOfClassPredicate(ModuleDescriptorStubA.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCannotCreateWithNullModuleDescritptorClassesArray() {
        new ModuleDescriptorOfClassPredicate((Class[]) null);
    }

    @Test
    public void testMatchesModuleWithModuleDescriptorClassExactlyMatchingClass() {
        assertTrue(moduleDescriptorPredicate.test(new ModuleDescriptorStubA()));
    }

    @Test
    public void testDoesNotMatchModuleWithModuleDescriptorClassExtendingButNotExactlyMatchingClass() {
        assertTrue(moduleDescriptorPredicate.test(new ModuleDescriptorStubB()));
    }

    @Test
    public void testDoesNotMatchModuleWithModuleDescriptorClassNotMatchingClass() {
        assertFalse(moduleDescriptorPredicate.test(new MockUnusedModuleDescriptor()));
    }

    private static class ModuleDescriptorStubA extends AbstractModuleDescriptor {
        ModuleDescriptorStubA() {
            super(ModuleFactory.LEGACY_MODULE_FACTORY);
        }

        public Object getModule() {
            return null;
        }
    }

    private static class ModuleDescriptorStubB extends ModuleDescriptorStubA {}
}
