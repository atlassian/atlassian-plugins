package com.atlassian.plugin.manager;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.PluginDependencies;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;

@RunWith(Parameterized.class)
public class TestDependentPluginsToPluginDependencyType extends TestDependentPlugins {
    @Parameterized.Parameters(name = "{index}: ({1}, {0})")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {
                ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC),
                ImmutableList.of("C"),
                new String[] {"A(OPTIONAL)", "B(DYNAMIC)", "D(OPTIONAL)", "F(OPTIONAL)", "E(MANDATORY)", "H(MANDATORY)"}
            },
            {
                ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC),
                ImmutableList.of("C", "D"),
                new String[] {"A(OPTIONAL)", "B(DYNAMIC)", "F(MANDATORY)", "E(MANDATORY)", "H(MANDATORY)"}
            },
            {
                ImmutableSet.of(MANDATORY, OPTIONAL),
                ImmutableList.of("G"),
                new String[] {
                    "C(MANDATORY)", "A(OPTIONAL)", "D(OPTIONAL)", "F(OPTIONAL)", "E(MANDATORY)", "H(MANDATORY)"
                }
            },
            {
                ImmutableSet.of(MANDATORY),
                ImmutableList.of("G"),
                new String[] {"C(MANDATORY)", "E(MANDATORY)", "H(MANDATORY)"}
            },
            {
                ImmutableSet.of(OPTIONAL),
                ImmutableList.of("C"),
                new String[] {
                    "A(OPTIONAL)", "D(OPTIONAL)", "F(OPTIONAL)",
                }
            },
            {ImmutableSet.of(OPTIONAL), ImmutableList.of("G"), new String[] {}},
            {
                ImmutableSet.of(DYNAMIC),
                ImmutableList.of("C"),
                new String[] {
                    "B(DYNAMIC)", "F(DYNAMIC)", "A(DYNAMIC)", "H(DYNAMIC)",
                }
            },
            {ImmutableSet.of(DYNAMIC), ImmutableList.of("G"), new String[] {}},
            {ImmutableSet.of(MANDATORY), ImmutableList.of("a"), new String[] {"b(MANDATORY)", "c(MANDATORY)"}},
            {
                ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC),
                ImmutableList.of("mC"),
                new String[] {"mA(MANDATORY)", "mX(MANDATORY)", "mB(DYNAMIC)", "mD(OPTIONAL)", "mE(MANDATORY)"}
            },
            {
                ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC),
                ImmutableList.of("mC", "mD", "mE"),
                new String[] {
                    "mA(MANDATORY)", "mB(DYNAMIC)", "mX(MANDATORY)",
                }
            },
            {ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC), ImmutableList.of("aC"), new String[] {}},
        });
    }

    @Parameterized.Parameter
    public Set<PluginDependencies.Type> dependencyTypes;

    @Parameterized.Parameter(1)
    public List<String> pluginKeys;

    @Parameterized.Parameter(2)
    public String[] expected;

    @Test
    public void testToPluginKeyDependencyTypes() throws Exception {
        final DependentPlugins dp = new DependentPlugins(pluginKeys, allPlugins, dependencyTypes);
        assertThat(dp.toStringList(), containsInAnyOrder(expected));
    }

    @Test
    public void testGet() throws Exception {
        final DependentPlugins dp = new DependentPlugins(pluginKeys, allPlugins, dependencyTypes);

        final Set<String> keysExpected = Arrays.stream(expected)
                .map(input -> input.substring(0, input.indexOf('(')))
                .collect(Collectors.toSet());

        assertThat(getKeys(dp.getPlugins(false)), is(equalTo(keysExpected)));
    }
}
