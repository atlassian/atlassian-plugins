package com.atlassian.plugin.manager;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;
import com.google.common.base.Predicates;

import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginTransactionEndEvent;
import com.atlassian.plugin.event.events.PluginTransactionStartEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.event.listeners.RecordingListener;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestPluginTransactionContext {

    private PluginTransactionContext pluginTransactionContext;
    private RecordingListener listener;

    @Before
    public void setUp() throws Exception {
        listener = new RecordingListener(PluginTransactionStartEvent.class, PluginTransactionEndEvent.class);
        final PluginEventManager pluginEventManager = new DefaultPluginEventManager();
        pluginEventManager.register(listener);
        this.pluginTransactionContext = new PluginTransactionContext(pluginEventManager);
    }

    private static class SomeEventFoo {}

    private static class SomeEventBar {}

    private static class SomeEventBaz {}

    @Test
    public void testStartStopLevelAndEvents() {
        final SomeEventBaz beforeTransaction = new SomeEventBaz();
        final SomeEventBar inTransaction1 = new SomeEventBar();
        final SomeEventFoo inTransaction2 = new SomeEventFoo();
        final SomeEventFoo inTransaction3 = new SomeEventFoo();
        final SomeEventBaz afterTransaction = new SomeEventBaz();

        pluginTransactionContext.addEvent(beforeTransaction);
        assertThat(pluginTransactionContext.getLevel(), equalTo(0));
        pluginTransactionContext.start();
        assertThat(pluginTransactionContext.getLevel(), equalTo(1));
        pluginTransactionContext.addEvent(inTransaction1);
        pluginTransactionContext.start();
        assertThat(pluginTransactionContext.getLevel(), equalTo(2));
        pluginTransactionContext.addEvent(inTransaction2);
        pluginTransactionContext.stop();
        assertThat(pluginTransactionContext.getLevel(), equalTo(1));
        pluginTransactionContext.addEvent(inTransaction3);
        pluginTransactionContext.stop();
        assertThat(pluginTransactionContext.getLevel(), equalTo(0));
        pluginTransactionContext.addEvent(afterTransaction);

        assertThat(
                listener.getEventClasses(),
                contains(PluginTransactionStartEvent.class, PluginTransactionEndEvent.class));

        final PluginTransactionEndEvent pluginTransactionStopEvent =
                (PluginTransactionEndEvent) listener.getEvents().get(1);
        assertThat(pluginTransactionStopEvent.numberOfEvents(), equalTo(3));
        assertTrue(pluginTransactionStopEvent.hasAnyEventOfTypeMatching(
                SomeEventBar.class, someEventBar -> someEventBar.equals(inTransaction1)));
        assertTrue(pluginTransactionStopEvent.hasAnyEventOfTypeMatching(
                SomeEventFoo.class, someEventFoo -> someEventFoo.equals(inTransaction2)));
        assertTrue(pluginTransactionStopEvent.hasAnyEventOfTypeMatching(
                SomeEventFoo.class, someEventFoo -> someEventFoo.equals(inTransaction3)));
    }

    @Test
    public void testNestedWrap() {
        final AtomicInteger callbackRuns = new AtomicInteger(0);
        assertThat(pluginTransactionContext.getLevel(), equalTo(0));
        pluginTransactionContext.wrap(() -> {
            assertThat(pluginTransactionContext.getLevel(), equalTo(1));
            pluginTransactionContext.wrap(() -> {
                assertThat(pluginTransactionContext.getLevel(), equalTo(2));
                callbackRuns.incrementAndGet();
            });
            assertThat(pluginTransactionContext.getLevel(), equalTo(1));
            callbackRuns.incrementAndGet();
        });
        assertThat(callbackRuns.get(), equalTo(2));
    }

    @Test
    public void testTransactionsInThreadContext() throws Exception {
        assertThat(pluginTransactionContext.getLevel(), equalTo(0));
        pluginTransactionContext.start();
        assertThat(pluginTransactionContext.getLevel(), equalTo(1));
        pluginTransactionContext.addEvent(new SomeEventFoo());
        pluginTransactionContext.addEvent(new SomeEventFoo());
        pluginTransactionContext.start();
        assertThat(pluginTransactionContext.getLevel(), equalTo(2));
        final AtomicBoolean threadRunWithNoException = new AtomicBoolean(false);
        final Thread t = new Thread(() -> {
            assertThat(pluginTransactionContext.getLevel(), equalTo(0));
            pluginTransactionContext.start();
            assertThat(pluginTransactionContext.getLevel(), equalTo(1));
            pluginTransactionContext.addEvent(new SomeEventBar());
            pluginTransactionContext.addEvent(new SomeEventBar());
            pluginTransactionContext.stop();
            assertThat(pluginTransactionContext.getLevel(), equalTo(0));
            threadRunWithNoException.set(true);
        });
        t.start();
        t.join();
        assertThat(threadRunWithNoException.get(), is(true));
        pluginTransactionContext.stop();
        pluginTransactionContext.stop();

        assertThat(
                listener.getEventClasses(),
                contains(
                        PluginTransactionStartEvent.class,
                        PluginTransactionStartEvent.class,
                        PluginTransactionEndEvent.class,
                        PluginTransactionEndEvent.class));

        final PluginTransactionEndEvent pluginTransactionStopEventOtherThread =
                (PluginTransactionEndEvent) listener.getEvents().get(2);
        final PluginTransactionEndEvent pluginTransactionStopEventCurrentThread =
                (PluginTransactionEndEvent) listener.getEvents().get(3);

        assertThat(pluginTransactionStopEventOtherThread.threadId(), equalTo(t.getId()));
        assertThat(
                pluginTransactionStopEventCurrentThread.threadId(),
                equalTo(Thread.currentThread().getId()));

        assertThat(
                pluginTransactionStopEventOtherThread.hasAnyEventOfTypeMatching(
                        SomeEventBar.class, Predicates.alwaysTrue()),
                equalTo(true));
        assertThat(
                pluginTransactionStopEventOtherThread.hasAnyEventOfTypeMatching(
                        SomeEventFoo.class, Predicates.alwaysTrue()),
                equalTo(false));
        assertThat(
                pluginTransactionStopEventCurrentThread.hasAnyEventOfTypeMatching(
                        SomeEventFoo.class, Predicates.alwaysTrue()),
                equalTo(true));
        assertThat(
                pluginTransactionStopEventCurrentThread.hasAnyEventOfTypeMatching(
                        SomeEventBar.class, Predicates.alwaysTrue()),
                equalTo(false));
    }
}
