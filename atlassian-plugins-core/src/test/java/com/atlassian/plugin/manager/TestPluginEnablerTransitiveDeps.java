package com.atlassian.plugin.manager;

import java.util.Collections;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.MockPluginAccessor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.exception.PluginExceptionInterception;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.mock;

public class TestPluginEnablerTransitiveDeps {

    @Rule
    public final MockitoRule mockito = MockitoJUnit.rule();

    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Mock
    private PluginController pluginController;

    @Mock
    private PluginExceptionInterception pluginExceptionInterception;

    private PluginEnabler pluginEnabler;

    @Before
    public void setUp() throws Exception {}

    private PluginAccessor mockAccessor(Plugin... plugins) {
        final MockPluginAccessor accessor = new MockPluginAccessor();

        for (Plugin p : plugins) {
            accessor.addPlugin(p);
        }
        return accessor;
    }

    private void assertPluginState(PluginState expected, PluginAccessor accessor, String... keys) {
        for (String key : keys) {
            Plugin p = accessor.getPlugin(key);
            assertThat("Plugin " + key + " not found", p, is(notNullValue()));
            assertThat(p + " not in " + expected + " state", p.getPluginState(), is(expected));
        }
    }

    @Test
    public void enablingPluginAlsoEnablesRequiredPluginsTransitively() {
        final PluginAccessor accessor = mockAccessor(
                new PluginWithDeps("foo", "foo2"), new PluginWithDeps("foo2", "foo3"), new PluginWithDeps("foo3"));

        pluginEnabler = new PluginEnabler(accessor, mock(PluginController.class), pluginExceptionInterception);

        // Enable a single plugin, which will recursively enable all deps
        pluginEnabler.enableAllRecursively(Collections.singletonList(accessor.getPlugin("foo")));

        assertPluginState(PluginState.ENABLED, accessor, "foo", "foo2", "foo3");
    }

    @Test
    public void enablingPluginAlsoEnablesRequiredPluginsTransitivelyWithCycles() {
        final PluginAccessor accessor = mockAccessor(
                new PluginWithDeps("foo", "foo2"),
                new PluginWithDeps("foo2", "foo3", "foo"),
                new PluginWithDeps("foo3", "foo2", "foo"));

        pluginEnabler = new PluginEnabler(accessor, mock(PluginController.class), pluginExceptionInterception);

        // Enable a single plugin, which will recursively enable all deps
        pluginEnabler.enableAllRecursively(Collections.singletonList(accessor.getPlugin("foo")));

        assertPluginState(PluginState.ENABLED, accessor, "foo", "foo2", "foo3");
    }

    @Test
    public void enablingPluginDoesNotEnableTransitivelyOptionalDepending() {
        final PluginAccessor accessor = mockAccessor(
                new PluginWithDeps("foo", "mandatory1"),
                new PluginWithDeps(
                        "mandatory1",
                        new PluginDependencies(
                                ImmutableSet.of("mandatory2"),
                                ImmutableSet.of("optional"),
                                ImmutableSet.of("dynamic"))),
                new PluginWithDeps("mandatory2"),
                new PluginWithDeps("optional", "mandatory_for_optional"),
                new PluginWithDeps("dynamic", "mandatory_for_dynamic"),
                new PluginWithDeps("mandatory_for_optional"),
                new PluginWithDeps("mandatory_for_dynamic"));

        pluginEnabler = new PluginEnabler(accessor, mock(PluginController.class), pluginExceptionInterception);

        // Enable a single plugin, which will recursively enable all deps
        pluginEnabler.enableAllRecursively(Collections.singletonList(accessor.getPlugin("foo")));

        assertPluginState(PluginState.ENABLED, accessor, "foo", "mandatory1", "mandatory2");
        assertPluginState(PluginState.DISABLED, accessor, "optional", "mandatory_for_optional");
        assertPluginState(PluginState.DISABLED, accessor, "dynamic", "mandatory_for_dynamic");
    }
}
