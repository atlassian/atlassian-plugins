package com.atlassian.plugin.manager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.manager.StateTracker.State;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.fail;

import static com.atlassian.plugin.manager.StateTracker.State.DELAYED;
import static com.atlassian.plugin.manager.StateTracker.State.NOT_STARTED;
import static com.atlassian.plugin.manager.StateTracker.State.RESUMING;
import static com.atlassian.plugin.manager.StateTracker.State.SHUTDOWN;
import static com.atlassian.plugin.manager.StateTracker.State.SHUTTING_DOWN;
import static com.atlassian.plugin.manager.StateTracker.State.STARTED;
import static com.atlassian.plugin.manager.StateTracker.State.STARTING;
import static com.atlassian.plugin.manager.StateTracker.State.WARM_RESTARTING;

@RunWith(MockitoJUnitRunner.class)
public class TestStateTracker {
    private StateTracker stateTracker;

    @Before
    public void setup() {
        stateTracker = new StateTracker();
    }

    @After
    public void tearDown() {
        stateTracker = null;
    }

    @Test
    public void testStandardTransitions() throws Exception {
        assertThat(stateTracker.get(), is(NOT_STARTED));
        // The standardTransitions is one loop of the path StateTracker usually runs through.
        // This is not just State.values() - it omits the start point NOT_STARTED, and also
        // WARM_RESTARTING which is tested independently later.
        final State[] standardTransitions = {STARTING, DELAYED, RESUMING, STARTED, SHUTTING_DOWN, SHUTDOWN, STARTING};
        for (final State state : standardTransitions) {
            stateTracker.setState(state);
            assertThat(stateTracker.get(), is(state));
        }
    }

    private void performStateSequence(final State... desiredStates) {
        for (final State state : desiredStates) {
            stateTracker.setState(state);
        }
    }

    @Test
    public void testIllegalNotStartedTransitions() throws Exception {
        performStateSequence();
        validateIllegalStates(NOT_STARTED, DELAYED, RESUMING, STARTED, WARM_RESTARTING, SHUTDOWN);
    }

    @Test
    public void testIllegalStartingTransitions() throws Exception {
        performStateSequence(STARTING);
        validateIllegalStates(NOT_STARTED, STARTING, RESUMING, STARTED, WARM_RESTARTING, SHUTTING_DOWN, SHUTDOWN);
    }

    @Test
    public void testIllegalDelayedTransitions() throws Exception {
        performStateSequence(STARTING, DELAYED);
        validateIllegalStates(NOT_STARTED, STARTING, DELAYED, STARTED, WARM_RESTARTING, SHUTDOWN);
    }

    @Test
    public void testIllegalResumingTransitions() throws Exception {
        performStateSequence(STARTING, DELAYED, RESUMING);
        validateIllegalStates(NOT_STARTED, STARTING, DELAYED, RESUMING, WARM_RESTARTING, SHUTTING_DOWN, SHUTDOWN);
    }

    @Test
    public void testIllegalStartedTransitions() throws Exception {
        performStateSequence(STARTING, DELAYED, RESUMING, STARTED);
        validateIllegalStates(NOT_STARTED, STARTING, DELAYED, RESUMING, STARTED, SHUTDOWN);
    }

    @Test
    public void testIllegalWarmRestartingTransitions() throws Exception {
        performStateSequence(STARTING, DELAYED, RESUMING, STARTED, WARM_RESTARTING);
        validateIllegalStates(NOT_STARTED, STARTING, DELAYED, RESUMING, WARM_RESTARTING, SHUTTING_DOWN, SHUTDOWN);
    }

    @Test
    public void testIllegalWarmRestartTransitions() throws Exception {
        performStateSequence(STARTING, DELAYED, RESUMING, STARTED, WARM_RESTARTING, STARTED);
        validateIllegalStates(NOT_STARTED, STARTING, DELAYED, RESUMING, STARTED, SHUTDOWN);
    }

    @Test
    public void testIllegalShuttingDownTransitions() throws Exception {
        performStateSequence(STARTING, DELAYED, RESUMING, STARTED, SHUTTING_DOWN);
        validateIllegalStates(NOT_STARTED, STARTING, DELAYED, RESUMING, STARTED, WARM_RESTARTING, SHUTTING_DOWN);
    }

    @Test
    public void testIllegalShutdownTransitions() throws Exception {
        performStateSequence(STARTING, DELAYED, RESUMING, STARTED, SHUTTING_DOWN, SHUTDOWN);
        validateIllegalStates(NOT_STARTED, DELAYED, RESUMING, STARTED, WARM_RESTARTING, SHUTTING_DOWN, SHUTDOWN);
    }

    @Test
    public void canShutdownDirectlyFromEarlyStartup() {
        performStateSequence(STARTING, DELAYED, SHUTTING_DOWN, SHUTDOWN);
    }

    private void validateIllegalStates(final State... illegalStates) {
        final State initialState = stateTracker.get();
        for (final State illegalState : illegalStates) {
            try {
                stateTracker.setState(illegalState);
                fail(initialState + " should not be allowed to transition to: " + illegalState);
            } catch (final IllegalStateException expected) {
                assertThat(stateTracker.get(), is(initialState));
            }
        }
    }
}
