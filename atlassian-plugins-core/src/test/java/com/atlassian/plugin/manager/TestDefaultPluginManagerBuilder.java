package com.atlassian.plugin.manager;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.classloader.PluginsClassLoader;
import com.atlassian.plugin.event.PluginEventManager;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.mockito.Mockito.when;

public class TestDefaultPluginManagerBuilder {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private PluginsClassLoader pluginsClassLoader;

    @Mock
    private PluginPersistentStateStore pluginsPersistentStateStore;

    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;

    @Mock
    private PluginEventManager pluginEventManager;

    @Test
    public void pluginsClassLoaderFromPluginAccessor() {
        when(pluginAccessor.getClassLoader()).thenReturn(pluginsClassLoader);

        DefaultPluginManager dpm = new DefaultPluginManager(DefaultPluginManager.newBuilder()
                .withPluginAccessor(pluginAccessor)
                .withStore(pluginsPersistentStateStore)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager));

        assertThat(dpm.getClassLoader(), is(pluginsClassLoader));
    }

    @Test
    public void pluginsClassLoaderCreatedInConstructor() {
        when(pluginAccessor.getClassLoader()).thenReturn(pluginsClassLoader);

        DefaultPluginManager dpm = new DefaultPluginManager(DefaultPluginManager.newBuilder()
                .withStore(pluginsPersistentStateStore)
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager));

        assertThat(dpm.getClassLoader(), isA(PluginsClassLoader.class));
    }
}
