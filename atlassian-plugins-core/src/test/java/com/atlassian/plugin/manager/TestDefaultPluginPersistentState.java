package com.atlassian.plugin.manager;

import java.util.Map;

import org.junit.Test;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.module.ModuleFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasKey;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class TestDefaultPluginPersistentState {
    @Test
    public void testSetEnabledPlugin() {
        PluginPersistentState state = PluginPersistentState.builder().toState();
        final StaticPlugin plugin = createMockPlugin("mock.plugin.key", true);
        state = PluginPersistentState.Builder.create(state)
                .setEnabled(plugin, true)
                .toState();
        assertTrue(state.isEnabled(plugin));
        state = PluginPersistentState.Builder.create(state)
                .setEnabled(plugin, false)
                .toState();
        assertFalse(state.isEnabled(plugin));
    }

    @Test
    public void testSetEnabledModuleDescriptor() {
        PluginPersistentState state = PluginPersistentState.builder().toState();
        final ModuleDescriptor<?> module = createModule("mock.plugin.key", "module.key");
        state = PluginPersistentState.Builder.create(state)
                .setEnabled(module, true)
                .toState();
        assertTrue(state.isEnabled(module));
        state = PluginPersistentState.Builder.create(state)
                .setEnabled(module, false)
                .toState();
        assertFalse(state.isEnabled(module));
    }

    @Test
    public void testGetPluginStateMap() {
        final StaticPlugin plugin1 = createMockPlugin("mock.plugin.key", true);
        final StaticPlugin plugin2 = createMockPlugin("two.mock.plugin.key", true);
        final ModuleDescriptor<?> module1 = createModule("mock.plugin.key", "module.key.1");
        final ModuleDescriptor<?> module2 = createModule("mock.plugin.key", "module.key.2");
        final ModuleDescriptor<?> module3 = createModule("mock.plugin.key", "module.key.3");
        // because all plugins and modules are enabled by default lets disable them

        final PluginPersistentState.Builder builder = PluginPersistentState.Builder.create();
        builder.setEnabled(plugin1, !plugin1.isEnabledByDefault());
        builder.setEnabled(plugin2, !plugin1.isEnabledByDefault());
        builder.setEnabled(module1, !module1.isEnabledByDefault());
        builder.setEnabled(module2, !module2.isEnabledByDefault());
        builder.setEnabled(module3, !module3.isEnabledByDefault());

        final PluginPersistentState state = builder.toState();
        final Map<String, PluginEnabledState> pluginStateMap = state.getPluginEnabledStateMap(plugin1);
        final PluginPersistentState pluginState = PluginPersistentState.builder()
                .addPluginEnabledState(pluginStateMap)
                .toState();

        assertNotEquals(pluginState.isEnabled(plugin1), plugin1.isEnabledByDefault());
        assertNotEquals(pluginState.isEnabled(module1), module1.isEnabledByDefault());
        assertNotEquals(pluginState.isEnabled(module2), module2.isEnabledByDefault());
        assertNotEquals(pluginState.isEnabled(module3), module3.isEnabledByDefault());
        // plugin2 should not be part of the map therefore it should have default enabled value
        assertEquals(pluginState.isEnabled(plugin2), plugin2.isEnabledByDefault());
    }

    @Test
    public void testDefaultModuleStateShouldBeStored() {
        final String pluginKey = "mock.plugin.key";
        StaticPlugin plugin = createMockPlugin(pluginKey, true);
        final PluginPersistentState.Builder builder = PluginPersistentState.Builder.create();
        builder.setEnabled(plugin, true);
        Map<String, PluginEnabledState> pluginStateMap = builder.toState().getPluginEnabledStateMap(plugin);
        assertThat(pluginStateMap, hasKey(pluginKey));

        builder.setEnabled(plugin, false);
        pluginStateMap = builder.toState().getPluginEnabledStateMap(plugin);
        assertFalse(pluginStateMap.isEmpty());

        builder.removeState(pluginKey);

        plugin = createMockPlugin(pluginKey, false);
        builder.setEnabled(plugin, false);
        pluginStateMap = builder.toState().getPluginEnabledStateMap(plugin);
        assertThat(pluginStateMap, hasKey(pluginKey));
        builder.setEnabled(plugin, true);
        pluginStateMap = builder.toState().getPluginEnabledStateMap(plugin);
        assertFalse(pluginStateMap.isEmpty());
    }

    @Test
    public void testPluginRestartState() {
        PluginPersistentState state = PluginPersistentState.builder().toState();
        assertEquals(PluginRestartState.NONE, state.getPluginRestartState("foo"));

        state = PluginPersistentState.Builder.create(state)
                .setPluginRestartState("foo", PluginRestartState.REMOVE)
                .toState();
        assertEquals(PluginRestartState.REMOVE, state.getPluginRestartState("foo"));

        PluginPersistentState stateCopy = PluginPersistentState.builder(state).toState();
        assertEquals(PluginRestartState.REMOVE, stateCopy.getPluginRestartState("foo"));
        stateCopy = PluginPersistentState.Builder.create(state)
                .clearPluginRestartState()
                .toState();
        assertEquals(PluginRestartState.NONE, stateCopy.getPluginRestartState("foo"));
        assertEquals(PluginRestartState.REMOVE, state.getPluginRestartState("foo"));
    }

    @Test
    public void testPluginRestartStateRemoveExisting() {
        final PluginPersistentState.Builder builder = PluginPersistentState.Builder.create();
        builder.setPluginRestartState("foo", PluginRestartState.UPGRADE);
        assertEquals(PluginRestartState.UPGRADE, builder.toState().getPluginRestartState("foo"));
        builder.setPluginRestartState("foo", PluginRestartState.REMOVE);
        assertEquals(PluginRestartState.REMOVE, builder.toState().getPluginRestartState("foo"));
    }

    private <T> ModuleDescriptor<T> createModule(final String pluginKey, final String moduleKey) {
        return new AbstractModuleDescriptor<T>(ModuleFactory.LEGACY_MODULE_FACTORY) {
            @Override
            public T getModule() {
                return null;
            }

            @Override
            public String getCompleteKey() {
                return pluginKey + ':' + moduleKey;
            }
        };
    }

    private StaticPlugin createMockPlugin(final String pluginKey, final boolean enabledByDefault) {
        final StaticPlugin plugin = new StaticPlugin();
        plugin.setKey(pluginKey);
        plugin.setEnabledByDefault(enabledByDefault);
        return plugin;
    }
}
