package com.atlassian.plugin.manager;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.factories.LegacyDynamicPluginFactory;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.loaders.DirectoryPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.repositories.FilePluginInstaller;
import com.atlassian.plugin.test.PluginJarBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import static com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils.createFillAndCleanTempPluginDirectory;

public class TestDefaultPluginManagerLongRunning {
    /**
     * the object being tested
     */
    private DefaultPluginManager manager;

    private PluginPersistentStateStore pluginStateStore;
    private DefaultModuleDescriptorFactory moduleDescriptorFactory; // we should be able to use the interface here?

    private DirectoryPluginLoader directoryPluginLoader;
    private PluginEventManager pluginEventManager;

    @Before
    public void setUp() throws Exception {
        pluginEventManager = new DefaultPluginEventManager();

        pluginStateStore = new MemoryPluginPersistentStateStore();
        moduleDescriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());
    }

    @After
    public void tearDown() throws Exception {
        manager = null;
        moduleDescriptorFactory = null;
        pluginStateStore = null;

        if (directoryPluginLoader != null) {
            directoryPluginLoader.onShutdown(null);
            directoryPluginLoader = null;
        }
    }

    private DefaultPluginManager newDefaultPluginManager(PluginLoader... pluginLoaders) {
        return new DefaultPluginManager(
                pluginStateStore,
                ImmutableList.copyOf(pluginLoaders),
                moduleDescriptorFactory,
                new DefaultPluginEventManager());
    }

    @Test
    public void testEnableFailed() {
        final Mock mockPluginLoader = new Mock(PluginLoader.class);
        final Plugin plugin = new StaticPlugin() {
            public PluginState enableInternal() {
                return PluginState.DISABLED;
            }

            public void disableInternal() {
                // do nothing
            }
        };
        plugin.setKey("foo");
        plugin.setEnabledByDefault(false);
        plugin.setPluginInformation(new PluginInformation());

        mockPluginLoader.expectAndReturn("loadAllPlugins", C.ANY_ARGS, Collections.singletonList(plugin));

        manager = newDefaultPluginManager((PluginLoader) mockPluginLoader.proxy());
        manager.init();

        assertEquals(1, manager.getPlugins().size());
        assertEquals(0, manager.getEnabledPlugins().size());
        assertFalse(plugin.getPluginState() == PluginState.ENABLED);
        manager.enablePlugins("foo");
        assertEquals(1, manager.getPlugins().size());
        assertEquals(0, manager.getEnabledPlugins().size());
        assertFalse(plugin.getPluginState() == PluginState.ENABLED);
    }

    private DefaultPluginManager makeClassLoadingPluginManager(File pluginsTestDir) {
        directoryPluginLoader = new DirectoryPluginLoader(
                pluginsTestDir,
                ImmutableList.of(new LegacyDynamicPluginFactory(PluginAccessor.Descriptor.FILENAME)),
                pluginEventManager);

        manager = newDefaultPluginManager(directoryPluginLoader);

        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        manager.init();
        return manager;
    }

    @Test
    public void testInstallPluginTwiceWithSameName() throws Exception {
        final DirectoryPluginLoaderUtils.ScannerDirectories directories = createFillAndCleanTempPluginDirectory();
        File pluginsTestDir = directories.pluginsTestDir;

        FileUtils.cleanDirectory(pluginsTestDir);
        final File plugin = File.createTempFile("plugin", ".jar");
        plugin.delete();
        File jar = new PluginJarBuilder("plugin")
                .addPluginInformation("some.key", "My name", "1.0", 1)
                .addResource("foo.txt", "foo")
                .addJava("my.MyClass", "package my; public class MyClass {}")
                .build();
        FileUtils.moveFile(jar, plugin);

        final DefaultPluginManager manager = makeClassLoadingPluginManager(pluginsTestDir);
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));

        final String pluginKey = Iterables.getOnlyElement(manager.installPlugins(new JarPluginArtifact(plugin)));

        assertTrue(new File(pluginsTestDir, plugin.getName()).exists());

        final Plugin installedPlugin = manager.getPlugin(pluginKey);
        assertNotNull(installedPlugin);
        InputStream s0 = installedPlugin.getClassLoader().getResourceAsStream("foo.txt");
        assertNotNull(s0);
        s0.close();
        assertNull(installedPlugin.getClassLoader().getResourceAsStream("bar.txt"));
        assertNotNull(installedPlugin.getClassLoader().loadClass("my.MyClass"));
        try {
            installedPlugin.getClassLoader().loadClass("my.MyNewClass");
            fail("Expected ClassNotFoundException for unknown class");
        } catch (final ClassNotFoundException e) {
            // expected
        }

        // sleep to ensure the new plugin is picked up
        Thread.sleep(1000);

        File jartmp = new PluginJarBuilder("plugin")
                .addPluginInformation("some.key", "My name", "1.0", 1)
                .addResource("bar.txt", "bar")
                .addJava("my.MyNewClass", "package my; public class MyNewClass {}")
                .build();
        plugin.delete();
        FileUtils.moveFile(jartmp, plugin);

        // reinstall the plugin
        final String pluginKey2 = Iterables.getOnlyElement(manager.installPlugins(new JarPluginArtifact(plugin)));

        assertTrue(new File(pluginsTestDir, plugin.getName()).exists());

        final Plugin installedPlugin2 = manager.getPlugin(pluginKey2);
        assertNotNull(installedPlugin2);
        assertEquals(1, manager.getEnabledPlugins().size());
        assertNull(installedPlugin2.getClassLoader().getResourceAsStream("foo.txt"));
        InputStream s1 = installedPlugin2.getClassLoader().getResourceAsStream("bar.txt");
        assertNotNull(s1);
        s1.close();
        assertNotNull(installedPlugin2.getClassLoader().loadClass("my.MyNewClass"));
        try {
            installedPlugin2.getClassLoader().loadClass("my.MyClass");
            fail("Expected ClassNotFoundException for unknown class");
        } catch (final ClassNotFoundException e) {
            // expected
        }
    }

    @Test
    public void testInstallPluginTwiceWithDifferentName() throws Exception {
        final DirectoryPluginLoaderUtils.ScannerDirectories directories = createFillAndCleanTempPluginDirectory();
        File pluginsTestDir = directories.pluginsTestDir;

        FileUtils.cleanDirectory(pluginsTestDir);
        final File plugin1 = new PluginJarBuilder("plugin")
                .addPluginInformation("some.key", "My name", "1.0", 1)
                .addResource("foo.txt", "foo")
                .addJava("my.MyClass", "package my; public class MyClass {}")
                .build();

        final DefaultPluginManager manager = makeClassLoadingPluginManager(pluginsTestDir);
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));

        final String pluginKey = Iterables.getOnlyElement(manager.installPlugins(new JarPluginArtifact(plugin1)));

        assertTrue(new File(pluginsTestDir, plugin1.getName()).exists());

        final Plugin installedPlugin = manager.getPlugin(pluginKey);
        assertNotNull(installedPlugin);
        InputStream s0 = installedPlugin.getClassLoader().getResourceAsStream("foo.txt");
        assertNotNull(s0);
        s0.close();
        InputStream s1 = installedPlugin.getClassLoader().getResourceAsStream("bar.txt");
        assertNull(s1);
        assertNotNull(installedPlugin.getClassLoader().loadClass("my.MyClass"));
        try {
            installedPlugin.getClassLoader().loadClass("my.MyNewClass");
            fail("Expected ClassNotFoundException for unknown class");
        } catch (final ClassNotFoundException e) {
            // expected
        }

        // sleep to ensure the new plugin is picked up
        Thread.sleep(1000);

        final File plugin2 = new PluginJarBuilder("plugin")
                .addPluginInformation("some.key", "My name", "1.0", 1)
                .addResource("bar.txt", "bar")
                .addJava("my.MyNewClass", "package my; public class MyNewClass {}")
                .build();

        // reinstall the plugin
        final String pluginKey2 = Iterables.getOnlyElement(manager.installPlugins(new JarPluginArtifact(plugin2)));

        assertFalse(new File(pluginsTestDir, plugin1.getName()).exists());
        assertTrue(new File(pluginsTestDir, plugin2.getName()).exists());

        final Plugin installedPlugin2 = manager.getPlugin(pluginKey2);
        assertNotNull(installedPlugin2);
        assertEquals(1, manager.getEnabledPlugins().size());
        InputStream s2 = installedPlugin2.getClassLoader().getResourceAsStream("foo.txt");
        assertNull(s2);
        InputStream s3 = installedPlugin2.getClassLoader().getResourceAsStream("bar.txt");
        assertNotNull(s3);
        s3.close();
        assertNotNull(installedPlugin2.getClassLoader().loadClass("my.MyNewClass"));
        try {
            installedPlugin2.getClassLoader().loadClass("my.MyClass");
            fail("Expected ClassNotFoundException for unknown class");
        } catch (final ClassNotFoundException e) {
            // expected
        }
    }

    @Test
    public void testAddPluginsWithDependencyIssuesNoResolution() throws Exception {
        final Plugin servicePlugin = new EnableInPassPlugin("service.plugin", 4);
        final Plugin clientPlugin = new EnableInPassPlugin("client.plugin", 1);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Arrays.asList(servicePlugin, clientPlugin));

        assertTrue(clientPlugin.getPluginState() == PluginState.ENABLED);
        assertFalse(servicePlugin.getPluginState() == PluginState.ENABLED);
    }

    private static class EnableInPassPlugin extends StaticPlugin {
        private int pass;

        public EnableInPassPlugin(final String key, final int pass) {
            this.pass = pass;
            setKey(key);
        }

        @Override
        public void enable() {
            if (--pass <= 0) {
                super.enable();
            }
        }
    }
}
