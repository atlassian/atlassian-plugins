package com.atlassian.plugin.manager.store;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.manager.PluginEnabledState;
import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.manager.PluginEnabledState.getPluginEnabledStateWithCurrentTime;
import static com.atlassian.plugin.manager.store.PluginPersistentStateStoreMigrator.removeDirectives;

@RunWith(MockitoJUnitRunner.class)
public class TestPluginPersistentStateStoreMigrator {

    @Mock
    private PluginPersistentStateStore customStore;

    @Before
    public void setUp() throws Exception {
        PluginPersistentState.Builder builder = PluginPersistentState.Builder.create();

        Plugin plugin = new StaticPlugin();
        plugin.setKey("foo111");
        plugin.setEnabledByDefault(true);
        builder.setEnabled(plugin, false);

        final Map<String, PluginEnabledState> state = new HashMap<>();
        state.put("foo-1.0.0", pluginEnabled());
        state.put("restarting--foo2-1.0.0", pluginEnabled());
        state.put("foo;singleton:=true-1.0.0", pluginEnabled());
        state.put("bar;singleton:=true-1.0.0", pluginEnabled());
        state.put("bar;singleton:=true-2.0.0", pluginEnabled());
        state.put("alpha;singleton:=true", pluginEnabled());
        state.put("beta-2.0.0", pluginEnabled());
        builder.addPluginEnabledState(state);

        when(customStore.load()).thenReturn(builder.toState());
        doAnswer(invocationOnMock -> {
                    final PluginPersistentState pluginState =
                            (PluginPersistentState) invocationOnMock.getArguments()[0];
                    when(customStore.load()).thenReturn(pluginState);
                    return null;
                })
                .when(customStore)
                .save(isA(PluginPersistentState.class));
    }

    @Test
    public void testRemoveDirectives() {
        removeDirectives(customStore);

        Map<String, PluginEnabledState> newState = customStore.load().getStatesMap();
        assertThat(newState.size(), is(7));
        assertThat(newState, hasKey("foo111"));
        assertThat(newState, hasKey("foo-1.0.0"));
        assertThat(newState, hasKey("restarting--foo2-1.0.0"));
        assertThat(newState, hasKey("bar-1.0.0"));
        assertThat(newState, hasKey("bar-2.0.0"));
        assertThat(newState, hasKey("alpha"));
        assertThat(newState, hasKey("beta-2.0.0"));
    }

    private static PluginEnabledState pluginEnabled() {
        return getPluginEnabledStateWithCurrentTime(true);
    }
}
