package com.atlassian.plugin.manager;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRegistry;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

import static com.atlassian.plugin.manager.PluginWithDeps.GET_KEY;

public class TestPluginsInEnableOrder {
    private static class PluginSystemStateAccessor implements PluginRegistry.ReadOnly {

        private final Map<String, Plugin> plugins;

        private PluginSystemStateAccessor(List<Plugin> pluginList) {
            this.plugins = Maps.uniqueIndex(pluginList, GET_KEY);
        }

        @Override
        public Collection<Plugin> getAll() {
            return plugins.values();
        }

        @Override
        public Plugin get(final String key) {
            return plugins.get(key);
        }
    }

    @Test(timeout = 1000)
    public void sortPluginsForEnableSorts() throws Exception {
        Plugin a = new PluginWithDeps("a");
        Plugin b = new PluginWithDeps("b", "a");
        Plugin c = new PluginWithDeps("c", "b");

        final ImmutableList<Plugin> plugins = ImmutableList.of(b, c, a);

        List<Plugin> sortedList = new PluginsInEnableOrder(plugins, new PluginSystemStateAccessor(plugins)).get();

        assertThat(sortedList, contains(a, b, c));
    }

    @Test(timeout = 1000)
    public void sortPluginsForEnableSortsCycle() throws Exception {
        Plugin a = new PluginWithDeps("a");
        Plugin b = new PluginWithDeps("b", "a", "d");
        Plugin c = new PluginWithDeps("c", "b");
        Plugin d = new PluginWithDeps("d", "c");

        final ImmutableList<Plugin> plugins = ImmutableList.of(b, c, a, d);

        List<Plugin> sortedList = new PluginsInEnableOrder(plugins, new PluginSystemStateAccessor(plugins)).get();

        assertThat(sortedList, containsInAnyOrder(a, b, c, d));

        // depending on the set key order the first could be either a or c
        assertThat(sortedList.get(0), anyOf(is(a), is(c)));
    }

    @Test(timeout = 1000)
    public void sortPluginsForEnableIgnoresNonPluginRequirements() {
        Plugin a = new PluginWithDeps("a");
        Plugin b = new PluginWithDeps("b", "a", "framework");
        Plugin c = new PluginWithDeps("c", "b", "host");

        final ImmutableList<Plugin> plugins = ImmutableList.of(b, c, a);

        List<Plugin> sortedList = new PluginsInEnableOrder(plugins, new PluginSystemStateAccessor(plugins)).get();

        assertThat(sortedList, contains(a, b, c));
    }

    @Test(timeout = 1000)
    public void sortPluginsForEnableTraversesButHidesUnrequestedPlugins() {
        Plugin a = new PluginWithDeps("a");
        Plugin b = new PluginWithDeps("b", "a");
        Plugin c = new PluginWithDeps("c", "b");

        final ImmutableList<Plugin> pluginsToEnable = ImmutableList.of(c, a);
        final ImmutableList<Plugin> allPlugins = ImmutableList.of(a, b, c);

        List<Plugin> sortedList =
                new PluginsInEnableOrder(pluginsToEnable, new PluginSystemStateAccessor(allPlugins)).get();

        assertThat(sortedList, contains(a, c));
    }
}
