package com.atlassian.plugin.manager.store;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

@RunWith(MockitoJUnitRunner.class)
public class TestDelegatingPluginPersistentStateStore {
    @Mock
    PluginPersistentState state;

    @Mock
    PluginPersistentStateStore delegate;

    DelegatingPluginPersistentStateStore delegatingStore;

    @Before
    public void setUp() {
        delegatingStore = new DelegatingPluginPersistentStateStore() {
            @Override
            public PluginPersistentStateStore getDelegate() {
                return delegate;
            }
        };
    }

    @Test
    public void getDelegateIsCalledBeforeDispatch() {
        final PluginPersistentStateStore oldDelegate = delegate;
        final PluginPersistentStateStore newDelegate = mock(PluginPersistentStateStore.class);

        // save with old delegate
        delegatingStore.save(state);
        // load with new delegate
        delegate = newDelegate;
        delegatingStore.load();

        verify(oldDelegate).save(state);
        verify(newDelegate, never()).save(argThat(any(PluginPersistentState.class)));
        verify(oldDelegate, never()).load();
        verify(newDelegate).load();
    }

    @Test
    public void saveIsForwarded() {
        delegatingStore.save(state);
        verify(delegate).save(state);
    }

    @Test
    public void loadIsForwarded() {
        when(delegate.load()).thenReturn(state);
        final PluginPersistentState loadedState = delegatingStore.load();
        assertThat(loadedState, is(state));
        verify(delegate).load();
    }
}
