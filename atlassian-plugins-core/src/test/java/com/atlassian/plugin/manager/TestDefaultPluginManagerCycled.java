package com.atlassian.plugin.manager;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.loaders.PluginLoader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.PluginState.DISABLED;
import static com.atlassian.plugin.PluginState.ENABLED;
import static com.atlassian.plugin.PluginState.UNINSTALLED;

public class TestDefaultPluginManagerCycled extends TestDependentPlugins {

    private static final List<Set<SingleAction>> initialEnabling = ImmutableList.of(
            actions(
                    ENABLED, "A", "a", "B", "b", "aC", "c", "C", "D", "E", "F", "G", "H", "k5", "k4", "k3", "k2", "k1",
                    "k0", "mX", "mA", "mB", "mC", "mD", "mE") // plugins are enabled in unspecified order
            );

    @Rule
    public final MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private PluginPersistentStateStore pluginPersistentStateStore;

    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;

    @Mock
    private PluginEventManager pluginEventManager;

    @Mock
    private PluginPersistentState pluginPersistentState;

    @Mock
    private PluginLoader pluginLoader;

    private DefaultPluginManager pluginManager;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        when(pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(allPlugins);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        doAnswer((Answer<Void>) invocationOnMock -> {
                    invocationOnMock.<Plugin>getArgument(0).uninstall();
                    return null;
                })
                .when(pluginLoader)
                .removePlugin(any(Plugin.class));

        pluginManager = new DefaultPluginManager(
                pluginPersistentStateStore, Arrays.asList(pluginLoader), moduleDescriptorFactory, pluginEventManager);
        when(pluginPersistentStateStore.load()).thenReturn(pluginPersistentState);
        when(pluginPersistentState.getPluginRestartState(any(String.class))).thenReturn(PluginRestartState.NONE);
        pluginManager.init();

        pluginManager.enablePlugins(getKeys(allPlugins).toArray(new String[allPlugins.size()]));
    }

    @Test
    public void disableSinglePlugin() {
        test(
                PluginState.DISABLED,
                ImmutableList.of("G"),
                ImmutableList.<Set<SingleAction>>builder()
                        .add(actions(DISABLED, "A", "F", "H"))
                        .add(actions(DISABLED, "D", "E"))
                        .add(actions(DISABLED, "C"))
                        .add(actions(DISABLED, "G"))
                        .add(actions(ENABLED, "D"))
                        .add(actions(ENABLED, "A", "F"))
                        .build());
    }

    @Test
    public void uninstallSinglePlugin() {
        test(
                PluginState.UNINSTALLED,
                ImmutableList.of("G"),
                ImmutableList.<Set<SingleAction>>builder()
                        .add(actions(DISABLED, "A", "F", "H"))
                        .add(actions(DISABLED, "D", "E", "B"))
                        .add(actions(DISABLED, "C"))
                        .add(actions(DISABLED, "G"))
                        .add(actions(UNINSTALLED, "G"))
                        .add(actions(ENABLED, "D", "B"))
                        .add(actions(ENABLED, "A", "F"))
                        .build());
    }

    @Test
    public void disableSinglePluginWithCircularDependency() {
        test(
                PluginState.DISABLED,
                ImmutableList.of("a"),
                ImmutableList.<Set<SingleAction>>builder()
                        .add(actions(DISABLED, "b"))
                        .add(actions(DISABLED, "c"))
                        .add(actions(DISABLED, "a"))
                        .build());
    }

    @Test
    public void uninstallSinglePluginWithCircularDependency() {
        test(
                PluginState.UNINSTALLED,
                ImmutableList.of("a"),
                ImmutableList.<Set<SingleAction>>builder()
                        .add(actions(DISABLED, "b"))
                        .add(actions(DISABLED, "c"))
                        .add(actions(DISABLED, "a"))
                        .add(actions(UNINSTALLED, "a"))
                        .build());
    }

    @Test
    public void uninstallMultiplePlugins() {
        test(
                PluginState.UNINSTALLED,
                ImmutableList.of("G", "D", "E"),
                ImmutableList.<Set<SingleAction>>builder()
                        .add(actions(DISABLED, "A", "F", "H"))
                        .add(actions(DISABLED, "D", "E", "B", "C"))
                        .add(actions(DISABLED, "G"))
                        .add(actions(UNINSTALLED, "G", "D", "E"))
                        .add(actions(ENABLED, "B"))
                        .add(actions(ENABLED, "A"))
                        .build());
    }

    @Test
    public void uninstallMultiplePluginsWithCircularDependency() {
        test(
                PluginState.UNINSTALLED,
                ImmutableList.of("b", "c"),
                ImmutableList.<Set<SingleAction>>builder()
                        .add(actions(DISABLED, "b", "c", "a"))
                        .add(actions(UNINSTALLED, "b", "c"))
                        .build());
    }

    @Test
    public void uninstallMultiplePluginsWithMultipleDependencyTypes() {
        test(
                PluginState.UNINSTALLED,
                ImmutableList.of("mC", "mD"),
                ImmutableList.<Set<SingleAction>>builder()
                        .add(actions(DISABLED, "mX"))
                        .add(actions(DISABLED, "mA"))
                        .add(actions(DISABLED, "mB", "mD", "mE", "mC"))
                        .add(actions(UNINSTALLED, "mC", "mD"))
                        .add(actions(ENABLED, "mB"))
                        .build());
    }

    @Test
    public void uninstallLongChain() {
        test(
                PluginState.UNINSTALLED,
                ImmutableList.of("k0"),
                ImmutableList.<Set<SingleAction>>builder()
                        .add(actions(DISABLED, "k5"))
                        .add(actions(DISABLED, "k4"))
                        .add(actions(DISABLED, "k3"))
                        .add(actions(DISABLED, "k2"))
                        .add(actions(DISABLED, "k1"))
                        .add(actions(DISABLED, "k0"))
                        .add(actions(UNINSTALLED, "k0"))
                        .build());
    }

    private void test(PluginState action, List<String> target, List<Set<SingleAction>> expectedActions) {
        performActionOnPlugins(action, target);
        assertOperations(expectedActions);
        assertTargetPluginStates(action, target);
    }

    private void performActionOnPlugins(PluginState action, List<String> target) {
        switch (action) {
            case DISABLED:
                pluginManager.disablePlugin(target.get(0));
                break;
            case UNINSTALLED:
                if (target.size() == 1) {
                    pluginManager.uninstall(pluginManager.getPlugin(target.get(0)));
                } else {
                    pluginManager.uninstallPlugins(
                            target.stream().map(pluginManager::getPlugin).collect(Collectors.toList()));
                }
                break;
            default:
                fail("Unsupported action " + action);
        }
    }

    private void assertOperations(List<Set<SingleAction>> expectedActions) {
        int resultsIdx = 0;
        int resultIdxTo;
        for (final Set<SingleAction> result : Iterables.concat(initialEnabling, expectedActions)) {
            resultIdxTo = resultsIdx + result.size();
            final List<SingleAction> singleActions = pluginActions.subList(resultsIdx, resultIdxTo);
            assertThat(
                    "[" + resultsIdx + ", " + resultIdxTo + "]", singleActions, containsInAnyOrder(result.toArray()));
            resultsIdx = resultIdxTo;
        }
        assertThat("Unexpected events count", pluginActions.size(), is(resultsIdx));
    }

    private void assertTargetPluginStates(PluginState action, List<String> target) {
        switch (action) {
            case DISABLED:
                target.forEach(
                        key -> assertThat(pluginManager.getPlugin(key).getPluginState(), is(PluginState.DISABLED)));
                break;
            case UNINSTALLED:
                target.forEach(key -> assertThat(pluginManager.getPlugin(key), is(nullValue())));
                break;
            default:
                fail("Unsupported action " + action);
        }
    }

    /**
     * A set of actions that happen to plugins in unspecified order,
     * e.g. disabling 3 plugins, neither of which has any dependencies.
     */
    private static Set<SingleAction> actions(PluginState action, String... pluginKeys) {
        return Arrays.stream(pluginKeys).map(k -> new SingleAction(k, action)).collect(Collectors.toSet());
    }
}
