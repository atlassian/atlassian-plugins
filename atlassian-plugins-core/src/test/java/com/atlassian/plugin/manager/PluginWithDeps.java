package com.atlassian.plugin.manager;

import javax.annotation.Nonnull;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.impl.StaticPlugin;

public class PluginWithDeps extends StaticPlugin {

    public interface StateChangeListener {
        void enabled(Plugin plugin);

        void disabled(Plugin plugin);

        void uninstalled(Plugin plugin);

        StateChangeListener NO_OP = new StateChangeListener() {
            @Override
            public void enabled(Plugin plugin) {}

            @Override
            public void disabled(Plugin plugin) {}

            @Override
            public void uninstalled(Plugin plugin) {}
        };
    }

    private final PluginDependencies dependencies;

    public static final Function<Plugin, String> GET_KEY = Plugin::getKey;

    private StateChangeListener stateChangeListener;

    public PluginWithDeps(final String key) {
        this(key, StateChangeListener.NO_OP, new PluginDependencies(null, null, null));
    }

    public PluginWithDeps(final String key, final String... deps) {
        this(key, StateChangeListener.NO_OP, new PluginDependencies(ImmutableSet.copyOf(deps), null, null));
    }

    public PluginWithDeps(final String key, final PluginDependencies deps) {
        this(key, StateChangeListener.NO_OP, deps);
    }

    public PluginWithDeps(final String key, StateChangeListener stateChangeListener, final PluginDependencies deps) {
        setKey(key);
        dependencies = deps;
        setPluginState(PluginState.DISABLED);
        this.stateChangeListener = stateChangeListener;
    }

    @Override
    public void enable() {
        stateChangeListener.enabled(this);
        super.enable();
    }

    @Override
    protected void disableInternal() {
        stateChangeListener.disabled(this);
        super.disableInternal();
    }

    @Override
    public boolean isUninstallable() {
        return true;
    }

    @Override
    protected void uninstallInternal() {
        stateChangeListener.uninstalled(this);
    }

    @Nonnull
    @Override
    public PluginDependencies getDependencies() {
        return dependencies;
    }
}
