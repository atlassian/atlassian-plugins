package com.atlassian.plugin.manager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.junit.Before;
import com.google.common.collect.Collections2;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginState;

import static com.atlassian.plugin.manager.PluginWithDeps.GET_KEY;

public class TestDependentPlugins {
    protected List<Plugin> allPlugins;

    protected List<SingleAction> pluginActions;

    @Before
    public void setUp() throws Exception {
        pluginActions = new ArrayList<>();
        final PluginWithDeps.StateChangeListener stateChangeListener = new PluginWithDeps.StateChangeListener() {

            @Override
            public void enabled(Plugin plugin) {
                pluginActions.add(new SingleAction(plugin.getKey(), PluginState.ENABLED));
            }

            @Override
            public void disabled(Plugin plugin) {
                pluginActions.add(new SingleAction(plugin.getKey(), PluginState.DISABLED));
            }

            @Override
            public void uninstalled(Plugin plugin) {
                pluginActions.add(new SingleAction(plugin.getKey(), PluginState.UNINSTALLED));
            }
        };

        allPlugins = new ArrayList<>();

        //  X --- uses --(m - mandatory, etc.) Y

        // A ---m B ---d C --m G
        //    \_o D __/o
        //    \_d E _/m

        // F ---d B
        //    \_m D
        //    \_o E

        // H ---o B
        //    \_d D
        //    \_m E

        allPlugins.add(new PluginWithDeps(
                "A",
                stateChangeListener,
                PluginDependencies.builder()
                        .withMandatory("B")
                        .withOptional("D")
                        .withDynamic("E")
                        .build()));

        allPlugins.add(new PluginWithDeps(
                "F",
                stateChangeListener,
                PluginDependencies.builder()
                        .withDynamic("B")
                        .withMandatory("D")
                        .withOptional("E")
                        .build()));

        allPlugins.add(new PluginWithDeps(
                "H",
                stateChangeListener,
                PluginDependencies.builder()
                        .withOptional("B")
                        .withDynamic("D")
                        .withMandatory("E")
                        .build()));

        allPlugins.add(new PluginWithDeps(
                "B",
                stateChangeListener,
                PluginDependencies.builder().withDynamic("C").build()));

        allPlugins.add(new PluginWithDeps(
                "D",
                stateChangeListener,
                PluginDependencies.builder().withOptional("C").build()));

        allPlugins.add(new PluginWithDeps(
                "E",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("C").build()));

        allPlugins.add(new PluginWithDeps(
                "C",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("G").build()));

        allPlugins.add(new PluginWithDeps(
                "G", stateChangeListener, PluginDependencies.builder().build()));

        // circular a -m b -m c -m a
        allPlugins.add(new PluginWithDeps(
                "a",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("b").build()));
        allPlugins.add(new PluginWithDeps(
                "b",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("c").build()));
        allPlugins.add(new PluginWithDeps(
                "c",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("a").build()));

        // circular AC -o AC
        //          AC /d
        //          AC /m

        allPlugins.add(new PluginWithDeps(
                "aC",
                stateChangeListener,
                PluginDependencies.builder()
                        .withOptional("aC")
                        .withDynamic("aC")
                        .withMandatory("aC")
                        .build()));

        // Multiple dependency types

        // mX ---m mA ---m mB ---d mC
        //             \_o mD __/o
        //             \_d mE _/m

        // mA ---d mB
        //     \_m mD
        //     \_o mE

        // mA ---o mB
        //     \_d mD
        //     \_m mE

        allPlugins.add(new PluginWithDeps(
                "mA",
                stateChangeListener,
                PluginDependencies.builder()
                        .withMandatory("mB", "mD", "mE")
                        .withOptional("mB", "mD", "mE")
                        .withDynamic("mB", "mD", "mE")
                        .build()));

        allPlugins.add(new PluginWithDeps(
                "mB",
                stateChangeListener,
                PluginDependencies.builder().withDynamic("mC").build()));

        allPlugins.add(new PluginWithDeps(
                "mD",
                stateChangeListener,
                PluginDependencies.builder().withOptional("mC").build()));

        allPlugins.add(new PluginWithDeps(
                "mE",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("mC").build()));

        allPlugins.add(new PluginWithDeps(
                "mX",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("mA").build()));

        allPlugins.add(new PluginWithDeps(
                "mC",
                stateChangeListener,
                PluginDependencies.builder()
                        .withMandatory("mE")
                        .withOptional("mD")
                        .withDynamic("mB")
                        .build()));

        // k5 ---m k4 ---m k3 ---m k2 ---m k1 ---m k0

        allPlugins.add(new PluginWithDeps(
                "k5",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("k4").build()));

        allPlugins.add(new PluginWithDeps(
                "k4",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("k3").build()));

        allPlugins.add(new PluginWithDeps(
                "k3",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("k2").build()));

        allPlugins.add(new PluginWithDeps(
                "k2",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("k1").build()));

        allPlugins.add(new PluginWithDeps(
                "k1",
                stateChangeListener,
                PluginDependencies.builder().withMandatory("k0").build()));

        allPlugins.add(new PluginWithDeps(
                "k0", stateChangeListener, PluginDependencies.builder().build()));
    }

    protected Set<String> getKeys(final List<Plugin> plugins) {
        return new HashSet<>(Collections2.transform(plugins, GET_KEY));
    }

    protected static class SingleAction {
        protected final String plugin;
        protected final PluginState action;

        public SingleAction(String plugin, PluginState action) {
            this.plugin = plugin;
            this.action = action;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SingleAction that = (SingleAction) o;
            return action == that.action && Objects.equals(plugin, that.plugin);
        }

        @Override
        public int hashCode() {
            return Objects.hash(plugin, action);
        }

        @Override
        public String toString() {
            return "{" + action + " " + plugin + "}";
        }
    }
}
