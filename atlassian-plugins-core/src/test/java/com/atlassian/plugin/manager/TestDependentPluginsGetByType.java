package com.atlassian.plugin.manager;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import com.atlassian.plugin.PluginDependencies;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;

@RunWith(Parameterized.class)
public class TestDependentPluginsGetByType extends TestDependentPlugins {
    @Parameterized.Parameters(name = "{index}: ({1}, {0})")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC), new String[] {"A", "B", "D", "F", "E", "H"}},
            {ImmutableSet.of(MANDATORY), new String[] {"E", "H"}},
            {ImmutableSet.of(OPTIONAL), new String[] {"A", "D", "F"}},
            {ImmutableSet.of(DYNAMIC), new String[] {"B"}},
        });
    }

    @Parameterized.Parameter
    public Set<PluginDependencies.Type> dependencyTypes;

    @Parameterized.Parameter(1)
    public String[] expected;

    @Test
    public void testGet() throws Exception {
        final DependentPlugins dp =
                new DependentPlugins(ImmutableList.of("C"), allPlugins, ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC));

        final Set<String> keysExpected = ImmutableSet.copyOf(expected);
        assertThat(getKeys(dp.getPluginsByTypes(dependencyTypes, false)), is(equalTo(keysExpected)));
    }
}
