package com.atlassian.plugin.event.listeners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;

public class RecordingListener {
    private final List<Object> events = new ArrayList<>();
    private final Set<Class> eventClasses = new HashSet<>();

    public RecordingListener(Class... eventClasses) {
        this.eventClasses.addAll(Arrays.asList(eventClasses));
    }

    public synchronized void channel(final Object event) {
        if (event != null && eventClasses.contains(event.getClass())) events.add(event);
    }

    /**
     * @return the events received by this listener in the order they were received
     */
    public synchronized List<Object> getEvents() {
        return events;
    }

    /**
     * @return the classes of each event received by this listener in the order they were received
     */
    public synchronized List<Class> getEventClasses() {
        List<Class> result = new ArrayList<>(events.size());
        for (Object event : events) {
            if (event != null) result.add(event.getClass());
        }
        return result;
    }

    public synchronized void reset() {
        events.clear();
    }

    public synchronized List<String> getEventPluginOrModuleKeys() {
        List<String> result = new ArrayList<>(events.size());
        for (Object event : events) {
            if (event instanceof PluginEnabledEvent)
                result.add(((PluginEnabledEvent) event).getPlugin().getKey());
            else if (event instanceof PluginDisabledEvent)
                result.add(((PluginDisabledEvent) event).getPlugin().getKey());
            else if (event instanceof PluginModuleEnabledEvent)
                result.add(((PluginModuleEnabledEvent) event).getModule().getCompleteKey());
            else if (event instanceof PluginModuleDisabledEvent)
                result.add(((PluginModuleDisabledEvent) event).getModule().getCompleteKey());
            else result.add(null);
        }
        return result;
    }
}
