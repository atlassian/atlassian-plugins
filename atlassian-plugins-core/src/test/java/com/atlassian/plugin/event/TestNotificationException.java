package com.atlassian.plugin.event;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since 2.3.0
 */
@SuppressWarnings({"ThrowableResultOfMethodCallIgnored", "ThrowableInstanceNeverThrown"})
public class TestNotificationException {

    @Test
    public void testSingletonConstructor() {
        Exception cause = new Exception("I don't like it");
        NotificationException notificationException = new NotificationException(cause);

        assertEquals(cause, notificationException.getCause());
        assertEquals(1, notificationException.getAllCauses().size());
        assertEquals(cause, notificationException.getAllCauses().get(0));
    }

    @Test
    public void testListConstructor() {
        Exception cause1 = new Exception("I don't like it");
        Exception cause2 = new Exception("Me neither");
        final List<Throwable> causes = new ArrayList<>();
        causes.add(cause1);
        causes.add(cause2);

        NotificationException notificationException = new NotificationException(causes);

        assertEquals(cause1, notificationException.getCause());
        assertEquals(2, notificationException.getAllCauses().size());
        assertEquals(cause1, notificationException.getAllCauses().get(0));
        assertEquals(cause2, notificationException.getAllCauses().get(1));
    }

    @Test(expected = NullPointerException.class)
    public void testListConstructorThrowsForNullCauses() {
        new NotificationException((List) null);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testListConstructorForEmptyCauses() {
        new NotificationException(new ArrayList<>());
    }
}
