package com.atlassian.plugin.mock;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;

import static org.junit.Assert.assertNotNull;

public class MockAnimalModuleDescriptor extends AbstractModuleDescriptor<MockAnimal>
        implements StateAware, ModuleDescriptor<MockAnimal> {
    MockAnimal module;

    private final String type;
    private final String name;

    public MockAnimalModuleDescriptor() {
        this(null, null);
    }

    public MockAnimalModuleDescriptor(String type, String name) {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
        this.type = type;
        this.name = name;
    }

    @Override
    public void init(final Plugin plugin, final Element element) {
        super.init(plugin, element);
        if (type != null && name != null) {
            assertNotNull(plugin.getResourceDescriptor(type, name));
        }
    }

    @Override
    public MockAnimal getModule() {
        if (module == null) {
            try {
                module = getModuleClass().newInstance();
            } catch (final InstantiationException | IllegalAccessException e) {
                throw new PluginParseException(e);
            }
        }
        return module;
    }
}
