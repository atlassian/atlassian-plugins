/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Jul 29, 2004
 * Time: 3:47:36 PM
 */
package com.atlassian.plugin.store;

import org.junit.Test;

import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;

import static org.junit.Assert.assertEquals;

public class TestMemoryPluginStateStore {

    @Test
    public void testStore() {
        final MemoryPluginPersistentStateStore store = new MemoryPluginPersistentStateStore();
        final PluginPersistentState state = PluginPersistentState.builder().toState();
        store.save(state);
        assertEquals(state.getStatesMap(), store.load().getStatesMap());
    }
}
