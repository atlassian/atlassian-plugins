package com.atlassian.plugin.hostcontainer;

import java.util.Collections;

import org.junit.Test;
import com.google.common.collect.ImmutableMap;

import static org.junit.Assert.assertEquals;

public class TestSimpleConstructorModuleFactory {

    @Test
    public void testCreateModule() {
        final SimpleConstructorHostContainer factory =
                new SimpleConstructorHostContainer(ImmutableMap.of(String.class, "bob"));
        final Base world = factory.create(OneArg.class);
        assertEquals("bob", world.getName());
    }

    @Test
    public void testCreateModuleFindBiggest() {
        final SimpleConstructorHostContainer factory =
                new SimpleConstructorHostContainer(ImmutableMap.of(String.class, "bob", Integer.class, 10));
        final Base world = factory.create(TwoArg.class);
        assertEquals("bob 10", world.getName());
    }

    @Test
    public void testCreateModuleFindSmaller() {
        final SimpleConstructorHostContainer factory =
                new SimpleConstructorHostContainer(ImmutableMap.of(String.class, "bob"));
        final Base world = factory.create(TwoArg.class);
        assertEquals("bob", world.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateModuleNoMatch() {
        final SimpleConstructorHostContainer factory = new SimpleConstructorHostContainer(Collections.emptyMap());
        factory.create(OneArg.class);
    }

    public abstract static class Base {
        private final String name;

        Base(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static class OneArg extends Base {
        public OneArg(final String name) {
            super(name);
        }
    }

    public static class TwoArg extends Base {
        public TwoArg(final String name) {
            super(name);
        }

        public TwoArg(final String name, final Integer age) {
            super(name + " " + age);
        }
    }
}
