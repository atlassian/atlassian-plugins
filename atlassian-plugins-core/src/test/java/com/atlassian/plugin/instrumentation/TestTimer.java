package com.atlassian.plugin.instrumentation;

import java.util.Optional;

import org.junit.Test;

import com.atlassian.instrumentation.operations.OpTimer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import static com.atlassian.plugin.test.Matchers.notPresent;
import static com.atlassian.plugin.test.Matchers.presentWithValue;

public class TestTimer {

    @Test
    public void endsOnClose() {
        final OpTimer opTimer = mock(OpTimer.class);

        try (Timer timer = new Timer(Optional.of(opTimer))) {
            assertThat(timer.getOpTimer(), presentWithValue(is(opTimer)));
        }

        verify(opTimer).end();
    }

    @Test
    public void closesWithEmpty() {
        try (Timer timer = new Timer(Optional.empty())) {
            assertThat(timer.getOpTimer(), notPresent());
        }
    }
}
