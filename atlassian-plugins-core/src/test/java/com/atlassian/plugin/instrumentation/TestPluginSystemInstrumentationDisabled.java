package com.atlassian.plugin.instrumentation;

import java.util.Optional;

import org.junit.Test;

import com.atlassian.instrumentation.InstrumentRegistry;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import static com.atlassian.plugin.test.Matchers.notPresent;

public class TestPluginSystemInstrumentationDisabled {

    private final PluginSystemInstrumentation pluginSystemInstrumentation = new PluginSystemInstrumentation();

    @Test
    public void instrumentationNotCreated() {
        final Optional<InstrumentRegistry> instrumentRegistry = pluginSystemInstrumentation.getInstrumentRegistry();
        assertThat(instrumentRegistry, notPresent());
    }

    @Test
    public void pullTimerReturnsEmpty() {
        final Timer timer = pluginSystemInstrumentation.pullTimer("sometimer");
        assertThat(timer, notNullValue());
        assertThat(timer.getOpTimer(), notPresent());
    }

    @Test
    public void pullSingleTimerReturnsEmpty() {
        final Timer timer = pluginSystemInstrumentation.pullSingleTimer("somesingletimer");
        assertThat(timer, notNullValue());
        assertThat(timer.getOpTimer(), notPresent());
    }
}
