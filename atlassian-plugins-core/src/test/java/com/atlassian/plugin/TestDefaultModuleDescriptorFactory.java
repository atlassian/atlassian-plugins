package com.atlassian.plugin;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockMineralModuleDescriptor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TestDefaultModuleDescriptorFactory {

    private DefaultModuleDescriptorFactory moduleDescriptorFactory;

    @Before
    public void setUp() throws Exception {
        moduleDescriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());
    }

    @Test(expected = PluginParseException.class)
    public void testInvalidModuleDescriptorType()
            throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        moduleDescriptorFactory.getModuleDescriptor("foobar");
    }

    @Test
    public void testModuleDescriptorFactory()
            throws PluginParseException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        assertTrue(moduleDescriptorFactory.getModuleDescriptor("animal") instanceof MockAnimalModuleDescriptor);
        assertTrue(moduleDescriptorFactory.getModuleDescriptor("mineral") instanceof MockMineralModuleDescriptor);

        assertTrue(moduleDescriptorFactory.hasModuleDescriptor("animal"));
        assertTrue(moduleDescriptorFactory.hasModuleDescriptor("mineral"));
        assertFalse(moduleDescriptorFactory.hasModuleDescriptor("something"));

        // Test removing a module descriptor
        moduleDescriptorFactory.removeModuleDescriptorForType("mineral");

        // Ensure the removed module descriptor is not there
        assertFalse(moduleDescriptorFactory.hasModuleDescriptor("mineral"));

        // Ensure the other one is still there
        assertTrue(moduleDescriptorFactory.hasModuleDescriptor("animal"));
        assertTrue(moduleDescriptorFactory.getModuleDescriptor("animal") instanceof MockAnimalModuleDescriptor);
    }

    // PLUG-5
    @Test
    public void testModuleDescriptorFactoryOnlyPermittedDescriptors()
            throws IllegalAccessException, PluginParseException, ClassNotFoundException, InstantiationException {
        // Add the "supported" module descriptors
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        // Exclude "mineral"
        final List<String> permittedList = new ArrayList<>();
        permittedList.add("animal");
        moduleDescriptorFactory.setPermittedModuleKeys(permittedList);
        // Try and grab the "animal" descriptor
        assertNotNull(moduleDescriptorFactory.getModuleDescriptor("animal"));

        // "mineral" is excluded, so it should return null
        assertNull(moduleDescriptorFactory.getModuleDescriptor("mineral"));
    }
}
