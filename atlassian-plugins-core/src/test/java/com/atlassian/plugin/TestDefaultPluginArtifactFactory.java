package com.atlassian.plugin;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.atlassian.plugin.test.PluginJarBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import static com.atlassian.plugin.ReferenceMode.FORBID_REFERENCE;
import static com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE;

public class TestDefaultPluginArtifactFactory {
    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    private File testDir;
    private File spaceTestDir;
    private File file;

    @Before
    public void setUp() throws IOException {
        testDir = temporaryFolder.newFolder("test");
        spaceTestDir = temporaryFolder.newFolder("space test");
        file = new File(testDir, "some.jar");
    }

    @Test
    public void testCreate() throws IOException {
        doCreationTestInDirectory(testDir);
    }

    @Test
    public void testCreateWithSpaceInArtifactPath() throws IOException {
        doCreationTestInDirectory(spaceTestDir);
    }

    @Test
    public void testReferenceModeForbidReferenceDoesNotAllowReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory =
                new DefaultPluginArtifactFactory(FORBID_REFERENCE);
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        assertFalse(pluginArtifact.getReferenceMode().allowsReference());
        assertThat(pluginArtifact.getReferenceMode(), is(FORBID_REFERENCE));
    }

    @Test
    public void testReferenceModePermitReferenceAllowsReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory =
                new DefaultPluginArtifactFactory(PERMIT_REFERENCE);
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        assertTrue(pluginArtifact.getReferenceMode().allowsReference());
        assertThat(pluginArtifact.getReferenceMode(), is(PERMIT_REFERENCE));
    }

    @Test
    public void testDefaultDoesNotAllowsReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory = new DefaultPluginArtifactFactory();
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        assertFalse(pluginArtifact.getReferenceMode().allowsReference());
        assertThat(pluginArtifact.getReferenceMode(), is(FORBID_REFERENCE));
    }

    @Test
    public void testLegacyReferenceModeForbidReferenceDoesNotAllowReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory =
                new DefaultPluginArtifactFactory(ReferenceMode.FORBID_REFERENCE);
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        assertFalse(pluginArtifact.getReferenceMode().allowsReference());
    }

    @Test
    public void testLegacyReferenceModePermitReferenceAllowsReference() {
        final DefaultPluginArtifactFactory defaultPluginArtifactFactory =
                new DefaultPluginArtifactFactory(ReferenceMode.PERMIT_REFERENCE);
        final PluginArtifact pluginArtifact = defaultPluginArtifactFactory.create(file.toURI());
        assertTrue(pluginArtifact.getReferenceMode().allowsReference());
    }

    private void doCreationTestInDirectory(File directory) throws IOException {
        File xmlFile = new File(directory, "foo.xml");
        FileUtils.writeStringToFile(xmlFile, "<xml/>");
        File jarFile = new PluginJarBuilder("jar").build(directory);

        DefaultPluginArtifactFactory factory = new DefaultPluginArtifactFactory();

        PluginArtifact jarArt = factory.create(jarFile.toURI());
        assertNotNull(jarArt);
        assertTrue(jarArt instanceof JarPluginArtifact);

        PluginArtifact xmlArt = factory.create(xmlFile.toURI());
        assertNotNull(xmlArt);
        assertTrue(xmlArt instanceof XmlPluginArtifact);

        try {
            factory.create(new File(testDir, "bob.jim").toURI());
            fail("Should have thrown exception");
        } catch (IllegalArgumentException ex) {
            // test passed
        }
    }
}
