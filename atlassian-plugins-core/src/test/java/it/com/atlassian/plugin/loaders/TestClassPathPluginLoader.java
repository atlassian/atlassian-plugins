package it.com.atlassian.plugin.loaders;

import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.loaders.ClassPathPluginLoader;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockMineralModuleDescriptor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;

public class TestClassPathPluginLoader {
    private ClassPathPluginLoader classPathPluginLoader;
    private ModuleDescriptorFactory moduleDescriptorFactory;

    @Before
    public void before() {
        classPathPluginLoader = new ClassPathPluginLoader("test-atlassian-plugin.xml");

        final DefaultModuleDescriptorFactory defaultModuleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        defaultModuleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        defaultModuleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        defaultModuleDescriptorFactory.addModuleDescriptor("vegetable", MockMineralModuleDescriptor.class);

        moduleDescriptorFactory = defaultModuleDescriptorFactory;
    }

    @Test
    public void atlassianPlugin() {
        Iterable plugins = classPathPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        Plugin plugin = (Plugin) plugins.iterator().next();
        assertThat(plugin.getName(), is("Test Plugin"));
        assertThat(plugin.getKey(), is("test.atlassian.plugin"));
        assertThat(
                plugin.getPluginInformation().getDescription(),
                is("This plugin descriptor is just used for test purposes!"));
        assertThat(plugin.getModuleDescriptors().size(), is(4));

        assertThat(plugin.getModuleDescriptor("bear").getName(), is("Bear Animal"));
    }

    @Test
    public void createModule() {
        final Iterable plugins = classPathPluginLoader.loadAllPlugins(moduleDescriptorFactory);
        final Plugin plugin = (Plugin) plugins.iterator().next();

        final Element dynamicModule = new DOMElement("vegetable");
        dynamicModule.addAttribute("key", "aubergine");
        dynamicModule.addAttribute("class", "com.atlassian.plugin.mock.MockVegetable");

        final ModuleDescriptor moduleDescriptor = classPathPluginLoader.createModule(
                plugin, new Dom4jDelegatingElement(dynamicModule), moduleDescriptorFactory);
        assertThat(moduleDescriptor, notNullValue());
        assertThat(moduleDescriptor.getKey(), is("aubergine"));
    }

    @Test
    public void createModuleNoFactory() {
        classPathPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertThat(
                classPathPluginLoader.createModule(
                        mock(Plugin.class), mock(com.atlassian.plugin.module.Element.class), moduleDescriptorFactory),
                nullValue());
    }
}
