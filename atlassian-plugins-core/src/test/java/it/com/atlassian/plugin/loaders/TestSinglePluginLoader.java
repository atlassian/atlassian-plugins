package it.com.atlassian.plugin.loaders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Test;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.loaders.SinglePluginLoader;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockBear;
import com.atlassian.plugin.mock.MockGold;
import com.atlassian.plugin.mock.MockMineral;
import com.atlassian.plugin.mock.MockMineralModuleDescriptor;
import com.atlassian.plugin.mock.MockVegetableModuleDescriptor;
import com.atlassian.plugin.util.ClassLoaderUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

public class TestSinglePluginLoader {
    @Test
    public void singlePluginLoader() {
        final SinglePluginLoader loader = new SinglePluginLoader("test-system-plugin.xml");
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);

        assertThat(plugins, IsIterableWithSize.iterableWithSize(1));

        // test the plugin information
        final Plugin plugin = plugins.iterator().next();
        assertThat(plugin.isSystemPlugin(), is(true));
    }

    @Test
    public void rejectOsgiPlugin() {
        final SinglePluginLoader loader = new SinglePluginLoader("test-osgi-plugin.xml");
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);

        assertThat(plugins, IsIterableWithSize.iterableWithSize(1));

        // test the plugin information
        final Plugin plugin = plugins.iterator().next();
        assertThat(plugin, instanceOf(UnloadablePlugin.class));
        assertThat(plugin.getKey(), is("test.atlassian.plugin"));
    }

    @Test
    public void atlassianPlugin() {
        final SinglePluginLoader loader = new SinglePluginLoader("test-atlassian-plugin.xml");
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetable", MockVegetableModuleDescriptor.class);
        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);

        assertThat(plugins, IsIterableWithSize.iterableWithSize(1));

        // test the plugin information
        final Plugin plugin = plugins.iterator().next();
        enableModules(plugin);
        assertThat(plugin.getName(), is("Test Plugin"));
        assertThat(plugin.getKey(), is("test.atlassian.plugin"));
        assertThat(plugin.getPluginInformation(), notNullValue());
        assertThat(plugin.getPluginInformation().getVersion(), is("1.0"));
        assertThat(plugin.getI18nNameKey(), is("test.atlassian.plugin.i18n"));
        assertThat(plugin.getPluginInformation().getDescriptionKey(), is("test.atlassian.plugin.desc.i18n"));
        assertThat(
                plugin.getPluginInformation().getDescription(),
                is("This plugin descriptor is just used for test purposes!"));
        assertThat(plugin.getPluginInformation().getVendorName(), is("Atlassian Software Systems Pty Ltd"));
        assertThat(plugin.getPluginInformation().getVendorUrl(), is("http://www.atlassian.com"));
        assertThat(plugin.getModuleDescriptors().size(), is(4));

        final ModuleDescriptor bearDescriptor = plugin.getModuleDescriptor("bear");
        assertThat(bearDescriptor.getCompleteKey(), is("test.atlassian.plugin:bear"));
        assertThat(bearDescriptor.getKey(), is("bear"));
        assertThat(bearDescriptor.getName(), is("Bear Animal"));
        assertThat(bearDescriptor.getModuleClass(), Matchers.<Class>is(MockBear.class));
        assertThat(bearDescriptor.getDescription(), is("A plugin module to describe a bear"));
        assertThat(bearDescriptor.isEnabledByDefault(), is(true));
        assertThat(bearDescriptor.getI18nNameKey(), is("test.atlassian.module.bear.name"));
        assertThat(bearDescriptor.getDescriptionKey(), is("test.atlassian.module.bear.description"));

        final Iterable<ResourceDescriptor> resources = bearDescriptor.getResourceDescriptors();
        assertThat(resources, IsIterableWithSize.iterableWithSize(3));

        final Map<String, String> params = bearDescriptor.getParams();
        assertThat(params.get("height"), is("20"));
        assertThat(params.get("colour"), is("brown"));

        final List<ModuleDescriptor<MockGold>> goldDescriptors =
                plugin.getModuleDescriptorsByModuleClass(MockGold.class);
        assertThat(goldDescriptors, hasSize(1));
        final ModuleDescriptor goldDescriptor = goldDescriptors.get(0);
        assertThat(goldDescriptor.getCompleteKey(), is("test.atlassian.plugin:gold"));
        assertThat(goldDescriptor.getModule(), Matchers.is(new MockGold(20)));
        final List<ModuleDescriptor<MockMineral>> mineralDescriptors =
                plugin.getModuleDescriptorsByModuleClass(MockMineral.class);
        assertThat(mineralDescriptors, hasSize(1));
        final ModuleDescriptor mineralDescriptor = mineralDescriptors.get(0);
        assertThat(mineralDescriptor, is(goldDescriptor));

        assertThat(plugin.getResourceDescriptors(), IsIterableWithSize.iterableWithSize(1));
        final ResourceLocation pluginResource = plugin.getResourceLocation("download", "icon.gif");
        assertThat(pluginResource.getLocation(), is("/icon.gif"));
    }

    @Test
    public void disabledPlugin() {
        final SinglePluginLoader loader = new SinglePluginLoader("test-disabled-plugin.xml");
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        assertThat(plugins, Matchers.iterableWithSize(1));
        final Plugin plugin = plugins.iterator().next();
        assertThat(plugin.isEnabledByDefault(), is(false));

        final Collection<ModuleDescriptor<?>> moduleDescriptors = plugin.getModuleDescriptors();
        assertThat(moduleDescriptors, hasSize(1));
        final ModuleDescriptor module = plugin.getModuleDescriptor("gold");
        assertThat(module.isEnabledByDefault(), is(false));
    }

    @Test
    public void pluginsInOrder() {
        final SinglePluginLoader loader = new SinglePluginLoader(
                ClassLoaderUtils.getResource("test-ordered-pluginmodules.xml", SinglePluginLoader.class));
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        final Iterable plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        final Plugin plugin = (Plugin) plugins.iterator().next();
        final Collection<ModuleDescriptor<?>> modules = plugin.getModuleDescriptors();
        assertThat(modules, hasSize(3));
        final Iterator iterator = modules.iterator();
        assertThat(((MockAnimalModuleDescriptor) iterator.next()).getKey(), is("yogi1"));
        assertThat(((MockAnimalModuleDescriptor) iterator.next()).getKey(), is("yogi2"));
        assertThat(((MockAnimalModuleDescriptor) iterator.next()).getKey(), is("yogi3"));
    }

    @Test
    public void unknownPluginModule() {
        final SinglePluginLoader loader = new SinglePluginLoader("test-bad-plugin.xml");
        final Iterable<Plugin> plugins =
                loader.loadAllPlugins(new DefaultModuleDescriptorFactory(new DefaultHostContainer()));

        assertThat(plugins, Matchers.iterableWithSize(1));

        final Plugin plugin = plugins.iterator().next();
        final List<ModuleDescriptor<?>> moduleList = ImmutableList.copyOf(plugin.getModuleDescriptors());
        assertThat(moduleList, hasSize(1));

        // The module that had the problem should be an
        // UnrecognisedModuleDescriptor
        assertThat(moduleList.get(0).getClass(), Matchers.<Class>is(UnrecognisedModuleDescriptor.class));
    }

    // PLUG-5
    @Test
    public void pluginWithOnlyPermittedModules() {
        final SinglePluginLoader loader = new SinglePluginLoader("test-atlassian-plugin.xml");

        // Define the module descriptor factory
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        // Exclude mineral
        final List<String> permittedList = new ArrayList<>();
        permittedList.add("animal");
        moduleDescriptorFactory.setPermittedModuleKeys(permittedList);

        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);

        // 1 plugin returned
        assertThat(plugins, Matchers.iterableWithSize(1));

        final Plugin plugin = plugins.iterator().next();

        // Only one descriptor, animal
        assertThat(plugin.getModuleDescriptors(), hasSize(1));
        assertThat(plugin.getModuleDescriptor("bear"), notNullValue());
        assertThat(plugin.getModuleDescriptor("gold"), nullValue());
    }

    // PLUG-5
    @Test
    public void pluginWithOnlyPermittedModulesAndMissingModuleDescriptor() {
        final SinglePluginLoader loader = new SinglePluginLoader("test-atlassian-plugin.xml");

        // Define the module descriptor factory
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        // Exclude mineral
        final List<String> permittedList = new ArrayList<>();
        permittedList.add("animal");
        moduleDescriptorFactory.setPermittedModuleKeys(permittedList);

        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);

        // 1 plugin returned
        assertThat(plugins, Matchers.iterableWithSize(1));

        final Plugin plugin = plugins.iterator().next();

        // Only one descriptor, animal
        assertThat(plugin.getModuleDescriptors(), hasSize(1));
        assertThat(plugin.getModuleDescriptor("bear"), notNullValue());
        assertThat(plugin.getModuleDescriptor("gold"), nullValue());
    }

    @Test
    public void badPluginKey() {
        final SinglePluginLoader loader = new SinglePluginLoader("test-bad-plugin-key-plugin.xml");
        final Iterable<Plugin> plugins = loader.loadAllPlugins(null);
        assertThat(plugins, Matchers.iterableWithSize(1));
        final Plugin plugin = plugins.iterator().next();
        assertThat(plugin, instanceOf(UnloadablePlugin.class));
        assertThat(plugin.getKey(), is("test-bad-plugin-key-plugin.xml"));
        assertThat(
                ((UnloadablePlugin) plugin).getErrorText(),
                endsWith("Plugin keys cannot contain ':'. Key is 'test:bad'"));
    }

    @Test
    public void nonUniqueKeysWithinAPlugin() {
        final SinglePluginLoader loader = new SinglePluginLoader("test-bad-non-unique-keys-plugin.xml");
        final DefaultModuleDescriptorFactory moduleDescriptorFactory =
                new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        final Iterable<Plugin> plugins = loader.loadAllPlugins(moduleDescriptorFactory);
        assertThat(plugins, Matchers.iterableWithSize(1));
        final Plugin plugin = Iterables.get(plugins, 0);
        assertThat(plugin, instanceOf(UnloadablePlugin.class));
        assertThat(
                ((UnloadablePlugin) plugin).getErrorText(),
                endsWith("Found duplicate key 'bear' within plugin 'test.bad.plugin'"));
    }

    @Test
    public void badResource() {
        final Iterable<Plugin> plugins = new SinglePluginLoader("foo").loadAllPlugins(null);
        assertThat(plugins, Matchers.iterableWithSize(1));
        assertThat(Iterables.get(plugins, 0), instanceOf(UnloadablePlugin.class));
        assertThat(Iterables.get(plugins, 0).getKey(), is("foo"));
    }

    public void enableModules(final Plugin plugin) {
        for (final ModuleDescriptor descriptor : plugin.getModuleDescriptors()) {
            if (descriptor instanceof StateAware) {
                ((StateAware) descriptor).enabled();
            }
        }
    }
}
