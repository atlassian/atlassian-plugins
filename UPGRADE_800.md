## Class Removals

| Class name                                                 |
|------------------------------------------------------------|
| `com.atlassian.plugin.predicate.ModuleDescriptorPredicate` |
| `com.atlassian.plugin.predicate.PluginPredicate`           |
| `com.atlassian.plugin.scope.ScopeManager`                  |
| `com.atlassian.plugin.DummyPlugin`                         |
| `com.atlassian.plugin.PluginNameComparator`                |
| `com.atlassian.plugin.ScopeAware`                          |
| `com.atlassian.plugin.cache.ConcurrentCacheFactory`        |
| `com.atlassian.plugin.util.validation.ValidationPattern`   |

## Method removals

| Removed method                                                     | Alternate method                                                               |
|--------------------------------------------------------------------|--------------------------------------------------------------------------------|
| `com.atlassian.plugin.event.PluginTransactionEndEvent#getEvents()` | `com.atlassian.plugin.event.PluginTransactionEndEvent#getUnmodifiableEvents()` |
| `com.atlassian.plugin.PluginDependencies#getByPluginKey()`         | `com.atlassian.plugin.PluginDependencies#getTypesByPluginKey()`                |


## Package renames 

### Atlassian Plugins API

| Old class package                                    | New class package                                             |
|------------------------------------------------------|---------------------------------------------------------------|
| `com.atlassian.plugin.module.Dom4jDelegatingElement` | `com.atlassian.plugin.internal.module.Dom4jDelegatingElement` |

### Atlassian Plugins Core

| Old class package                                         | New class package                                                  |
|-----------------------------------------------------------|--------------------------------------------------------------------|
| `com.atlassian.plugin.parsers.ModuleReader`               | `com.atlassian.plugin.internal.parsers.ModuleReader`               |
| `com.atlassian.plugin.parsers.PluginDescriptorReader`     | `com.atlassian.plugin.internal.parsers.PluginDescriptorReader`     |
| `com.atlassian.plugin.parsers.PluginInformationReader`    | `com.atlassian.plugin.internal.parsers.PluginInformationReader`    |
| `com.atlassian.plugin.parsers.XmlDescriptorParser`        | `com.atlassian.plugin.internal.parsers.XmlDescriptorParser`        |
| `com.atlassian.plugin.parsers.XmlDescriptorParserFactory` | `com.atlassian.plugin.internal.parsers.XmlDescriptorParserFactory` |
| `com.atlassian.plugin.parsers.XmlDescriptorParserUtils`   | `com.atlassian.plugin.internal.parsers.XmlDescriptorParserUtils`   |
| `com.atlassian.plugin.util.ModuleRestricts`               | `com.atlassian.plugin.internal.util.ModuleRestricts`               |
| `com.atlassian.plugin.util.PluginUtils`                   | `com.atlassian.plugin.internal.util.PluginUtils`                   |
| `com.atlassian.plugin.util.VersionRange`                  | `com.atlassian.plugin.internal.util.VersionRange`                  |

### Atlassian Plugins OSGi

| Old class package                                                                     | New class package                                                                              |
|---------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------|
| `com.atlassian.plugin.osgi.factory.transform.model.ComponentImport`                   | `com.atlassian.plugin.osgi.internal.factory.transform.model.ComponentImport`                   |
| `com.atlassian.plugin.osgi.factory.transform.stage.AddBundleOverridesStage`           | `com.atlassian.plugin.osgi.internal.factory.transform.stage.AddBundleOverridesStage`           |
| `com.atlassian.plugin.osgi.factory.transform.stage.ComponentImportSpringStage`        | `com.atlassian.plugin.osgi.internal.factory.transform.stage.ComponentImportSpringStage`        |
| `com.atlassian.plugin.osgi.factory.transform.stage.ComponentSpringStage`              | `com.atlassian.plugin.osgi.internal.factory.transform.stage.ComponentSpringStage`              |
| `com.atlassian.plugin.osgi.factory.transform.stage.GenerateManifestStage`             | `com.atlassian.plugin.osgi.internal.factory.transform.stage.GenerateManifestStage`             |
| `com.atlassian.plugin.osgi.factory.transform.stage.HostComponentSpringStage`          | `com.atlassian.plugin.osgi.internal.factory.transform.stage.HostComponentSpringStage`          |
| `com.atlassian.plugin.osgi.factory.transform.stage.ModuleTypeSpringStage`             | `com.atlassian.plugin.osgi.internal.factory.transform.stage.ModuleTypeSpringStage`             |
| `com.atlassian.plugin.osgi.factory.transform.stage.ScanDescriptorForHostClassesStage` | `com.atlassian.plugin.osgi.internal.factory.transform.stage.ScanDescriptorForHostClassesStage` |
| `com.atlassian.plugin.osgi.factory.transform.stage.ScanInnerJarsStage`                | `com.atlassian.plugin.osgi.internal.factory.transform.stage.ScanInnerJarsStage`                |
| `com.atlassian.plugin.osgi.factory.transform.stage.SpringHelper`                      | `com.atlassian.plugin.osgi.internal.factory.transform.stage.SpringHelper`                      |
| `com.atlassian.plugin.osgi.factory.transform.stage.TransformStageUtils`               | `com.atlassian.plugin.osgi.internal.factory.transform.stage.TransformStageUtils`               |
| `com.atlassian.plugin.osgi.factory.transform.DefaultPluginTransformer`                | `com.atlassian.plugin.osgi.internal.factory.transform.DefaultPluginTransformer`                |
| `com.atlassian.plugin.osgi.factory.transform.JarUtils`                                | `com.atlassian.plugin.osgi.internal.factory.transform.JarUtils`                                |
| `com.atlassian.plugin.osgi.factory.transform.TransformContext`                        | `com.atlassian.plugin.osgi.internal.factory.transform.TransformContext`                        |
| `com.atlassian.plugin.osgi.factory.transform.TransformStage`                          | `com.atlassian.plugin.osgi.internal.factory.transform.TransformStage`                          |
| `com.atlassian.plugin.osgi.hook.rest.JaxRsFilterFactory`                              | `com.atlassian.plugin.osgi.internal.hook.rest.JaxRsFilterFactory`                              |


### Atlassian Plugins Schema

| Old class package                                     | New class package                                              |
|-------------------------------------------------------|----------------------------------------------------------------|
| `com.atlassian.plugin.schema.spi.DocumentBasedSchema` | `com.atlassian.plugin.internal.schema.spi.DocumentBasedSchema` |
| `com.atlassian.plugin.schema.spi.IdUtils`             | `com.atlassian.plugin.internal.schema.spi.IdUtils`             |
| `com.atlassian.plugin.schema.spi.Schema`              | `com.atlassian.plugin.internal.schema.spi.Schema`              |
| `com.atlassian.plugin.schema.spi.SchemaDocumented`    | `com.atlassian.plugin.internal.schema.spi.SchemaDocumented`    |
| `com.atlassian.plugin.schema.spi.SchemaFactory`       | `com.atlassian.plugin.internal.schema.spi.SchemaFactory`       |
| `com.atlassian.plugin.schema.spi.SchemaTransformer`   | `com.atlassian.plugin.internal.schema.spi.SchemaTransformer`   |


### Atlassian Plugins Validation

| Old class package                                     | New class package                                              |
|-------------------------------------------------------|----------------------------------------------------------------|
| `com.atlassian.plugin.validation.DescriptorValidator` | `com.atlassian.plugin.internal.validation.DescriptorValidator` |
| `com.atlassian.plugin.validation.Dom4jUtils`          | `com.atlassian.plugin.internal.validation.Dom4jUtils`          |
| `com.atlassian.plugin.validation.SchemaReader`        | `com.atlassian.plugin.internal.validation.SchemaReader`        |

## Class changes

### New methods

| Class name                                    | Method                                    |
|-----------------------------------------------|-------------------------------------------|
| `com.atlassian.plugin.manage.SafeModeManager` | `boolean isRequiredPlugin(Plugin plugin)` |
