# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [7.0.1]
### Changed
- Upgrade Spring Framework from 5.3.10 to 5.3.18 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade spring 53x framework bundle from 5.3.10_1 to 5.3.18_1 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade log4j from 1.2.17-atlassian-13 to 1.2.17-atlassian-16 to address [CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228) [CVE-2022-23307](https://nvd.nist.gov/vuln/detail/CVE-2022-23307) [CVE-2020-9493](https://nvd.nist.gov/vuln/detail/CVE-2020-9493) [CVE-2022-23305](https://nvd.nist.gov/vuln/detail/CVE-2022-23305) [CVE-2022-23302](https://nvd.nist.gov/vuln/detail/CVE-2022-23302)
- Upgrade jaxb-runtime from 2.3.5 to 2.3.6
- Upgrade third party pom from 5.2.4 to 5.2.8
- Upgrade bytebuddy from 1.11.16 to 1.11.22
- Upgrade public-pom from 6.2.1 to 6.2.2

## [7.0.0]
### Changed
- Upgrade to platform 6
- Upgrade findbugs annotation from 1.3.9 to 3.0.1
- Replace fast-classpath-scanner 2.18.2 with classgraph 4.8.115
- Upgrade atlassian secure xml from 3.1.5 to 4.0.3
- Upgrade AMPS from 8.0.4 to 8.2.3
- Upgrade public-pom from 6.1.3 to 6.2.1

## [6.0.1]
### Added
- Add new ETagCachingFilter

## [6.0.0]
### Changed
- Upgrade dom4j from 1.6.1-atlassian-3 to 2.1.3 [BSP-1412](https://bulldog.internal.atlassian.com/browse/BSP-1412)

## [5.8.3]
### Changed
- Upgrade Spring Framework from 5.3.10 to 5.3.18 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade spring 53x framework bundle from 5.3.10_1 to 5.3.18_1 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade log4j from 1.2.17-atlassian-13 to 1.2.17-atlassian-16 to address [CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228) [CVE-2022-23307](https://nvd.nist.gov/vuln/detail/CVE-2022-23307) [CVE-2020-9493](https://nvd.nist.gov/vuln/detail/CVE-2020-9493) [CVE-2022-23305](https://nvd.nist.gov/vuln/detail/CVE-2022-23305) [CVE-2022-23302](https://nvd.nist.gov/vuln/detail/CVE-2022-23302)
- Upgrade jaxb-runtime from 2.3.5 to 2.3.6
- Upgrade third party pom from 5.2.4 to 5.2.8
- Upgrade bytebuddy from 1.11.16 to 1.11.22

## [5.8.2]
### Changed
- [PLUG-1268](https://ecosystem.atlassian.net/browse/PLUG-1268] fix last modified handler to account for negative date values
- Upgrade Spring framework from 5.3.9 to 5.3.10, and servicemix bundle version from 5.3.3_1 to 5.3.10_1
- Add metrics for plugin enabled/disabled
- Add dependency on atlassian-profiling 3.7.0

## [5.8.1]
### Changed
- Switch to byte-buddy dependency that bundles its own copy of asm

## [5.8.0]
### Added
- A new `atlassian.plugins.filter.async.default` system property has been added to allow products to control the
  default `async-supported` value for plugin filters registered using `<servlet-filter/>` which do not have an
  explicit `<async-supported/>` element.
  - The default remains `false` in keeping with the servlet spec, which says:
    > By default, servlet and filters do not support asynchronous operations.

    See the [`setAsyncSupported` Javadocs](https://docs.oracle.com/javaee/7/api/javax/servlet/Registration.Dynamic.html#setAsyncSupported-boolean-)
  - _Tomcat does not follow the spec_, and _filters_ registered in Tomcat are marked `async-supported=true` unless
    `setAsyncSupported(false)` is explicitly called. _Servlets_ registered in Tomcat follow the spec and default to
    `false` unless `setAsyncSupported(true) is explicitly called. This system property allows products to apply the
    same default behavior as Tomcat does.
### Changed
- When `HttpServletRequest.startAsync` is blocked by a plugin-registered filter, a warning is logged to provide
  the complete key of the blocking filter(s).

## [5.7.8]
### Changed
- Upgrade spring 53x framework bundle from 5.3.16_1 to 5.3.18_1 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade log4j from 1.2.17-atlassian-15 to 1.2.17-atlassian-16 to address [CVE-2022-23307](https://nvd.nist.gov/vuln/detail/CVE-2022-23307) [CVE-2020-9493](https://nvd.nist.gov/vuln/detail/CVE-2020-9493) [CVE-2022-23305](https://nvd.nist.gov/vuln/detail/CVE-2022-23305) [CVE-2022-23302](https://nvd.nist.gov/vuln/detail/CVE-2022-23302)

## [5.7.7]
### Changed
- Upgrade Spring Framework from 5.3.15 to 5.3.18 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade spring 53x framework bundle from 5.3.10_1 to 5.3.16_1

## [5.7.6]
### Reverted
- Revert "PluginKeyStack.suppress() for suppressing specific plugin-key from the stack" (introduced in 5.7.5)
### Changed
- Upgrade Spring framework from 5.3.10 to 5.3.15
- Upgrade log4j from 1.2.17-atlassian-13 to 1.2.17-atlassian-15 to address [CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228)
- Upgrade jaxb-runtime from 2.3.5 to 2.3.6
- Upgrade third party pom from 5.2.4 to 5.2.8
- Upgrade bytebuddy from 1.11.16 to 1.11.22

## [5.7.5] - DO NOT USE
### Added
- PluginKeyStack.suppress() for suppressing specific plugin-key from the stack

## [5.7.4]
### Changed
- [PLUG-1268](https://ecosystem.atlassian.net/browse/PLUG-1268] fix last modified handler to account for negative date values

## [5.7.3]
### Changed
- Upgrade Spring framework from 5.3.9 to 5.3.10, and servicemix bundle version from 5.3.9_1 to 5.3.10_1
- Add metrics for plugin enabled/disabled
- Add dependency on atlassian-profiling 3.7.0

## [5.7.2]
### Changed
- Switch to byte-buddy dependency that bundles its own copy of asm

## [5.7.1]
### Changed
- Fix system property that enables/disables PluginKeyStack tracking

## [5.7.0]
### Added
- Introduce PluginKeyStack that tracks the call-stack between plugins

## [5.6.4]
### Changed
- Upgrade Spring Framework from 5.3.9 to 5.3.18 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade spring 53x framework bundle from 5.3.3_1 to 5.3.18_1 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade log4j from 1.2.17-atlassian-13 to 1.2.17-atlassian-16 to address [CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228) [CVE-2022-23307](https://nvd.nist.gov/vuln/detail/CVE-2022-23307) [CVE-2020-9493](https://nvd.nist.gov/vuln/detail/CVE-2020-9493) [CVE-2022-23305](https://nvd.nist.gov/vuln/detail/CVE-2022-23305) [CVE-2022-23302](https://nvd.nist.gov/vuln/detail/CVE-2022-23302)
- Upgrade jaxb-runtime from 2.3.5 to 2.3.6
- Upgrade third party pom from 5.2.4 to 5.2.8

## [5.6.3]
### Changed
- [PLUG-1268](https://ecosystem.atlassian.net/browse/PLUG-1268] fix last modified handler to account for negative date values

## [5.6.2]
### Changed
- Use SortedSet in API

## [5.6.1]
- Mark commons-lang3 dependency as provided

## [5.6.0]
### Changed
- Replace Guava usages in API

## [5.5.5]
### Changed
- Upgrade Spring Framework from 5.3.9 to 5.3.18 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade spring 53x framework bundle from 5.3.10_1 to 5.3.18_1 to address [CVE-2022-22965](https://nvd.nist.gov/vuln/detail/CVE-2022-22965)
- Upgrade log4j from 1.2.17-atlassian-13 to 1.2.17-atlassian-16 to address [CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228) [CVE-2022-23307](https://nvd.nist.gov/vuln/detail/CVE-2022-23307) [CVE-2020-9493](https://nvd.nist.gov/vuln/detail/CVE-2020-9493) [CVE-2022-23305](https://nvd.nist.gov/vuln/detail/CVE-2022-23305) [CVE-2022-23302](https://nvd.nist.gov/vuln/detail/CVE-2022-23302)
- Upgrade jaxb-runtime from 2.3.5 to 2.3.6
- Upgrade third party pom from 5.2.4 to 5.2.8

## [5.5.4]
 - Empty release

## [5.5.3]
### Changed
- [PLUG-1268](https://ecosystem.atlassian.net/browse/PLUG-1268] fix last modified handler to account for negative date values
- Upgrade jacoco-maven-plugin from 0.8.6 to 0.8.7
- Upgrade jaxb-runtime from 2.3.3 to 2.3.5
- Upgrade Spring Framework from 5.3.3 to 5.3.9
- Upgrade atlassian event from 4.0.3 to 4.0.4
- Upgrade atlassian annotations from 2.2.1 to 2.2.2
- Upgrade third party pom from 5.2.2 to 5.2.4
- Upgrade public-pom from 6.1.2 to 6.1.3

## [5.5.2]
### Changed
- Upgrade dom4j from 1.6.1-atlassian-2 to 1.6.1-atlassian-3 to address [CVE-2018-1000632](https://nvd.nist.gov/vuln/detail/CVE-2018-1000632)
- Upgrade third party pom from 5.0.28 to 5.2.2

## [5.5.1]
### Changed
- Remove platform POM dependency

## [5.5.0]
### Changed
- Provide a spring 53x framework bundle using spring 5.3.3, and remove older spring 50x and 51x framework bundles

## [5.4.3]
### Changed
- [SPFE-286](https://ecosystem.atlassian.net/browse/SPFE-286) Fix bug while generating etag

## [5.4.2]
### Changed
- Upgrade spring 51x framework bundle from 5.1.14.RELEASE_1 to 5.1.18.RELEASE_1.

## [5.3.11] 
### Changed
- Upgrade spring 51x framework bundle from 5.1.14.RELEASE_1 to 5.1.18.RELEASE_1.

## [5.3.8]
### Changed
- Dropped log4j dependency (see [PLUG-1243](https://ecosystem.atlassian.net/browse/PLUG-1243)). 

## [5.3.7]
### Fixed
- Upgrade spring 51x framework bundle from 5.1.8.RELEASE_1 to 5.1.14.RELEASE_1 in order to fix [CVE-2020-5398](https://nvd.nist.gov/vuln/detail/CVE-2020-5398).

## [5.3.6]
### Fixed
- Fixed problem with filtering interfaces during plugin transformation [PLUG-1242](https://ecosystem.atlassian.net/browse/PLUG-1242).

## [5.3.5]
### Fixed
- Upgrade commons-collections from 3.2 to 3.2.2
- Exception handling fix in XmlDynamicPluginFactory.

## [5.3.4]
### Fixed
- Upgrade dom4j from 1.4 to 1.6.1-atlassian-1

## [5.3.3]
### Fixed
- Concurrency fixes in FelixOsgiContainerManager.
- Use synchronous Gemini shutdown by default [PLUG-1239](https://ecosystem.atlassian.net/browse/PLUG-1239).
### Changed
- Changes to package imports for transformed plugins [PLUG-1240](https://ecosystem.atlassian.net/browse/PLUG-1240).

## [5.3.2]
### Changed
- Stable order of plugin events.
- Provide the ability to use synchronous context shutdown.

## [5.3.1]
### Fixed 
- Disable plugins in correct order, so that the plugin that is being disabled does not block waiting for its dependency.
- Fixed loading of transformed plugins with module-info.class [PLUG-1236](https://ecosystem.atlassian.net/browse/PLUG-1236).
- Upgrade spring 51x framework bundle from 5.1.5.RELEASE_1 to 5.1.8.RELEASE_1 [PLUG-1237](https://ecosystem.atlassian.net/browse/PLUG-1237).
- Fixed the case of XML parsing exceptions being logged to stderr.

## [5.3.0]

### Added
- Now supports a spring51x bundle, with Spring 5.1.5.RELEASE_1

## [5.1.0]

### Added
- The `StoredPluginStateAccessor` shows when plugins have been disabled by an administrator and other persistent plugin state.

## [5.0.3]

### Fixed
- Updates Apache Felix to 5.6.12, fixing [FELIX-6035](https://issues.apache.org/jira/browse/FELIX-6035).
  This bug prevented Atlassian Plugins from working with Java 11.0.2, while the 11.0.0 & 11.0.1 versions did
  not experience this problem.

## [5.0.2]
- Reverts the change "Restored autowiring behaviour from pre-5.0.0" made in 5.0.1

## [5.0.1]

### Changed
- Restored autowiring behaviour from pre-5.0.0
- Avoid creation of multiple PluginHttp wrappers around Request and Session objects

### Fixed
- Fixed a regression introduced in 5.0.0 whereby P1 plugins would not load from `$JIRA_HOME/plugins/installed-plugins`

## [5.0.0]

### Changed
- This release adds support for compiling and running `atlassian-plugins` on Java 11
- Upgraded Spring to 5.0. As part of this change, the autowire mode has changed from the deprecated
  auto-detect mode to 'by constructor'. Any components that relied on setter injection will need to annotate
  these setters or fields with `@Inject` or `@Autowired`
- Upgraded Gemini Blueprints to 3.0.0.M01
- Switched from commons-lang to commons-lang3
- Added dependencies to axb, jaxws, javax-annotation and javax-activation, which are no longer provided
  by JDK 11
- Upgraded Mockito
- Updated the plugin transformation pipeline for host components that plugins use without declaring a
  `component-import` for them. Previously, upon encountering the use of a service (say `SomeService`) that
  is provided by the host application, plugin transformation would look up the host component that implements
  this interface and add an OSGI import for _all_ interfaces this component implemented. So, if `SomeServiceImpl`
  implemented both `SomeService` and `FancySomeService` (and exported both interfaces), the OSGI import
  would import both `SomeService` and `FancySomeService`. In addition, OSGI package imports would be added
  for all packages used in the method signatures on `SomeService` and `FancySomeService`.
  
  Starting with 5.0, only the _requested_ interface is considered (`SomeService` in this example), and no OSGI
  imports are added for any additional interfaces that a matching host component also provides, or the packages
  use in their method signatures. 

### Removed
- Removed most methods and types that had been deprecated prior to 1-1-2016. See [api-changelog.md](api-changelog.md)
  for a detailed breakdown of what has been removed and what the replacement functionality is. 

## [4.6.2]

### Changed
- Upgraded Spring 43x bundle to Spring 4.3.20.RELEASE.

## [4.6.1]

### Added
- Now supports a Spring 43x bundle, with Spring 4.3.18.RELEASE and Gemini blueprints 2.1.0.RELEASE.
- Now supports running in Java 11, but *not* compiling against Java 11, when using the Spring 43x bundle.
- Added dependencies on JAXB, javax-annotation and javax-activation jars, as these are no longer provided by JDK 11.

### Changed
- Upgraded Felix framework to 5.6.10
- Improved the exported jdk packages to match Java 8, 9 and 11, so it matches the version we're running in.
- Updated package scanning to use FastClasspathScanner, for compatibility with modern Java platform classloaders.
- Prevents the spring-internal ImportRegistry from being wired as a Collection into any object with a setFoo(Collection).

## [4.6.0]

No changes in this release. Accidentally released from the wrong branch. Do not use.

[5.3.8]: https://bitbucket.org/atlassian/atlassian-plugins/branches/compare/atlassian-plugins-parent-5.3.8%0Datlassian-plugins-parent-5.3.7
[5.3.7]: https://bitbucket.org/atlassian/atlassian-plugins/branches/compare/atlassian-plugins-parent-5.3.7%0Datlassian-plugins-parent-5.3.6
[5.3.6]: https://bitbucket.org/atlassian/atlassian-plugins/branches/compare/atlassian-plugins-parent-5.3.6%0Datlassian-plugins-parent-5.3.5
[5.3.5]: https://bitbucket.org/atlassian/atlassian-plugins/branches/compare/atlassian-plugins-parent-5.3.5%0Datlassian-plugins-parent-5.3.4
[5.3.4]: https://bitbucket.org/atlassian/atlassian-plugins/branches/compare/atlassian-plugins-parent-5.3.4%0Datlassian-plugins-parent-5.3.3
[5.3.3]: https://bitbucket.org/atlassian/atlassian-plugins/branches/compare/atlassian-plugins-parent-5.3.3%0Datlassian-plugins-parent-5.3.2
[5.3.2]: https://bitbucket.org/atlassian/atlassian-plugins/branches/compare/atlassian-plugins-parent-5.3.2%0Datlassian-plugins-parent-5.3.1
[5.3.1]: https://bitbucket.org/atlassian/atlassian-plugins/branches/compare/atlassian-plugins-parent-5.3.1%0Datlassian-plugins-parent-5.3.0