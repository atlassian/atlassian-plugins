package com.atlassian.plugin.servlet.util;

import java.time.LocalDateTime;
import java.util.Date;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.BooleanUtils;
import com.google.common.annotations.VisibleForTesting;

import com.atlassian.plugin.servlet.cache.model.CacheableRequest;

import static com.google.common.net.HttpHeaders.LAST_MODIFIED;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static java.util.Objects.nonNull;
import static java.util.Optional.ofNullable;
import static javax.servlet.http.HttpServletResponse.SC_NOT_MODIFIED;

import static com.atlassian.plugin.servlet.util.date.DateUtil.defaultIfNull;
import static com.atlassian.plugin.servlet.util.date.DateUtil.localDateTimeOf;

/**
 * This class manages the last modified date of a single HTTP resource.
 */
public class LastModifiedHandler {

    /**
     * System property used to identify if the cache should be disabled or not.
     */
    @VisibleForTesting
    static final String ATLASSIAN_DISABLE_CACHES_PROPERTY = "atlassian.disable.caches";

    private LocalDateTime lastModified;

    public LastModifiedHandler() {
        this(new Date());
    }

    public LastModifiedHandler(@Nullable final LocalDateTime lastModifiedDate) {
        this.lastModified = lastModifiedDate;
    }

    public LastModifiedHandler(@Nullable final Date lastModifiedDate) {
        this(localDateTimeOf(lastModifiedDate).orElse(null));
    }

    /**
     * <p>This static method is used when the resource being served by the servlet keeps track of the last modified date,
     * and so no state needs to be maintained by this handler.</p>
     *
     * @see LastModifiedHandler#isNotCacheableResponse(HttpServletRequest)
     * @see LastModifiedHandler#setCacheHeadersIfCacheable(HttpServletRequest, HttpServletResponse)
     * @deprecated <p>In the future the method {@link LastModifiedHandler#isNotCacheableResponse(HttpServletRequest)} should be used for the check for a better naming convention.</p>
     */
    @Deprecated
    public static boolean checkRequest(
            @Nonnull final HttpServletRequest request,
            @Nonnull final HttpServletResponse response,
            @Nonnull final Date lastModifiedDate) {
        final LocalDateTime lastModified = defaultIfNull(lastModifiedDate, now());
        setCacheHeadersIfCacheable(request, response, lastModified);
        return isCacheableResponse(new CacheableRequest(request), lastModified);
    }

    /**
     * Specification used to identify if the property {@link LastModifiedHandler#ATLASSIAN_DISABLE_CACHES_PROPERTY} was defined as {@link Boolean#FALSE}.
     *
     * @return <p>{@code true}: The cache is enabled.</p>
     * <p>{@code false}: The cache is not enabled.</p>
     */
    private static boolean isCacheEnabled() {
        return ofNullable(getProperty(ATLASSIAN_DISABLE_CACHES_PROPERTY))
                .map(BooleanUtils::toBoolean)
                .filter(FALSE::equals)
                .orElse(TRUE);
    }

    /**
     * <p>Verifies whether the current response is cacheable or not, based on the {@link CacheableRequest} and {@link LastModifiedHandler#isCacheEnabled()}.</p>
     *
     * @param cachingInformation The object containing the caching information.
     * @return <p>{@code true}: If the current defined response is cacheable.</p>
     * <p>{@code false}: If the current defined response is not cacheable.</p>
     */
    private static boolean isCacheableResponse(
            final CacheableRequest cachingInformation, final LocalDateTime lastModifiedDate) {
        return isCacheEnabled()
                && nonNull(lastModifiedDate)
                && cachingInformation.isCacheable(lastModifiedDate)
                && !cachingInformation
                        .getIfNoneMatchHeader()
                        .isPresent(); // the last modified date will only be used if there is not any if-none-match
        // header.
    }

    public static void setCacheHeadersIfCacheable(
            @Nonnull final HttpServletRequest request,
            @Nonnull final HttpServletResponse response,
            @Nonnull final LocalDateTime lastModifiedDate) {
        final CacheableRequest cachingInformation = new CacheableRequest(request);
        if (isCacheableResponse(cachingInformation, lastModifiedDate)) {
            response.setStatus(SC_NOT_MODIFIED);
        }
        // this is used to expose the plugin last modified date to be used by other classes.
        if (nonNull(lastModifiedDate)) {
            final CacheableRequest cacheableRequest = new CacheableRequest(request);
            cacheableRequest.setPluginLastModifiedDate(lastModifiedDate);
            response.setDateHeader(LAST_MODIFIED, lastModifiedDate.toEpochSecond(UTC));
        }
    }

    /**
     * <p>Check whether we need to generate a response for this request.</p>
     * <p>Set the necessary headers on the response, and if we don't need to provide content, set the response status to 304.</p>
     * <p>If this method returns true, the caller should not perform any more processing on the request.</p>
     *
     * @param request  The request with the caching headers used for the verification.
     * @param response The response with the content used to generate the ETag.
     * @return true if we don't need to provide any data to satisfy this request.
     * @deprecated This method performs two operations, which is setting the response headers and checking the cache headers.
     *
     * <p>In the future the method {@link LastModifiedHandler#isNotCacheableResponse(HttpServletRequest)} should be used for the check.</p>
     * <p>In the future the method {@link LastModifiedHandler#setCacheHeadersIfCacheable(HttpServletRequest, HttpServletResponse)} should be used to defined the response headers.</p>
     */
    @Deprecated
    public boolean checkRequest(
            @Nonnull final HttpServletRequest request, @Nonnull final HttpServletResponse response) {
        setCacheHeadersIfCacheable(request, response);
        return isCacheableResponse(new CacheableRequest(request), this.lastModified);
    }

    /**
     * <p>Verifies whether the current response is cacheable or not, based on the {@link HttpServletRequest} and {@link LastModifiedHandler#isCacheEnabled()}.</p>
     *
     * @param request The request with the caching headers used for the verification.
     * @return <p>{@code true}: If the current defined response is not cacheable.</p>
     * <p>{@code false}: If the current defined response is cacheable.</p>
     */
    public boolean isNotCacheableResponse(@Nonnull final HttpServletRequest request) {
        return !isCacheableResponse(new CacheableRequest(request), this.lastModified);
    }

    /**
     * Defines the caching headers for the current {@link HttpServletResponse} if it is a {@link LastModifiedHandler#isNotCacheableResponse}.
     */
    public void setCacheHeadersIfCacheable(
            @Nonnull final HttpServletRequest request, @Nonnull final HttpServletResponse response) {
        setCacheHeadersIfCacheable(request, response, this.lastModified);
    }

    /**
     * The content has changed, reset the modified date.
     */
    public void modified() {
        this.lastModified = now();
    }
}
