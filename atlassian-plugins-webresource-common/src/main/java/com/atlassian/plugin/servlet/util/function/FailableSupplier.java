package com.atlassian.plugin.servlet.util.function;

import java.util.function.Supplier;

/**
 * Represents a supplier of results which can throw exceptions.
 *
 * @param <T> the type of results supplied by this supplier
 * @param <E> the type of the exception thrown by the supplier
 *
 * @since 4.1.19
 */
@FunctionalInterface
public interface FailableSupplier<T, E extends Exception> {

    /**
     * Gets a result.
     *
     * @return a result
     * @throws E an exception
     */
    T get() throws E;

    /**
     * Wrapper responsible for converting a certain {@link FailableSupplier} into a {@link Supplier}.
     * @param supplier The supplier function to be converted.
     * @param <T> the type of results supplied by this supplier
     * @param <E> the type of the exception thrown by the supplier
     * @return The build {@link Supplier}.
     */
    static <T, E extends Exception> Supplier<T> wrapper(final FailableSupplier<T, E> supplier) {
        return () -> {
            try {
                return supplier.get();
            } catch (final Exception exception) {
                throw new IllegalStateException(exception);
            }
        };
    }
}
