package com.atlassian.plugin.servlet;

import java.io.IOException;
import java.io.InputStream;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceLocation;

/**
 * A DownloadableResource with no content
 *
 * @since v3.0
 */
public class EmptyDownloadableResource extends AbstractDownloadableResource {
    private static final InputStream EMPTY_INPUT_STREAM = new EmptyInputStream();

    private static class EmptyInputStream extends InputStream {
        @Override
        public int read() throws IOException {
            return -1;
        }
    }

    /**
     * @param plugin           used to find last modified date and plugin key
     * @param resourceLocation used to get content type for the resource
     */
    public EmptyDownloadableResource(Plugin plugin, ResourceLocation resourceLocation) {
        super(plugin, resourceLocation, null);
    }

    @Override
    protected InputStream getResourceAsStream(String resourceLocation) {
        return EMPTY_INPUT_STREAM;
    }
}
