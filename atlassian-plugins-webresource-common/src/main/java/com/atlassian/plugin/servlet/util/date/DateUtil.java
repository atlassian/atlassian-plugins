package com.atlassian.plugin.servlet.util.date;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.time.ZoneOffset.UTC;

/**
 * Class responsible for handling processes related to dates.
 *
 * @since 4.1.18
 */
public final class DateUtil {

    private DateUtil() {}

    /**
     * Converts a certain {@link Date} to {@link java.time.LocalDate} and filter negative dates.
     * @param date The date to be converted.
     * @return The object containing the converted date.
     */
    public static Optional<LocalDateTime> localDateTimeOf(@Nullable final Date date) {
        return Optional.ofNullable(date)
                .filter(value -> value.getTime() >= 0L)
                .map(Date::toInstant)
                .map(instant -> instant.atZone(UTC))
                .map(ZonedDateTime::toLocalDateTime);
    }

    /**
     * Converts a certain {@link Date} to {@link java.time.LocalDate}.
     * @param date The date to be converted.
     * @param defaultValue The default value if the date is null.
     * @return The object containing the converted date.
     */
    public static LocalDateTime defaultIfNull(@Nullable final Date date, @Nonnull final LocalDateTime defaultValue) {
        return localDateTimeOf(date).orElse(defaultValue);
    }
}
