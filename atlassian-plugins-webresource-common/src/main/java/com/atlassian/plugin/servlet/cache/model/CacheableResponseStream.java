package com.atlassian.plugin.servlet.cache.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.annotation.Nonnull;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;

/**
 * Represents a {@link ServletOutputStream} used as helper for {@link CacheableResponse} to copy its content body.
 *
 * @since 4.1.19
 */
class CacheableResponseStream extends ServletOutputStream {

    private final ByteArrayOutputStream outputStream;

    CacheableResponseStream() {
        outputStream = new ByteArrayOutputStream(1024);
    }

    @Override
    public void close() throws IOException {
        outputStream.close();
    }

    @Override
    public void flush() throws IOException {
        outputStream.flush();
    }

    @Override
    public void write(final int data) {
        outputStream.write((byte) data);
    }

    @Override
    public void write(@Nonnull final byte[] data, final int offset, int length) {
        outputStream.write(data, offset, length);
    }

    @Override
    public void write(@Nonnull final byte[] data) {
        write(data, 0, data.length);
    }

    /**
     * Get a copy of the current stream.
     * @return The representation of the current stream as byte array.
     */
    @Nonnull
    public byte[] getCopy() {
        return outputStream.toByteArray();
    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setWriteListener(final WriteListener writeListener) {}
}
