package com.atlassian.plugin.servlet.cache.model;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import com.google.common.net.HttpHeaders;

import static com.google.common.net.HttpHeaders.ETAG;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

import static com.atlassian.plugin.servlet.util.function.FailableConsumer.wrapper;
import static com.atlassian.plugin.servlet.util.function.FailableSupplier.wrapper;

/**
 * <p>Represents a {@link HttpServletResponseWrapper} responsible for exposing the content of a {@link HttpServletResponse} as {@link ETagToken}.</p>
 *
 * @since 4.1.18
 */
public class CacheableResponse extends HttpServletResponseWrapper implements Closeable {

    private CacheableResponseStream outputStream;
    private PrintWriter writer;

    public CacheableResponse(@Nonnull final HttpServletResponse response) {
        super(response);
    }

    @Override
    public final void close() throws IOException {
        if (nonNull(writer)) {
            writer.close();
        }
        if (nonNull(outputStream)) {
            outputStream.close();
        }
    }

    @Override
    public final void flushBuffer() {
        try {
            if (nonNull(writer)) {
                writer.flush();
            }
            if (nonNull(outputStream)) {
                outputStream.flush();
            }
        } catch (final IOException exception) {
            throw new IllegalStateException("Error while flushing data.");
        }
    }

    /**
     * Flushes the content to the original-wrapped response.
     */
    public final void flushResponse() {
        this.getContentBody().ifPresent(wrapper(contentBody -> {
            final HttpServletResponse response = (HttpServletResponse) getResponse();
            response.setContentLength(contentBody.length);
            response.getOutputStream().write(contentBody);
            response.getOutputStream().flush();
        }));
    }

    /**
     * Get the current response body.
     *
     * @return The possible response body if there is any.
     */
    public final Optional<byte[]> getContentBody() {
        flushBuffer();
        return ofNullable(outputStream).map(CacheableResponseStream::getCopy);
    }

    @Nonnull
    @Override
    public final ServletOutputStream getOutputStream() {
        outputStream = ofNullable(outputStream).orElseGet(CacheableResponseStream::new);
        return outputStream;
    }

    @Nonnull
    @Override
    public final PrintWriter getWriter() {
        writer = ofNullable(writer).orElseGet(wrapper(() -> {
            final String characterEncoding = getResponse().getCharacterEncoding();
            final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getOutputStream(), characterEncoding);
            return new PrintWriter(outputStreamWriter, true);
        }));
        return writer;
    }

    public final boolean isCacheable(@Nonnull final CacheableRequest request) {
        requireNonNull(request, "The request is mandatory to verify if the current response is cacheable.");
        return request.isCacheable(this);
    }

    /**
     * Defines the current {@link HttpHeaders#ETAG} header based on the response body content.
     */
    public final void setETagHeader() {
        this.toETagToken().map(ETagToken::getDoubleQuotedValue).ifPresent(token -> {
            final HttpServletResponse response = (HttpServletResponse) getResponse();
            response.setHeader(ETAG, token);
        });
    }

    /**
     * Get the representation of the current response body as {@link ETagToken}.
     *
     * @return A possible representation of the current response body if the body is not null.
     */
    @Nonnull
    public Optional<ETagToken> toETagToken() {
        return getContentBody().map(ETagToken::new);
    }
}
