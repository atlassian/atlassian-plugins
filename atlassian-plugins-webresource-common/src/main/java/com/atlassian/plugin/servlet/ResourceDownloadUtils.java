package com.atlassian.plugin.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ResourceDownloadUtils {

    private static final String CACHE_CONTROL = "Cache-Control";
    private static final long ONE_YEAR_SECONDS = 60L * 60L * 24L * 365L;
    private static final long ONE_YEAR_MILLISECONDS = 1000 * ONE_YEAR_SECONDS;

    /**
     * Set 'expire' headers to cache for one year. Also adds the additional cache control values passed in.
     * Note, this method resets the cache control headers if set previously.
     *
     * If caches are disabled (via the system property atlassian.disable.caches), this method sets 'expires' to 0.
     */
    public static void addCachingHeaders(
            HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final String... cacheControls) {
        boolean cacheDisabledByQueryParam = "false".equals(httpServletRequest.getParameter("cache"));
        if (Boolean.getBoolean("atlassian.disable.caches")) {
            httpServletResponse.setDateHeader("Expires", 0);
            httpServletResponse.setHeader(CACHE_CONTROL, "no-cache, must-revalidate");
        } else if (!cacheDisabledByQueryParam) {
            httpServletResponse.setDateHeader("Expires", System.currentTimeMillis() + ONE_YEAR_MILLISECONDS);
            httpServletResponse.setHeader(CACHE_CONTROL, "max-age=" + ONE_YEAR_SECONDS);
            for (final String cacheControl : cacheControls) {
                httpServletResponse.addHeader(CACHE_CONTROL, cacheControl);
            }
        }
    }

    /**
     * Sets caching headers with public cache control. Applications should call this method from urlrewrite.xml to
     * decorate urls like <code>/s/{build num}/.../_/resourceurl</code>.
     *
     * @see <a href="http://tuckey.org/urlrewrite/manual/2.6/">http://tuckey.org/urlrewrite/manual/2.6/</a>
     */
    public static void addPublicCachingHeaders(
            final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) {
        addCachingHeaders(httpServletRequest, httpServletResponse, "public");
    }

    /**
     * Sets caching headers with private cache control. Applications should call this method from urlrewrite.xml to
     * decorate urls like <code>/sp/{build num}/.../_/resourceurl</code>.
     *
     * @see <a href="http://tuckey.org/urlrewrite/manual/2.6/">http://tuckey.org/urlrewrite/manual/2.6/</a>
     */
    public static void addPrivateCachingHeaders(
            final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) {
        addCachingHeaders(httpServletRequest, httpServletResponse, "private");
    }
}
