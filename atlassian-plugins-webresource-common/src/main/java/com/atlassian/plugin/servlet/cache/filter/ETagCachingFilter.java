package com.atlassian.plugin.servlet.cache.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugin.servlet.cache.model.CacheableRequest;
import com.atlassian.plugin.servlet.cache.model.CacheableResponse;

import static javax.servlet.http.HttpServletResponse.SC_NOT_MODIFIED;

/**
 * <p>Filter responsible for performing caching of resources.</p>
 *
 * <p>This filter will cover the following scenarios:</p>
 *
 * <ul>
 *     <li>WHEN If-None-Match matches the ETag generated from a Response body.</li>
 *     <li>THEN set the ETag header with the current ETag and set Http Status code to 304.</li>
 *     <li>AND an empty response body.</li>
 * </ul>
 * <ul>
 *     <li>WHEN If-None-Match does not match the ETag generated from a response body.</li>
 *     <li>THEN set the ETag header with the current ETag.</li>
 *     <li>AND set the response body with the current content.</li>
 * </ul>
 *
 * @since 4.1.19
 */
public class ETagCachingFilter implements Filter {

    @Override
    public void init(final FilterConfig filterConfig) {
        // not used
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {
            doFilter((HttpServletRequest) request, (HttpServletResponse) response, chain);
        } else {
            chain.doFilter(request, response);
        }
    }

    private void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        final CacheableRequest wrappedRequest = new CacheableRequest(request);
        final CacheableResponse wrappedResponse = new CacheableResponse(response);

        chain.doFilter(wrappedRequest, wrappedResponse);

        wrappedResponse.setETagHeader();
        if (wrappedResponse.isCacheable(wrappedRequest)) {
            response.setStatus(SC_NOT_MODIFIED);
        } else {
            wrappedResponse.flushResponse();
        }
    }

    @Override
    public void destroy() {
        // not used
    }
}
