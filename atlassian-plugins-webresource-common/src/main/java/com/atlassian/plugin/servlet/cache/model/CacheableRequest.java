package com.atlassian.plugin.servlet.cache.model;

import java.time.LocalDateTime;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import com.google.common.annotations.VisibleForTesting;

import static java.time.ZoneOffset.UTC;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * Represents a caching information for a current {@link HttpServletRequest} headers and the {@link HttpServletResponse} response body.
 *
 * @since 4.1.18
 */
public class CacheableRequest extends HttpServletRequestWrapper {

    @VisibleForTesting
    static final long EMPTY_MODIFIED_SINCE_HEADER = -1;

    @VisibleForTesting
    static final String PLUGIN_LAST_MODIFIED_DATE = "Plugin-Last-Modified-Date";

    /**
     * The double quote used by {@code If-None-Match} header.
     */
    private static final String DOUBLE_QUOTE = "\"";

    /**
     * The HTTP {@code If-None-Match} header field name.
     */
    private static final String IF_MODIFIED_SINCE = "If-Modified-Since";

    /**
     * The HTTP {@code If-None-Match} header field name.
     */
    private static final String IF_NONE_MATCH = "If-None-Match";

    private final HttpServletRequest request;

    public CacheableRequest(@Nonnull final HttpServletRequest request) {
        super(request);
        this.request = request;
    }

    public boolean isCacheable(@Nonnull final LocalDateTime pluginLastModifiedDate) {
        final long ifModifiedSinceHeader = request.getDateHeader(IF_MODIFIED_SINCE);
        final long pluginLastModifiedSeconds = pluginLastModifiedDate.toEpochSecond(UTC);
        return EMPTY_MODIFIED_SINCE_HEADER != ifModifiedSinceHeader
                && ifModifiedSinceHeader >= pluginLastModifiedSeconds;
    }

    public boolean isCacheable(@Nonnull final CacheableResponse response) {
        if (getIfNoneMatchHeader().isPresent()) {
            return response.toETagToken().map(ETagToken::getValue).equals(getIfNoneMatchHeader());
        }
        return ofNullable(request.getAttribute(PLUGIN_LAST_MODIFIED_DATE))
                .map(pluginLastModifiedDate -> (LocalDateTime) pluginLastModifiedDate)
                .map(this::isCacheable)
                .orElse(false);
    }

    @Nonnull
    public Optional<String> getIfNoneMatchHeader() {
        return ofNullable(request.getHeader(IF_NONE_MATCH))
                .map(ifNoneMatchHeader -> ifNoneMatchHeader.replace(DOUBLE_QUOTE, EMPTY))
                .filter(StringUtils::isNotEmpty);
    }

    public void setPluginLastModifiedDate(@Nullable final LocalDateTime pluginLastModifiedDate) {
        ofNullable(pluginLastModifiedDate)
                .ifPresent(lastModifiedDate -> getRequest().setAttribute(PLUGIN_LAST_MODIFIED_DATE, lastModifiedDate));
    }
}
