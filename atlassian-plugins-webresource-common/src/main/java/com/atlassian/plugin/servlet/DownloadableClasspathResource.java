package com.atlassian.plugin.servlet;

import java.io.InputStream;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceLocation;

/**
 * A {@link DownloadableResource} that will serve the resource from the plugin.
 *
 * @see Plugin#getResourceAsStream(String)
 */
public class DownloadableClasspathResource extends AbstractDownloadableResource {
    public DownloadableClasspathResource(Plugin plugin, ResourceLocation resourceLocation, String extraPath) {
        super(plugin, resourceLocation, extraPath);
    }

    @Override
    protected InputStream getResourceAsStream(final String resourceLocation) {
        return plugin.getResourceAsStream(resourceLocation);
    }

    public ResourceLocation getResourceLocation() {
        return resourceLocation;
    }
}
