package com.atlassian.plugin.servlet.util.function;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Represents a consumer of arguments which can throw exceptions.
 *
 * @param <T> the type of results supplied by this supplier
 * @param <E> the type of the exception thrown by the supplier
 *
 * @since 4.1.19
 */
@FunctionalInterface
public interface FailableConsumer<T, E extends Exception> {

    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     */
    void accept(T t) throws E;

    /**
     * Wrapper responsible for converting a certain {@link FailableConsumer} into a {@link Consumer}.
     * @param consumer The consumer function to be converted.
     * @param <T> the type of results supplied by this consumer
     * @param <E> the type of the exception thrown by the consumer
     * @return The build {@link Supplier}.
     */
    static <T, E extends Exception> Consumer<T> wrapper(final FailableConsumer<T, E> consumer) {
        return argument -> {
            try {
                consumer.accept(argument);
            } catch (final Exception exception) {
                throw new IllegalArgumentException(exception);
            }
        };
    }
}
