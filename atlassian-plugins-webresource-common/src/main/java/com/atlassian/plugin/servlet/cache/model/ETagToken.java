package com.atlassian.plugin.servlet.cache.model;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * Represents an ETag to be used to verify the content of an {@link HttpServletResponse} body.
 *
 * @since 4.1.18
 */
public class ETagToken {
    private static final String DOUBLE_QUOTED_TEMPLATE = "\"%s\"";
    private static final String HASHING_ALGORITHM = "MD5";
    private static final int POSITIVE_NUMBER_SIGN = 1;
    private static final int TOKEN_RADIX = 16;

    private final String value;

    /**
     * Build a certain ETag token based on a {@link HttpServletResponse} body and {@link Base64}.
     *
     * @param responseBody The response body to be hashed.
     */
    public ETagToken(@Nonnull final byte[] responseBody) {
        requireNonNull(responseBody, "The response body is mandatory to build the ETag token.");
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance(HASHING_ALGORITHM);
            final byte[] digestedBody = messageDigest.digest(responseBody);
            final BigInteger token = new BigInteger(POSITIVE_NUMBER_SIGN, digestedBody);
            value = token.toString(TOKEN_RADIX);
        } catch (final NoSuchAlgorithmException exception) {
            throw new IllegalStateException("MD5 cryptographic algorithm is not available.", exception);
        }
    }

    @Nonnull
    public String getValue() {
        return value;
    }

    /**
     * Entity tag uniquely representing the requested resource.
     * They are a string of ASCII characters placed between double quotes.
     *
     * @return Double quoted etag token.
     * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag">Mozilla ETag description</a>
     */
    @Nonnull
    public String getDoubleQuotedValue() {
        return format(DOUBLE_QUOTED_TEMPLATE, value);
    }
}
