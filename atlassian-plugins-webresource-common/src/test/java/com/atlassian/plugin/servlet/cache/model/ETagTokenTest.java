package com.atlassian.plugin.servlet.cache.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ETagTokenTest {

    @Test
    public void testWhenInputIsNotNull_shouldGenerateValidETagToken() {
        // given
        final byte[] input = "Test".getBytes();
        // when
        final String actualToken = new ETagToken(input).getValue();
        // then
        final String expectedToken = "cbc6611f5540bd0809a388dc95a615b"; // String 'Test' in MD5 hashing algorithm.
        assertEquals(expectedToken, actualToken);
    }

    @Test
    public void testWhenInputIsNull_shouldNotGenerateValidETagToken() {
        try {
            // when
            new ETagToken(null);
        } catch (final NullPointerException exception) {
            // then
            assertEquals("The response body is mandatory to build the ETag token.", exception.getMessage());
        }
    }

    @Test
    public void testWhenInputIsEmpty_shouldGenerateValidETagToken() {
        // given
        final byte[] emptyInput = "".getBytes();
        // when
        final String actualToken = new ETagToken(emptyInput).getValue();
        // then
        final String expectedToken = "d41d8cd98f00b204e9800998ecf8427e"; // Empty string in MD5 hashing algorithm.
        assertEquals(expectedToken, actualToken);
    }
}
