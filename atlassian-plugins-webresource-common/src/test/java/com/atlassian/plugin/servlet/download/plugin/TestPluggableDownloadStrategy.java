package com.atlassian.plugin.servlet.download.plugin;

import java.io.IOException;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.module.ClassPrefixModuleFactory;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.module.PrefixDelegatingModuleFactory;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadStrategy;
import com.atlassian.plugin.servlet.util.CapturingHttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPluggableDownloadStrategy {

    private PluggableDownloadStrategy strategy;

    @Before
    public void setUp() {
        strategy = new PluggableDownloadStrategy(new DefaultPluginEventManager());
    }

    @Test
    public void testRegister() throws Exception {
        strategy.register("monkey.key", new StubDownloadStrategy("/monkey", "Bananas"));

        assertTrue(strategy.matches("/monkey/something"));

        CapturingHttpServletResponse response = new CapturingHttpServletResponse();
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/monkey/something");

        strategy.serveFile(request, response);
        assertEquals("Bananas\r\n", response.toString());
    }

    @Test
    public void testUnregister() {
        strategy.register("monkey.key", new StubDownloadStrategy("/monkey", "Bananas"));
        strategy.unregister("monkey.key");

        assertFalse(strategy.matches("/monkey/something"));
    }

    @Test
    public void testPluginModuleEnabled() throws Exception {
        ModuleDescriptor module = new DownloadStrategyModuleDescriptor(getDefaultModuleClassFactory()) {
            public String getCompleteKey() {
                return "jungle.plugin:lion-strategy";
            }

            public DownloadStrategy getModule() {
                return new StubDownloadStrategy("/lion", "ROAR!");
            }
        };

        strategy.pluginModuleEnabled(new PluginModuleEnabledEvent(module));

        assertTrue(strategy.matches("/lion/something"));

        CapturingHttpServletResponse response = new CapturingHttpServletResponse();
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/lion/something");

        strategy.serveFile(request, response);
        assertEquals("ROAR!\r\n", response.toString());
    }

    @Test
    public void testPluginModuleDisabled() {
        ModuleDescriptor module = new DownloadStrategyModuleDescriptor(getDefaultModuleClassFactory()) {
            public String getCompleteKey() {
                return "jungle.plugin:lion-strategy";
            }

            public DownloadStrategy getModule() {
                return new StubDownloadStrategy("/lion", "ROAR!");
            }
        };

        strategy.pluginModuleEnabled(new PluginModuleEnabledEvent(module));
        assertTrue(strategy.matches("/lion/something"));

        strategy.pluginModuleDisabled(new PluginModuleDisabledEvent(module, true));
        assertFalse(strategy.matches("/lion/something"));
    }

    @Test
    public void testUnregisterPluginModule() {
        ModuleDescriptor module = new DownloadStrategyModuleDescriptor(getDefaultModuleClassFactory()) {
            public String getCompleteKey() {
                return "jungle.plugin:lion-strategy";
            }

            public DownloadStrategy getModule() {
                return new StubDownloadStrategy("/lion", "ROAR!");
            }
        };

        strategy.pluginModuleEnabled(new PluginModuleEnabledEvent(module));
        assertTrue(strategy.matches("/lion/something"));

        strategy.unregister("jungle.plugin:lion-strategy");
        assertFalse(strategy.matches("/lion/something"));
    }

    private ModuleFactory getDefaultModuleClassFactory() {
        return new PrefixDelegatingModuleFactory(
                Collections.singleton(new ClassPrefixModuleFactory(new DefaultHostContainer())));
    }

    private static class StubDownloadStrategy implements DownloadStrategy {

        private final String urlPattern;
        private final String output;

        StubDownloadStrategy(String urlPattern, String output) {
            this.urlPattern = urlPattern;
            this.output = output;
        }

        public boolean matches(String urlPath) {
            return urlPath.contains(urlPattern);
        }

        public void serveFile(HttpServletRequest request, HttpServletResponse response) throws DownloadException {
            try {
                response.getOutputStream().println(output);
            } catch (IOException e) {
                throw new DownloadException(e);
            }
        }
    }
}
