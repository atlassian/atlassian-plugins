package com.atlassian.plugin.servlet;

import java.io.InputStream;
import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceLocation;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDownloadableWebResource {
    private DownloadableWebResource downloadableWebResource;

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Plugin plugin;

    @Mock
    private ResourceLocation resourceLocation;

    @Mock
    private ServletContext servletContext;

    @Before
    public void setUp() {
        when(resourceLocation.getLocation()).thenReturn("resource/location");

        downloadableWebResource = new DownloadableWebResource(
                plugin, resourceLocation, "extraPath", servletContext, true); // disableMinification
    }

    @Test
    public void resourceLocationWithoutSlashInTheBeginningShouldBeFixedToHaveOne() {
        final String resourceLocationWithoutSlash = "resource/without/slash/in/the/beginning";
        final String fixedLocation = "/" + resourceLocationWithoutSlash;
        testGetResourceAsStream(resourceLocationWithoutSlash, fixedLocation);
    }

    @Test
    public void resourceLocationWithSlashInTheBeginningShouldWorkUnchanged() {
        final String resourceLocation = "/resource/with/slash/in/the/beginning";
        testGetResourceAsStream(resourceLocation, resourceLocation);
    }

    private void testGetResourceAsStream(final String passedResourceLocation, final String expectedLocation) {
        final InputStream expectedStream = mock(InputStream.class);

        when(servletContext.getResourceAsStream(expectedLocation)).thenReturn(expectedStream);

        final InputStream resourceAsStream = downloadableWebResource.getResourceAsStream(passedResourceLocation);

        assertThat(resourceAsStream, equalTo(expectedStream));
    }
}
