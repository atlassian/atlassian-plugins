package com.atlassian.plugin.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestResourceDownloadUtils {

    private static final long ONE_YEAR = 60L * 60L * 24L * 365L;
    private static final long ONE_YEAR_MS = ONE_YEAR * 1000;
    private static final String CACHE_CONTROL = "Cache-Control";

    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private HttpServletResponse mockResponse;

    @Test
    public void testAddPublicCachingHeaders() {
        when(mockRequest.getParameter(eq("cache"))).thenReturn("true");

        // This is part of a ghetto assert for time. We could freeze time if we were using jodatime.
        long expectedTime = aboutNowPlusAYear();

        ResourceDownloadUtils.addPublicCachingHeaders(mockRequest, mockResponse);

        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(mockResponse).setDateHeader(eq("Expires"), argument.capture());
        assertTrue("Set expires header beyond one year from now", expectedTime <= argument.getValue());

        verify(mockResponse).setHeader(CACHE_CONTROL, "max-age=" + ONE_YEAR);
    }

    @Test
    public void testAddCachingHeadersWithCacheControls() {
        when(mockRequest.getParameter(eq("cache"))).thenReturn("true");

        // This is part of a ghetto assert for time. We could freeze time if we were using jodatime.
        long expectedTime = aboutNowPlusAYear();

        ResourceDownloadUtils.addCachingHeaders(mockRequest, mockResponse, "private", "foo");

        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(mockResponse).setDateHeader(eq("Expires"), argument.capture());
        assertTrue("Set expires header beyond one year from now", expectedTime <= argument.getValue());

        verify(mockResponse).setHeader(CACHE_CONTROL, "max-age=" + ONE_YEAR);
        verify(mockResponse).addHeader(CACHE_CONTROL, "private");
        verify(mockResponse).addHeader(CACHE_CONTROL, "foo");
    }

    @Test
    public void testNoCachingHeadersWhenCachingDisabled() {
        when(mockRequest.getParameter(eq("cache"))).thenReturn("false");

        ResourceDownloadUtils.addPublicCachingHeaders(mockRequest, mockResponse);

        verifyNoMoreInteractions(mockResponse);
    }

    private static long aboutNowPlusAYear() {
        return System.currentTimeMillis() + ONE_YEAR_MS;
    }
}
