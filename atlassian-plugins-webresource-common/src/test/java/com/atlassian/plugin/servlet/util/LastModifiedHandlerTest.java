package com.atlassian.plugin.servlet.util;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.servlet.cache.model.CacheableResponse;
import com.atlassian.plugin.servlet.cache.model.ETagToken;

import static com.google.common.net.HttpHeaders.IF_MODIFIED_SINCE;
import static com.google.common.net.HttpHeaders.IF_NONE_MATCH;
import static com.google.common.net.HttpHeaders.LAST_MODIFIED;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.System.setProperty;
import static java.time.LocalDateTime.of;
import static java.time.Month.JANUARY;
import static java.time.ZoneOffset.UTC;
import static javax.servlet.http.HttpServletResponse.SC_NOT_MODIFIED;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.servlet.util.LastModifiedHandler.ATLASSIAN_DISABLE_CACHES_PROPERTY;

public class LastModifiedHandlerTest {

    /**
     * Fixed {@link LocalDateTime} used as base for the tests to avoid test anomalies.
     */
    private static final LocalDateTime BASE_DATE_TIME = of(2020, JANUARY, 1, 0, 0);

    private static final String NON_HASHED_TEST = "Test";

    /**
     * Representation of the {@link String} 'Test' in {@code MD5} hashing algorithm.
     */
    private static final String MD5_HASHED_TEST = "cbc6611f5540bd0809a388dc95a615b";

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private HttpServletRequest mockedRequest;

    @Mock
    private CacheableResponse mockedResponse;

    @AfterClass
    public static void setUp() {
        // Guarantee that the cache will be disabled after executing the tests.
        setProperty(ATLASSIAN_DISABLE_CACHES_PROPERTY, FALSE.toString());
    }

    @Test
    public void testIfRequestIsNotCacheableWhenThereIsNotAnyConditionalHeader_shouldNotBeCacheableRequest() {
        // given
        setProperty(ATLASSIAN_DISABLE_CACHES_PROPERTY, TRUE.toString());

        when(mockedRequest.getDateHeader(IF_MODIFIED_SINCE)).thenReturn((long) -1);

        // when
        final boolean isNotCacheableResponse =
                new LastModifiedHandler(new Date()).isNotCacheableResponse(mockedRequest);

        // then
        assertTrue(isNotCacheableResponse);
    }

    @Test
    public void
            testIfRequestIsNotCacheableWhenIfModifiedSinceHeaderLowerThanLastModifiedSince_shouldNotBeCacheableRequest() {
        // given
        setProperty(ATLASSIAN_DISABLE_CACHES_PROPERTY, TRUE.toString());

        final long ifModifiedSinceHeader = BASE_DATE_TIME.minusHours(1).toEpochSecond(UTC);
        when(mockedRequest.getDateHeader(IF_MODIFIED_SINCE)).thenReturn(ifModifiedSinceHeader);

        // when
        final boolean isNotCacheableResponse =
                new LastModifiedHandler(new Date()).isNotCacheableResponse(mockedRequest);

        // then
        assertTrue(isNotCacheableResponse);
    }

    @Test
    public void testWhenIfModifiedSinceHeaderGreaterThanLastModifiedSince_shouldBeCacheableRequest() {
        // given
        setProperty(ATLASSIAN_DISABLE_CACHES_PROPERTY, TRUE.toString());

        final long ifModifiedSinceHeader = BASE_DATE_TIME.plusHours(1).toEpochSecond(UTC);
        when(mockedRequest.getDateHeader(IF_MODIFIED_SINCE)).thenReturn(ifModifiedSinceHeader);

        // when
        final boolean isNotCacheableResponse =
                new LastModifiedHandler(BASE_DATE_TIME).isNotCacheableResponse(mockedRequest);

        // then
        assertFalse(isNotCacheableResponse);
    }

    @Test
    public void
            testSettingCacheHeaderWhenIfModifiedSinceHeaderGreaterThanLastModified_shouldDefineNotModifiedHttpStatusCode() {
        // given
        setProperty(ATLASSIAN_DISABLE_CACHES_PROPERTY, TRUE.toString());

        final long ifModifiedSinceHeader = BASE_DATE_TIME.plusHours(1).toEpochSecond(UTC);
        when(mockedRequest.getDateHeader(IF_MODIFIED_SINCE)).thenReturn(ifModifiedSinceHeader);

        when(mockedRequest.getHeader(IF_NONE_MATCH)).thenReturn(EMPTY);

        final ETagToken eTagToken = new ETagToken(NON_HASHED_TEST.getBytes());
        when(mockedResponse.toETagToken()).thenReturn(Optional.of(eTagToken));

        // when
        new LastModifiedHandler(BASE_DATE_TIME).setCacheHeadersIfCacheable(mockedRequest, mockedResponse);

        // then
        verify(mockedResponse, times(1)).setStatus(SC_NOT_MODIFIED);
    }

    @Test
    public void
            testSettingCacheHeaderWhenThereIsNotAnyIfModifiedSinceHeader_shouldNotDefineNotModifiedHttpStatusCode() {
        // given
        setProperty(ATLASSIAN_DISABLE_CACHES_PROPERTY, TRUE.toString());

        when(mockedRequest.getHeader(IF_NONE_MATCH)).thenReturn(MD5_HASHED_TEST);

        final ETagToken eTagToken = new ETagToken(NON_HASHED_TEST.getBytes());
        when(mockedResponse.toETagToken()).thenReturn(Optional.of(eTagToken));

        // when
        new LastModifiedHandler(new Date()).setCacheHeadersIfCacheable(mockedRequest, mockedResponse);

        // then
        verify(mockedResponse, never()).setStatus(SC_NOT_MODIFIED);
    }

    @Test
    public void testSettingNegativeLastModifiedDate_shouldNotDefineLastModifiedHeader() {
        // given
        setProperty(ATLASSIAN_DISABLE_CACHES_PROPERTY, TRUE.toString());
        when(mockedRequest.getDateHeader(IF_MODIFIED_SINCE)).thenReturn(-1L);
        when(mockedRequest.getHeader(IF_NONE_MATCH)).thenReturn(EMPTY);

        // when
        new LastModifiedHandler(new Date(-1L)).setCacheHeadersIfCacheable(mockedRequest, mockedResponse);

        // then
        verify(mockedResponse, times(0)).setDateHeader(eq(LAST_MODIFIED), anyLong());
        verify(mockedRequest, times(0)).setAttribute(eq("Plugin-Last-Modified-Date"), anyObject());
    }
}
