package com.atlassian.plugin.servlet.cache.model;

import java.time.LocalDateTime;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.google.common.net.HttpHeaders.IF_MODIFIED_SINCE;
import static com.google.common.net.HttpHeaders.IF_NONE_MATCH;
import static java.time.LocalDateTime.of;
import static java.time.Month.JANUARY;
import static java.time.ZoneOffset.UTC;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.servlet.cache.model.CacheableRequest.EMPTY_MODIFIED_SINCE_HEADER;
import static com.atlassian.plugin.servlet.cache.model.CacheableRequest.PLUGIN_LAST_MODIFIED_DATE;

public class CacheableRequestTest {

    /**
     * Fixed {@link LocalDateTime} used as base for the tests to avoid test anomalies.
     */
    private static final LocalDateTime BASE_DATE_TIME = of(2020, JANUARY, 1, 0, 0);

    private static final String NON_HASHED_TEST = "Test";

    /**
     * Representation of the {@link CacheableRequestTest#NON_HASHED_TEST} in {@code MD5} hashing algorithm.
     */
    private static final String DOUBLE_QUOTED_MD5_HASHED_TEST = "\"cbc6611f5540bd0809a388dc95a615b\"";

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private HttpServletRequest request;

    @Mock
    private CacheableResponse response;

    @InjectMocks
    private CacheableRequest underTest;

    @Before
    public void setUp() {
        when(request.getAttribute(PLUGIN_LAST_MODIFIED_DATE)).thenReturn(BASE_DATE_TIME);
        when(response.toETagToken()).thenReturn(Optional.of(new ETagToken(NON_HASHED_TEST.getBytes())));
    }

    /**
     * WHEN ETag not equals to 'If-None-Match' header.
     * AND 'If-Modified-Since' is null.
     * THEN return fresh content.
     */
    @Test
    public void when_IfNoneMatchHeaderNotEqualsToEtag_And_IfModifiedSinceHeaderIsNull_Then_ReturnFreshContent() {
        // given
        when(request.getHeader(IF_NONE_MATCH)).thenReturn("");
        when(request.getDateHeader(IF_MODIFIED_SINCE)).thenReturn(EMPTY_MODIFIED_SINCE_HEADER);
        // when
        final boolean isCacheable = underTest.isCacheable(response);
        // then
        assertFalse(isCacheable);
    }

    /**
     * WHEN ETag equals to 'If-None-Match' header.
     * AND 'If-Modified-Since' is null.
     * THEN return cached content.
     */
    @Test
    public void when_IfNoneMatchHeaderEqualsToETag_And_IfModifiedSinceHeaderIsNull_Then_ReturnCachedContent() {
        // given
        when(request.getDateHeader(IF_MODIFIED_SINCE)).thenReturn(EMPTY_MODIFIED_SINCE_HEADER);
        when(request.getHeader(IF_NONE_MATCH)).thenReturn(DOUBLE_QUOTED_MD5_HASHED_TEST);
        // when
        final boolean isCacheable = underTest.isCacheable(response);
        // then
        assertTrue(isCacheable);
    }

    /**
     * WHEN 'If-None-Match' header is null.
     * AND 'If-Modified-Since' header equals to 'Plugin-Last-Modified-Date'.
     * THEN return cached content.
     */
    @Test
    public void
            when_IfNoneMatchHeaderIsNull_And_IfModifiedSinceHeaderEqualsToPluginLastModifiedDate_Then_ReturnCachedContent() {
        // given
        when(request.getDateHeader(IF_MODIFIED_SINCE)).thenReturn(BASE_DATE_TIME.toEpochSecond(UTC));
        when(request.getHeader(IF_NONE_MATCH)).thenReturn(null);
        // when
        final boolean isCacheable = underTest.isCacheable(response);
        // then
        assertTrue(isCacheable);
    }

    /**
     * WHEN 'If-None-Match' header is null.
     * AND 'If-Modified-Since' header higher than 'Plugin-Last-Modified-Date'.
     * THEN return cached content.
     */
    @Test
    public void
            when_IfNoneMatchHeaderIsNull_And_IfModifiedSinceHeaderHigherThanPluginLastModifiedDate_Then_ReturnCachedContent() {
        // given
        when(request.getDateHeader(IF_MODIFIED_SINCE))
                .thenReturn(BASE_DATE_TIME.plusHours(1).toEpochSecond(UTC));
        when(request.getHeader(IF_NONE_MATCH)).thenReturn(null);
        // when
        final boolean isCacheable = underTest.isCacheable(response);
        // then
        assertTrue(isCacheable);
    }

    /**
     * WHEN 'If-None-Match' header is null.
     * AND 'If-Modified-Since' header lower than 'Plugin-Last-Modified-Date'.
     * THEN return fresh content.
     */
    @Test
    public void
            when_IfNoneMatchHeaderIsNull_And_IfModifiedSinceHeaderLowerThanPluginLastModifiedDate_Then_ReturnFreshContent() {
        // given
        when(request.getDateHeader(IF_MODIFIED_SINCE))
                .thenReturn(BASE_DATE_TIME.minusHours(1).toEpochSecond(UTC));
        when(request.getHeader(IF_NONE_MATCH)).thenReturn(null);
        // when
        final boolean isCacheable = underTest.isCacheable(response);
        // then
        assertFalse(isCacheable);
    }

    /**
     * WHEN ETag equals to 'If-None-Match' header.
     * AND 'If-Modified-Since' header lower than 'Plugin-Last-Modified-Date'.
     * THEN return cached content.
     */
    @Test
    public void
            when_IfNoneMatchHeaderEqualsToETag_And_IfModifiedSinceHeaderLowerThanPluginLastModifiedDate_Then_ReturnCachedContent() {
        // given
        when(request.getDateHeader(IF_MODIFIED_SINCE))
                .thenReturn(BASE_DATE_TIME.minusHours(1).toEpochSecond(UTC));
        when(request.getHeader(IF_NONE_MATCH)).thenReturn(DOUBLE_QUOTED_MD5_HASHED_TEST);
        // when
        final boolean isCacheable = underTest.isCacheable(response);
        // then
        assertTrue(isCacheable);
    }

    /**
     * WHEN ETag not equals to 'If-None-Match' header.
     * AND 'If-Modified-Since' header higher than 'Plugin-Last-Modified-Date'.
     * THEN return cached content.
     */
    @Test
    public void
            when_IfNoneMatchHeaderNotEqualsToETag_And_IfModifiedSinceHeaderHigherThanPluginLastModifiedDate_Then_ReturnFreshContent() {
        // given
        when(request.getDateHeader(IF_MODIFIED_SINCE))
                .thenReturn(BASE_DATE_TIME.plusHours(1).toEpochSecond(UTC));
        when(request.getHeader(IF_NONE_MATCH)).thenReturn("");
        // when
        final boolean isCacheable = underTest.isCacheable(response);
        // then
        assertTrue(isCacheable);
    }
}
